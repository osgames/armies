// clsNaturalResource.cpp: implementation of the clsNaturalResource class.
//
//////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

/*!
 * Includes
 */
#include "clsNaturalResource.h"
#include "Global.h"
#include "ClsApplication.h"
#include "clsBoard.h"
#include "clsFrame.h"

IMPLEMENT_ABSTRACT_CLASS(clsNaturalResource, clsUpperLevelObject) // to enable RTTI

//.......................................................................................................................
// Definition of static members

wxString clsNaturalResource::sVstrTypeDescription = "";
wxString clsNaturalResource::sVstrPersistenceName;

c4_IntProp clsNaturalResource::sVprpIntConstruction(_T("intConstruction"));
c4_IntProp clsNaturalResource::sVprpIntCreation(_T("intCreation"));


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//_______________________________________________________________________________________________________________________

clsNaturalResource::clsNaturalResource() {
	if(sVstrTypeDescription == "") {
		// The property is not set jet (it's set when the first instance of this class is instantiated)
		sVstrTypeDescription = _("Natural Resource");
		sVstrPersistenceName = _T("NaturalResources");
	}
}

//_______________________________________________________________________________________________________________________

clsNaturalResource::~clsNaturalResource() {

}

//_______________________________________________________________________________________________________________________

void clsNaturalResource::SetOffsetImageEmpty(const wxPoint &PVpntOffset) {
	int VintSquareLength = gOobjApplication->GetSobjBoard()->GetVintSQUARE_LENGTH();
	wxBitmap bmpSubBitmap;
	
	mVrctSourceEmpty.SetLeft(PVpntOffset.x);
	mVrctSourceEmpty.SetTop(PVpntOffset.y);
	mVrctSourceEmpty.SetWidth(VintSquareLength);
	mVrctSourceEmpty.SetHeight(VintSquareLength);
	
	bmpSubBitmap = gOobjApplication->GetSbmpGraphics()->GetSubBitmap(mVrctSourceEmpty);
	this->SetVimgEmptyGraphic(bmpSubBitmap.ConvertToImage());
}

//_______________________________________________________________________________________________________________________

void clsNaturalResource::ShowProperties(clsPointer<wxListCtrl> pOlvwProperties) {
	int intPosition = -1;

	pOlvwProperties->DeleteAllItems(); // because the list could already be showing another set of properties
	
	intPosition++;
	pOlvwProperties->InsertItem(intPosition, _("Name"), clsFrame::enm_imlProperties_Information);
	pOlvwProperties->SetItem(intPosition, 1, this->GetVstrDescription());
	
	intPosition++;
	pOlvwProperties->InsertItem(intPosition, _("Type"), clsFrame::enm_imlProperties_Information);
	pOlvwProperties->SetItem(intPosition, 1, this->GetVstrTypeDescription());
	
	mOobjFrame->InsertVariableProperty(_("Health"), this->GetVintHealth(), this->GetVintMaxHealth(), pOlvwProperties, intPosition);
	mOobjFrame->InsertVariableProperty(_("Construction"), this->GetVintConstruction(), this->GetVintMaxConstruction(), pOlvwProperties, intPosition);
	mOobjFrame->InsertVariableProperty(_("Creation"), this->GetVintCreation(), this->GetVintMaxCreation(), pOlvwProperties, intPosition);
}

//_______________________________________________________________________________________________________________________

wxString clsNaturalResource::strGetPersistenceStructure() {
	wxString strResult = _T("[") + clsUpperLevelObject::strGetPersistenceStructure();
	
	strResult += _T(",intConstruction:I");
	strResult += _T(",intCreation:I");
	strResult += _T("]");
	
	return(strResult);
}

//_______________________________________________________________________________________________________________________

void clsNaturalResource::Save(c4_Row& pRrowRow) {
	clsUpperLevelObject::Save(pRrowRow);
	
	sVprpIntConstruction(pRrowRow) = mVintConstruction;
	sVprpIntCreation(pRrowRow) = mVintCreation;
}

//_______________________________________________________________________________________________________________________

void clsNaturalResource::Load(c4_RowRef pVrowRow) {
	clsUpperLevelObject::Load(pVrowRow);
	
	mVintConstruction = sVprpIntConstruction(pVrowRow);
	mVintCreation = sVprpIntCreation(pVrowRow);
}
