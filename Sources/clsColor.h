// clsColor.h: interface for the clsColor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLSCOLOR_H__8A0AB5EF_85DF_45C9_8508_1B8AA9496E16__INCLUDED_)
#define AFX_CLSCOLOR_H__8A0AB5EF_85DF_45C9_8508_1B8AA9496E16__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

#include <wx/object.h>
#include <wx/colour.h>

class clsColor : public wxColour
{
public:
	wxString* strLabel;
	wxString* strName;
	clsColor();
	virtual ~clsColor();

};

#endif // !defined(AFX_CLSCOLOR_H__8A0AB5EF_85DF_45C9_8508_1B8AA9496E16__INCLUDED_)
