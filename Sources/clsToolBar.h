//-----------------------------------------------------------------------------------------------------------------------
// This class is a wrapper to manage the visibility of the buttons contained in the toolbar
//-----------------------------------------------------------------------------------------------------------------------

#if !defined(AFX_CLSTOOLBAR_H__7028D740_6094_4162_A9AC_A0A71A3E6FF5__INCLUDED_)
#define AFX_CLSTOOLBAR_H__7028D740_6094_4162_A9AC_A0A71A3E6FF5__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */

#include "clsPointer.h"
#include <wx/toolbar.h>
#include "arraytemplate.h"
#include "Containers.h"
#include "clsVector.h"

class clsToolBar : public wxToolBar
{
//-----------------------------------------------------------------------------------------------------------------------
public:
	clsToolBar(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxTB_HORIZONTAL | wxNO_BORDER, const wxString& name = wxPanelNameStr);
	virtual ~clsToolBar();

	wxToolBarToolBase* AddTool(int toolId, const wxString& label, const wxBitmap& bitmap1, const wxString& shortHelpString = "", wxItemKind kind = wxITEM_NORMAL);
	wxToolBarToolBase* AddTool(int toolId, const wxString& label, const wxBitmap& bitmap1, const wxBitmap& bitmap2 = wxNullBitmap, wxItemKind kind = wxITEM_NORMAL, const wxString& shortHelpString = "", const wxString& longHelpString = "", wxObject* clientData = NULL);
	wxToolBarToolBase* AddTool(wxToolBarToolBase* tool);

	// Makes visible a specified tool button
	void ShowTool(int pVintToolId);

	// Simulates the hidden of a wxToolBarToolBase object. Actually, this behavior cannot be done by wxWidgets.
	// Therefore, it is simulated by dropping the tool button and preserving it in the mVarrToolObjects array
	void HideTool(int pVintToolId);

//-----------------------------------------------------------------------------------------------------------------------
private:

	//...................................................................................................................
	// Variables

	// Toolbar object to administrate
	clsPointer<wxToolBar> mOtlbToolbar;

	wxArrayInt mVarrToolIDs;
	
	// It should be an array of booleans, but due the fact that vector<bool> is treat in a special way for the compiler, there was some incompatibilities. For this reason, the type "int" is used intead. It only can store 0 or 1.
	// 0 = invisible
	// 1 = visible
	clsVector<int> mVarrToolVisibilities;

	// This is an owner collection. Therefore, the responsibility for the clean
	// of the wxMenu objects contained here is yours.
	clsVector< clsPointer<wxToolBarToolBase> > mVarrToolObjects;

	//...................................................................................................................
	// Functions

	// Clears the invisible tools which are stored inside the mVarrToolObjects array
	void EmptyTools();

	// Adds a new tool object to the internal arrays
	void AddToolToArrays(int pVintToolId, clsPointer<wxToolBarToolBase> pOobjTool);

	// Returns the ordinal position for a given wxToolBarToolBase object into the toolbar,
	// referenced by its ID number.
	// If the wanted tool button cannot be found, a runtime error is showed and the function returns -1.
	int intScreenPositionTool(int pVintToolId);

	// Returns the position of the tool button inside the arrays menus
	int intArrayPositionTool(int pVintToolId);
};

#endif // !defined(AFX_CLSTOOLBAR_H__7028D740_6094_4162_A9AC_A0A71A3E6FF5__INCLUDED_)
