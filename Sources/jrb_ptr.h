// jrb_ptr.h

// ***********************************
// Copyright 1999 John R. Bandela
// This work may be freely used, copied, distributed, and extended, provided
// the copyright notice above and this statement are maintained at the top of the code



// To use this file need to to link to BackPtrMan.cpp or else include that file
// once in another file.
#ifndef JRB_REF_PTR
#define JRB_REF_PTR

#include <cassert>
#include <typeinfo>


#include "BackPtrMan.h"

#include <wx/debug.h>

namespace jrb_sptr{
	// Exceptions
	struct JRB_PTR_ERROR{};

	// A null struct
	struct JRB_PTR_NULL {};
	extern JRB_PTR_NULL CP_NULL;


	// Abstract class to provide uniform interface to object services
	class ObjectUtility{
	public:

		virtual ~ObjectUtility() {};

		// Function name	: CloneUtility
		// Description	    : Creates a copy of itself on the free store
		// Return type		: A pointer to the newly allocated copy
		virtual ObjectUtility* CloneUtility() = 0;

		// Function name	: Clone
		// Description	    : Creates a clone of the object pointed to by p
		// Return type		: a void* to the newly created object
		// Argument         : void* p - a pointer of the object to clone
		virtual void* Clone(void* p)=0; 

		// Function name	: Destroy
		// Description	    : Calls the appropriate delete for the object
		// Return type		: void
		// Argument         : void* p
		virtual void Destroy(void* p)=0; 



	// Function name	: GetTypeInfo
	// Description	    : Returns the type_info of the class this utilizes
	// Return type		: const type_info
		virtual const std::type_info& GetTypeInfo()=0;
	};


	// ===================================================================================================================
	// A class that implements Object Utility

	template <class T> 
	class ObjectUtilityImp:public ObjectUtility{
	public:

		// -------------------------------------------------------------------------------------------------------------------
		// Returns a clone of this object
		virtual ObjectUtility* CloneUtility(){
			return new ObjectUtilityImp<T>;
		}

		// -------------------------------------------------------------------------------------------------------------------
		// Create a new object using the appropriately defined operator new
		// and the copy constructor
		virtual void* Clone(void* WXUNUSED(p)){
			// [Germ�n Blando] If this line is not commented, then the object pointed has to have a copy constructor defined.
			//return new T(*static_cast<T*>(p));
			assert(false); // to generate an error if some code is using this function
			return NULL; 
												
		};

		// -------------------------------------------------------------------------------------------------------------------
		// Call the right delete for an object
		virtual void Destroy(void* p){
			T* ptr = static_cast<T*>(p);
			assert(ptr == p);
			delete ptr;
		};

		// -------------------------------------------------------------------------------------------------------------------
		// Get the typeinfo for the created class
		virtual const std::type_info& GetTypeInfo(){
			return typeid(T);
		}

	};

	// ===================================================================================================================
	// A class to hold and manage the actual pointer

	class PtrRep{
	private:
		
		// A pointer to the object
		void* pObject;
		
		// The reference count
		int nRefCount;
		
		// Pointer to the the Object Utility
		ObjectUtility* pUtility;
		
		// Use private to hide copy ctor and op = to keep it from being copied
		PtrRep(PtrRep&){}
		PtrRep& operator=(PtrRep& WXUNUSED(other)){return *this;}

		// The unique ID of this pointer, used in managing back references
		JRB_BPCOUNT_TYPE nID;
	public:
		// The constructor, initializes
		PtrRep(ObjectUtility* pU, void* p=0){
			// Initialize
			pObject = p;
			nRefCount = 1;
			pUtility = pU;
			nID = 0;
		}
		// The destructor
		~PtrRep(){
			// Destroy the contained pointer
			pUtility->Destroy(pObject);
			// Delete the Utility object
			delete pUtility;
			// If we have an ID then remove it from
			// the Back Reference set because it is no longer valid
			if(nID){
				GetBackRefManager().RemoveID(nID);
			}
		}
		// Increment the ref count
		int AddReference(){++nRefCount; return nRefCount;}
		// Decrement the ref count, and if 0 self-destroy
		int RemoveReference(){
			// Decrement the ref count
			--nRefCount;
			// nRefCount should never be less than 0, assert this
			assert(nRefCount >= 0);
			// Check if the reference count is 0
			if(nRefCount == 0){
				// Self-destruct, we are no longer needed
				delete this;
				return 0;
			}
			// Return the reference count
			return nRefCount;
		}
		// Return pointer to Utility
		ObjectUtility* GetUtility(){return pUtility;}
		// Return pointer to object
		void* GetObject(){return pObject;}

		// Return the unique ID of the pointer
		JRB_BPCOUNT_TYPE GetUniqueID(){
			// Obtain an ID only if we haven't got one yet
			if(nID==0){
				nID = GetBackRefManager().GetNewID();
			}
			return nID;
		}

		// Return the ref Count
		int GetRefCount(){
			return nRefCount;
		}
	};


// A macro to test if another CountedPtr can be assigned to this one
#define JRB_CP_CHECK_COMPAT if(0){T* test = other.GetPtr(); T* test1 = test; test = test1;}



	// ===================================================================================================================
	// The actual CountedPtr class
	// This class is a reference counted smart pointer
	// It allows you to directly assign CountedPtrs for types
	// that you could assign pointers to
	// It also supports static and dynamic casts

	template <class T> 
	class CountedPtr{
	public:
		PtrRep* pRep; // Do not mess with.  This is only public so
					  // that other classes of this template can access it
	private:
	// This is inaccessible. Use Make_CP to assign a pointer to a CountedPtr.
	// This is used internally by Clone.
	CountedPtr(T* p, ObjectUtility* pU=0){
		// Allocate the ObjectUtility if we need to
		if(!pU){
			pU = new ObjectUtilityImp<T>;
		}
		// Create a new ptr rep
		pRep = new PtrRep(pU,static_cast<void*>(p));
	}

	// This assigns a pRep from another CountedPtr to this one managing reference counts
	void DoPRAssignment(PtrRep* other_pRep){
		if(other_pRep){
			other_pRep->AddReference();
		}
		if(pRep){
			pRep->RemoveReference();
		}
		pRep = other_pRep;
	}

	public:
		// The default constructor, initializes pRep to NULL
		CountedPtr():pRep(0){}
		
		// This allows assignment to CP_NULL upon creation
		CountedPtr(const JRB_PTR_NULL):pRep(0){}

		// Constructor takes a pointer to a PtrRep
		CountedPtr(PtrRep* p):pRep(0){
			DoPRAssignment(p);
		}
		// Destructor, Decrement the reference count on the pRep	
		~CountedPtr(){
			if(pRep){
				pRep->RemoveReference();
			}
		}
	public:
		// Copy constructor, takes a reference to a counted pointer for another type.
		// If the other type is compatible will assign, if not you will get a compiler error
		template <typename O>
			CountedPtr(const CountedPtr<O>& other):pRep(0){
			JRB_CP_CHECK_COMPAT; // If it fails to compile here then you are trying to assign an invalid type
			DoPRAssignment(other.pRep);
		}
		// Copy constructor, takes a reference to counted pointer of same type
		CountedPtr(const CountedPtr& other):pRep(0){
			DoPRAssignment(other.pRep);
		}
		// Operator=, takes a reference to a counted pointer for another type.
		// If the other type is compatible will assign, if not you will get a compiler error
		template <class O>
		CountedPtr& operator=(const CountedPtr<O>& other){
			JRB_CP_CHECK_COMPAT;
			DoPRAssignment(other.pRep);
			return* this;
		}
		// Operator=, takes a reference to counted pointer of same type
		CountedPtr& operator=(const CountedPtr& other){
			DoPRAssignment(other.pRep);
			return* this;
		}

		// Operator=, takes a JRB_PTR_NULL
		CountedPtr& operator=(const JRB_PTR_NULL){
			if(pRep){
				pRep->RemoveReference();
			}
			pRep = 0;
			return *this;
		}

		// The smart pointer member operator. Throws JRB_PTR_ERROR if pRep=0
		T* operator->() const{
			wxASSERT(pRep);
			return static_cast<T*>(pRep->GetObject());
		}
		
		// Returns a pointer to the pointed to object, 0 if no object pointed to
		T* GetPtr() const{
			if(!pRep){
				return 0;
			}
			return static_cast<T*>(pRep->GetObject());
		}
		// Creates a clone of the pointed to object
		// Using the methods in ObjectUtility
		CountedPtr Clone() const{
			ObjectUtility* pU = pRep->GetUtility();
			CountedPtr<T> ret(static_cast<T*>(pU->Clone(pRep->GetObject())),pRep->GetUtility()->CloneUtility());
			return ret;
		}
		
		// Assigns a counted ref, of another type, to this type
		template <class U>
		void StaticCastFrom( const CountedPtr<U>& other ){
			DoPRAssignment(other.pRep);
		}
		// Assigns a counted ref, of another type, to this type
		// doing a dynamic_cast if it is supported. If it is not supported, you
		// will get a compile time error
		template <class U>
		void DynamicCastFrom(const CountedPtr<U>& other ){
			if(other.pRep){
				other.pRep->AddReference();
			}
			if(pRep){
				pRep->RemoveReference();
			}
			if(other.pRep){
				T* test= dynamic_cast<T*>(other.GetPtr()); // If it fails to compile here
				// then you are using a type that doesn't support dynamic_cast
				if(!test){
					pRep = 0;
					// Decrement reference count on other, since we no longer reference it
					other.pRep->RemoveReference();
					return;
				}
			}
			pRep = other.pRep;
		}
		// Returns if the pointer is valid
		bool IsValid() const{
			if(pRep){
				if(pRep->GetObject())
					return true;
				else
					return false;
			}
			return false;
		}
		// Get the Ref Count
		int GetRefCount() const{
			// Check if pRep valid, and if so, get the Ref Count
			if(pRep){
				return pRep->GetRefCount();
			}
			// pRep is invalid, return 0
			else{
				return 0;
			}
		}
		// Implicit conversion to a  regular pointer
		operator T*() const {
			return GetPtr();
		}

		// Another implicit conversion to prevent deletion
		operator void*() const{
			return GetPtr();
		}


		// Returns the type info of the object pointed to.
		// If this class is used correctly, it will return the
		// type info of the object actually created, even if this
		// counted pointer is a base class
		const std::type_info& GetTypeInfo() const {
			return pRep->GetUtility()->GetTypeInfo();
		}

		// Methods for equality comparison to NULL
		bool operator==(const JRB_PTR_NULL) const{
			return !IsValid();
		}
		bool operator!=(const JRB_PTR_NULL)const{
			return IsValid();
		}
	};

	//____________________________________________________________________________________________________________________

	// Template functions for (in)equality comparisons
	template <class T, class U>
		bool operator==(const CountedPtr<T> A, const CountedPtr<U> B){
		// Check if A and B have valid pReps
		if(A.pRep && B.pRep){
				// If so, compare the objects
				if(A.pRep->GetObject() == B.pRep->GetObject()){
					return true;
				}
				else{
					return false;
				}
			}
			else{
				// At least one is invalid, check if both are, and if so return true
				if(A.pRep==B.pRep){
					return true;
				}
				else{
					return false;
				}
			}
		}
	template <class T, class U>
		bool operator!=(const CountedPtr<T> A, const CountedPtr<U> B){
			return !(A==B);
	}

	template <class T, class U>
		bool operator==(const CountedPtr<T> A, const U* B){
			// Check if A has valid pRep
			if(A.pRep){
				// Compare the A object to B
				if(A.pRep->GetObject() == static_cast<const void*>(B)){
					return true;
				}
				else{
					return false;
				}
			}
			else{
				// A is invalid, check if B is null, if so return true
				if(!B){
					return true;
				}
				else{
					return false;
				}
			}
		}
	template <class T, class U>
		bool operator!=(const CountedPtr<T> A, const U* B){
			return !(A==B);
	}

	template <class U, class T>
		bool operator==(const U* B, const CountedPtr<T> A){
			// Check if A has valid pRep
			if(A.pRep){
				// CHeck for equality of objects
				if(A.pRep->GetObject() == static_cast<const void*>(B)){
					return true;
				}
				else{
					return false;
				}
			}
			else{
				// A is invalid, check if B is null and return true if it is
				if(!B){
					return true;
				}
				else{
					return false;
				}
			}
		}

	template <class U, class T>
		bool operator!=(const U* B, const CountedPtr<T> A ){
			return !(A==B);
	}


	// ===================================================================================================================
	// A back pointer class
	// This solves the problem of circular reference counts
	// It does not cause the reference count to increase
	// However, it allows you to check that the object has not
	// been deleted before trying to access it

	template <class T> class CountedPtr;

	template <class T>
		class BackPtr{
		private:
			// A pointer to the  PtrRep class
			PtrRep* pRep;

			// The unique ID of this object
			JRB_BPCOUNT_TYPE nID;
		public:
		// Default constructor
		BackPtr(){
			pRep = 0;
			nID = 0;
		}
		// Copy constructor
		BackPtr(BackPtr& other){
			pRep = other.pRep;
			nID = other.nID;
		}
		// Operator =
		BackPtr& operator=(BackPtr& other){
			pRep = other.pRep;
			nID = other.nID;
		}
		// Various constructors and operator= for CountedPtr
		template <typename O>
		BackPtr(const CountedPtr<O>& other){
			if(other.pRep){
				T* test = other.GetPtr(); // If it fails to compile here
				// then you are trying to assign an invalid type
			}
			nID = 0;
			pRep = other.pRep;
			if(pRep){
				nID = pRep->GetUniqueID();
				GetBackRefManager().AddID(nID);
			}
		}
		BackPtr(const CountedPtr<T>& other){
			nID = 0;
			pRep = other.pRep;
			if(pRep){
				nID = pRep->GetUniqueID();
				GetBackRefManager().AddID(nID);

			}
		}
		template <class O>
		BackPtr& operator=(const CountedPtr<O>& other){
			if(other.pRep){
				T* test = other.GetPtr(); // If it fails to compile here
				// then you are trying to assign an invalid type
			}
			nID = 0;
			pRep = other.pRep;
			if(pRep){
				nID = pRep->GetUniqueID();
				GetBackRefManager().AddID(nID);
			}
			return* this;
		}
		BackPtr& operator=(const CountedPtr<T>& other){
			nID = 0;
			pRep = other.pRep;
			if(pRep){
				nID = pRep->GetUniqueID();
				GetBackRefManager().AddID(nID);
			}
			return* this;
		}
		// Checks if the back pointer is valid
		bool IsValid(){
			return GetBackRefManager().QueryID(nID);
		}
		// Gets the counted ptr, will throw an exception if not valid
		CountedPtr<T> GetCountedPtr(){
			wxASSERT(IsValid());
			return CountedPtr<T>(pRep);
		}
	};


	// When an a counted ptr is first assigned a new pointer
	// It is essential that the exact type of the pointer be known
	// This function correctly initializes the correct type information
	// and returns a CountedPtr which can then be assigned.
	template <typename T>
		CountedPtr<T> Make_CP(T* p, ObjectUtility* pU=0){
		// Allocate the ObjectUtility if we need to
		if(!pU){
			pU = new ObjectUtilityImp<T>;
		}
		// Create a new ptr rep
		PtrRep* pr = new PtrRep(pU,static_cast<void*>(p));
		// Create a counted ptr object
		CountedPtr<T> R(pr);
		// When PtrRep is created, its reference count is one. Assigning
		// it to R adds another reference. We need to remove one otherwise
		// it will never be deallocated
		pr->RemoveReference();
		// Return a created Counted Pointer
		return R;
		
	};

}
// These next functions mimic the static and dynamic cast
// the only difference is that you must use the type without the *
//ie  CountedPtr<A> = CP_static_cast<A>(bptr);
template <class U, class T>
jrb_sptr::CountedPtr<U> CP_static_cast(jrb_sptr::CountedPtr<T>& ptr){
	jrb_sptr::CountedPtr<U> r;
	r.StaticCastFrom(ptr);
	return r;
}
template <class U, class T>
jrb_sptr::CountedPtr<U> CP_dynamic_cast(jrb_sptr::CountedPtr<T>& ptr){
	jrb_sptr::CountedPtr<U> r;
	r.DynamicCastFrom(ptr);
	return r;
}

#endif
