// TODO: add RTTI information to this class

#if !defined(AFX_clsOil_H__166240E1_0948_420A_BC5A_ECF79B41A4FB__INCLUDED_)
#define AFX_clsOil_H__166240E1_0948_420A_BC5A_ECF79B41A4FB__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

#include "clsNaturalResource.h"

//_____________________________________________________________________________________________________________________

/**
* This is one of the classes at the bottom of the clsBoardObject inheritance tree.
* One object of this type will be created at the beginning as the "Object Type", and through it will be constructed
* all the rest of the objects of the same type.
*/
class clsOil : public clsNaturalResource
{

//_____________________________________________________________________________________________________________________

public:
	
	//-------------------------------------------------------------------------------------------------------------------
	// Constructors / Destructors (public)
	
	/**
	* It's a constructor that enables to create the class Object Type
	*/
	clsOil(bool pVblnObjectType);
	
	virtual ~clsOil();

	//-------------------------------------------------------------------------------------------------------------------
	// Methods (public)

	/**
	 * @Override
	 */
	virtual void ResetCounter();

	/**
	* @Override
	*/
	virtual clsPointer<clsNaturalResource> OobjCreateNew();
	
	/**
	* @Override
	*/
	virtual clsPointer<clsNaturalResource> OobjCreateFromFile(c4_RowRef pVrowRow);
	
	/**
	* @Override
	*/
	virtual void ShowContent(clsPointer<wxTreeCtrl> pOtrcContent);

	//-------------------------------------------------------------------------------------------------------------------
	// Getters / Setters (public)
	
	virtual int GetVintMaxHealth() const { return(sVintMaxHealth); } // @Override
	virtual int GetVintMaxCreation() const { return(sVintMaxCreation); } // @Override
	virtual int GetVintMaxConstruction() const { return(sVintMaxConstruction); } // @Override
	virtual wxColour* GetVclrMapColor() const { return(sSclrMapColor); } // @Override
	virtual wxString GetVstrInternalTypeName() const { return(sVstrInternalTypeName); } // @Override
	
	virtual wxImage GetVimgStaticRepresentation() const { return(sVimgStaticRepresentation); } // @ToOverride
	virtual void SetVimgStaticRepresentation(wxImage pVimgGraphic) { sVimgStaticRepresentation = pVimgGraphic; } // @ToOverride

//_____________________________________________________________________________________________________________________

protected:
	
	//-------------------------------------------------------------------------------------------------------------------
	// Getters / Setters (protected)
	
	virtual wxImage GetVimgEmptyGraphic() const { return(sVimgEmptyGraphic); } // @Override
	virtual void SetVimgEmptyGraphic(wxImage pVimgImage) { sVimgEmptyGraphic = pVimgImage; } // @Override
	
	virtual int GetVintInstances() const { return(sVintInstances); } // @Override
	virtual void SetVintInstances(int pVintValue) { sVintInstances = pVintValue; } // @Override
	
	virtual wxString GetVstrBaseDescription() { return(sVstrBaseDescription); } // @Override
	virtual void SetVstrBaseDescription(wxString pVstrBaseDescription) { sVstrBaseDescription = pVstrBaseDescription; } // @Override
	
	virtual void SetVstrInternalTypeName(wxString pVstrValue) { sVstrInternalTypeName = pVstrValue; } // @Override
	
//_____________________________________________________________________________________________________________________

private:
	
	//-------------------------------------------------------------------------------------------------------------------
	// Constructors / Destructors (private)
	
	/**
	* Only to create normal objects (no the class Object Type).
	* Only this class can create normal objects through the method OobjCreateNew
	*/
	clsOil();
	
	//-------------------------------------------------------------------------------------------------------------------
	// Variables (private)

	/**
	* Number of instances created inherited from this class.
	* It is used to create unique names for the objects.
	*/
	static int sVintInstances;

	/**
	* Color to show the object in the Map
	*/
	static CountedPtr<wxColour> sSclrMapColor;

	/**
	* Maximum value for the mVintHealth member variable
	*/
	static int sVintMaxHealth;
	
	/**
	* Maximum creations units.
	*/
	static int sVintMaxCreation;
	
	/**
	* Maximum construction units.
	*/
	static int sVintMaxConstruction;
	
	/**
	 * Graphic for store the base representation of the object.
	 */
	static wxImage sVimgStaticRepresentation;
	
	/**
	* Graphic for representing the object when doesn't contain any resource
	*/
	static wxImage sVimgEmptyGraphic;

	/**
	* This base is later concatenated with a sequential number to make the user friendly description
	* in mVstrDescription. This member variable must be translated.
	*/
	static wxString sVstrBaseDescription;
	
	/**
	* Store the object type name to share among all the objects of the same type
	*/
	static wxString sVstrInternalTypeName;
	
	//-------------------------------------------------------------------------------------------------------------------
	// Methods (private)
	
	/**
	* Initializer for the class Object Type
	*/
	void ObjectTypeConstructor();
	
	/**
	* Initializer for normal objects
	*/
	void NormalConstructor();
};

#endif // !defined(AFX_clsOil_H__166240E1_0948_420A_BC5A_ECF79B41A4FB__INCLUDED_)
