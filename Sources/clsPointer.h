//========================================================================================================================
// Template class for simple pointer with dangling checking
//========================================================================================================================

#ifndef _CLSPOINTER_H_
#define _CLSPOINTER_H_

#include <wx/debug.h>

#include "clsLibrary.h"

template<class T> class clsPointer {
public:
    //typedef T element_type;

	//____________________________________________________________________________________________________________________

	/*!
		The default constructor, initializes m_AutoPtr to NULL
	*/
	
	//clsPointer() : m_AutoPtr(NULL) {}

    //....................................................................................................................
	
	/*! 
		Constructor
	*/
    clsPointer(T *pVal = 0) throw()
    {
        if(pVal)
            m_AutoPtr = pVal;
        else
            m_AutoPtr = NULL;
    }
    
	//....................................................................................................................
	
	/*!
		Copy Constructor. Takes another smart pointer of the same type.
	*/
    clsPointer(const clsPointer& ptrCopy) throw()
    {
        if(ptrCopy.get())
            m_AutoPtr = ptrCopy;
        else
            m_AutoPtr = 0;
    }

	//....................................................................................................................
	
	/*!
		Copy Constructor. Takes another smart pointer of a child class.
	*/
    template <typename O>
	clsPointer(const clsPointer<O>& pOobjPointer) throw() {
		if(pOobjPointer.get()) {
			m_AutoPtr = pOobjPointer.get();
		} else {
			m_AutoPtr = 0;
		}
	}

	//____________________________________________________________________________________________________________________

	/*!
		Destructor
	*/
    ~clsPointer()
    {
		// This pointer is not deleted automatically because it hasn't the ownership of the object pointed to
    }
	
	//____________________________________________________________________________________________________________________

	/*!
		Overload the = (assignment) operator to accept another smart pointer of the same type
	*/
    clsPointer& operator=(clsPointer& ptrCopy) throw()
    {
        if(ptrCopy.get())
            m_AutoPtr = ptrCopy.get();
        else
            m_AutoPtr = 0;
        return *this;
    }

	//....................................................................................................................

	/*!
		Overload the = (assignment) operator to accept another smart pointer of a child class (upcasting)
	*/
	template <class O>
	clsPointer& operator=(const clsPointer<O>& pOobjPointer){
		if(pOobjPointer.get()) {
			m_AutoPtr = pOobjPointer.get();
		} else {
			m_AutoPtr = 0;
		}
		
		return* this;
	}
    
	//....................................................................................................................

	/*!
		Overload the = (assignment) operator to accept a raw pointer
	*/
    T* operator=(T* ptrCopy) throw()
    {
        if(ptrCopy)
            m_AutoPtr = ptrCopy;
        else
            m_AutoPtr = 0;
        return ptrCopy;
    }

	//____________________________________________________________________________________________________________________

	/**
	* Equality comparisons with other clsPointer
	*/
	template <class O>
	bool operator==(const clsPointer<O>& pOobjRightValue) const {
		return(m_AutoPtr == pOobjRightValue.get());
	}

	//____________________________________________________________________________________________________________________

	/**
	* Inequality comparisons with other clsPointer
	*/
	template <class O>
	bool operator!=(const clsPointer<O>& pOobjRightValue) const {
		return(m_AutoPtr != pOobjRightValue.get());
	}

	//____________________________________________________________________________________________________________________

	// Implicit conversion to a regular pointer
	operator T*() const {
		return m_AutoPtr;
	}

	//____________________________________________________________________________________________________________________

    // Overload the * operator
    T& operator*() const throw()
    {
        wxASSERT(m_AutoPtr != NULL);
		return *m_AutoPtr;
    }
    
	//____________________________________________________________________________________________________________________

	// Overload the -> operator
    T *operator->() const throw()
    {
#if defined __WXDEBUG__
	    if(m_AutoPtr == NULL) {
		    wxString strError = _("Attempt to use a clsPointer not set");
		    wxFAIL_MSG(strError);
	    }
#endif
		
	    return m_AutoPtr;
    }
    
	//____________________________________________________________________________________________________________________

	// Function to get the pointer to the class
    T *get() const throw()
    {
        return m_AutoPtr;
    }

	//____________________________________________________________________________________________________________________

	// Function to set the pointer to the class
    void set(T* pPobjPointer) const throw()
    {
        m_AutoPtr = pPobjPointer;
    }

	//____________________________________________________________________________________________________________________

	//! Return true if the object is pointing to a valid memory address
	bool blnValid() {
		return(m_AutoPtr != NULL);
	}

	//____________________________________________________________________________________________________________________

	//! To make a static_cast
	template <class T_FromClass> 
	void StaticCastFrom( const clsPointer<T_FromClass>& pOobjPointerToCast ) {
		m_AutoPtr = static_cast<T*>(pOobjPointerToCast.get());
	}

//________________________________________________________________________________________________________________________

private:
    T *m_AutoPtr;
};

template <class T_ToClass, class T_FromClass>
static clsPointer<T_ToClass> clsPointer_static_cast(clsPointer<T_FromClass>& pOobjPointerToCast) {
	clsPointer<T_ToClass> OobjCastedPointer;
	OobjCastedPointer.StaticCastFrom(pOobjPointerToCast);
	return OobjCastedPointer;
}

#endif // _CLSPOINTER_H_
