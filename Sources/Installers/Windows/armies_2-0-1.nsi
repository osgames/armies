; Script generated by the HM NIS Edit Script Wizard.

; Set the compression mechanism first.
; As of NSIS 2.07, solid compression which makes installer about 1MB smaller
; is no longer the default, so use the /SOLID switch.
; This unfortunately is unknown to NSIS prior to 2.07 and creates an error.
; So if you get an error here, please update to at least NSIS 2.07!
SetCompressor /SOLID lzma

; HM NIS Edit Wizard helper defines
!define PRODUCT_NAME "Armies"
!define PRODUCT_VERSION "2.0.1"
!define PRODUCT_PUBLISHER "German Blando"
!define PRODUCT_WEB_SITE "http://armies.sourceforge.net"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\Armies.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_STARTMENU_REGVAL "NSIS:StartMenuDir"

; MUI 1.67 compatible ------
!include "MUI.nsh"

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; Language Selection Dialog Settings
!define MUI_LANGDLL_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_LANGDLL_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_LANGDLL_REGISTRY_VALUENAME "NSIS:Language"

; Welcome page
!insertmacro MUI_PAGE_WELCOME
; License page
!insertmacro MUI_PAGE_LICENSE "C:\Documents and Settings\gblando\My Documents\Proyectos\Viejos\Armies\Sources\Installers\Windows\license.rtf"
; Directory page
!insertmacro MUI_PAGE_DIRECTORY
; Start menu page
var ICONS_GROUP
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "Armies"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${PRODUCT_STARTMENU_REGVAL}"
!insertmacro MUI_PAGE_STARTMENU Application $ICONS_GROUP
; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!define MUI_FINISHPAGE_RUN "$INSTDIR\Armies.exe"
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "Spanish"

; MUI end ------

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "Armies_2-0-1.exe"
InstallDir "$PROGRAMFILES\Armies"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""
ShowInstDetails show
ShowUnInstDetails show

Function .onInit
  !insertmacro MUI_LANGDLL_DISPLAY
FunctionEnd

Section "Principal" SEC01
  SetOutPath "$INSTDIR"
  SetOverwrite ifnewer
  File "C:\Documents and Settings\gblando\My Documents\Proyectos\Viejos\Armies\Sources\Release\Armies.exe"
  File "c:\Program Files\Microsoft Visual Studio 8\VC\redist\x86\Microsoft.VC80.CRT\msvcm80.dll"
  File "c:\Program Files\Microsoft Visual Studio 8\VC\redist\x86\Microsoft.VC80.CRT\msvcp80.dll"
  File "c:\Program Files\Microsoft Visual Studio 8\VC\redist\x86\Microsoft.VC80.CRT\msvcr80.dll"
  File "C:\Documents and Settings\gblando\My Documents\Proyectos\Viejos\Armies\Sources\Installers\Windows\Microsoft.VC80.CRT.manifest"

  SetOutPath "$DOCUMENTS\Armies\Boards"
  SetOverwrite ifnewer
  File "C:\Documents and Settings\gblando.LA\My Documents\Armies\Boards\Paso del R�o.map"
  File "C:\Documents and Settings\gblando.LA\My Documents\Armies\Boards\R�o Medio.map"

  SetOutPath "$INSTDIR\Locale"
  SetOverwrite ifnewer
  File "C:\Documents and Settings\gblando\My Documents\Proyectos\Viejos\Armies\Sources\Locale\language.dat"

  SetOutPath "$INSTDIR\Locale\es"
  SetOverwrite ifnewer
  File "C:\Documents and Settings\gblando\My Documents\Proyectos\Viejos\Armies\Sources\Locale\es\armies_es.mo"
  File "C:\Documents and Settings\gblando\My Documents\Proyectos\Viejos\Armies\Sources\Locale\es\armies_es.po"
  File "C:\Documents and Settings\gblando\My Documents\Proyectos\Viejos\Armies\Sources\Locale\es\es.mo"
  File "C:\Documents and Settings\gblando\My Documents\Proyectos\Viejos\Armies\Sources\Locale\es\es.po"
  File "C:\Documents and Settings\gblando\My Documents\Proyectos\Viejos\Armies\Sources\Locale\es\wxstd.po"

  SetOutPath "$INSTDIR\Graphics"
  SetOverwrite ifnewer
  File "C:\Documents and Settings\gblando\My Documents\Proyectos\Viejos\Armies\Sources\Graphics\classic_tiles.bmp"

; Shortcuts
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
  !insertmacro MUI_STARTMENU_WRITE_END
SectionEnd

Section -AdditionalIcons
  SetOutPath $INSTDIR
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
  WriteIniStr "$INSTDIR\${PRODUCT_NAME}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"
  CreateDirectory "$SMPROGRAMS\$ICONS_GROUP"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Armies.lnk" "$INSTDIR\Armies.exe"
  CreateShortCut "$DESKTOP\Armies.lnk" "$INSTDIR\Armies.exe"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Website.lnk" "$INSTDIR\${PRODUCT_NAME}.url"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Uninstall.lnk" "$INSTDIR\uninst.exe"
  !insertmacro MUI_STARTMENU_WRITE_END
SectionEnd

Section -Post
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\Armies.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\Armies.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
SectionEnd

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "La desinstalaci�n de $(^Name) finaliz� satisfactoriamente."
FunctionEnd

Function un.onInit
!insertmacro MUI_UNGETLANGUAGE
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "�Est� completamente seguro que desea desinstalar $(^Name) junto con todos sus componentes?" IDYES +2
  Abort
FunctionEnd

Section Uninstall
  !insertmacro MUI_STARTMENU_GETFOLDER "Application" $ICONS_GROUP
  Delete "$INSTDIR\${PRODUCT_NAME}.url"
  Delete "$INSTDIR\uninst.exe"
  Delete "$INSTDIR\Locale\es\wxstd.po"
  Delete "$INSTDIR\Locale\es\es.po"
  Delete "$INSTDIR\Locale\es\es.mo"
  Delete "$INSTDIR\Locale\es\armies_es.po"
  Delete "$INSTDIR\Locale\es\armies_es.mo"
  Delete "$INSTDIR\Locale\language.dat"

  Delete "$DOCUMENTS\Armies\Boards\R�o Medio.map"
  Delete "$DOCUMENTS\Armies\Boards\Paso del R�o.map"

  Delete "$INSTDIR\Armies.exe"
  Delete "$INSTDIR\msvcm80.dll"
  Delete "$INSTDIR\msvcp80.dll"
  Delete "$INSTDIR\msvcr80.dll"
  Delete "$INSTDIR\Microsoft.VC80.CRT.manifest"

  Delete "$INSTDIR\Graphics\classic_tiles.bmp"

  Delete "$DESKTOP\Armies.lnk"
  Delete "$SMPROGRAMS\$ICONS_GROUP\Armies.lnk"
  Delete "$SMPROGRAMS\$ICONS_GROUP\Uninstall.lnk"
  Delete "$SMPROGRAMS\$ICONS_GROUP\Website.lnk"

  RMDir "$SMPROGRAMS\$ICONS_GROUP"
  RMDir "$INSTDIR\Locale\es"
  RMDir "$INSTDIR\Locale"
  RMDir "$INSTDIR\Graphics"
  RMDir "$INSTDIR"
  RMDir "$DOCUMENTS\Armies\Boards"
  RMDir "$DOCUMENTS\Armies"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  SetAutoClose true
SectionEnd