// throwmap.h: map operators which throw on unexpected conditions
// Functions to work safety with Maps objects

// ==========================================================================================================================
// Notes: the data type for the keys of the Map MUST be a wxString object
// ==========================================================================================================================

#if !defined THROWMAP_H
#define THROWMAP_H

#include "stdafx.h"

#include <wx/debug.h>
#include <wx/log.h>

#include "clsLibrary.h"

namespace safemap {

//_____________________________________________________________________________________________________________________

/**
* Create a new (key,value) pair in the map
*/
template <class MAP>
inline void insert(MAP &map, const wxString& key, const typename MAP::mapped_type &value)
{
#if defined __WXDEBUG__
	typename MAP::iterator i = map.find(key);
	
	if (i != map.end() && i->first == key) {
		wxString strError = wxString::Format(_("Duplicated key while inserting a new element. Key: %s"), key.c_str());
		wxFAIL_MSG(strError);
	}
#endif
	
	map[key] = value;
}

//_____________________________________________________________________________________________________________________

/**
* Change the value of an existing map entry
*/
template <class MAP>
inline void modify(MAP &map, const wxString& key, const typename MAP::mapped_type &value)
{
	typename MAP::iterator i = map.find(key);

#if defined __WXDEBUG__
	if (i == map.end()) {
		wxString strError = wxString::Format(_("The Key you try to modify does not exist. Key: %s"), key.c_str());
		wxFAIL_MSG(strError);
	}
#endif

	i->second = value;
}

//_____________________________________________________________________________________________________________________

/**
* Remove the entry indicated by the given key
*/
template <typename MAP>
inline void erase(MAP &map, const wxString& key)
{
	typename MAP::iterator i = map.find(key);

#if defined __WXDEBUG__
	if (i == map.end()) {
		wxString strError = wxString::Format(_("The Key you are trying to erase does not exist. Key: %s"), key.c_str());
		wxFAIL_MSG(strError);
	}
#endif	

	map.erase(i);
}

//_____________________________________________________________________________________________________________________

/**
* Find the value associated with the given key
*/
template <typename MAP>
inline typename MAP::mapped_type& find(MAP &map, const wxString& key)
{
	typename MAP::iterator i = map.find(key);

#if defined __WXDEBUG__
	if (i == map.end()) {
		wxString strError = wxString::Format(_("The Key you are trying to find does not exist. Key: %s"), key.c_str());
		wxFAIL_MSG(strError);
	}
#endif	
	
	return i->second;
}

//_____________________________________________________________________________________________________________________

template <typename MAP>
inline const typename MAP::mapped_type & // find the value associated with the given key
const_find(const MAP &map, const wxString& key) // [[17]]
{
	typename MAP::iterator i = map.find(key);

#if defined __WXDEBUG__
	if (i == map.end()) {
		wxString strError = wxString::Format(_("The Key you are trying to [const] find does not exist. Key: %s"), key.c_str());
		wxFAIL_MSG(strError);
	}
#endif

	return i->second;
}

//_____________________________________________________________________________________________________________________

} // end of namespace safemap

#endif // end of file
