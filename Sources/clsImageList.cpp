#include "clsImageList.h"

#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

/*!
* Includes
*/
#include "throwmap.h"
#include <wx/imaglist.h>

/*!
* Definition of static variables
*/

//--------------------------------------------------------------------------------------------------------------------------------

clsImageList::clsImageList(int width, int height, const bool mask, int initialCount)
 : wxImageList(width, height, mask, initialCount)
{
}


//--------------------------------------------------------------------------------------------------------------------------------

clsImageList::~clsImageList()
{
}

//--------------------------------------------------------------------------------------------------------------------------------

int clsImageList::Add(const wxBitmap& bitmap, const wxColour& maskColour, wxString pVstrKey) {
	int intIndex;

	intIndex = wxImageList::Add(bitmap, maskColour);
	safemap::insert(mVdctIndexes, pVstrKey	, intIndex);

	return(intIndex);
}

//--------------------------------------------------------------------------------------------------------------------------------

int clsImageList::intGetIndex(wxString pVstrKey) {
	cls_dct_Integer::iterator itrIndexes = mVdctIndexes.find(pVstrKey);

#if defined __WXDEBUG__
	if (itrIndexes == mVdctIndexes.end()) {
		wxString strError = wxString::Format(_("The Key you are trying to find does not exist. Method: clsImageList::intGetIndex - Key: %s"), pVstrKey.c_str());
		wxFAIL_MSG(strError);
	}
#endif
	
	return(itrIndexes->second);
}

//--------------------------------------------------------------------------------------------------------------------------------

wxIcon clsImageList::GetIcon(wxString pVstrKey) {
	int intIndex = this->intGetIndex(pVstrKey);

	if(intIndex == -1) {
		// The index belonging to the given key was not found
		return(wxNullIcon);
	}
	
	return(wxImageList::GetIcon(intIndex));
}

//--------------------------------------------------------------------------------------------------------------------------------

int clsImageList::Add(const wxIcon& icon, wxString pVstrKey) {
	int intIndex;
	
	intIndex = wxImageList::Add(icon);
	safemap::insert(mVdctIndexes, pVstrKey	, intIndex);

	return(intIndex);
}
