/////////////////////////////////////////////////////////////////////////////
// Name:        clsFrame.cpp
// Purpose:     
// Author:      Germ�n Blando
// Modified by: 
// Created:     05/22/05 10:30:17
// RCS-ID:      
// Copyright:   Interfase
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include <wx/image.h>
#include <wx/file.h>
#include <wx/statline.h>
#include <wx/filesys.h>
#include <wx/imaglist.h>
#include <wx/tooltip.h>
#include <wx/filedlg.h>
#include <wx/filefn.h>
#include <wx/filename.h>

#include "clsFrame.h"
#include "clsDlgNewBoard.h"
#include "Enumerations.h"
#include "Global.h"
#include "ClsApplication.h"
#include "clsBoard.h"
#include "clsToolBar.h"
#include "clsfixedselection.h"
#include "clsTerrain.h"
#include "clsNaturalResource.h"
#include "clsMap.h"
#include "clsOptions.h"

////@begin XPM images
#include "../Graficos Desarrollo/Iconos/16x16/filenew.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/new_board.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/fileopen.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/fileclose.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/FileSave.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/FileSaveAs.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/configure.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/exit.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/insert_object.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/delete_object.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/cancel_action.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/grassland.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/water.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/mountain.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/left_sea.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/right_sea.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/top_sea.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/bottom_sea.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/top_left_island.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/top_right_island.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/bottom_left_island.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/bottom_right_island.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/top_left_lake.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/top_right_lake.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/bottom_left_lake.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/bottom_right_lake.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/left_mount.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/right_mount.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/top_mount.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/bottom_mount.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/top_right_valley.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/top_left_valley.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/bottom_right_valley.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/bottom_left_valley.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/top_right_mountain_range.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/top_left_mountain_range.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/bottom_right_mountain_range.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/bottom_left_mountain_range.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/eraser.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/tree.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/minerals.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/vegetables.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/fish.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/petroleum.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/properties.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/help.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/gohome.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/mail_generic.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/About.xpm"
////@end XPM images

// -------------------------------------------------------------------------------------------------------------------
// Graphics included manually

#include "../Graficos Desarrollo/Iconos/16x16/clock.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/turn_round.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/actions.xpm"

// Images for the property list
#include "../Graficos Desarrollo/Iconos/16x16/information.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_0.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_1.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_2.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_3.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_4.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_5.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_6.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_7.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_8.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_9.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_10.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_11.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_12.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_13.xpm"
#include "../Graficos Desarrollo/Iconos/16x16/health_14.xpm"

// -------------------------------------------------------------------------------------------------------------------

// Application icon for Linux
#if defined(__GNUG__)
#include "Ejercitos.xpm"
#endif

/*!
 * clsFrame type definition
 */

IMPLEMENT_CLASS( clsFrame, wxFrame )

//-----------------------------------------------------------------------------------------------------------------------
/*!
 * clsFrame event table definition
 */

BEGIN_EVENT_TABLE( clsFrame, wxFrame )

////@begin clsFrame event table entries
 EVT_KEY_DOWN( clsFrame::OnKeyDown )

 EVT_MENU( ID_File_New_Board, clsFrame::OnFileNewBoardClick )

 EVT_MENU( ID_File_Open_Board, clsFrame::OnFileOpenBoardClick )

 EVT_MENU( ID_File_Close, clsFrame::OnFileCloseClick )

 EVT_MENU( ID_File_Save, clsFrame::OnFileSaveClick )

 EVT_MENU( ID_File_SaveAs, clsFrame::OnFileSaveAsClick )

 EVT_MENU( wxID_File_Exit, clsFrame::OnFileExitClick )

 EVT_MENU( ID_MENUITEM, clsFrame::Prueba_OnMenuitemClick )

 EVT_MENU( ID_Board_AddObject, clsFrame::OnBoardAddobjectClick )

 EVT_MENU( ID_Board_DeleteObject, clsFrame::OnBoardDeleteObjectClick )

 EVT_MENU( ID_Board_CancelAction, clsFrame::OnBoardCancelactionClick )

 EVT_MENU( ID_Board_AutoBorder, clsFrame::OnBoardAutoBorderClick )

 EVT_MENU( ID_Board_FillRectangularArea, clsFrame::OnBoardFillrectangularareaClick )

 EVT_MENU( ID_Board_RecallLastMark, clsFrame::OnBoardRecalllastmarkClick )

 EVT_MENU( ID_Board_Grassland, clsFrame::OnBoardGrasslandClick )

 EVT_MENU( ID_Board_Water, clsFrame::OnBoardWaterClick )

 EVT_MENU( ID_Board_Mountain, clsFrame::OnBoardMountainClick )

 EVT_MENU( ID_LeftSea, clsFrame::OnLeftSeaClick )

 EVT_MENU( ID_RightSea, clsFrame::OnRightSeaClick )

 EVT_MENU( ID_TopSea, clsFrame::OnTopSeaClick )

 EVT_MENU( ID_BottomSea, clsFrame::OnBottomSeaClick )

 EVT_MENU( ID_TopLeftIsland, clsFrame::OnTopLeftIslandClick )

 EVT_MENU( ID_TopRightIsland, clsFrame::OnTopRightIslandClick )

 EVT_MENU( ID_BottomLeftIsland, clsFrame::OnBottomLeftIslandClick )

 EVT_MENU( ID_BottomRightIsland, clsFrame::OnBottomRightIslandClick )

 EVT_MENU( ID_TopLeftLake, clsFrame::OnTopLeftLakeClick )

 EVT_MENU( ID_TopRightLake, clsFrame::OnTopRightLakeClick )

 EVT_MENU( ID_BottomLeftLake, clsFrame::OnBottomLeftLakeClick )

 EVT_MENU( ID_BottomRightLake, clsFrame::OnBottomRightLakeClick )

 EVT_MENU( ID_LeftMount, clsFrame::OnLeftMountClick )

 EVT_MENU( ID_RightMount, clsFrame:: OnRightMountClick )

 EVT_MENU( ID_TopMount, clsFrame::OnTopMountClick )

 EVT_MENU( ID_BottomMount, clsFrame::OnBottomMountClick )

 EVT_MENU( ID_TopRightValley, clsFrame::OnTopRightValleyClick )

 EVT_MENU( ID_TopLeftValley, clsFrame::OnTopLeftValleyClick )

 EVT_MENU( ID_BottomRightValley, clsFrame::OnBottomRightValleyClick )

 EVT_MENU( ID_BottomLeftValley, clsFrame::OnBottomLeftValleyClick )

 EVT_MENU( ID_TopRightMountainRange, clsFrame::OnTopRightMountainRangeClick )

 EVT_MENU( ID_TopLeftMountainRange, clsFrame::OnTopLeftMountainRangeClick )

 EVT_MENU( ID_BottomRightMountainRange, clsFrame::OnBottomRightMountainRangeClick )

 EVT_MENU( ID_BottomLeftMountainRange, clsFrame::OnBottomLeftMountainRangeClick )

 EVT_MENU( ID_Eraser, clsFrame::OnEraserClick )

 EVT_MENU( ID_Board_Tree, clsFrame::OnBoardTreeClick )

 EVT_MENU( ID_Board_Minerals, clsFrame::OnBoardMineralsClick )

 EVT_MENU( ID_Board_Vegetables, clsFrame::OnBoardVegetablesClick )

 EVT_MENU( ID_Board_Fish, clsFrame::OnBoardFishClick )

 EVT_MENU( ID_Board_Oil, clsFrame::OnBoardOilClick )

 EVT_MENU( ID_Board_Properties, clsFrame::OnBoardPropertiesClick )

 EVT_MENU( ID_Help_About, clsFrame::OnHelpAboutClick )

////@end clsFrame event table entries

EVT_TIMER(ID_TIMER_Cursor, clsFrame::OnTimer_Cursor)
EVT_CLOSE(clsFrame::OnClose)

//-----------------------------------------------------------------------------------------------------------------------
// Common toolbar events
EVT_TOOL(ID_TOOL_NewBoard, clsFrame::OnToolClick_NewBoard)
EVT_TOOL(ID_TOOL_Eraser, clsFrame::OnToolClick_Eraser)
EVT_TOOL(ID_TOOL_Grassland, clsFrame::OnToolClick_Grassland)
EVT_TOOL(ID_TOOL_Water, clsFrame::OnToolClick_Water)
EVT_TOOL(ID_TOOL_Mountain, clsFrame::OnToolClick_Mountain)
EVT_TOOL(ID_TOOL_Tree, clsFrame::OnToolClick_Tree)
EVT_TOOL(ID_TOOL_Minerals, clsFrame::OnToolClick_Minerals)
EVT_TOOL(ID_TOOL_Oil, clsFrame::OnToolClick_Oil)
EVT_TOOL(ID_TOOL_Fish, clsFrame::OnToolClick_Fish)
EVT_TOOL(ID_TOOL_Vegetables, clsFrame::OnToolClick_Vegetables)

END_EVENT_TABLE()
//-----------------------------------------------------------------------------------------------------------------------

//_______________________________________________________________________________________________________________________

clsFrame::clsFrame(wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style)
	: mVimlProperties(16, 16, true)
	, mVarrMenuNames(3)
	, mVarrMenuVisibilities(mVarrMenuNames.getSize())
	, mVarrMenuObjects(mVarrMenuNames.getSize())
	, mVarrMenuCaptions(mVarrMenuNames.getSize())
{
    Create( parent, id, caption, pos, size, style );

	mOobjOptions = clsOptions::GetInstance();
	CreateLayout();
	this->ConstructStatusBar();
	
   // Set the frame icon
    SetIcon(wxICON(Ejercitos));

 	mOobjMenuBar = this->GetMenuBar();
	
	CreateEmptyToolbar();
	CreateBoardEditionToolbar();

	FillMenuArrays();

	mVtmrCursor.SetOwner(this, ID_TIMER_Cursor);
}

//_______________________________________________________________________________________________________________________

// frame destructor
clsFrame::~clsFrame()
{
	EmptyMenus();

    if (mVtmrCursor.IsRunning()) {
        mVtmrCursor.Stop();
    }
}

//_______________________________________________________________________________________________________________________

void clsFrame::CreateEmptyToolbar() {
	CountedPtr<wxBitmap> ObmpBitmap;

	mOobjEmptyToolBar = new wxToolBar(this, wxID_ANY);
    mOobjEmptyToolBar->SetToolBitmapSize(wxSize(16, 16));

	CreateCommonToolbarButtons(mOobjEmptyToolBar);

	mOobjEmptyToolBar->Show(false); // all toolbars in the system begin hidden
}

//_______________________________________________________________________________________________________________________

void clsFrame::CreateCommonToolbarButtons(clsPointer<wxToolBar> pOtlbToolbar) {
	CountedPtr<wxBitmap> ObmpBitmap;

    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/filenew.xpm"))));
    pOtlbToolbar->AddTool(ID_TOOL_NewGame, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("New game (Ctrl+N)"), wxEmptyString);
	
	ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/new_board.xpm"))));
    pOtlbToolbar->AddTool(ID_TOOL_NewBoard, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("New Board (Ctrl+Shift+N)"), wxEmptyString);
	
    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/fileopen.xpm"))));
    pOtlbToolbar->AddTool(ID_TOOL_Open, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Open a new game or board (Ctrl+O)"), wxEmptyString);
}

//_______________________________________________________________________________________________________________________

void clsFrame::CreateBoardEditionToolbar() {
	CountedPtr<wxBitmap> ObmpBitmap;
	
	mOobjBoardEditionToolBar = new wxToolBar(this, wxID_ANY);
    mOobjBoardEditionToolBar->SetToolBitmapSize(wxSize(16, 16));

	CreateCommonToolbarButtons(mOobjBoardEditionToolBar);

    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/FileSave.xpm"))));
    mOobjBoardEditionToolBar->AddTool(ID_TOOL_Save, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Save the current game or board (Ctrl+S)"), wxEmptyString);
    
    mOobjBoardEditionToolBar->AddSeparator();

	ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/eraser.xpm"))));
    mOobjBoardEditionToolBar->AddTool(ID_TOOL_Eraser, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Erase objects on the board (Ctrl+Space)"), wxEmptyString);

    mOobjBoardEditionToolBar->AddSeparator();

    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/grassland.xpm"))));
    mOobjBoardEditionToolBar->AddTool(ID_TOOL_Grassland, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Selects grassland surface to insert (Ctrl+P)"), wxEmptyString);

    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/water.xpm"))));
    mOobjBoardEditionToolBar->AddTool(ID_TOOL_Water, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Selects water surface to insert (Ctrl+A)"), wxEmptyString);

    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/mountain.xpm"))));
    mOobjBoardEditionToolBar->AddTool(ID_TOOL_Mountain, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Selects mountain surface to insert (Ctrl+M)"), wxEmptyString);

    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/tree.xpm"))));
    mOobjBoardEditionToolBar->AddTool(ID_TOOL_Tree, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Selects tree object to insert (Ctrl+B)"), wxEmptyString);

    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/minerals.xpm"))));
    mOobjBoardEditionToolBar->AddTool(ID_TOOL_Minerals, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Selects minerals object to insert (Ctrl+I)"), wxEmptyString);

    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/petroleum.xpm"))));
    mOobjBoardEditionToolBar->AddTool(ID_TOOL_Oil, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Selects oil object to insert (Ctrl+L)"), wxEmptyString);

    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/fish.xpm"))));
    mOobjBoardEditionToolBar->AddTool(ID_TOOL_Fish, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Selects fish object to insert (Ctrl+D)"), wxEmptyString);

    ObmpBitmap = Make_CP(new wxBitmap(this->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/vegetables.xpm"))));
    mOobjBoardEditionToolBar->AddTool(ID_TOOL_Vegetables, _T(""), *ObmpBitmap, wxNullBitmap, wxITEM_NORMAL, _("Selects vegetables object to insert (Ctrl+E)"), wxEmptyString);

    mOobjBoardEditionToolBar->AddSeparator();

	mOobjBoardEditionToolBar->Show(false); // all toolbars in the system begin hidden
}

//_______________________________________________________________________________________________________________________

void clsFrame::EmptyMenus() {
	for(int i = 0; i < mVarrMenuObjects.getSize(); i++) {
		if(mVarrMenuObjects[i].get() != NULL) {
			// There is a wxMenu object stored here
			delete(mVarrMenuObjects[i].get());
		}
	}
}

//_______________________________________________________________________________________________________________________

void clsFrame::FillMenuArrays() {
	// This menu names have to be no translated because they will be used only for internal references
	mVarrMenuNames[0] = _T("File");
	mVarrMenuCaptions[0] = _("&File");
	
	mVarrMenuNames[1] = _T("Board");
	mVarrMenuCaptions[1] = _("&Board");
	
	mVarrMenuNames[2] = _T("Help");
	mVarrMenuCaptions[2] = _("&Help");

	// Fill the others menu arrays with the same number of element of mVarrMenuNames
	for(int i = 0; i < mVarrMenuNames.getSize(); i++) {
		mVarrMenuVisibilities[i] = true;
		mVarrMenuObjects[i] = NULL;
	}
}

//_______________________________________________________________________________________________________________________

/*!
 * clsFrame creator
 */

bool clsFrame::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin clsFrame member initialization
////@end clsFrame member initialization

////@begin clsFrame creation
 wxFrame::Create( parent, id, caption, pos, size, style );

 CreateControls();
 Centre();
////@end clsFrame creation
    return TRUE;
}

//_______________________________________________________________________________________________________________________

/*!
 * Control creation for clsFrame
 */

void clsFrame::CreateControls()
{    
////@begin clsFrame content construction
 clsFrame* itemFrame1 = this;

 wxMenuBar* menuBar = new wxMenuBar;
 wxMenu* itemMenu3 = new wxMenu;
 wxMenu* itemMenu4 = new wxMenu;
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu4, ID_File_New_LocalGame, _("&Local Game...\tCtrl+N"), _("Creates a new Local Game"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/filenew.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu4->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu4, ID_File_New_Board, _("&Board...\tCtrl+Shift+N"), _("Creates a new Board"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/new_board.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu4->Append(menuItem);
 }
 itemMenu3->Append(ID_File_New, _("&New"), itemMenu4);
 wxMenu* itemMenu7 = new wxMenu;
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu7, ID_File_Open_LocalGame, _("&Local Game...\tCtrl+O"), _("Opens an existing game or board file"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/fileopen.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu7->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu7, ID_File_Open_Board, _("&Board...\tCtrl+Shift+O"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/new_board.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu7->Append(menuItem);
 }
 itemMenu3->Append(ID_File_Open, _("&Open"), itemMenu7);
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu3, ID_File_Close, _("&Close\tCtrl+W"), _("Closes the actual game or board"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/fileclose.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu3->Append(menuItem);
 }
 itemMenu3->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu3, ID_File_Save, _("&Save\tCtrl+S"), _("Saves the current game or board"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/FileSave.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu3->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu3, ID_File_SaveAs, _("Save &As..."), _("Saves the current game or board with another name"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/FileSaveAs.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu3->Append(menuItem);
 }
 itemMenu3->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu3, ID_File_Settings, _("Se&ttings..."), _("Allows you to change game preferences"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/configure.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu3->Append(menuItem);
 }
 itemMenu3->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu3, wxID_File_Exit, _("&Exit\tCtrl+Q"), _("Quits the application"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/exit.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu3->Append(menuItem);
 }
 itemMenu3->Append(ID_MENUITEM, _("Prueba"), _T(""), wxITEM_NORMAL);
 menuBar->Append(itemMenu3, _("&File"));
 mPmnuBoard = new wxMenu;
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_AddObject, _("&Add Object\tEnter"), _("Inserts the selected object into the board"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/insert_object.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_DeleteObject, _("&Delete Object\tDel"), _("Deletes the object from the selected square"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/delete_object.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_CancelAction, _("&Release Object\tEsc"), _("Releases the current object seleted"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/cancel_action.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 mPmnuBoard->Append(ID_Board_AutoBorder, _("A&uto-Border"), _T(""), wxITEM_CHECK);
 mPmnuBoard->AppendSeparator();
 mPmnuBoard->Append(ID_Board_FillRectangularArea, _("&Mark Rectangular Area\tCtrl+R"), _("Marks a rectangular area to fill with the selected object"), wxITEM_CHECK);
 mPmnuBoard->Append(ID_Board_RecallLastMark, _("&Recall Last Mark"), _T(""), wxITEM_NORMAL);
 mPmnuBoard->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_Grassland, _("&Grassland\tCtrl+P"), _("Selects grassland surface to insert"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/grassland.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_Water, _("&Water\tCtrl+A"), _("Selects water surface to insert"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/water.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_Mountain, _("&Mountain\tCtrl+M"), _("Selects mountain surface to insert"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/mountain.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 wxMenu* itemMenu31 = new wxMenu;
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_LeftSea, _("Left Sea"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/left_sea.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_RightSea, _("Right Sea"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/right_sea.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_TopSea, _("Top Sea"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/top_sea.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_BottomSea, _("Bottom Sea"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/bottom_sea.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 itemMenu31->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_TopLeftIsland, _("Top Left Island"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/top_left_island.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_TopRightIsland, _("Top Right Island"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/top_right_island.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_BottomLeftIsland, _("Bottom Left Island"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/bottom_left_island.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_BottomRightIsland, _("Bottom Right Island"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/bottom_right_island.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 itemMenu31->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_TopLeftLake, _("Top Left Lake"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/top_left_lake.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_TopRightLake, _("Top Right Lake"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/top_right_lake.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_BottomLeftLake, _("Bottom Left Lake"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/bottom_left_lake.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu31, ID_BottomRightLake, _("Bottom Right Lake"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/bottom_right_lake.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu31->Append(menuItem);
 }
 mPmnuBoard->Append(ID_Board_GrasslandWaterLimits, _("Grassland / Water Limits"), itemMenu31);
 wxMenu* itemMenu46 = new wxMenu;
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_LeftMount, _("Left Mount"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/left_mount.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_RightMount, _("Right Mount"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/right_mount.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_TopMount, _("Top Mount"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/top_mount.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_BottomMount, _("Bottom Mount"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/bottom_mount.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 itemMenu46->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_TopRightValley, _("Top Right Valley"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/top_right_valley.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_TopLeftValley, _("Top Left Valley"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/top_left_valley.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_BottomRightValley, _("Bottom Right Valley"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/bottom_right_valley.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_BottomLeftValley, _("Bottom Left Valley"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/bottom_left_valley.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 itemMenu46->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_TopRightMountainRange, _("Top Right Mountain Range"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/top_right_mountain_range.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_TopLeftMountainRange, _("Top Left Mountain Range"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/top_left_mountain_range.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_BottomRightMountainRange, _("Bottom Right Mountain Range"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/bottom_right_mountain_range.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu46, ID_BottomLeftMountainRange, _("Bottom Left Mountain Range"), _T(""), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/bottom_left_mountain_range.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu46->Append(menuItem);
 }
 mPmnuBoard->Append(ID_Board_GrasslandMountainLimits, _("Grassland / Mountain Limits"), itemMenu46);
 mPmnuBoard->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Eraser, _("&Eraser\tCtrl+Space"), _("Deletes objects placed at the top level of the map"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/eraser.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_Tree, _("&Tree\tCtrl+B"), _("Selects tree object to insert"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/tree.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_Minerals, _("&Minerals\tCtrl+I"), _("Selects minerals object to insert"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/minerals.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_Vegetables, _("&Vegetables\tCtrl+E"), _("Selects vegetables object to insert"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/vegetables.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_Fish, _("F&ish\tCtrl+D"), _("Selects fish object to insert"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/fish.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_Oil, _("&Oil\tCtrl+L"), _("Selects oil object to insert"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/petroleum.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 mPmnuBoard->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(mPmnuBoard, ID_Board_Properties, _("Board P&roperties..."), _("Shows the board properties dialog"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/properties.xpm")));
  menuItem->SetBitmap(bitmap);
  mPmnuBoard->Append(menuItem);
 }
 menuBar->Append(mPmnuBoard, _("&Board"));
 wxMenu* itemMenu70 = new wxMenu;
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu70, ID_Help_Contents, _("&Contents\tF1"), _("Shows the help contents"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/help.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu70->Append(menuItem);
 }
 itemMenu70->Append(ID_Help_Tutorial, _("&Tutorial"), _("Shows the tutorial for the Armies"), wxITEM_NORMAL);
 itemMenu70->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu70, ID_Help_HomePage, _("&Home Page"), _("Go to the Armies Web Site"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/gohome.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu70->Append(menuItem);
 }
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu70, ID_Help_SendComments, _("&Send Comments..."), _("&Send comments or suggestions via E-Mail"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/mail_generic.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu70->Append(menuItem);
 }
 itemMenu70->AppendSeparator();
 itemMenu70->Append(ID_NewsGroup, _("&Newsgroup"), _("Go to the Newsgroup Home Page"), wxITEM_NORMAL);
 itemMenu70->Append(ID_MENU, _("Newsgroup &Registration..."), _("Send an E-Mail to register you into the Newsgroup"), wxITEM_NORMAL);
 itemMenu70->AppendSeparator();
 {
  wxMenuItem* menuItem = new wxMenuItem(itemMenu70, ID_Help_About, _("&About..."), _("About this program"), wxITEM_NORMAL);
  wxBitmap bitmap(itemFrame1->GetBitmapResource(wxT("../Graficos Desarrollo/Iconos/16x16/About.xpm")));
  menuItem->SetBitmap(bitmap);
  itemMenu70->Append(menuItem);
 }
 menuBar->Append(itemMenu70, _("&Help"));
 itemFrame1->SetMenuBar(menuBar);

////@end clsFrame content construction
}

//_______________________________________________________________________________________________________________________

/*!
 * Should we show tooltips?
 */

bool clsFrame::ShowToolTips()
{
    return TRUE;
}

//_______________________________________________________________________________________________________________________

/*!
 * Get bitmap resources
 */

wxBitmap clsFrame::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin clsFrame bitmap retrieval
 wxUnusedVar(name);
 if (name == _T("../Graficos Desarrollo/Iconos/16x16/filenew.xpm"))
 {
  wxBitmap bitmap( filenew_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/new_board.xpm"))
 {
  wxBitmap bitmap( new_board_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/fileopen.xpm"))
 {
  wxBitmap bitmap( fileopen_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/fileclose.xpm"))
 {
  wxBitmap bitmap( fileclose_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/FileSave.xpm"))
 {
  wxBitmap bitmap( FileSave_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/FileSaveAs.xpm"))
 {
  wxBitmap bitmap( FileSaveAs_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/configure.xpm"))
 {
  wxBitmap bitmap( configure_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/exit.xpm"))
 {
  wxBitmap bitmap( exit_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/insert_object.xpm"))
 {
  wxBitmap bitmap( insert_object_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/delete_object.xpm"))
 {
  wxBitmap bitmap( delete_object_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/cancel_action.xpm"))
 {
  wxBitmap bitmap( cancel_action_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/grassland.xpm"))
 {
  wxBitmap bitmap( grassland_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/water.xpm"))
 {
  wxBitmap bitmap( water_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/mountain.xpm"))
 {
  wxBitmap bitmap( mountain_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/left_sea.xpm"))
 {
  wxBitmap bitmap( left_sea_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/right_sea.xpm"))
 {
  wxBitmap bitmap( right_sea_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/top_sea.xpm"))
 {
  wxBitmap bitmap( top_sea_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/bottom_sea.xpm"))
 {
  wxBitmap bitmap( bottom_sea_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/top_left_island.xpm"))
 {
  wxBitmap bitmap( top_left_island_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/top_right_island.xpm"))
 {
  wxBitmap bitmap( top_right_island_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/bottom_left_island.xpm"))
 {
  wxBitmap bitmap( bottom_left_island_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/bottom_right_island.xpm"))
 {
  wxBitmap bitmap( bottom_right_island_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/top_left_lake.xpm"))
 {
  wxBitmap bitmap( top_left_lake_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/top_right_lake.xpm"))
 {
  wxBitmap bitmap( top_right_lake_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/bottom_left_lake.xpm"))
 {
  wxBitmap bitmap( bottom_left_lake_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/bottom_right_lake.xpm"))
 {
  wxBitmap bitmap( bottom_right_lake_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/left_mount.xpm"))
 {
  wxBitmap bitmap( left_mount_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/right_mount.xpm"))
 {
  wxBitmap bitmap( right_mount_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/top_mount.xpm"))
 {
  wxBitmap bitmap( top_mount_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/bottom_mount.xpm"))
 {
  wxBitmap bitmap( bottom_mount_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/top_right_valley.xpm"))
 {
  wxBitmap bitmap( top_right_valley_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/top_left_valley.xpm"))
 {
  wxBitmap bitmap( top_left_valley_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/bottom_right_valley.xpm"))
 {
  wxBitmap bitmap( bottom_right_valley_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/bottom_left_valley.xpm"))
 {
  wxBitmap bitmap( bottom_left_valley_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/top_right_mountain_range.xpm"))
 {
  wxBitmap bitmap( top_right_mountain_range_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/top_left_mountain_range.xpm"))
 {
  wxBitmap bitmap( top_left_mountain_range_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/bottom_right_mountain_range.xpm"))
 {
  wxBitmap bitmap( bottom_right_mountain_range_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/bottom_left_mountain_range.xpm"))
 {
  wxBitmap bitmap( bottom_left_mountain_range_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/eraser.xpm"))
 {
  wxBitmap bitmap(eraser_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/tree.xpm"))
 {
  wxBitmap bitmap( tree_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/minerals.xpm"))
 {
  wxBitmap bitmap( minerals_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/vegetables.xpm"))
 {
  wxBitmap bitmap( vegetables_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/fish.xpm"))
 {
  wxBitmap bitmap( fish_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/petroleum.xpm"))
 {
  wxBitmap bitmap( petroleum_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/properties.xpm"))
 {
  wxBitmap bitmap( properties_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/help.xpm"))
 {
  wxBitmap bitmap( help_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/gohome.xpm"))
 {
  wxBitmap bitmap( gohome_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/mail_generic.xpm"))
 {
  wxBitmap bitmap( mail_generic_xpm);
  return bitmap;
 }
 else if (name == _T("../Graficos Desarrollo/Iconos/16x16/About.xpm"))
 {
  wxBitmap bitmap( About_xpm);
  return bitmap;
 }
 return wxNullBitmap;
////@end clsFrame bitmap retrieval
}

//_______________________________________________________________________________________________________________________

/*!
 * Get icon resources
 */

wxIcon clsFrame::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin clsFrame icon retrieval
 wxUnusedVar(name);
 return wxNullIcon;
////@end clsFrame icon retrieval
}

//_______________________________________________________________________________________________________________________

void clsFrame::CreateLayout() {
	wxBoxSizer* szrPrincipal = new wxBoxSizer(wxHORIZONTAL);
	
	//-------------------------------------------------------------------------------------------------------------------
	wxPanel* pnlJuego = new wxPanel(this);
	wxBoxSizer* szrJuego = new wxBoxSizer(wxVERTICAL);
	
    szrJuego->Add( new wxStaticLine(pnlJuego, -1), 0, wxGROW);
	
	// Create the Canvas Object
	mPobjCanvas = new clsCanvas(pnlJuego, 10, 10);
	szrJuego->Add( mPobjCanvas, 1, wxGROW | wxALL, 2 );
	
	szrJuego->Add( new wxStaticLine(pnlJuego, -1), 0, wxGROW);
	
    pnlJuego->SetSizer( szrJuego );
    pnlJuego->SetAutoLayout( TRUE );
    szrJuego->Fit( pnlJuego );
	
	szrPrincipal->Add(pnlJuego, 1, wxEXPAND);
	
	//-------------------------------------------------------------------------------------------------------------------
	wxPanel * pnlIzquierda = new wxPanel(this);
	wxBoxSizer * szrIzquierda = new wxBoxSizer(wxVERTICAL);
	
	//-------------------------------------------------------------------------------------------------------------------
	// Map
	wxPanel* PpnlMap = new wxPanel(pnlIzquierda, wxID_ANY, wxDefaultPosition, wxSize(300, -1), wxSUNKEN_BORDER|wxTAB_TRAVERSAL);
	szrIzquierda->Add(PpnlMap, 1, wxGROW|wxALL, 2);

	wxFlexGridSizer* PszrMap = new wxFlexGridSizer(3, 1, 0, 0);
	PszrMap->AddGrowableRow(0);
	PszrMap->AddGrowableCol(0);
	PpnlMap->SetSizer(PszrMap);

	mOpnlCanvasMap = new wxPanel( PpnlMap, wxID_ANY, wxPoint(1, 1), wxDefaultSize, wxRAISED_BORDER|wxTAB_TRAVERSAL );
	PszrMap->Add(mOpnlCanvasMap, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL|wxSHAPED, 2);

	wxBoxSizer* PszrCanvasMap = new wxBoxSizer(wxHORIZONTAL);
	mOpnlCanvasMap->SetSizer(PszrCanvasMap);

	mOobjMap = new clsMap(mOpnlCanvasMap, wxID_ANY);
	PszrCanvasMap->Add(mOobjMap, 1, wxGROW|wxALL, 2);
	
	mPobjCanvas->SetOobjMap(mOobjMap); // connect the canvas object with the map
	mPobjCanvas->SetOobjFrame(this);
	mOobjMap->SetOobjFrame(this);

	//-------------------------------------------------------------------------------------------------------------------
	// Properties
	wxPanel* PpnlProperties = new wxPanel(pnlIzquierda, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNO_BORDER|wxTAB_TRAVERSAL);
	szrIzquierda->Add(PpnlProperties, 1, wxGROW);
	CreateProperties(PpnlProperties);
	
    pnlIzquierda->SetSizer(szrIzquierda);
    pnlIzquierda->SetAutoLayout(TRUE);
    szrIzquierda->Fit(pnlIzquierda);
	
	szrPrincipal->Add(pnlIzquierda, 0, wxEXPAND);
	
	//-------------------------------------------------------------------------------------------------------------------
	this->SetSizer(szrPrincipal);
	this->SetAutoLayout(TRUE);
	szrPrincipal->Fit(this);
}

//_______________________________________________________________________________________________________________________

void clsFrame::CreateProperties(wxPanel* pPpnlPanel) {
	wxPanel* PpnlPanel;
	wxBoxSizer* PszrBoxSizer;
	wxStaticBitmap* PsbmStaticBitmap;
	
	wxGridBagSizer* PszrProperties = new wxGridBagSizer(0, 0);
	PszrProperties->AddGrowableRow(0);
	PszrProperties->AddGrowableCol(0);
	PszrProperties->SetEmptyCellSize(wxSize(1, 1));
	pPpnlPanel->SetSizer(PszrProperties);

	// ..................................................................................................................
	// Top sizer
	mOszrPropertiesTop = new wxBoxSizer(wxHORIZONTAL);
	PszrProperties->Add(mOszrPropertiesTop, wxGBPosition(0, 0), wxGBSpan(1, 1), wxGROW|wxGROW|wxALL, 2); // adds the top sizer to the main sizer
	
	// ..................................................................................................................
	// No object selected

	// No object selected panel
	mOpnlNoObject = new wxPanel(pPpnlPanel, ID_NoObject, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL);
	wxBoxSizer* PszrNoObject = new wxBoxSizer(wxHORIZONTAL);
	mOpnlNoObject->SetSizer(PszrNoObject);
	
	// No object selected label
	wxStaticText* PlblNoObject = new wxStaticText(mOpnlNoObject, wxID_STATIC, _("No Object Selected"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE);
	PszrNoObject->Add(PlblNoObject, 1, wxALIGN_CENTER_VERTICAL|wxALL, 2);

	// ..................................................................................................................
	// Adds the "No Object Selected" panel to the top sizer
	mOszrPropertiesTop->Add(mOpnlNoObject, 1, wxGROW|wxALL, 0);

	// ..................................................................................................................
	// Tabulator
	mOtabProperties = new wxNotebook(pPpnlPanel, ID_tabProperties, wxDefaultPosition, wxDefaultSize, wxBK_DEFAULT);
	wxImageList* PimlImageList = new wxImageList(16, 16, true, 2);
	{
		PimlImageList->Add(wxIcon(properties_xpm));
		PimlImageList->Add(wxIcon(content_xpm));
	}
	mOtabProperties->AssignImageList(PimlImageList);
	mOtabProperties->Show(false); // at the first time, the tab properties is hidden and the label "No Object Selected" is visible

	// ..................................................................................................................
	// Properties
	// Page properties
	PpnlPanel = new wxPanel(mOtabProperties, ID_pagProperties, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL);
	PszrBoxSizer = new wxBoxSizer(wxVERTICAL);
	PpnlPanel->SetSizer(PszrBoxSizer);

	// List view properties
	mOlvwProperties = new wxListCtrl(PpnlPanel, ID_lvwProperties, wxDefaultPosition, wxSize(100, 100), wxLC_REPORT | wxLC_SINGLE_SEL | wxLC_HRULES | wxLC_VRULES);
	ArrangeListProperties();
	PszrBoxSizer->Add(mOlvwProperties, 1, wxGROW|wxALL, 5);
	
	// Adds the page to the tabulator
	mOtabProperties->AddPage(PpnlPanel, _("Properties"), false, 0);

	// ..................................................................................................................
	// Content
	// Page content
	PpnlPanel = new wxPanel(mOtabProperties, ID_pagContent, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL);
	PszrBoxSizer = new wxBoxSizer(wxVERTICAL);
	PpnlPanel->SetSizer(PszrBoxSizer);

	// Tree content
	mOtrcContent = new wxTreeCtrl( PpnlPanel, ID_trcContent, wxDefaultPosition, wxSize(100, 100), wxTR_SINGLE );
	PszrBoxSizer->Add(mOtrcContent, 1, wxGROW|wxALL, 5);

	// Adds the page to the tabulator
	mOtabProperties->AddPage(PpnlPanel, _("Content"), false, 1);

	// ..................................................................................................................
	// Adds the tabulator to top sizer
	mOszrPropertiesTop->Add(mOtabProperties, 1, wxGROW|wxALL, 0);

	// ..................................................................................................................
	
	// Line separator
	wxStaticLine* PobjStaticLine = new wxStaticLine(pPpnlPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL|wxNO_BORDER);
	PszrProperties->Add(PobjStaticLine, wxGBPosition(1, 0), wxGBSpan(1, 1), wxGROW|wxALIGN_CENTER_VERTICAL|wxALL, 2);

	// Bottom sizer
	wxBoxSizer* PszrBottonSizer = new wxBoxSizer(wxHORIZONTAL);
	PszrProperties->Add(PszrBottonSizer, wxGBPosition(2, 0), wxGBSpan(1, 1), wxGROW|wxALIGN_CENTER_VERTICAL|wxALL, 2);

	// ..................................................................................................................
	// Seconds left
	// Seconds left panel
	wxPanel* PpnlSecondsLeft = new wxPanel(pPpnlPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL);
	PpnlSecondsLeft->SetToolTip(_("Seconds left in the actual turn"));
	PszrBottonSizer->Add(PpnlSecondsLeft, 1, wxALIGN_CENTER_VERTICAL|wxALL, 1);

	// Seconds left sizer
	wxBoxSizer* PszrSecondsLeftSizer = new wxBoxSizer(wxHORIZONTAL);
	PpnlSecondsLeft->SetSizer(PszrSecondsLeftSizer);

	// Seconds left icon
	PsbmStaticBitmap = new wxStaticBitmap(PpnlSecondsLeft, wxID_STATIC, wxBitmap(clock_xpm), wxDefaultPosition, wxSize(16, 16), 0);
	PsbmStaticBitmap->SetToolTip(PpnlSecondsLeft->GetToolTip()->GetTip()); // to show the tooltip information as in the panel
	PszrSecondsLeftSizer->Add(PsbmStaticBitmap, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

	// Seconds left label
	mOlblSecondsLeft = new wxStaticText(PpnlSecondsLeft, ID_lblSecondsLeft, _("120"), wxDefaultPosition, wxSize(20, -1), wxALIGN_CENTRE);
	mOlblSecondsLeft->SetToolTip(PpnlSecondsLeft->GetToolTip()->GetTip()); // to show the tooltip information as in the panel
	PszrSecondsLeftSizer->Add(mOlblSecondsLeft, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);
	
	// Spacer
	PszrBottonSizer->Add(5, 5, 0, wxALIGN_CENTER_VERTICAL|wxALL, 0);
	
	// ..................................................................................................................
	// Round
	// Round panel
	wxPanel* PpnlRound = new wxPanel(pPpnlPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL);
	PpnlRound->SetToolTip(_("Round (Actual / Limit)"));
	PszrBottonSizer->Add(PpnlRound, 1, wxALIGN_CENTER_VERTICAL|wxALL, 1);
	
	// Round sizer
	wxBoxSizer* PszrRoundSizer = new wxBoxSizer(wxHORIZONTAL);
	PpnlRound->SetSizer(PszrRoundSizer);

	// Round icon
	PsbmStaticBitmap = new wxStaticBitmap(PpnlRound, wxID_STATIC, wxBitmap(turn_round_xpm), wxDefaultPosition, wxSize(16, 16), 0);
	PsbmStaticBitmap->SetToolTip(PpnlRound->GetToolTip()->GetTip()); // to show the tooltip information as in the panel
	PszrRoundSizer->Add(PsbmStaticBitmap, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);
	
	// Actual round label
	mOlblRound = new wxStaticText(PpnlRound, ID_lblRound, _("999"), wxDefaultPosition, wxSize(20, -1), wxALIGN_CENTRE);
	mOlblRound->SetToolTip(_("Actual Round"));
	PszrRoundSizer->Add(mOlblRound, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);
	
	// Round limit label
	mOlblRoundLimit = new wxStaticText(PpnlRound, ID_lblRoundLimit, _("1"), wxDefaultPosition, wxSize(20, -1), wxALIGN_CENTRE);
	mOlblRoundLimit->SetToolTip(_("Round Limit"));
	PszrRoundSizer->Add(mOlblRoundLimit, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);
	
	// Spacer
	PszrBottonSizer->Add(5, 5, 0, wxALIGN_CENTER_VERTICAL|wxALL, 0);
	
	// ..................................................................................................................
	// Actions left
	// Actions left panel
	wxPanel* PpnlActionsLeft = new wxPanel(pPpnlPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL);
	if(clsFrame::ShowToolTips()) {
		PpnlActionsLeft->SetToolTip(_("Actions left in the actual turn"));
	}
	PszrBottonSizer->Add(PpnlActionsLeft, 1, wxALIGN_CENTER_VERTICAL|wxALL, 1);
	
	// Actions left sizer
	wxBoxSizer* PszrActionsLeft = new wxBoxSizer(wxHORIZONTAL);
	PpnlActionsLeft->SetSizer(PszrActionsLeft);
	
	// Actions left icon
	PsbmStaticBitmap = new wxStaticBitmap(PpnlActionsLeft, wxID_STATIC, wxBitmap(actions_xpm), wxDefaultPosition, wxSize(16, 16), 0);
	PsbmStaticBitmap->SetToolTip(PpnlActionsLeft->GetToolTip()->GetTip()); // to show the tooltip information as in the panel
	PszrActionsLeft->Add(PsbmStaticBitmap, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);
	
	// Actions left label
	mOlblActionsLeft = new wxStaticText(PpnlActionsLeft, ID_lblActionsLeft, _("120"), wxDefaultPosition, wxSize(20, -1), wxALIGN_CENTRE);
	mOlblActionsLeft->SetToolTip(PpnlActionsLeft->GetToolTip()->GetTip()); // to show the tooltip information as in the panel
	PszrActionsLeft->Add(mOlblActionsLeft, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);
}

//_______________________________________________________________________________________________________________________

void clsFrame::ArrangeListProperties() {
	CountedPtr<wxListItem> SobjColumnInfo;
	int intColumnOrder = -1;
	
	intColumnOrder++;
	SobjColumnInfo = Make_CP(new wxListItem());
	SobjColumnInfo->SetText(_("Propertie"));
	mOlvwProperties->InsertColumn(intColumnOrder, *SobjColumnInfo.GetPtr());
	mOlvwProperties->SetColumnWidth(intColumnOrder, 125);

	intColumnOrder++;
	SobjColumnInfo = Make_CP(new wxListItem());
	SobjColumnInfo->SetText(_("Value"));
	mOlvwProperties->InsertColumn(intColumnOrder, *SobjColumnInfo.GetPtr());
	mOlvwProperties->SetColumnWidth(intColumnOrder, 125);
	
	// Image list
	mVimlProperties.Add(wxIcon(information_xpm));
	mVimlProperties.Add(wxIcon(health_0_xpm));
	mVimlProperties.Add(wxIcon(health_1_xpm));
	mVimlProperties.Add(wxIcon(health_2_xpm));
	mVimlProperties.Add(wxIcon(health_3_xpm));
	mVimlProperties.Add(wxIcon(health_4_xpm));
	mVimlProperties.Add(wxIcon(health_5_xpm));
	mVimlProperties.Add(wxIcon(health_6_xpm));
	mVimlProperties.Add(wxIcon(health_7_xpm));
	mVimlProperties.Add(wxIcon(health_8_xpm));
	mVimlProperties.Add(wxIcon(health_9_xpm));
	mVimlProperties.Add(wxIcon(health_10_xpm));
	mVimlProperties.Add(wxIcon(health_11_xpm));
	mVimlProperties.Add(wxIcon(health_12_xpm));
	mVimlProperties.Add(wxIcon(health_13_xpm));
	mVimlProperties.Add(wxIcon(health_14_xpm));
	
	mOlvwProperties->SetImageList(&mVimlProperties, wxIMAGE_LIST_SMALL);
}

//_______________________________________________________________________________________________________________________

void clsFrame::ChangeMenuState(clsPointer<wxMenu> pOmnuMenu, bool pVblnEnable) {
	clsPointer<wxMenuItem> OmniMenuItem;
	
	for(unsigned int i = 0; i < pOmnuMenu->GetMenuItemCount(); i++) {
		OmniMenuItem = pOmnuMenu->FindItemByPosition(i);
		OmniMenuItem->Enable(pVblnEnable);
	}
}

//_______________________________________________________________________________________________________________________

void clsFrame::ChangeStateEmpty()
{
	clsPointer<wxToolBar> OtlbPrevToolbar;

	StopTimers();
	mPobjCanvas->SetCursor(wxNullCursor); // normalizes the cursor before hide the Canvas
	mPobjCanvas->Show(false);
	
	// Hide the properties tabulator
	mOpnlNoObject->Show(true);
	mOtabProperties->Show(false);
	mOszrPropertiesTop->Layout();

	// Hide the map
	mOpnlCanvasMap->Show(false);

	//.................................................................................................................
	// Menus

	ChangeMenuState(mPmnuBoard, /* Enable */ false); // because in spite of hiding the menu, the hotkeys continue active (at least in GTK)
	HideMenu("Board");
	
	mOobjMenuBar->Enable(ID_File_Close, false);
	mOobjMenuBar->Enable(ID_File_Save, false);
	mOobjMenuBar->Enable(ID_File_SaveAs, false);

	//.................................................................................................................
	// Toolbar
	
	OtlbPrevToolbar = this->GetToolBar();
	
	if(OtlbPrevToolbar.blnValid()) {
		// There was a previous toolbar set
		this->SetToolBar(NULL);
		OtlbPrevToolbar->Show(false);
		//OtlbPrevToolbar->Realize(); // to disable the previous toolbar
	}

	this->SetToolBar(mOobjEmptyToolBar);
	mOobjEmptyToolBar->Show(true);
	mOobjEmptyToolBar->Realize();

	this->SendSizeEvent(); // to resize the frame to be aware of the new toolbar showed

	//.................................................................................................................
	// Status bar

	mPobjStatusBar->SetModality(gOobjApplication->strGetStatesLabels(enmStates_Empty));
	mPobjStatusBar->SetCreation(_T(""));
	mPobjStatusBar->SetConstruction(_T(""));
	mPobjStatusBar->SetPlayer(_T(""));
	mPobjStatusBar->SetPlayerColor(wxNullIcon);
	mPobjStatusBar->SetTerrainType(_T(""));
	mPobjStatusBar->SetActionIcon(wxNullIcon);
	mPobjStatusBar->SetAction(_T(""));
	mPobjStatusBar->HideCursorPosition();

	//.................................................................................................................
	mOlblActionsLeft->SetLabel(_T(""));
	mOlblRound->SetLabel(_T(""));
	mOlblRoundLimit->SetLabel(_T(""));
	mOlblSecondsLeft->SetLabel(_T(""));

	gOobjApplication->SetVstrFileName("");
	this->SetTitle(gOobjApplication->GetVstrTitle().c_str());
}

//_______________________________________________________________________________________________________________________

void clsFrame::ChangeStateBoardEdition() {
	clsPointer<wxToolBar> OtlbPrevToolbar;
	
	// ..................................................................................................................
	// Menus
	ShowMenu("Board");
	ChangeMenuState(mPmnuBoard, /* Enable */ true); // because in spite of hiding the menu, the hotkeys continue active (at least in GTK)
	
	mOobjMenuBar->Check(ID_Board_FillRectangularArea, false);
	
	mOobjMenuBar->Enable(ID_File_Close, true);
	mOobjMenuBar->Enable(ID_File_Save, true);
	mOobjMenuBar->Enable(ID_File_SaveAs, true);

	//-------------------------------------------------------------------------------------------------------------------
	// Toolbar
	OtlbPrevToolbar = this->GetToolBar();
	
	if(OtlbPrevToolbar.blnValid()) {
		// There was a previous toolbar set
		this->SetToolBar(NULL);
		OtlbPrevToolbar->Show(false);
	}

	this->SetToolBar(mOobjBoardEditionToolBar);
	mOobjBoardEditionToolBar->Show(true);
	mOobjBoardEditionToolBar->Realize();
	
	this->SendSizeEvent(); // resize the frame to be aware about the new toolbar showed

	//-------------------------------------------------------------------------------------------------------------------
	mPobjCanvas->Show(true);
	mPobjCanvas->SetFocus();
	mPobjStatusBar->SetModality(gOobjApplication->strGetStatesLabels(enmStates_BoardEdition));
	
	// Show the map
	mOpnlCanvasMap->Show(true);
	wxTheApp->Yield(); // it's necessary to get the correct size for the map window at this moment, in order to set the visible region
	mOobjMap->SetVisibleRegion(mPobjCanvas->GetVrctVisibleRegion()); // because at the beginning the visible region rect is not set, due some interaction between visibility and event execution
	mOobjMap->ShowMap();

	this->UpdateEnvironment();
	StartTimers();
}

//_______________________________________________________________________________________________________________________

void clsFrame::UpdateEnvironment() {
	if(gOobjApplication->GetVienState() == enmStates_BoardEdition) {
		UpdateActionsBoardEdition();
		UpdateInformationBoardEdition();
	} else {
		// The game is playing
		UpdateActionsGame();
	}
}

//_______________________________________________________________________________________________________________________

void clsFrame::UpdateInformationBoardEdition() {
	clsPointer<clsTerrain> OobjActiveTerrain;
	
	//-------------------------------------------------------------------------------------------------------------------
	// Toolbar
	UpdateToolbarBoardEdition();
	
	//-------------------------------------------------------------------------------------------------------------------
	// Status bar
	OobjActiveTerrain = mOobjBoard->OobjGetTerrain();
	mPobjStatusBar->SetTerrainType(OobjActiveTerrain->GetVstrDescription());
	mPobjStatusBar->SetCursorPosition(mOobjBoard->GetVintRowSelected(), mOobjBoard->GetVintColumnSelected());
	
	//-------------------------------------------------------------------------------------------------------------------
	// Properties
	if(mOobjBoard->blnBusySquare()) {
		// Exists an upper level object in the selected square
		mOtabProperties->Show(true);
		mOpnlNoObject->Show(false);
		mOszrPropertiesTop->Layout();
		
		// Show the properties for the selected object
		clsPointer<clsUpperLevelObject> OobjUpperLevelObject = mOobjBoard->OobjGetUpperLevelObject();
		OobjUpperLevelObject->ShowProperties(mOlvwProperties);
		
	} else {
		// There isn't any upper level object in the selected square
		mOpnlNoObject->Show(true);
		mOtabProperties->Show(false);
		mOszrPropertiesTop->Layout();
	}
}

//_______________________________________________________________________________________________________________________

void clsFrame::UpdateActionsBoardEdition() {
	//-------------------------------------------------------------------------------------------------------------------
	// Menus
	mOobjMenuBar->Enable(ID_Board_AddObject, true);
	mOobjMenuBar->Enable(ID_Board_CancelAction, true);
	mOobjMenuBar->Enable(ID_Board_DeleteObject, true);

	mOobjMenuBar->Enable(ID_Board_RecallLastMark, true);
	
	if(gOobjApplication->GetVienActionSelected() == enmActionSelected_Empty) {
		// There isn't a selected action
		mOobjMenuBar->Enable(ID_Board_AddObject, false);
		mOobjMenuBar->Enable(ID_Board_CancelAction, false);
	}

	if(!mOobjBoard->blnBusySquare(mOobjBoard->GetVintRowSelected(), mOobjBoard->GetVintColumnSelected())) {
			// The active square doesn't store a surface level object
		mOobjMenuBar->Enable(ID_Board_DeleteObject, false);
	}
	
	if(!mOobjBoard->blnFixedSelection()) {
		// There isn't a fixed selection
		mOobjMenuBar->Check(ID_Board_FillRectangularArea, false); // because the fixed selection can be released in different ways, no only through this menu item
		
		if(!mOobjBoard->GetSobjFixedSelection()->blnExistsPrevious()) {
			// There is not a previous fixed selection that can be restored
			mOobjMenuBar->Enable(ID_Board_RecallLastMark, false);
		}
	
	} else {
		// The fixed selection is set
		mOobjMenuBar->Check(ID_Board_FillRectangularArea, true); // because the fixed selection can be set in different ways, no only through this menu item
		mOobjMenuBar->Enable(ID_Board_RecallLastMark, false);
	}

	mOobjMenuBar->Check(ID_Board_AutoBorder, mOobjBoard->GetVblnApplyAutoBorder());
}

//_______________________________________________________________________________________________________________________

void clsFrame::UpdateToolbarBoardEdition() {
	mOobjBoardEditionToolBar->EnableTool(ID_TOOL_Eraser, mOobjMenuBar->IsEnabled(ID_Eraser));
}

//_______________________________________________________________________________________________________________________

void clsFrame::UpdateActionsGame() {
	// TODO: implement
}

//_______________________________________________________________________________________________________________________

int clsFrame::intScreenPositionMenu(wxString pVstrMenuName) {
	int intPosition = 0;
	
	for(int i = 0; i < mVarrMenuNames.getSize(); i++) {
		if(mVarrMenuNames[i] == pVstrMenuName) {
			// It's the wanted menu
			return(intPosition);

		} else {
			if(mVarrMenuVisibilities[i]) {
				// The current menu is visible
				intPosition++;
			}
		}
	}

	// If the execution reach this point, it means that the menu name can't be found

	wxString strError = _("The object wxMenu cannot be found: ");
	strError += pVstrMenuName;
	strError += _(" - Function: intScreenPositionMenu");

	wxMessageBox(strError, _("Error"), wxICON_ERROR);

	return(-1);
}

//_______________________________________________________________________________________________________________________

int clsFrame::intArrayPositionMenu(wxString pVstrMenuName) {
	for(int i = 0; i < mVarrMenuNames.getSize(); i++) {
		if(mVarrMenuNames[i] == pVstrMenuName) {
			// It's the wanted menu
			return(i);
		}
	}
	
	// If the execution reach this point, it means that the menu name can't be found
	
	wxString strError = _("The object wxMenu cannot be found: ");
	strError += pVstrMenuName;
	strError += _(" - Function: intArrayPositionMenu");
	
	wxMessageBox(strError, _("Error"), wxICON_ERROR);
	
	return(-1);
}

//_______________________________________________________________________________________________________________________


void clsFrame::HideMenu(wxString pVstrMenuName) {
	int intScreenPosition = intScreenPositionMenu(pVstrMenuName);
	int intArrayPosition = intArrayPositionMenu(pVstrMenuName);

	mVarrMenuObjects[intArrayPosition] = mOobjMenuBar->GetMenu(intScreenPosition); // to preserve the wxMenu object before the remotion from the menu bar
	mVarrMenuVisibilities[intArrayPosition] = false;

	mOobjMenuBar->Remove(intScreenPosition);
}

//_______________________________________________________________________________________________________________________


void clsFrame::ShowMenu(wxString pVstrMenuName)  {
	int intScreenPosition = intScreenPositionMenu(pVstrMenuName);
	int intArrayPosition = intArrayPositionMenu(pVstrMenuName);

	mOobjMenuBar->Insert(intScreenPosition, mVarrMenuObjects[intArrayPosition], mVarrMenuCaptions[intArrayPosition]);
	mVarrMenuVisibilities[intArrayPosition] = true;
	mVarrMenuObjects[intArrayPosition] = NULL;
}

//_______________________________________________________________________________________________________________________


void clsFrame::ConstructStatusBar() {
	mPobjStatusBar = new clsStatusBar(this);
	this->SetStatusBar(mPobjStatusBar);
}

//_______________________________________________________________________________________________________________________


void clsFrame::NewBoard() {
	bool blnCloseDocument;
	clsDlgNewBoard* pDlgNewBoard;
	int intResponse;
	CountedPtr<clsBoard> SobjBoard = gOobjApplication->GetSobjBoard();
	int intPixelWidth;
	int intPixelHeight;
	wxString strTitle;
	
	if(!blnConfirmClosePreviousDoc(blnCloseDocument)) {
		return;
	}
	
	pDlgNewBoard = new clsDlgNewBoard();
	pDlgNewBoard->Create(this);
	
	intResponse = pDlgNewBoard->ShowModal();
	
	if(intResponse != wxID_OK) {
		// Dialog canceled
		pDlgNewBoard->Destroy();
		return;
	}
	
	if(blnCloseDocument) {
		// First of all, close previous document
		gOobjApplication->ChangeStateEmpty();
	}
	
	SobjBoard->New(atoi(pDlgNewBoard->pTxtWidth->GetValue()), atoi(pDlgNewBoard->pTxtHeight->GetValue()));
	SobjBoard->SetvStrTitle(pDlgNewBoard->pTxtTitle->GetValue());
	SobjBoard->SetvStrDescription(pDlgNewBoard->pTxaDescription->GetValue());
	SobjBoard->SetvIntNumberPlayers(pDlgNewBoard->pCmbNumberOfPlayers->GetSelection() + 2);
	
	// Set virtual size in the OffScreen buffer
	intPixelWidth = SobjBoard->GetVintWidth() * SobjBoard->GetVintSQUARE_LENGTH();
	intPixelHeight = SobjBoard->GetVintHeight() * SobjBoard->GetVintSQUARE_LENGTH();
	
	mPobjCanvas->SetTotalSize(intPixelWidth, intPixelHeight);
	
	mPobjCanvas->SetvRctVisibleRegion(0, 0);
	mPobjCanvas->AdjustScrollbars();
	
	gOobjApplication->ChangeStateBoardEdition();
	mPobjCanvas->GetParent()->Layout(); // to force the game window to resize according with its frame container
	
	SobjBoard->DrawAll();
	mPobjCanvas->Draw();

	// Set the title bar
	strTitle.Printf(_("%s - [New Board]"), gOobjApplication->GetVstrTitle().c_str());
	this->SetTitle(strTitle);
	
	pDlgNewBoard->Destroy();
}

//_______________________________________________________________________________________________________________________

bool clsFrame::blnConfirmClosePreviousDoc(bool& pRblnCloseDocument) {
	wxString VstrDocumentType;
	wxString VstrMessage;
	enmStates VienState = gOobjApplication->GetVienState();
	
	pRblnCloseDocument = false;
	
	if(VienState != enmStates_Empty) {
		// Actually exists an open document
		if(VienState == enmStates_BoardEdition) {
			// Board edition is activated
			VstrDocumentType = _("Board");
		} else {
			// Game play is activated
			VstrDocumentType = _("Game");
		}

		VstrMessage.Printf(_("Confirm close the current %s?"), VstrDocumentType.c_str());
		
		if(wxMessageBox(VstrMessage, _("Confirmation"), wxICON_QUESTION | wxYES_NO, this) == wxNO) {
			// New document canceled
			return(false);
		} else {
			pRblnCloseDocument = true;
		}
	}

	return(true);
}

//.......................................................................................................................

bool clsFrame::blnConfirmClosePreviousDoc() {
	bool blnDummy;
	return(blnConfirmClosePreviousDoc(blnDummy));
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_File_New_Board
 */
void clsFrame::OnFileNewBoardClick( wxCommandEvent& WXUNUSED(event) )
{
	NewBoard();
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnToolClick_NewBoard(wxCommandEvent& event) {
	this->OnFileNewBoardClick(event);
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM
 */
void clsFrame::Prueba_OnMenuitemClick(wxCommandEvent& WXUNUSED(event))
{
	wxMessageBox("Test", "Test", wxICON_INFORMATION, wxTheApp->GetTopWindow());
}

//_______________________________________________________________________________________________________________________

void clsFrame::SetOobjBoard(clsPointer<clsBoard> value) {
	mOobjBoard = value; 
	mPobjCanvas->SetOobjBoard(value);
	mOobjMap->SetOobjBoard(value);
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for wxID_File_Exit
 */

void clsFrame::OnFileExitClick( wxCommandEvent& WXUNUSED(event) )
{
	Close(false);
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_File_Close
 */

void clsFrame::OnFileCloseClick( wxCommandEvent& WXUNUSED(event) )
{
	CloseDocument();
}

//_______________________________________________________________________________________________________________________

void clsFrame::CloseDocument() {
	bool blnCloseDocument;
	
	if(!blnConfirmClosePreviousDoc(blnCloseDocument)) {
		return;
	}

	gOobjApplication->ChangeStateEmpty();
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnTimer_Cursor(wxTimerEvent& WXUNUSED(event)) {
	mOobjBoard->DrawCursor();
}

//_______________________________________________________________________________________________________________________

void clsFrame::StartTimers() {
	mVtmrCursor.Start(100, false);
}

//_______________________________________________________________________________________________________________________

void clsFrame::StopTimers() {
	mVtmrCursor.Stop();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_FillRectangularArea
 * Really, it doesn't fill anything. Instead of this, it creates the rectangular area
 */

void clsFrame::OnBoardFillrectangularareaClick( wxCommandEvent& WXUNUSED(event) )
{
	// Note: the checked state comes already updated at this point
	
	if(mOobjMenuBar->IsChecked(ID_Board_FillRectangularArea)) {
		// The rectangular area in not set yet
		mOobjBoard->SetFixedSelection();
	} else {
		// The rectangular area is already set
		mOobjBoard->EraseFixedSelection();
	}
	
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_RecallLastMark
 */

void clsFrame::OnBoardRecalllastmarkClick( wxCommandEvent& WXUNUSED(event) )
{
	mOobjBoard->GetSobjFixedSelection()->Restore();
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Water
 */

void clsFrame::OnBoardWaterClick( wxCommandEvent& WXUNUSED(event) )
{
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("Water")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Eraser
 */

void clsFrame::OnEraserClick(wxCommandEvent& WXUNUSED(event))
{
	mOobjBoard->SetEraser();
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_CancelAction
 */

void clsFrame::OnBoardCancelactionClick(wxCommandEvent& WXUNUSED(event))
{
	mOobjBoard->ReleaseObjectToInsert();
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnToolClick_Eraser(wxCommandEvent& event) {
	this->OnEraserClick(event);
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_AddObject
 */

void clsFrame::OnBoardAddobjectClick(wxCommandEvent& WXUNUSED(event))
{
	mOobjBoard->AddObject();
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Tree
 */
void clsFrame::OnBoardTreeClick(wxCommandEvent& WXUNUSED(event)) {
	CountedPtr<clsNaturalResource> SobjNaturalResourceToInsert = safemap::find(gOobjApplication->GetVdctNaturalResourceTypes(), _T("Tree"));
	
	mOobjBoard->SetOobjObjectToInsert(SobjNaturalResourceToInsert.GetPtr());
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_KEY_DOWN event handler for ID_FRAME
 */
void clsFrame::OnKeyDown(wxKeyEvent& event) {
	int intKeyCode = event.GetKeyCode();

	if(gOobjApplication->GetVienState() == enmStates_BoardEdition) {
		// Board edition
		if(!event.ControlDown() && !event.AltDown() && !event.ShiftDown()) {
			// There isn't any modifier key pressed
			if(intKeyCode == WXK_ESCAPE) {
				// The ESC key was pressed
				if(mOobjMenuBar->IsEnabled(ID_Board_CancelAction)) {
					// The option is enabled
					wxCommandEvent objDummyEvent;
					OnBoardCancelactionClick(objDummyEvent);
				}
				
				return;
			}
		}
	}
	
	// If the process arrives here, it means that the pressed key was not been processed
	event.Skip();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_DeleteObject
 */
void clsFrame::OnBoardDeleteObjectClick(wxCommandEvent& WXUNUSED(event))
{
	mOobjBoard->EraseSelectedSquare();
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnClose(wxCloseEvent& event) {
	if(!event.CanVeto()) {
		// It's impossible to abort the application ending. So, continues with the destruction.
		this->Destroy();
		return;
	}
	
	if(!blnConfirmClosePreviousDoc()) {
		// The closing has been canceled
		event.Veto(); // notifies the calling code that we didn't delete the frame
	} else {
		// The closing has been confirmed
		this->Destroy();
	}
}

//_______________________________________________________________________________________________________________________

clsFrame::enm_imlProperties clsFrame::FienPropertyLevelBar(int pVintActualLevel, int pVintMaxLevel) {
	double dblProportionalHealth;
	int intProportionalHealth;
	
	if(pVintMaxLevel == 0) {
		// The property doesn't have a total value
		return(enm_imlProperties_Health_0); // to avoid division by 0 error
	}

	dblProportionalHealth = pVintActualLevel * 14 / pVintMaxLevel;
	intProportionalHealth = clsLibrary::intFromDouble(dblProportionalHealth);
	enm_imlProperties ienResult = static_cast<enm_imlProperties>(intProportionalHealth + 1); // because the first image in the list is the icon information
	
	return(ienResult);
}

//_______________________________________________________________________________________________________________________

void clsFrame::InsertVariableProperty(wxString pVstrLabel, int pVintActualLevel, int pVintMaxLevel, clsPointer<wxListCtrl> pOlvwProperties, int& pVintPosition) {
	wxString strValueProperty;
	wxString strActualValue;
	wxString strTotalValue;
	clsFrame::enm_imlProperties ienPropertyLevelBar;
	
	pVintPosition++;
	ienPropertyLevelBar = FienPropertyLevelBar(pVintActualLevel, pVintMaxLevel);
	
	strActualValue = wxString::Format(_T("%d"), pVintActualLevel);
	strTotalValue = wxString::Format(_T("%d"), pVintMaxLevel);
	strValueProperty = strActualValue + "/" + strTotalValue;
	
	pOlvwProperties->InsertItem(pVintPosition, pVstrLabel, ienPropertyLevelBar);
	pOlvwProperties->SetItem(pVintPosition, 1, strValueProperty);
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Grassland
 */
void clsFrame::OnBoardGrasslandClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("Grassland")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Mountain
 */
void clsFrame::OnBoardMountainClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("Mountain")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Minerals
 */
void clsFrame::OnBoardMineralsClick(wxCommandEvent& WXUNUSED(event)) {
	CountedPtr<clsNaturalResource> SobjNaturalResourceToInsert = safemap::find(gOobjApplication->GetVdctNaturalResourceTypes(), _T("Mineral"));

	mOobjBoard->SetOobjObjectToInsert(SobjNaturalResourceToInsert.GetPtr());
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Vegetables
 */
void clsFrame::OnBoardVegetablesClick(wxCommandEvent& WXUNUSED(event)) {
	CountedPtr<clsNaturalResource> SobjNaturalResourceToInsert = safemap::find(gOobjApplication->GetVdctNaturalResourceTypes(), _T("Vegetables"));

	mOobjBoard->SetOobjObjectToInsert(SobjNaturalResourceToInsert.GetPtr());
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Fish
 */
void clsFrame::OnBoardFishClick(wxCommandEvent& WXUNUSED(event)) {
	CountedPtr<clsNaturalResource> SobjNaturalResourceToInsert = safemap::find(gOobjApplication->GetVdctNaturalResourceTypes(), _T("Fish"));

	mOobjBoard->SetOobjObjectToInsert(SobjNaturalResourceToInsert.GetPtr());
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Oil
 */
void clsFrame::OnBoardOilClick(wxCommandEvent& WXUNUSED(event)) {
	CountedPtr<clsNaturalResource> SobjNaturalResourceToInsert = safemap::find(gOobjApplication->GetVdctNaturalResourceTypes(), _T("Oil"));

	mOobjBoard->SetOobjObjectToInsert(SobjNaturalResourceToInsert.GetPtr());
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnToolClick_Grassland(wxCommandEvent& event) {
	this->OnBoardGrasslandClick(event);
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnToolClick_Water(wxCommandEvent& event) {
	this->OnBoardWaterClick(event);
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnToolClick_Mountain(wxCommandEvent& event) {
	this->OnBoardMountainClick(event);
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnToolClick_Tree(wxCommandEvent& event) {
	this->OnBoardTreeClick(event);
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnToolClick_Minerals(wxCommandEvent& event) {
	this->OnBoardMineralsClick(event);
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnToolClick_Oil(wxCommandEvent& event) {
	this->OnBoardOilClick(event);
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnToolClick_Fish(wxCommandEvent& event) {
	this->OnBoardFishClick(event);
}

//_______________________________________________________________________________________________________________________

void clsFrame::OnToolClick_Vegetables(wxCommandEvent& event) {
	this->OnBoardVegetablesClick(event);
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_LeftSea
 */
void clsFrame::OnLeftSeaClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("RightCoast")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_RightSea
 */
void clsFrame::OnRightSeaClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("LeftCoast")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopSea
 */
void clsFrame::OnTopSeaClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("BottomCoast")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomSea
 */
void clsFrame::OnBottomSeaClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("TopCoast")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopLeftIsland
 */
void clsFrame::OnTopLeftIslandClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("TopLeftIsland")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopRightIsland
 */
void clsFrame::OnTopRightIslandClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("TopRightIsland")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomLeftIsland
 */
void clsFrame::OnBottomLeftIslandClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("BottomLeftIsland")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomRightIsland
 */
void clsFrame::OnBottomRightIslandClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("BottomRightIsland")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopLeftLake
 */
void clsFrame::OnTopLeftLakeClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("TopLeftLake")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopRightLake
 */
void clsFrame::OnTopRightLakeClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("TopRightLake")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomLeftLake
 */
void clsFrame::OnBottomLeftLakeClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("BottomLeftLake")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomRightLake
 */

void clsFrame::OnBottomRightLakeClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("BottomRightLake")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_LeftMount
 */
void clsFrame::OnLeftMountClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("RightMountain")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_RightMount
 */

void clsFrame:: OnRightMountClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("LeftMountain")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopMount
 */
void clsFrame::OnTopMountClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("BottomMountain")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomMount
 */
void clsFrame::OnBottomMountClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("TopMountain")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopRightValley
 */
void clsFrame::OnTopRightValleyClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("TopRightValley")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopLeftValley
 */
void clsFrame::OnTopLeftValleyClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("TopLeftValley")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomRightValley
 */
void clsFrame::OnBottomRightValleyClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("BottomRightValley")));
	this->UpdateEnvironment();
}

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomLeftValley
 */
void clsFrame::OnBottomLeftValleyClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("BottomLeftValley")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopRightMountainRange
 */
void clsFrame::OnTopRightMountainRangeClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("TopRightMount")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopLeftMountainRange
 */
void clsFrame::OnTopLeftMountainRangeClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("TopLeftMount")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomRightMountainRange
 */
void clsFrame::OnBottomRightMountainRangeClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("BottomRightMount")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomLeftMountainRange
 */
void clsFrame::OnBottomLeftMountainRangeClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetOobjObjectToInsert(gOobjApplication->GetOobjTerrain(_T("BottomLeftMount")));
	this->UpdateEnvironment();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_AutoBorder
 */
void clsFrame::OnBoardAutoBorderClick(wxCommandEvent& WXUNUSED(event)) {
	mOobjBoard->SetVblnApplyAutoBorder(mOobjMenuBar->IsChecked(ID_Board_AutoBorder));
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_File_Save
 */
void clsFrame::OnFileSaveClick(wxCommandEvent& event) {
	if(gOobjApplication->GetVstrFileName() == "") {
		// The document is unsaved
		OnFileSaveAsClick(event);
	} else {
		if(gOobjApplication->GetVienState() == enmStates_BoardEdition) {
			// The user is editing a board
			gOobjApplication->SaveBoard();
		} else {
			// The user is playing a game
			// TODO: call to clsGame.Save()
		}
	}
	
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_File_SaveAs
 */
void clsFrame::OnFileSaveAsClick(wxCommandEvent& WXUNUSED(event)) {
	wxString strCaption;
	wxString strWildcard;
	long lngStyles = wxFD_SAVE | wxFD_OVERWRITE_PROMPT;
	wxString strDefaultDir;
	wxString strDefaultFilename = wxFileNameFromPath(gOobjApplication->GetVstrFileName());
	wxString strTitle;
	wxFileName objFileName;
	wxString strPreviousFileName = gOobjApplication->GetVstrFileName(); // to restore the previous file name in case of a saving failure
	
	if(gOobjApplication->GetVienState() == enmStates_BoardEdition) {
		// The user is editing a board
		strCaption = _("Save board as");
		strWildcard = _("Board (*.map)|*.map|All Files (*.*)|*.*");
		strDefaultDir = mOobjOptions->GetVstrBoardsDirectory();
	} else {
		// The user is playing a game
		strCaption = _("Save game as");
		strWildcard = _("Game (*.arm)|*.arm|All Files (*.*)|*.*");
		strDefaultDir = mOobjOptions->GetVstrGamesDirectory();
	}	
	
	wxFileDialog dlgDialog(this, strCaption, strDefaultDir, strDefaultFilename, strWildcard, lngStyles);
	
	if(dlgDialog.ShowModal() == wxID_OK) {
		// The dialog was accepted
		gOobjApplication->SetVstrFileName(dlgDialog.GetPath());
		objFileName.Assign(dlgDialog.GetPath());
		
		if(gOobjApplication->GetVienState() == enmStates_BoardEdition) {
			// The user is editing a board
			if(objFileName.GetExt() == "") {
				// The user haven't specified an extension
				objFileName.SetExt(_T("map"));
			}

			gOobjApplication->SetVstrFileName(objFileName.GetFullPath());
			gOobjApplication->SaveBoard();
		
		} else {
			// The user is playing a game
			if(objFileName.GetExt() == "") {
				// The user haven't specified an extension
				objFileName.SetExt(_T("arm"));
			}

			gOobjApplication->SetVstrFileName(objFileName.GetFullPath());
			// TODO: call to clsGame.Save()
		}
	
		if(!gOobjApplication->GetVblnFileOperationFailure()) {
			// The file could be saved successfully
			// Set the title bar
			strTitle.Printf(_T("%s - [%s]"), gOobjApplication->GetVstrTitle().c_str(), gOobjApplication->GetVstrFileName().c_str());
			this->SetTitle(strTitle);
		} else {
			// There was an error
			gOobjApplication->SetVstrFileName(strPreviousFileName); // restore the previous file name
		}
	}
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Properties
 */
void clsFrame::OnBoardPropertiesClick(wxCommandEvent& WXUNUSED(event)) {
	clsPointer<clsDlgNewBoard> OdlgBoardProperties;
	int intResponse;
	wxString strBoardWidth = wxString::Format(_T("%d"), mOobjBoard->GetVintWidth());
	wxString strBoardHeight = wxString::Format(_T("%d"), mOobjBoard->GetVintHeight());
	
	OdlgBoardProperties = new clsDlgNewBoard();
	OdlgBoardProperties->Create(this);
	
	OdlgBoardProperties->SetVienDialogAction(enmDialogAction_Edit);
	OdlgBoardProperties->pTxtTitle->SetValue(mOobjBoard->GetVstrTitle());
	OdlgBoardProperties->pTxaDescription->SetValue(mOobjBoard->GetVstrDescription());
	OdlgBoardProperties->pTxtWidth->SetValue(strBoardWidth);
	OdlgBoardProperties->pTxtHeight->SetValue(strBoardHeight);
	OdlgBoardProperties->SetVintNumberOfPlayersIndex(mOobjBoard->GetVintNumberPlayers() - 2);
	
	intResponse = OdlgBoardProperties->ShowModal();
	
	if(intResponse != wxID_OK) {
		// Dialog canceled
		OdlgBoardProperties->Destroy();
		return;
	}
	
	mOobjBoard->SetvStrTitle(OdlgBoardProperties->pTxtTitle->GetValue());
	mOobjBoard->SetvStrDescription(OdlgBoardProperties->pTxaDescription->GetValue());
	
	OdlgBoardProperties->Destroy();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_File_Open_Board
 */
void clsFrame::OnFileOpenBoardClick(wxCommandEvent& WXUNUSED(event)) {
	wxString strCaption = _("Open board");
	wxString strWildcard = _("Board (*.map)|*.map|All Files (*.*)|*.*");
	long lngStyles = wxFD_OPEN | wxFD_FILE_MUST_EXIST;
	wxString strDefaultDir = mOobjOptions->GetVstrBoardsDirectory();
	wxString strTitle;
	bool blnClosePreviousDocument;

	if(!blnConfirmClosePreviousDoc(blnClosePreviousDocument)) {
		// There was a previous document open and the user didn't confirm the closing
		return;
	}

	wxFileDialog dlgDialog(this, strCaption, strDefaultDir, "", strWildcard, lngStyles);

	if(dlgDialog.ShowModal() == wxID_OK) {
		// The dialog was accepted
		if(blnClosePreviousDocument) {
			// There is a previous document that have to be closed
			gOobjApplication->ChangeStateEmpty();
		}
		
		gOobjApplication->OpenDocument(dlgDialog.GetPath());

		if(!gOobjApplication->GetVblnFileOperationFailure()) {
			// The file could be opened successfully
			gOobjApplication->SetVstrFileName(dlgDialog.GetPath());
			
			// Set the title bar
			strTitle.Printf(_T("%s - [%s]"), gOobjApplication->GetVstrTitle().c_str(), gOobjApplication->GetVstrFileName().c_str());
			this->SetTitle(strTitle);
		}
	}
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_Help_About
 */
void clsFrame::OnHelpAboutClick(wxCommandEvent& WXUNUSED(event)) {
	wxString strMessage = gOobjApplication->GetAppName();
	
	strMessage += _T("\n");
	strMessage += _("Version ") + gOobjApplication->fVstrVersion() + _(" (Alfa)");
	
	wxMessageBox(strMessage, _("About"), wxICON_INFORMATION, wxTheApp->GetTopWindow());
}
