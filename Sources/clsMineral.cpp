#include "clsMineral.h"

#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

//.......................................................................................................................
// Includes
#include "Global.h"
#include "throwmap.h"
#include "ClsApplication.h"

//.......................................................................................................................
// Definition of static members

int clsMineral::sVintInstances;
CountedPtr<wxColour> clsMineral::sSclrMapColor;
int clsMineral::sVintMaxHealth;
int clsMineral::sVintMaxCreation;
int clsMineral::sVintMaxConstruction;
wxImage clsMineral::sVimgStaticRepresentation;
wxImage clsMineral::sVimgEmptyGraphic;
wxString clsMineral::sVstrBaseDescription;
wxString clsMineral::sVstrInternalTypeName;

//_______________________________________________________________________________________________________________________

clsMineral::clsMineral() {
	NormalConstructor();
}

//_______________________________________________________________________________________________________________________

clsMineral::clsMineral(bool pVblnObjectType) {
	if(pVblnObjectType) {
		// Construction of the class Object Type
		ObjectTypeConstructor();
	} else {
		NormalConstructor();
	}
}

//_______________________________________________________________________________________________________________________

void clsMineral::NormalConstructor() {
	mVintHealth = sVintMaxHealth;
	mVintCreation = sVintMaxCreation;
	mVintConstruction = sVintMaxConstruction;
	mVimgDynamicRepresentation = this->GetVimgStaticRepresentation(); // at the beginning, the representation must be equal to the static one (a non empty resource)
}

//_______________________________________________________________________________________________________________________

void clsMineral::ObjectTypeConstructor() {
	mVstrName = _T("Mineral");
	sVstrInternalTypeName = mVstrName; // to share the base type among all the specific child objects
	sVstrBaseDescription = _("Mineral");

	sSclrMapColor = Make_CP(new wxColor(117, 76, 36));
	sVintMaxHealth = 10;
	sVintMaxCreation = 0;
	sVintMaxConstruction = 30;
	
	this->SetOffsetImage(wxPoint(80, 120));
	this->SetOffsetImageEmpty(wxPoint(280, 120));
}

//_______________________________________________________________________________________________________________________

clsMineral::~clsMineral()
{

}

//_______________________________________________________________________________________________________________________

void clsMineral::ResetCounter() {
	sVintInstances = 0;
}

//_______________________________________________________________________________________________________________________

clsPointer<clsNaturalResource> clsMineral::OobjCreateNew() {
	clsPointer<clsMineral> OobjNewInstance = new clsMineral();
	wxString strInstanceNumber;
	
	sVintInstances++;
	strInstanceNumber = wxString::Format(_T("%d"), sVintInstances);

	OobjNewInstance->SetVstrName(mVstrName + strInstanceNumber);
	OobjNewInstance->SetVstrDescription(sVstrBaseDescription + " " + strInstanceNumber);
	
	// Adds the new object on the corresponding collection
	safemap::insert(gOobjApplication->GetVdctNaturalResources(), OobjNewInstance->GetVstrName(), Make_CP(OobjNewInstance.get()));
	safemap::insert(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNewInstance->GetVstrName(), OobjNewInstance);
	
	return(OobjNewInstance);
}

//_______________________________________________________________________________________________________________________

clsPointer<clsNaturalResource> clsMineral::OobjCreateFromFile(c4_RowRef pVrowRow) {
	clsPointer<clsMineral> OobjNewInstance = new clsMineral();
	
	OobjNewInstance->Load(pVrowRow);
	
	// Adds the new object on the corresponding collection
	safemap::insert(gOobjApplication->GetVdctNaturalResources(), OobjNewInstance->GetVstrName(), Make_CP(OobjNewInstance.get()));
	safemap::insert(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNewInstance->GetVstrName(), OobjNewInstance);
	
	return(OobjNewInstance);
}

//_______________________________________________________________________________________________________________________

void clsMineral::ShowContent(clsPointer<wxTreeCtrl> pOtrcContent) {
	// TODO: maybe the best solution here is to hide the wxTreeCtrl when a non container object is selected
}
