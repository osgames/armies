// ***********************************
// Copyright 1999 John R. Bandela
// This work may be freely used, copied, distributed, and extended, provided
// the copyright notice above and this statement are maintained at the top of the code

#include "stdafx.h"

#include <assert.h>
#include "jrb_ptr.h"
namespace jrb_sptr{	
	BackRefManager& GetBackRefManager(){
		static BackRefManager Man;
		return Man;
	}
	JRB_PTR_NULL CP_NULL;
}
