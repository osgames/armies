/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// The majority of the enumerations will be included here  to avoid the cross dependencies that takes place when these
// enumerations are declared as part of the public interface of the classes that they logically belong to.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _ENUMERATIONS_H_
#define _ENUMERATIONS_H_

enum enmStates
{
    enmStates_Empty,
	enmStates_Playing,
	enmStates_BoardEdition,
	enmStates_PlayingEMail
};

// Actions that the user can select in board edition or while playing a game
enum enmActionSelected
{
	enmActionSelected_Empty = 0,
	
	// Specific actions in board edition mode 
	enmActionSelected_Eraser,
	enmActionSelected_InsertSurface,
	enmActionSelected_InsertNaturalResource,

	// Actions in game mode
};

enum enmBaseType {
	enmBaseType_Empty = -1,
	enmBaseType_GrassLand = 0,
	enmBaseType_Water,
	enmBaseType_Coast,
	enmBaseType_Mount,
	enmBaseType_Mountain
};

enum enmBoardDirection {
	enmBoardDirection_TopLeft = 0,
	enmBoardDirection_Top = 1,
	enmBoardDirection_TopRight = 2,
	enmBoardDirection_Left = 3,
	enmBoardDirection_Right = 4,
	enmBoardDirection_BottomLeft = 5,
	enmBoardDirection_Bottom = 6,
	enmBoardDirection_BottomRight = 7
};

enum enmDialogAction {
	enmDialogAction_New = 0,
	enmDialogAction_Edit,
	enmDialogAction_View
};

#endif // _ENUMERATIONS_H_
