/////////////////////////////////////////////////////////////////////////////
// Name:        clsDlgNewBoard.cpp
// Purpose:     
// Author:      Germ�n Blando
// Modified by: 
// Created:     05/29/05 08:05:02
// RCS-ID:      
// Copyright:   Interfase
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "clsDlgNewBoard.h"

#include "clsLibrary.h"

////@begin XPM images
////@end XPM images

/*!
 * clsDlgNewBoard type definition
 */

IMPLEMENT_DYNAMIC_CLASS( clsDlgNewBoard, wxDialog )

/*!
 * clsDlgNewBoard event table definition
 */

BEGIN_EVENT_TABLE( clsDlgNewBoard, wxDialog )

////@begin clsDlgNewBoard event table entries
 EVT_COMBOBOX( ID_cmbNumberrOfPlayers, clsDlgNewBoard::OnCmbNumberrOfPlayersSelected )

 EVT_BUTTON( wxID_OK, clsDlgNewBoard::OnOk )

////@end clsDlgNewBoard event table entries
	
	EVT_INIT_DIALOG(clsDlgNewBoard::OnInitDialog)
END_EVENT_TABLE()

//_______________________________________________________________________________________________________________________

/*!
 * clsDlgNewBoard constructors
 */
clsDlgNewBoard::clsDlgNewBoard( ) {
	mVienDialogAction = enmDialogAction_New;
}

//_______________________________________________________________________________________________________________________

clsDlgNewBoard::clsDlgNewBoard( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style ) {
	mVienDialogAction = enmDialogAction_New;

    Create(parent, id, caption, pos, size, style);
}

//_______________________________________________________________________________________________________________________

/*!
 * clsDlgNewBoard creator
 */
bool clsDlgNewBoard::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin clsDlgNewBoard member initialisation
 pTxtTitle = NULL;
 pCmbNumberOfPlayers = NULL;
 pTxtWidth = NULL;
 pTxtHeight = NULL;
 pTxaDescription = NULL;
////@end clsDlgNewBoard member initialisation

////@begin clsDlgNewBoard creation
 SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
 wxDialog::Create( parent, id, caption, pos, size, style );

 CreateControls();
 if (GetSizer())
 {
  GetSizer()->SetSizeHints(this);
 }
 Centre();
////@end clsDlgNewBoard creation
    
	return TRUE;
}

//_______________________________________________________________________________________________________________________

/*!
 * Control creation for clsDlgNewBoard
 */
void clsDlgNewBoard::CreateControls()
{    
////@begin clsDlgNewBoard content construction
 clsDlgNewBoard* itemDialog1 = this;

 wxFlexGridSizer* itemFlexGridSizer2 = new wxFlexGridSizer(5, 1, 0, 0);
 itemFlexGridSizer2->AddGrowableRow(2);
 itemFlexGridSizer2->AddGrowableCol(0);
 itemDialog1->SetSizer(itemFlexGridSizer2);

 wxFlexGridSizer* itemFlexGridSizer3 = new wxFlexGridSizer(2, 2, 0, 0);
 itemFlexGridSizer2->Add(itemFlexGridSizer3, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

 wxStaticText* itemStaticText4 = new wxStaticText( itemDialog1, wxID_STATIC, _("&Title:"), wxDefaultPosition, wxDefaultSize, 0 );
 itemFlexGridSizer3->Add(itemStaticText4, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

 pTxtTitle = new wxTextCtrl( itemDialog1, ID_txtTitle, _T(""), wxDefaultPosition, wxSize(209, -1), 0 );
 itemFlexGridSizer3->Add(pTxtTitle, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

 wxStaticText* itemStaticText6 = new wxStaticText( itemDialog1, wxID_STATIC, _("&Number of Players:"), wxDefaultPosition, wxDefaultSize, 0 );
 itemFlexGridSizer3->Add(itemStaticText6, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

 wxArrayString pCmbNumberOfPlayersStrings;
 pCmbNumberOfPlayers = new wxComboBox( itemDialog1, ID_cmbNumberrOfPlayers, _T(""), wxDefaultPosition, wxSize(209, -1), pCmbNumberOfPlayersStrings, wxCB_READONLY );
 itemFlexGridSizer3->Add(pCmbNumberOfPlayers, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

 wxStaticBox* itemStaticBoxSizer8Static = new wxStaticBox(itemDialog1, wxID_ANY, _("Size"));
 wxStaticBoxSizer* itemStaticBoxSizer8 = new wxStaticBoxSizer(itemStaticBoxSizer8Static, wxHORIZONTAL);
 itemFlexGridSizer2->Add(itemStaticBoxSizer8, 0, wxGROW|wxALIGN_CENTER_VERTICAL|wxALL, 5);

 itemStaticBoxSizer8->Add(5, 5, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);

 wxStaticText* itemStaticText10 = new wxStaticText( itemDialog1, wxID_STATIC, _("&Width:"), wxDefaultPosition, wxDefaultSize, 0 );
 itemStaticBoxSizer8->Add(itemStaticText10, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

 pTxtWidth = new wxTextCtrl( itemDialog1, ID_txtWidth, _T(""), wxDefaultPosition, wxSize(81, -1), 0 );
 itemStaticBoxSizer8->Add(pTxtWidth, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

 itemStaticBoxSizer8->Add(5, 5, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);

 wxStaticText* itemStaticText13 = new wxStaticText( itemDialog1, wxID_STATIC, _("&Height:"), wxDefaultPosition, wxDefaultSize, 0 );
 itemStaticBoxSizer8->Add(itemStaticText13, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

 pTxtHeight = new wxTextCtrl( itemDialog1, ID_txtHeight, _T(""), wxDefaultPosition, wxSize(81, -1), 0 );
 itemStaticBoxSizer8->Add(pTxtHeight, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

 itemStaticBoxSizer8->Add(5, 5, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);

 wxFlexGridSizer* itemFlexGridSizer16 = new wxFlexGridSizer(2, 1, 0, 0);
 itemFlexGridSizer16->AddGrowableRow(1);
 itemFlexGridSizer16->AddGrowableCol(0);
 itemFlexGridSizer2->Add(itemFlexGridSizer16, 0, wxGROW|wxGROW|wxALL, 5);

 wxStaticText* itemStaticText17 = new wxStaticText( itemDialog1, wxID_STATIC, _("&Description:"), wxDefaultPosition, wxDefaultSize, 0 );
 itemFlexGridSizer16->Add(itemStaticText17, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

 pTxaDescription = new wxTextCtrl( itemDialog1, ID_txaDescription, _T(""), wxDefaultPosition, wxSize(-1, 100), wxTE_MULTILINE );
 itemFlexGridSizer16->Add(pTxaDescription, 1, wxGROW|wxGROW|wxALL, 5);

 wxBoxSizer* itemBoxSizer19 = new wxBoxSizer(wxHORIZONTAL);
 itemFlexGridSizer2->Add(itemBoxSizer19, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

 wxButton* itemButton20 = new wxButton( itemDialog1, wxID_OK, _("&OK"), wxDefaultPosition, wxDefaultSize, 0 );
 itemButton20->SetDefault();
 itemBoxSizer19->Add(itemButton20, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

 wxButton* itemButton21 = new wxButton( itemDialog1, wxID_CANCEL, _("&Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
 itemBoxSizer19->Add(itemButton21, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

////@end clsDlgNewBoard content construction
}

//_______________________________________________________________________________________________________________________

/*!
 * Should we show tooltips?
 */
bool clsDlgNewBoard::ShowToolTips()
{
    return TRUE;
}

//_______________________________________________________________________________________________________________________
/*!
 * Get bitmap resources
 */

wxBitmap clsDlgNewBoard::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin clsDlgNewBoard bitmap retrieval
 wxUnusedVar(name);
 return wxNullBitmap;
////@end clsDlgNewBoard bitmap retrieval
}

//_______________________________________________________________________________________________________________________

/*!
 * Get icon resources
 */
wxIcon clsDlgNewBoard::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin clsDlgNewBoard icon retrieval
 wxUnusedVar(name);
 return wxNullIcon;
////@end clsDlgNewBoard icon retrieval
}

//_______________________________________________________________________________________________________________________

void clsDlgNewBoard::ArrangeDialog() {
	wxTextValidator vldNumberValidator(wxFILTER_INCLUDE_CHAR_LIST, &mVstrDummyDataTransfer);
	wxArrayString arsIncludeStrings;
	
	// Items for combo players
	this->pCmbNumberOfPlayers->Append(_T("2"));
	this->pCmbNumberOfPlayers->SetClientObject(this->pCmbNumberOfPlayers->GetCount() - 1, new wxStringClientData("2"));
	
	this->pCmbNumberOfPlayers->Append(_T("3"));
	this->pCmbNumberOfPlayers->SetClientObject(this->pCmbNumberOfPlayers->GetCount() - 1, new wxStringClientData("3"));

	this->pCmbNumberOfPlayers->Append(_T("4"));
	this->pCmbNumberOfPlayers->SetClientObject(this->pCmbNumberOfPlayers->GetCount() - 1, new wxStringClientData("4"));

	this->pCmbNumberOfPlayers->Append(_T("5"));
	this->pCmbNumberOfPlayers->SetClientObject(this->pCmbNumberOfPlayers->GetCount() - 1, new wxStringClientData("5"));
	
	this->pCmbNumberOfPlayers->Append(_T("6"));
	this->pCmbNumberOfPlayers->SetClientObject(this->pCmbNumberOfPlayers->GetCount() - 1, new wxStringClientData("6"));
	
	// Set validators
	arsIncludeStrings.Add(_T("0"));
	arsIncludeStrings.Add(_T("1"));
	arsIncludeStrings.Add(_T("2"));
	arsIncludeStrings.Add(_T("3"));
	arsIncludeStrings.Add(_T("4"));
	arsIncludeStrings.Add(_T("5"));
	arsIncludeStrings.Add(_T("6"));
	arsIncludeStrings.Add(_T("7"));
	arsIncludeStrings.Add(_T("8"));
	arsIncludeStrings.Add(_T("9"));

	vldNumberValidator.SetIncludes(arsIncludeStrings);
	
	// Set controls
	this->pTxtWidth->SetValidator(vldNumberValidator);
	this->pTxtWidth->SetMaxLength(3);
	this->pTxtWidth->SetEditable(false);
	this->pTxtWidth->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNFACE));
	
	this->pTxtHeight->SetValidator(vldNumberValidator);
	this->pTxtHeight->SetMaxLength(3);
	this->pTxtHeight->SetEditable(false);
	this->pTxtHeight->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNFACE));

	this->pTxtTitle->SetMaxLength(40);

	// Arrange the dialog state according to the molality
	if(mVienDialogAction == enmDialogAction_Edit) {
		// The user is modifying the board properties
		// Width and height can't be modified
		this->pCmbNumberOfPlayers->SetSelection(mVintNumberOfPlayersIndex);
		this->pCmbNumberOfPlayers->Enable(false);
	} else {
		// The user is creating a new board
		this->pCmbNumberOfPlayers->SetSelection(0);
		this->OnCmbNumberrOfPlayersSelected(*(wxCommandEvent*) NULL); // for refresh the fields Width and Height
	}
	
	this->SetFocus(); // for some reason, in Linux the focus is not set to the dialog at the begining
}

//_______________________________________________________________________________________________________________________

// Intentionally, this event doesn't call to "wxWindow::TransferDataToWindow"
// Data transfer services are not used.
//
void clsDlgNewBoard::OnInitDialog(wxInitDialogEvent& WXUNUSED(event))
{
	ArrangeDialog();
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_COMBOBOX_SELECTED event handler for ID_cmbNumberrOfPlayers
 */
void clsDlgNewBoard::OnCmbNumberrOfPlayersSelected(wxCommandEvent& WXUNUSED(event))
{
    int intSelection = this->pCmbNumberOfPlayers->GetSelection();

	if(intSelection == 0) {
		// 2 players
		this->pTxtWidth->SetValue(_T("25"));
		this->pTxtHeight->SetValue(_T("25"));
	
	} else if(intSelection == 1) {
		// 3 players
		this->pTxtWidth->SetValue(_T("35"));
		this->pTxtHeight->SetValue(_T("35"));
	
	} else if(intSelection == 2) {
		// 4 players
		this->pTxtWidth->SetValue(_T("50"));
		this->pTxtHeight->SetValue(_T("50"));
	
	} else if(intSelection == 3) {
		// 5 players
		this->pTxtWidth->SetValue(_T("75"));
		this->pTxtHeight->SetValue(_T("75"));
	
	} else if(intSelection == 4) {
		// 6 players
		this->pTxtWidth->SetValue(_T("100"));
		this->pTxtHeight->SetValue(_T("100"));
	}
}

//_______________________________________________________________________________________________________________________

bool clsDlgNewBoard::blnValid() {
	if(!clsLibrary::blnFieldCompleted(this->pTxtTitle, _("Title"))) {
		return(false);
	}
	
	return(true);
}

//_______________________________________________________________________________________________________________________

/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK
 */

// Intentionally, this event doesn't call to "wxWindow::TransferDataFromWindow"
// Data transfer services are not used.

void clsDlgNewBoard::OnOk( wxCommandEvent& WXUNUSED(event) )
{
    // Standard validation (using Validators)
	if(!Validate()) {
		return;
    }

	// Custom validation
	if(!blnValid()) {
		return;
	}

    if ( IsModal() )
        EndModal(wxID_OK);
    else
    {
        SetReturnCode(wxID_OK);
        this->Show(false);
	}
}
