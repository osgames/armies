#include "clsfixedselection.h"

#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

/*!
* Includes
*/
#include "Global.h"
#include "ClsApplication.h"
#include "clsFrame.h"
#include "clsImageList.h"

/*!
* Definition of static variables
*/
wxImage clsFixedSelection::mVimgGraphic;
wxIcon clsFixedSelection::mVicnIcon;
clsPointer<clsBoard> clsFixedSelection::mOobjBoard;
clsPointer<clsCanvas> clsFixedSelection::mOobjCanvas;

//_____________________________________________________________________________________________________________________

clsFixedSelection::clsFixedSelection()
	: mVintRow(-1)
	, mVintColumn(-1)
	, mVintPrevRow(-1)
	, mVintPrevColumn(-1)
{
}

//_____________________________________________________________________________________________________________________

clsFixedSelection::~clsFixedSelection()
{
}

//_____________________________________________________________________________________________________________________

void clsFixedSelection::Draw() {
	wxRect rctArea;  // area used by the square
	wxRect rctAreaToUpdate; // area that must be updated. May be different from "rctArea" if the square is not completely showed in the screen.
	wxPoint pntOffset;
	
	rctArea = mOobjBoard->rctSquareArea(mVintRow, mVintColumn);
	rctAreaToUpdate = mOobjCanvas->rctUpdatableArea(rctArea);
	
	if(rctAreaToUpdate.IsEmpty()) {
		// It's not necessary to draw, because the update take place outside the visible region in the screen
		return;
	}
	
	// Calculates the offsets for the case in that the square doesn't have to draw entirely
	// Only calculates the left and upper offsets and not the right and bottom ones, because they still simply excluded after coping the secondary screen in the primary (as long as the secondary screen would had enough extra pixeles to support the excess).
	pntOffset.x = rctAreaToUpdate.GetLeft() - rctArea.GetLeft();
	pntOffset.y = rctAreaToUpdate.GetTop() - rctArea.GetTop();
	
	mOobjCanvas->DrawSecondary(mVimgGraphic, rctAreaToUpdate, pntOffset);
}

//_____________________________________________________________________________________________________________________

void clsFixedSelection::Erase() {
	mVintPrevColumn = mVintColumn;
	mVintPrevRow = mVintRow;

	mVintColumn = -1;
	mVintRow = -1;

	//gOobjApplication->GetPobjFrame()->GetPobjStatusBar()->SetActionIcon(wxNullIcon);
	gOobjApplication->GetPobjFrame()->GetPobjStatusBar()->HideActionIcon(); // it must be hidden rather than set to wxNullIcon because under GTK that method doesn't work properly

	// Refresh the square containing the fixed selection
	if(gOobjApplication->GetVienState() != enmStates_Empty) {
		// The board is active
		mOobjBoard->Draw(mVintPrevRow, mVintPrevColumn);
	}
}

//_____________________________________________________________________________________________________________________

void clsFixedSelection::LoadGraphics() {
	LoadGraphic();
	LoadIcon();
}

//_____________________________________________________________________________________________________________________

void clsFixedSelection::SetShortcuts() {
	
	mOobjBoard = gOobjApplication->GetSobjBoard();
	mOobjCanvas = gOobjApplication->GetPobjFrame()->GetpObjCanvas();
}

//_____________________________________________________________________________________________________________________

void clsFixedSelection::LoadGraphic() {
	int intSquareLength = mOobjBoard->GetVintSQUARE_LENGTH();
	wxBitmap bmpSubBitmap;
	wxRect rctSource;
	wxColor clrColor = gOobjApplication->GetVclrTransparentBackground();
	
	rctSource.SetTop(320);
	rctSource.SetLeft(0);
	rctSource.SetWidth(intSquareLength);
	rctSource.SetHeight(intSquareLength);
	
	bmpSubBitmap = gOobjApplication->GetSbmpGraphics()->GetSubBitmap(rctSource);
	mVimgGraphic = bmpSubBitmap.ConvertToImage();
	mVimgGraphic.SetMaskColour(clrColor.Red(), clrColor.Green(), clrColor.Blue());
}

//_____________________________________________________________________________________________________________________

void clsFixedSelection::Set(int pVintColumn, int pVintRow) {
	mVintColumn = pVintColumn;
	mVintRow = pVintRow;
	
	gOobjApplication->GetPobjFrame()->GetPobjStatusBar()->SetActionIcon(mVicnIcon);
	gOobjApplication->GetPobjFrame()->GetPobjStatusBar()->ShowActionIcon();
	
	mOobjBoard->Draw(pVintRow, pVintColumn);
}

//_____________________________________________________________________________________________________________________

void clsFixedSelection::LoadIcon() {
	mVicnIcon = gOobjApplication->GetOimlImageList_16()->GetIcon(_T("FixedSelection"));
}

//_____________________________________________________________________________________________________________________

void clsFixedSelection::Restore() {
	this->Set(mVintPrevColumn, mVintPrevRow);
}
