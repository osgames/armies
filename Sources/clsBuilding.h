// clsBuilding.h: interface for the clsBuilding class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLSBUILDING_H__F50F8AB4_87F3_43B2_B93C_1388070FC1A9__INCLUDED_)
#define AFX_CLSBUILDING_H__F50F8AB4_87F3_43B2_B93C_1388070FC1A9__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

#include "clsUserObject.h"

// -----------------------------------------------------------------------------------------------------------------------
class clsBuilding : public clsUserObject
{
public:
	clsBuilding();
	virtual ~clsBuilding();

	//....................................................................................................................
	// Methods

	// Resets the counter of a specific class.
	// This method is created to be specialized in the derived classes
	virtual void ResetCounter() {};

// -----------------------------------------------------------------------------------------------------------------------
private:

};

#endif // !defined(AFX_CLSBUILDING_H__F50F8AB4_87F3_43B2_B93C_1388070FC1A9__INCLUDED_)
