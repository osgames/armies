// clsMobileUnit.h: interface for the clsMobileUnit class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLSMOBILEUNIT_H__8C1F525D_5189_4AFF_B0CC_E5283E1B8947__INCLUDED_)
#define AFX_CLSMOBILEUNIT_H__8C1F525D_5189_4AFF_B0CC_E5283E1B8947__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

#include "clsUserObject.h"

// -----------------------------------------------------------------------------------------------------------------------

class clsMobileUnit : public clsUserObject
{
public:
	clsMobileUnit();
	virtual ~clsMobileUnit();

	//....................................................................................................................
	// Methods

	// Resets the counter of a specific class.
	// This method is created to be specialized in the derived classes
	virtual void ResetCounter() {};

// -----------------------------------------------------------------------------------------------------------------------
private:
};

#endif // !defined(AFX_CLSMOBILEUNIT_H__8C1F525D_5189_4AFF_B0CC_E5283E1B8947__INCLUDED_)
