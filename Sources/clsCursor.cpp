// clsCursor.cpp: implementation of the clsCursor class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

/*!
* Includes
*/
#include "clsCursor.h"
#include "Global.h"
#include "ClsApplication.h"
#include "clsFrame.h"
#include <wx/image.h>
#include "clsBoard.h"

/*!
* Definition of static variables
*/
const int clsCursor::mVintSlidesCount(10);
Array<wxImage> clsCursor::mVarrSlides(clsCursor::mVintSlidesCount);
clsPointer<clsBoard> clsCursor::mOobjBoard;
clsPointer<clsCanvas> clsCursor::mOobjCanvas;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------------------------------------------------

clsCursor::clsCursor()
{
	mVintSequence = 0;
	mVintColumn = 0;
	mVintRow = 0;
	
	mOobjFrame = gOobjApplication->GetPobjFrame();
}

//-----------------------------------------------------------------------------------------------------------------------

clsCursor::~clsCursor()
{

}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::Draw() {
	wxRect rctArea;  // area used by the square
	wxRect rctAreaToUpdate; // area that must be updated. May be different from "rctArea" if the square is not completely showed in the screen.
	wxPoint pntOffset;

	rctArea = mOobjBoard->rctSquareArea(mVintRow, mVintColumn);
	rctAreaToUpdate = mOobjCanvas->rctUpdatableArea(rctArea);

	if(rctAreaToUpdate.IsEmpty()) {
		// It's not necessary to draw, because the update take place outside the visible region in the screen
		return;
	}
	
	// Calculates the offsets for the case in that the square doesn't have to draw entirely
	// Only calculates the left and upper offsets and not the right and bottom ones, because they still simply excluded after coping the secondary screen in the primary (as long as the secondary screen would had enough extra pixeles to support the excess).
	pntOffset.x = rctAreaToUpdate.GetLeft() - rctArea.GetLeft();
	pntOffset.y = rctAreaToUpdate.GetTop() - rctArea.GetTop();

	mOobjCanvas->DrawSecondary(mVarrSlides[mVintSequence], rctAreaToUpdate, pntOffset);

	AdvanceSecuence();
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::AdvanceSecuence() {
	mVintSequence++;
	
	if(mVintSequence == mVintSlidesCount) {
		// It was reached the end of the sequence
		mVintSequence = 0;
	}
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::LoadGraphics() {
	int intImageIndex = 0;
	wxRect rctSource;
	int i;
	wxBitmap bmpTemporal;
	wxColor clrColor = gOobjApplication->GetVclrTransparentBackground();
	
	rctSource.SetTop(40);
	rctSource.SetBottom(80);

	for(i = 0; i < 361; i += 40) {
		// Process all cursor states
		rctSource.SetLeft(i);
		rctSource.SetRight(i + mOobjBoard->GetVintSQUARE_LENGTH());

		bmpTemporal = gOobjApplication->GetSbmpGraphics()->GetSubBitmap(rctSource);
		mVarrSlides[intImageIndex] = bmpTemporal.ConvertToImage();
		mVarrSlides[intImageIndex].SetMaskColour(clrColor.Red(), clrColor.Green(), clrColor.Blue());
		
		intImageIndex++;
	}
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::SetShortcuts() {
	
	mOobjBoard = gOobjApplication->GetSobjBoard();
	mOobjCanvas = gOobjApplication->GetPobjFrame()->GetpObjCanvas();
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::MoveUp() {
	int intPrevRow;

	if(mVintRow == 0) {
		// The cursor is at the top of the board
		return;
	}
	
	intPrevRow = mVintRow;
	mVintRow--;
	
	PostProcessMovementCursor(intPrevRow, mVintColumn); // to erase the previous position of the cursor
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::MoveDown() {
	int intPrevRow;
	
	if(mVintRow == mOobjBoard->GetVintHeight() - 1) {
		// The cursor is at the bottom of the board
		return;
	}
	
	intPrevRow = mVintRow;
	mVintRow++;
	
	PostProcessMovementCursor(intPrevRow, mVintColumn); // to erase the previous position of the cursor
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::MoveLeft() {
	int intPrevCol;

	if(mVintColumn == 0) {
		// The cursor is on the left of the board
		return;
	}
	
	intPrevCol = mVintColumn;
	mVintColumn--;
	
	PostProcessMovementCursor(mVintRow, intPrevCol); // to erase the previous position of the cursor
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::MoveRight() {
	int intPrevCol;
	
	if(mVintColumn == mOobjBoard->GetVintHeight() - 1) {
		// The cursor is on the right of the board
		return;
	}
	
	intPrevCol = mVintColumn;
	mVintColumn++;
	
	PostProcessMovementCursor(mVintRow, intPrevCol); // to erase the previous position of the cursor
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::MovePageUp() {
	int intPrevRow;
	int intHorizontalScrollSquares; // the amount of displacement for the long scroll, in square unit
	
	if(mVintRow == 0) {
		// The cursor is at the top of the board
		return;
	}

	intPrevRow = mVintRow;
	intHorizontalScrollSquares = mOobjBoard->intPixel2Square(mOobjCanvas->GetVintVerticalLongScroll());
	mVintRow -= intHorizontalScrollSquares;

	if(mVintRow < 0) {
		// The top of the board was overtaken
		mVintRow = 0;
	}
	
	PostProcessMovementCursor(intPrevRow, mVintColumn); // to erase the previous position of the cursor
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::MovePageDown() {
	int intPrevRow;
	int intHorizontalScrollSquares; // the amount of displacement for the long scroll, in square unit
	int intLimit = mOobjBoard->GetVintHeight() - 1;
	
	if(mVintRow == intLimit) {
		// The cursor is at the bottom of the board
		return;
	}
	
	intPrevRow = mVintRow;
	intHorizontalScrollSquares = mOobjBoard->intPixel2Square(mOobjCanvas->GetVintVerticalLongScroll());
	mVintRow += intHorizontalScrollSquares;

	if(mVintRow > intLimit) {
		// The bottom of the board was overtaken
		mVintRow = intLimit;
	}

	PostProcessMovementCursor(intPrevRow, mVintColumn); // to erase the previous position of the cursor
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::MovePageLeft() {
	int intPrevColumn;
	int intVerticalScrollSquares; // the amount of displacement for the long scroll, in square unit
	
	if(mVintColumn == 0) {
		// The cursor is on the left of the board
		return;
	}
	
	intPrevColumn = mVintColumn;
	intVerticalScrollSquares = mOobjBoard->intPixel2Square(mOobjCanvas->GetVintVerticalLongScroll());
	mVintColumn -= intVerticalScrollSquares;
	
	if(mVintColumn < 0) {
		// The top of the board was overtaken
		mVintColumn = 0;
	}
	
	PostProcessMovementCursor(mVintRow, intPrevColumn); // to erase the previous position of the cursor
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::MovePageRight() {
	int intPrevColumn;
	int intVerticalScrollSquares; // the amount of displacement for the long scroll, in square unit
	int intLimit = mOobjBoard->GetVintHeight() - 1;
	
	if(mVintColumn == intLimit) {
		// The cursor is on the right of the board
		return;
	}
	
	intPrevColumn = mVintColumn;
	intVerticalScrollSquares = mOobjBoard->intPixel2Square(mOobjCanvas->GetVintVerticalLongScroll());
	mVintColumn += intVerticalScrollSquares;
	
	if(mVintColumn > intLimit) {
		// The bottom of the board was overtaken
		mVintColumn = intLimit;
	}
	
	PostProcessMovementCursor(mVintRow, intPrevColumn); // to erase the previous position of the cursor
}

//-----------------------------------------------------------------------------------------------------------------------

void clsCursor::PostProcessMovementCursor(int pVintPrevRowPosition, int pVintPrevColumnPosition) {
	mOobjBoard->Draw(pVintPrevRowPosition, pVintPrevColumnPosition); // to erase the previous position of the cursor
	mOobjBoard->ScrollWithSelection();
	mOobjFrame->UpdateEnvironment();
	mOobjBoard->UpdateCoordinates();
}

//---------------------------------------------------------------------------------------------------------------------

void clsCursor::SelectSquareFromPixel(int pVintPixelX, int pVintPixelY) {
	int intPrevRowSelected;
	int intPrevColumnSelected;
	wxPoint pntSquare = mOobjBoard->fVrctGetSquareFromPixel(pVintPixelX, pVintPixelY);
	int intRowClicked = pntSquare.y; // the row clicked, in square units
	int intColumClicked = pntSquare.x; // the column clicked, in square units
	
	if(mVintRow != intRowClicked || mVintColumn != intColumClicked) {
		// The selected square has been changed
		intPrevRowSelected = mVintRow;
		intPrevColumnSelected = mVintColumn;
		
		mVintRow = intRowClicked;
		mVintColumn = intColumClicked;

		PostProcessMovementCursor(intPrevRowSelected, intPrevColumnSelected);
	}
}
