/////////////////////////////////////////////////////////////////////////////
// Name:        ClsApplication.cpp
// Purpose:		Root object
// Author:		Germ�n Blando
// Modified by:
// Created:		2006-03-22
// RCS-ID:
// Copyright:	(C)2005
// License:     wxWindows
/////////////////////////////////////////////////////////////////////////////

// Enable only to debug memory leak problems
//#include <vld.h>

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------
#include <wx/image.h>
#include <wx/file.h>

#include "ClsApplication.h"
#include "clsFrame.h"

#ifdef _MSC_VER
#include "resource.h"
#endif

#include <wx/textfile.h>
#include <wx/stdpaths.h>
#include <wx/filename.h>

#include "clsBoard.h"
#include "clsColor.h"
#include "Global.h"
#include "clsOptions.h"
#include "clsTerrain.h"
#include "clsNaturalResource.h"
#include "clsBuilding.h"
#include "clsMobileUnit.h"
#include "clsLibrary.h"
#include "clsCursor.h"
#include "clsfixedselection.h"
#include "clsImageList.h"
#include "clsTree.h"
#include "clsMineral.h"
#include "clsVegetables.h"
#include "clsFish.h"
#include "clsOil.h"

// XPM images
#include "../Graficos Desarrollo/Iconos/16x16/fixed_selection.xpm"

// Create a new application object: this macro will allow wxWindows to create
// the application object during program execution (it's better than using a
// static object for many reasons) and also declares the accessor function
// wxGetApp() which will return the reference of the right type (i.e. ClsApplication and
// not wxApp)
IMPLEMENT_APP(clsApplication)

/*!
 * clsApplication type definition
 */

IMPLEMENT_CLASS( clsApplication, wxApp )

/*!
 * clsApplication event table definition
 */

BEGIN_EVENT_TABLE( clsApplication, wxApp )
END_EVENT_TABLE()

// ============================================================================
// implementation
// ============================================================================

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------

clsApplication::clsApplication()
	// Constants
	: mVstrLOCALE_FOLDER(_T("Locale"))
	, mVclrTransparentBackground(0, 131, 231)
	, mVclrTransparentPlayer(131, 231, 0)
	, mVstrBOARD_FILE_TYPE(_T("BOARD"))
	, mVstrGAME_FILE_TYPE(_T("GAME"))

	// Variables
	, mVstrTitle(_T("Armies")) // the title is not translated
	, mVarrStatesLabels(4)

	// Metakit properties
	
	, mVprpStrType(_T("strType"))
	, mVprpIntFileVersion(_T("intFileVersion"))
	, mVprpStrName(_T("strName"))
	, mVprpStrInternalTypeName(_T("strInternalTypeName"))
{
	// Set the global variable to get the root object from anywhere
	gOobjApplication = this;

	mVienState = enmStates_Empty;

	mSimlImageList_16 = Make_CP(new clsImageList(16, 16));
}

//_______________________________________________________________________________________________________________________

clsApplication::~clsApplication()
{
}

//_______________________________________________________________________________________________________________________

void clsApplication::CreateStatesLabels() {
	mVarrStatesLabels[enmStates_Empty] = _("Inactive");
	mVarrStatesLabels[enmStates_Playing] = _("Local Game");
	mVarrStatesLabels[enmStates_BoardEdition] = _("Board Edition");
	mVarrStatesLabels[enmStates_PlayingEMail] = _("Play by eMail");
}

//_______________________________________________________________________________________________________________________

bool clsApplication::OnInit()
{
	// Debug settings
	//wxLog::SetVerbose(true);
	//wxLog::AddTraceMask("i18n");

    this->SetVendorName("Interface"); // for use in the windows' registry
	this->SetAppName(mVstrTitle);
	
	this->SetVintMajorVersion(2);
	this->SetVintMinorVersion(0);
	this->SetVintReleaseNumber(1);

	clsLibrary::RandomSeed();
	LoadHandlers(); // load image handlers
	SetPaths();
	SetInternationalInfo();
	CreateStatesLabels();

    // Create the main application window
	mPobjFrame = new clsFrame((wxFrame *) NULL, -1, mVstrTitle, wxPoint(50, 50), wxSize(400, 300), wxDEFAULT_FRAME_STYLE);

// Due to work issues
#if !defined(__WINDOWS__)
	// Unix, Mac, etc.
	mPobjFrame->Maximize(TRUE);
#else
	// Window system
#if !defined(__WXDEBUG__)
	// Release
	mPobjFrame->Maximize(TRUE);
#endif // !defined(__WXDEBUG__)
#endif // !defined(__WINDOWS__) ()
	
	mPobjFrame->Show(TRUE);
	SetTopWindow(mPobjFrame);

	CreateObjects();
	mPobjFrame->SetOobjBoard(mSobjBoard.GetPtr());

	this->ChangeStateEmpty();

	// success: wxApp::OnRun() will be called which will enter the main message
    // loop and the application will run. If we returned FALSE here, the
    // application would exit immediately.
    return TRUE;
}

//_______________________________________________________________________________________________________________________

void clsApplication::LoadHandlers() {
#if wxUSE_XPM
    wxImage::AddHandler( new wxXPMHandler );
#endif
#if wxUSE_LIBPNG
    wxImage::AddHandler( new wxPNGHandler );
#endif
#if wxUSE_LIBJPEG
    wxImage::AddHandler( new wxJPEGHandler );
#endif
#if wxUSE_GIF
    wxImage::AddHandler( new wxGIFHandler );
#endif
}

//_______________________________________________________________________________________________________________________

void clsApplication::ChangeStateEmpty() {
	mVienState = enmStates_Empty;
	EmptyCollections();
	mPobjFrame->ChangeStateEmpty();
	mSobjBoard->ChangeEmpty();
}

//_______________________________________________________________________________________________________________________

void clsApplication::EmptyCollections() {
	mVdctNaturalResources.clear();
	ResetCounters(mVdctNaturalResourcesTypes.begin(), mVdctNaturalResourcesTypes.end());

	mVdctPlayers.clear();

	mVdctBuildings.clear();
	ResetCounters(mVdctBuildingsTypes.begin(), mVdctBuildingsTypes.end());

	mVdctMovileUnits.clear();
	ResetCounters(mVdctMobileUnitesTypes.begin(), mVdctMobileUnitesTypes.end());

	mVdctExtractions.clear();
	
	mVdcoUpperLevelObjects.clear();
}

//_______________________________________________________________________________________________________________________

void clsApplication::ChangeStateBoardEdition() {
	mVienState = enmStates_BoardEdition;
	mVienActionSelected = enmActionSelected_Empty;
	mPobjFrame->ChangeStateBoardEdition();
}

//_______________________________________________________________________________________________________________________

void clsApplication::CreateObjects()
{
	LoadGraphics();
	LoadIcons();

	mSobjBoard = Make_CP(new clsBoard());

	CreateColors();
	
	CreateTerrains();
	mSobjBoard->FillArraysAutoborderRules(); // it's possible to call it only when the terrains had been created
	
	CreateNaturalResourcesTypes();

	clsCursor::SetShortcuts();
	clsCursor::LoadGraphics();

	clsFixedSelection::SetShortcuts();
	clsFixedSelection::LoadGraphics();

}

//_______________________________________________________________________________________________________________________

void clsApplication::CreateColors()
{
	CountedPtr<clsColor> SobjColor;

	SobjColor = Make_CP(new clsColor());
	SobjColor->strName->Printf("Red");
	SobjColor->strLabel->Printf(_("Red"));
	SobjColor->Set(255, 0, 0);
	safemap::insert(mVdctColors, *SobjColor->strName, SobjColor);

	SobjColor = Make_CP(new clsColor());
	SobjColor->strName->Printf("Yellow");
	SobjColor->strLabel->Printf(_("Yellow"));
	SobjColor->Set(255, 255, 0);
	safemap::insert(mVdctColors, *SobjColor->strName, SobjColor);

	SobjColor = Make_CP(new clsColor());
	SobjColor->strName->Printf("Blue");
	SobjColor->strLabel->Printf(_("Blue"));
	SobjColor->Set(0, 0, 255);
	safemap::insert(mVdctColors, *SobjColor->strName, SobjColor);

	SobjColor = Make_CP(new clsColor());
	SobjColor->strName->Printf("Green");
	SobjColor->strLabel->Printf(_("Green"));
	SobjColor->Set(0, 255, 0);
	safemap::insert(mVdctColors, *SobjColor->strName, SobjColor);

	SobjColor = Make_CP(new clsColor());
	SobjColor->strName->Printf("Magenta");
	SobjColor->strLabel->Printf(_("Magenta"));
	SobjColor->Set(255, 0, 255);
	safemap::insert(mVdctColors, *SobjColor->strName, SobjColor);

	SobjColor = Make_CP(new clsColor());
	SobjColor->strName->Printf("Cyan");
	SobjColor->strLabel->Printf(_("Cyan"));
	SobjColor->Set(0, 255, 255);
	safemap::insert(mVdctColors, *SobjColor->strName, SobjColor);
}

//_______________________________________________________________________________________________________________________

void clsApplication::CreateTerrains() {
	CountedPtr<clsTerrain> SobjTerrain;

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("Grassland"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("Grassland2"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("Water"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("Water2"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("LeftCoast"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("RightCoast"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("TopCoast"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("BottomCoast"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("TopLeftIsland"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("TopRightIsland"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("BottomLeftIsland"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("BottomRightIsland"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("TopRightLake"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("TopLeftLake"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("BottomRightLake"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("BottomLeftLake"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("Mountain"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("LeftMountain"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("RightMountain"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("TopMountain"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("BottomMountain"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("TopRightValley"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("TopLeftValley"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("BottomRightValley"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("BottomLeftValley"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("TopRightMount"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("TopLeftMount"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("BottomRightMount"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);

	SobjTerrain = Make_CP(new clsTerrain());
	SobjTerrain->SetVstrName(_T("BottomLeftMount"));
	safemap::insert(mVdctTerrainsTypes, SobjTerrain->GetVstrName(), SobjTerrain);
}

//_______________________________________________________________________________________________________________________

void clsApplication::CreateNaturalResourcesTypes() {
	CountedPtr<clsNaturalResource> SobjNaturalResourceType;
	
	SobjNaturalResourceType = Make_CP(new clsTree(/* pVblnObjectType */ true));
	safemap::insert(mVdctNaturalResourcesTypes, SobjNaturalResourceType->GetVstrName(), SobjNaturalResourceType);
	
	SobjNaturalResourceType = Make_CP(new clsMineral(/* pVblnObjectType */ true));
	safemap::insert(mVdctNaturalResourcesTypes, SobjNaturalResourceType->GetVstrName(), SobjNaturalResourceType);

	SobjNaturalResourceType = Make_CP(new clsVegetables(/* pVblnObjectType */ true));
	safemap::insert(mVdctNaturalResourcesTypes, SobjNaturalResourceType->GetVstrName(), SobjNaturalResourceType);

	SobjNaturalResourceType = Make_CP(new clsFish(/* pVblnObjectType */ true));
	safemap::insert(mVdctNaturalResourcesTypes, SobjNaturalResourceType->GetVstrName(), SobjNaturalResourceType);

	SobjNaturalResourceType = Make_CP(new clsOil(/* pVblnObjectType */ true));
	safemap::insert(mVdctNaturalResourcesTypes, SobjNaturalResourceType->GetVstrName(), SobjNaturalResourceType);
}

//_______________________________________________________________________________________________________________________

void clsApplication::SetPaths() {
	wxStandardPathsBase& RobjStandardPaths = wxStandardPaths::Get();
		
	//-------------------------------------------------------------------------------------------------------------------
	// Resources path
#if defined(__WXMSW__)
	// Windows Platform
	mVstrAppPath = wxGetCwd() + wxFileName::GetPathSeparator(); // the executable file is created in a subdirectory, but the active system directory still are the project's one
#endif

#if defined(__GNUG__)
	mVstrAppPath = RobjStandardPaths.GetDataDir() + wxFileName::GetPathSeparator();
#endif

	//-------------------------------------------------------------------------------------------------------------------
	// Documents path
	mVstrDocumentsPath = RobjStandardPaths.GetDocumentsDir() + _T("/") + mVstrTitle + _T("/");
}

//_______________________________________________________________________________________________________________________

void clsApplication::SetInternationalInfo() {
	wxString strLanguageSetting;
	wxLanguage ienLanguage = wxLANGUAGE_DEFAULT;
	wxString strCatalogName;
	wxString strCatalogNameLibrary; // translations for the wxWidget library
	wxString strLanguageCode;
	wxString strLocaleSearchPath1;
	wxString strLocaleSearchPath2;
	wxString strLocaleSearchPath3;

	strLanguageSetting = strReadConfigLanguage().Lower();

	if (strLanguageSetting == _T("default")) {
		ienLanguage = wxLANGUAGE_DEFAULT;
	} else if (strLanguageSetting == _T("spanish")) {
		ienLanguage = wxLANGUAGE_SPANISH_ARGENTINA;
	} else {
		// The set language is unknown
		ienLanguage = wxLANGUAGE_DEFAULT;
	}

	if(!mVobjLocale.Init(ienLanguage, wxLOCALE_CONV_ENCODING)) {
		wxLogWarning(_T("No se pudo inicializar el objeto wxLocale. Lenguaje seleccionado: %s"), strLanguageSetting.c_str());
	}

	strLanguageCode = mVobjLocale.GetCanonicalName().Mid(0,2);
	strCatalogName = this->GetAppName().MakeLower() + "_" + strLanguageCode;
	strCatalogNameLibrary = strLanguageCode;

	// Add the full name language path (xx_YY) ej. "es_AR"
	strLocaleSearchPath1 = mVstrAppPath + mVstrLOCALE_FOLDER + wxFileName::GetPathSeparator() + mVobjLocale.GetCanonicalName();
	mVobjLocale.AddCatalogLookupPathPrefix(strLocaleSearchPath1);

	// Add the code language only path (xx) ej. "es"
	strLocaleSearchPath2 = mVstrAppPath + mVstrLOCALE_FOLDER + wxFileName::GetPathSeparator() + strLanguageCode;
	mVobjLocale.AddCatalogLookupPathPrefix(strLocaleSearchPath2);

	// Add the path to the folder containing all the locale subdirectories
	strLocaleSearchPath3 = mVstrAppPath + mVstrLOCALE_FOLDER;
	mVobjLocale.AddCatalogLookupPathPrefix(strLocaleSearchPath3);

	if (!mVobjLocale.AddCatalog(strCatalogName)) {
		wxString strError = _T("No se pudo cargar el catalogo: %s");
		strError += _T("\n Locale Search Path 1: %s");
		strError += _T("\n Locale Search Path 2: %s");
		strError += _T("\n Locale Search Path 3: %s");
		wxLogWarning(strError, strCatalogName.c_str(), strLocaleSearchPath1.c_str(), strLocaleSearchPath2.c_str(), strLocaleSearchPath3.c_str());
	}

	if (!mVobjLocale.AddCatalog(strCatalogNameLibrary)) {
		wxString strError = _T("No se pudo cargar el catalogo: %s");
		strError += _T("\n Locale Search Path 1: %s");
		strError += _T("\n Locale Search Path 2: %s");
		strError += _T("\n Locale Search Path 3: %s");
		wxLogWarning(strError, strCatalogName.c_str(), strLocaleSearchPath1.c_str(), strLocaleSearchPath2.c_str(), strLocaleSearchPath3.c_str());
	}
}


//_______________________________________________________________________________________________________________________

wxString clsApplication::strReadConfigLanguage() {
	wxString strNombreConfigLenguaje = mVstrAppPath + mVstrLOCALE_FOLDER + _T("/language.dat");
	wxTextFile objArchivo;
	wxString strSeteo;

	if (!objArchivo.Open(strNombreConfigLenguaje)) {
		// Can't open the file
		return(_T(""));
	}

	strSeteo = objArchivo.GetFirstLine();

	return(strSeteo);
}

//_______________________________________________________________________________________________________________________

void clsApplication::LoadGraphics()
{
	wxImage VimgImage;
	wxString VstrFileName;
	CountedPtr<clsOptions> SobjOptions(clsOptions::GetInstance());

	VstrFileName = SobjOptions->GetVstrGraphicsDirectory() + SobjOptions->GetVstrGraphicsFileName();
	VimgImage.LoadFile(VstrFileName, wxBITMAP_TYPE_BMP);

	if(!VimgImage.Ok()) {
		wxString strError = _("The graphics file cannot be loaded. File name: ");
		strError += VstrFileName;
		wxMessageBox(strError, _("Error"), wxICON_ERROR);
		exit(1);
	}

	mSbmpGraphics = Make_CP(new wxBitmap(VimgImage));
}

//_______________________________________________________________________________________________________________________

template <typename T> void clsApplication::ResetCounters(T it, T end) {
	for(; it != end; ++it)
    {
        it->second->ResetCounter();
    }
}

//_______________________________________________________________________________________________________________________

template <typename T> void clsApplication::SaveCollection(T& pRdctCollection, wxString pVstrCollectionName, wxString pVstrPersistenceStructure) {
	typename T::iterator it;
	c4_View viwView;
	c4_Row objRow;

	viwView = mOdbfDatabase->GetAs(pVstrCollectionName + pVstrPersistenceStructure);
	
	for(it = pRdctCollection.begin(); it != pRdctCollection.end(); ++it) {
		it->second->Save(objRow);
		viwView.Add(objRow);
	}
}

//_______________________________________________________________________________________________________________________

template <typename T> void clsApplication::LoadCollection(T& pRdctObjectTypeCollection, wxString pVstrCollectionName, wxString pVstrPersistenceStructure) {
	c4_View viwView;
	c4_Row objRow;
	wxString strInternalTypeName;
	typename T::mapped_type vrnObjectType; // should be an object type wrapped in a CountedPtr. This object have to implement the method OobjCreateFromFile
	
	viwView = mOdbfDatabase->GetAs(pVstrCollectionName + pVstrPersistenceStructure);
	
	for(int i = 0; i < viwView.GetSize(); ++i) {
		strInternalTypeName = mVprpStrInternalTypeName(viwView[i]);
		vrnObjectType = safemap::find(pRdctObjectTypeCollection, strInternalTypeName);
		vrnObjectType->OobjCreateFromFile(viwView[i]);
	}
}

//_______________________________________________________________________________________________________________________

template <typename T> void clsApplication::SaveObjectTypeCollection(T& pRdctCollection, c4_View& pRviwView) {
	typename T::iterator it;
	c4_Row objRow;

	for(it = pRdctCollection.begin(); it != pRdctCollection.end(); ++it) {
		it->second->SaveObjectTypeInformation(objRow);
		pRviwView.Add(objRow);
	}
}

//_______________________________________________________________________________________________________________________

template <typename T> void clsApplication::UpdateObjectTypeCollection(T& pRdctCollection, c4_View& pRviwView) {
	wxString strKey;
	typename T::mapped_type PvrnObjectType;
	
	for(int i = 0; i < pRviwView.GetSize(); ++i) {
		strKey = mVprpStrName(pRviwView[i]);
		PvrnObjectType = safemap::find(pRdctCollection, strKey);
		PvrnObjectType->UpdateObjectTypeInformation(pRviwView[i]);
	}
}

//_______________________________________________________________________________________________________________________

void clsApplication::LoadIcons() {
	mSimlImageList_16->Add(wxIcon(fixed_selection_xpm), _T("FixedSelection"));
}

//_______________________________________________________________________________________________________________________

void clsApplication::SaveBoard() {
	wxBusyCursor objBusyCursorManager; // to show the busy cursor while this function is working
	wxString strFileName = gOobjApplication->GetVstrFileName();
	CountedPtr<c4_Storage> SdbfDatabase; // temporal database to store the file
	wxString strPersistenceStructure;
	c4_View viwObjectTypeCollection;
	
	mVblnFileOperationFailure = false;
	
	if(wxFile::Exists(strFileName)) {
		// File already exists
		wxRemoveFile(strFileName);
	}

	SdbfDatabase = Make_CP(new c4_Storage(strFileName, true /* canModify */));
	if(!SdbfDatabase->Commit()) { // only to test the file creation
		wxString strError = wxString::Format(_("The file could not be created. File name: %s"), strFileName.c_str());
		clsLibrary::ShowError(strError);
		
		mVblnFileOperationFailure = true;
		return;
	}
	
	mOdbfDatabase = SdbfDatabase.GetPtr(); // to expose the database to the whole application
	
	Save(); // save data related to the whole application
	
	// Save object type collections
	viwObjectTypeCollection = mOdbfDatabase->GetAs(strGetObjectTypeCollectionStructure());
	SaveObjectTypeCollection(mVdctNaturalResourcesTypes, viwObjectTypeCollection);
	
	// Save owner collections
	strPersistenceStructure = clsNaturalResource::strGetPersistenceStructure();
	SaveCollection(mVdctNaturalResources, clsNaturalResource::GetVstrPersistenceName(), strPersistenceStructure);
	
	mSobjBoard->Save();
	
	// Save all changes into a file
	if(!SdbfDatabase->Commit()) {
		wxString strError = wxString::Format(_("The map could not be save successfully. File name: %s"), strFileName.c_str());
		clsLibrary::ShowError(strError);
		
		mVblnFileOperationFailure = true;
	}
	
	mOdbfDatabase = NULL;
}

//_______________________________________________________________________________________________________________________

void clsApplication::OpenDocument(wxString pStrFileName) {
	wxBusyCursor objBusyCursorManager; // to show the busy cursor while this function is working
	CountedPtr<c4_Storage> SdbfDatabase; // temporal database to load the document
	c4_View viwApplication;
	wxString strType;
	wxString strPreviousStatus = mPobjFrame->GetPobjStatusBar()->strGetModality(); // to restore the status bar text

	mVblnFileOperationFailure = false;
	mPobjFrame->GetPobjStatusBar()->SetModality(_("Loading document..."));

	SdbfDatabase = Make_CP(new c4_Storage(pStrFileName, true /* canModify */));
	mOdbfDatabase = SdbfDatabase.GetPtr(); // to expose the database to the whole application
	
	viwApplication = mOdbfDatabase->GetAs(strGetPersistenceStructure());
	if(viwApplication.GetSize() != 1) {
		// If the file doesn't content only one row in this view, this means that something is wrong
		wxString strError = _("The file has an unknown format.");
		strError += _T("\n"); strError += _("Application view structure error.");
		clsLibrary::ShowError(strError);
		
		mVblnFileOperationFailure = true;
		mPobjFrame->GetPobjStatusBar()->SetModality(strPreviousStatus); // restore the previous status bar text
		return;
	}
	strType = mVprpStrType(viwApplication[0]);
	
	if(strType == mVstrBOARD_FILE_TYPE) {
		// The document is a board
		OpenBoard();
	} else if(strType == mVstrGAME_FILE_TYPE) {
		// The document is a game
	} else {
		// The document has an unknown format
		wxString strError = wxString::Format(_("The file has an unknown format. File type: %s"), strType.c_str());
		clsLibrary::ShowError(strError);
		
		mVblnFileOperationFailure = true;
		mPobjFrame->GetPobjStatusBar()->SetModality(strPreviousStatus); // restore the previous status bar text
		return;
	}
}

//_______________________________________________________________________________________________________________________

void clsApplication::OpenBoard() {
	c4_View viwObjectTypeCollection;
	wxString strPersistenceStructure;
	int intPixelWidth;
	int intPixelHeight;
	clsPointer<clsCanvas> OobjCanvas = mPobjFrame->GetpObjCanvas();
	
	// Update object type collections
	viwObjectTypeCollection = mOdbfDatabase->GetAs(strGetObjectTypeCollectionStructure());
	UpdateObjectTypeCollection(mVdctNaturalResourcesTypes, viwObjectTypeCollection);
	
	// Load owner collections
	strPersistenceStructure = clsNaturalResource::strGetPersistenceStructure();
	LoadCollection(mVdctNaturalResourcesTypes, clsNaturalResource::GetVstrPersistenceName(), strPersistenceStructure);

	mSobjBoard->Open();

	// Set virtual size in the OffScreen buffer
	intPixelWidth = mSobjBoard->GetVintWidth() * mSobjBoard->GetVintSQUARE_LENGTH();
	intPixelHeight = mSobjBoard->GetVintHeight() * mSobjBoard->GetVintSQUARE_LENGTH();
	
	OobjCanvas->SetTotalSize(intPixelWidth, intPixelHeight);

	OobjCanvas->SetvRctVisibleRegion(0, 0);
	OobjCanvas->AdjustScrollbars();

	this->ChangeStateBoardEdition();
	OobjCanvas->GetParent()->Layout(); // to force the game window to resize according with its frame container
	
	mSobjBoard->DrawAll();
	OobjCanvas->Draw();
}

//_______________________________________________________________________________________________________________________

void clsApplication::Save() {
	c4_View viwApplication;
	c4_Row objRow;
	
	viwApplication = mOdbfDatabase->GetAs(strGetPersistenceStructure());
	mVprpStrType(objRow) = mVstrBOARD_FILE_TYPE;
	mVprpIntFileVersion(objRow) = 1;
	
	viwApplication.Add(objRow);
}

//_______________________________________________________________________________________________________________________

wxString clsApplication::strGetPersistenceStructure() {
	wxString strResult;

	strResult = _T("Application[");
	strResult += _T("strType:S");
	strResult += _T(",intFileVersion:I");
	strResult += _T("]");

	return(strResult);
}

//_______________________________________________________________________________________________________________________

wxString clsApplication::strGetObjectTypeCollectionStructure() {
	wxString strResult;
	
	strResult = _T("ObjectTypeCollection[");
	strResult += _T("strName:S");
	strResult += _T(",intInstances:I");
	strResult += _T("]");
	
	return(strResult);
}

//_______________________________________________________________________________________________________________________

wxString clsApplication::fVstrVersion() const {
	wxString strResult = wxString::Format(_T("%i"), mVintMajorVersion);
	
	strResult += _T(".");
	strResult += wxString::Format(_T("%i"), mVintMinorVersion);

	strResult += _T(".");
	strResult += wxString::Format(_T("%i"), mVintReleaseNumber);
	
	return(strResult);
}
