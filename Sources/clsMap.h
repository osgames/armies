#ifndef _CLSMAP_H_
#define _CLSMAP_H_

/*!
* Includes
*/

#include <wx/window.h>

#include "jrb_ptr.h"
#include "clsPointer.h"

/*!
 * Namespaces
 */

using namespace jrb_sptr;

/*!
 * Forward declarations
 */

class clsBoard;
class clsFrame;

//_______________________________________________________________________________________________________________________

/**
* Represents the small view of the board, that appears in a reduced scale in the top right corner of the screen.
*/
class clsMap : public wxWindow
{
public:
	clsMap(wxWindow *parent, wxWindowID id = wxID_ANY);
	~clsMap();

	//-------------------------------------------------------------------------------------------------------------------
	// Getters / Setters

	void SetOobjBoard(clsPointer<clsBoard> value);
	void SetOobjFrame(clsPointer<clsFrame> value) { mOobjFrame = value; }
	
	//-------------------------------------------------------------------------------------------------------------------
	// Methods

	/**
	* Draw a pixel on the map
	*/
	void DrawPixel(int pVintRow, int pVintColumn, const wxColour& pRclrColor);

	/**
	* Set the visible area, to draw later a rectangle in the map in order to show it.
	* The rectangle passed is expressed in pixels. This method resizes it proportionally to the map window.
	* The scaled rectangle is used later to show the visible area on the map.
	*/
	void SetVisibleRegion(wxRect pVrctBoardVisibleRegion);

	/**
	* Draw the content of the screen buffer in the map visible area.
	* It can't be named "Show" because overlaps with a wxWindow method.
	*/
	void ShowMap();
	
	/**
	* Return an image that is the current state of the map, unscaled and cropped to fit the exact board dimensions.
	*/
	wxImage imgGetPreview();
	
	//-------------------------------------------------------------------------------------------------------------------
	// Events

	/**
	* Used to select an arbitrary square by clicking on the window map
	*/
	void OnLeftDown(wxMouseEvent& event);

//_______________________________________________________________________________________________________________________
	
private:
	
	//-------------------------------------------------------------------------------------------------------------------
	// Variables
	
	/**
	* This image implements a double buffer system for the map. Its size is set to the maximum squares allowed for a 
	* board. Therefor, there could be some remaining space unused, depending on how long is the actual board.
	* In this screen buffer, the scale will ever be fixed to one pixel per board square.
	* The change of scale according to the real window map dimensions in the screen is made on the fly before
	* show it.
	*/
	CountedPtr<wxBitmap> mSbmpBuffer;

	/**
	* Store the area showed in the main screen, to be able to draw it in the map.
	* The sizes are scaled proportionally to the map window.
	*/
	wxRect mVrctVisibleRegion;

	/**
	* Store the proportion of how many pixels are used to draw an square in the map.
	* It's set in the SetVisibleRegion method.
	*/
	double mVdblScreenToMapScale;

	// ---------------------------------------------------------------------------------------------------------------------
	// Shortcuts
	
	clsPointer<clsBoard> mOobjBoard;
	clsPointer<clsFrame> mOobjFrame;

	// ---------------------------------------------------------------------------------------------------------------------

	DECLARE_EVENT_TABLE()
};

#endif // _CLSMAP_H_
