// clsUpperLevelObject.cpp: implementation of the clsUpperLevelObject class.
//
//////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "clsUpperLevelObject.h"

IMPLEMENT_ABSTRACT_CLASS(clsUpperLevelObject, clsBoardObject) // to enable RTTI

//.......................................................................................................................
// Definition of static members

c4_StringProp clsUpperLevelObject::sVprpStrName(_T("strName"));
c4_IntProp clsUpperLevelObject::sVprpIntInstances(_T("intInstances"));
c4_IntProp clsUpperLevelObject::sVprpIntHealth(_T("intHealth"));
c4_StringProp clsUpperLevelObject::sVprpStrInternalTypeName(_T("strInternalTypeName"));

//_______________________________________________________________________________________________________________________

clsUpperLevelObject::clsUpperLevelObject()
{
}

//_______________________________________________________________________________________________________________________

clsUpperLevelObject::~clsUpperLevelObject() {
}

//_______________________________________________________________________________________________________________________

wxString clsUpperLevelObject::strGetPersistenceStructure() {
	wxString strResult = clsBoardObject::strGetPersistenceStructure();
	
	strResult += _T(",intHealth:I");
	strResult += _T(",strInternalTypeName:S");
	
	return(strResult);
}

//_______________________________________________________________________________________________________________________

void clsUpperLevelObject::Save(c4_Row& pRrowRow) {
	clsBoardObject::Save(pRrowRow);
	
	sVprpIntHealth(pRrowRow) = mVintHealth;
	sVprpStrInternalTypeName(pRrowRow) = this->GetVstrInternalTypeName();
}

//_______________________________________________________________________________________________________________________

void clsUpperLevelObject::Load(c4_RowRef pVrowRow) {
	clsBoardObject::Load(pVrowRow);

	wxString strInternalTypeName(sVprpStrInternalTypeName(pVrowRow));
	
	mVintHealth = sVprpIntHealth(pVrowRow);
	this->SetVstrInternalTypeName(strInternalTypeName);
}

//_______________________________________________________________________________________________________________________

void clsUpperLevelObject::SaveObjectTypeInformation(c4_Row& pRrowRow) {
	sVprpStrName(pRrowRow) = this->GetVstrName();
	sVprpIntInstances(pRrowRow) = this->GetVintInstances();
}

//_______________________________________________________________________________________________________________________

void clsUpperLevelObject::UpdateObjectTypeInformation(c4_RowRef pVrowRow) {
	this->SetVintInstances(sVprpIntInstances(pVrowRow));
}
