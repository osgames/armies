// clsBoard.cpp: implementation of the clsBoard class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <wx/mstream.h>

#include "clsBoard.h"
#include "ClsApplication.h"
#include "Global.h"
#include "clsLibrary.h"
#include "clsCanvas.h"
#include "clsFrame.h"
#include "clsBoardObject.h"
#include "clsTerrain.h"
#include "clsCursor.h"
#include "clsfixedselection.h"
#include "clsUpperLevelObject.h"
#include "clsNaturalResource.h"
#include "clsMap.h"

#include "../Graficos Desarrollo/Iconos/16x16/eraser.xpm"

//____________________________________________________________________________________________________________________

clsBoard::clsBoard()
	: mVintSQUARE_LENGTH(40)
	, mVintMAX_SQUARES(100)
	, mVarrWaterAutoborderRules(256)
	, mVarrMountainAutoborderRules(256)
	, mVarrGrasslandWaterAutoborderRules(256)
	, mVarrGrasslandMountainAutoborderRules(256)
	, mVarrRelativePositions(3, 3)
	, mVblnApplyAutoBorder(false)
	, mVblnAutoBorderInstantRefresh(true)

	// Metakit properties
	, mVprpStrTitle(_T("strTitle"))
	, mVprpStrDescription(_T("strDescription"))
	, mVprpIntHeight(_T("intHeight"))
	, mVprpIntWidth(_T("intWidth"))
	, mVprpIntPlayers(_T("intPlayers"))
	, mVprpBytPreview(_T("bytPreview"))

	, mVprpStrSurface(_T("strSurface"))
	, mVprpStrUpperObject(_T("strUpperObject"))

{
	LoadImages();
	CreateMouseCursors();
	FillRelativePositions();

	mOobjFrame = gOobjApplication->GetPobjFrame();
	mOobjCanvas = mOobjFrame->GetpObjCanvas();
	mOobjMap = mOobjFrame->GetPobjMap();
}

//____________________________________________________________________________________________________________________

clsBoard::~clsBoard() {
}

//____________________________________________________________________________________________________________________

void clsBoard::FillRelativePositions() {
	mVarrRelativePositions.SetAt(0, 2, enmBoardDirection_TopRight);
	mVarrRelativePositions.SetAt(0, 1, enmBoardDirection_Right);
	mVarrRelativePositions.SetAt(0, 0, enmBoardDirection_BottomRight);
	mVarrRelativePositions.SetAt(1, 0, enmBoardDirection_Bottom);
	mVarrRelativePositions.SetAt(2, 0, enmBoardDirection_BottomLeft);
	mVarrRelativePositions.SetAt(2, 1, enmBoardDirection_Left);
	mVarrRelativePositions.SetAt(2, 2, enmBoardDirection_TopLeft);
	mVarrRelativePositions.SetAt(1, 2, enmBoardDirection_Top);
}

//____________________________________________________________________________________________________________________

void clsBoard::FillArraysAutoborderRules() {
	clsPointer<clsTerrain> OobjTerrain;
	
	//-------------------------------------------------------------------------------------------------------------------
	// Water - Grassland
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopCoast"));
	mVarrWaterAutoborderRules[248] = OobjTerrain;
	mVarrWaterAutoborderRules[252] = OobjTerrain;
	mVarrWaterAutoborderRules[249] = OobjTerrain;
	mVarrWaterAutoborderRules[253] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomCoast"));
	mVarrWaterAutoborderRules[31] = OobjTerrain;
	mVarrWaterAutoborderRules[159] = OobjTerrain;
	mVarrWaterAutoborderRules[63] = OobjTerrain;
	mVarrWaterAutoborderRules[191] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("LeftCoast"));
	mVarrWaterAutoborderRules[214] = OobjTerrain;
	mVarrWaterAutoborderRules[215] = OobjTerrain;
	mVarrWaterAutoborderRules[246] = OobjTerrain;
	mVarrWaterAutoborderRules[247] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("RightCoast"));
	mVarrWaterAutoborderRules[107] = OobjTerrain;
	mVarrWaterAutoborderRules[111] = OobjTerrain;
	mVarrWaterAutoborderRules[235] = OobjTerrain;
	mVarrWaterAutoborderRules[239] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopLeftLake"));
	mVarrWaterAutoborderRules[208] = OobjTerrain;
	mVarrWaterAutoborderRules[240] = OobjTerrain;
	mVarrWaterAutoborderRules[244] = OobjTerrain;
	mVarrWaterAutoborderRules[212] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopRightLake"));
	mVarrWaterAutoborderRules[232] = OobjTerrain;
	mVarrWaterAutoborderRules[104] = OobjTerrain;
	mVarrWaterAutoborderRules[233] = OobjTerrain;
	mVarrWaterAutoborderRules[105] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomRightLake"));
	mVarrWaterAutoborderRules[11] = OobjTerrain;
	mVarrWaterAutoborderRules[43] = OobjTerrain;
	mVarrWaterAutoborderRules[15] = OobjTerrain;
	mVarrWaterAutoborderRules[47] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomLeftLake"));
	mVarrWaterAutoborderRules[22] = OobjTerrain;
	mVarrWaterAutoborderRules[23] = OobjTerrain;
	mVarrWaterAutoborderRules[150] = OobjTerrain;
	mVarrWaterAutoborderRules[151] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomRightIsland"));
	mVarrWaterAutoborderRules[254] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomLeftIsland"));
	mVarrWaterAutoborderRules[251] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopLeftIsland"));
	mVarrWaterAutoborderRules[127] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopRightIsland"));
	mVarrWaterAutoborderRules[223] = OobjTerrain;

	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("Water"));
	mVarrWaterAutoborderRules[255] = OobjTerrain;

	//-------------------------------------------------------------------------------------------------------------------
	// Mountain - Grassland
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopMountain"));
	mVarrMountainAutoborderRules[248] = OobjTerrain;
	mVarrMountainAutoborderRules[252] = OobjTerrain;
	mVarrMountainAutoborderRules[249] = OobjTerrain;
	mVarrMountainAutoborderRules[253] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomMountain"));
	mVarrMountainAutoborderRules[31] = OobjTerrain;
	mVarrMountainAutoborderRules[159] = OobjTerrain;
	mVarrMountainAutoborderRules[63] = OobjTerrain;
	mVarrMountainAutoborderRules[191] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("LeftMountain"));
	mVarrMountainAutoborderRules[214] = OobjTerrain;
	mVarrMountainAutoborderRules[215] = OobjTerrain;
	mVarrMountainAutoborderRules[246] = OobjTerrain;
	mVarrMountainAutoborderRules[247] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("RightMountain"));
	mVarrMountainAutoborderRules[107] = OobjTerrain;
	mVarrMountainAutoborderRules[111] = OobjTerrain;
	mVarrMountainAutoborderRules[235] = OobjTerrain;
	mVarrMountainAutoborderRules[239] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopLeftMount"));
	mVarrMountainAutoborderRules[208] = OobjTerrain;
	mVarrMountainAutoborderRules[240] = OobjTerrain;
	mVarrMountainAutoborderRules[244] = OobjTerrain;
	mVarrMountainAutoborderRules[212] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopRightMount"));
	mVarrMountainAutoborderRules[232] = OobjTerrain;
	mVarrMountainAutoborderRules[104] = OobjTerrain;
	mVarrMountainAutoborderRules[233] = OobjTerrain;
	mVarrMountainAutoborderRules[105] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomRightMount"));
	mVarrMountainAutoborderRules[11] = OobjTerrain;
	mVarrMountainAutoborderRules[43] = OobjTerrain;
	mVarrMountainAutoborderRules[15] = OobjTerrain;
	mVarrMountainAutoborderRules[47] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomLeftMount"));
	mVarrMountainAutoborderRules[22] = OobjTerrain;
	mVarrMountainAutoborderRules[23] = OobjTerrain;
	mVarrMountainAutoborderRules[150] = OobjTerrain;
	mVarrMountainAutoborderRules[151] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomRightValley"));
	mVarrMountainAutoborderRules[254] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomLeftValley"));
	mVarrMountainAutoborderRules[251] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopLeftValley"));
	mVarrMountainAutoborderRules[127] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopRightValley"));
	mVarrMountainAutoborderRules[223] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("Mountain"));
	mVarrMountainAutoborderRules[255] = OobjTerrain;

	//-------------------------------------------------------------------------------------------------------------------
	// Grassland - Water
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomCoast"));
	mVarrGrasslandWaterAutoborderRules[248] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[252] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[249] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[253] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopCoast"));
	mVarrGrasslandWaterAutoborderRules[31] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[159] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[63] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[191] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("RightCoast"));
	mVarrGrasslandWaterAutoborderRules[214] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[215] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[246] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[247] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("LeftCoast"));
	mVarrGrasslandWaterAutoborderRules[107] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[111] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[235] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[239] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopLeftIsland"));
	mVarrGrasslandWaterAutoborderRules[208] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[240] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[244] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[212] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopRightIsland"));
	mVarrGrasslandWaterAutoborderRules[232] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[104] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[233] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[105] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomRightIsland"));
	mVarrGrasslandWaterAutoborderRules[11] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[43] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[15] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[47] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomLeftIsland"));
	mVarrGrasslandWaterAutoborderRules[22] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[23] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[150] = OobjTerrain;
	mVarrGrasslandWaterAutoborderRules[151] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomRightLake"));
	mVarrGrasslandWaterAutoborderRules[254] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomLeftLake"));
	mVarrGrasslandWaterAutoborderRules[251] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopLeftLake"));
	mVarrGrasslandWaterAutoborderRules[127] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopRightLake"));
	mVarrGrasslandWaterAutoborderRules[223] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("Grassland"));
	mVarrGrasslandWaterAutoborderRules[255] = OobjTerrain;

	//-------------------------------------------------------------------------------------------------------------------
	// Grassland - Mountain
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomMountain"));
	mVarrGrasslandMountainAutoborderRules[248] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[252] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[249] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[253] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopMountain"));
	mVarrGrasslandMountainAutoborderRules[31] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[159] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[63] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[191] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("RightMountain"));
	mVarrGrasslandMountainAutoborderRules[214] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[215] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[246] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[247] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("LeftMountain"));
	mVarrGrasslandMountainAutoborderRules[107] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[111] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[235] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[239] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopLeftValley"));
	mVarrGrasslandMountainAutoborderRules[208] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[240] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[244] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[212] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopRightValley"));
	mVarrGrasslandMountainAutoborderRules[232] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[104] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[233] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[105] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomRightValley"));
	mVarrGrasslandMountainAutoborderRules[11] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[43] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[15] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[47] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomLeftValley"));
	mVarrGrasslandMountainAutoborderRules[22] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[23] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[150] = OobjTerrain;
	mVarrGrasslandMountainAutoborderRules[151] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomRightMount"));
	mVarrGrasslandMountainAutoborderRules[254] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("BottomLeftMount"));
	mVarrGrasslandMountainAutoborderRules[251] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopLeftMount"));
	mVarrGrasslandMountainAutoborderRules[127] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("TopRightMount"));
	mVarrGrasslandMountainAutoborderRules[223] = OobjTerrain;
	
	OobjTerrain = gOobjApplication->GetOobjTerrain(_T("Grassland"));
	mVarrGrasslandMountainAutoborderRules[255] = OobjTerrain;
}

//____________________________________________________________________________________________________________________

void clsBoard::LoadImages() {
	wxRect rctSource;
	wxMemoryDC VmdcMemoryDC;

	// -----------------------------------------------------------------------------------------------------------------
	// Explosion
	mSbmpExplosion = Make_CP(new wxBitmap(mVintSQUARE_LENGTH, mVintSQUARE_LENGTH));
	VmdcMemoryDC.SelectObject(*mSbmpExplosion);
	
	rctSource.SetTop(40);
	rctSource.SetBottom(80);
	rctSource.SetLeft(400);
	rctSource.SetRight(rctSource.GetLeft() + mVintSQUARE_LENGTH);
	
	VmdcMemoryDC.DrawBitmap(gOobjApplication->GetSbmpGraphics()->GetSubBitmap(rctSource), 0, 0);
	
}

//____________________________________________________________________________________________________________________

void clsBoard::New(int PvIntWidth, int PvIntHeight) {
	wxBusyCursor objBusyCursorManager; // to show the busy cursor while this function is working
	int intRow;
	int intColumn;

	mVintWidth = PvIntWidth;
	mVintHeight = PvIntHeight;

	mVarrTerrain.resize(PvIntWidth, PvIntHeight);
	mVarrBoardObject.resize(PvIntWidth, PvIntHeight);
	
	// TODO: show feedback information in the StatusBar
	
	// Insert Grassland surface for all squares
	for(intRow = 0; intRow < PvIntHeight; intRow++) {
		for(intColumn = 0; intColumn < PvIntWidth; intColumn++) {
			SetTerrain(intRow, intColumn, gOobjApplication->GetOobjTerrain(_T("Grassland")), false /* ApplyAutoBorder */);
		}
	}

	// Make a new cursor at (0,0) position
	mSobjCursor = Make_CP(new clsCursor());
	mOobjCanvas->SetOobjCursor(mSobjCursor.GetPtr()); // sets the shortcut to clsCursor

	// Make a new fixed selection
	mSobjFixedSelection = Make_CP(new clsFixedSelection());

	mVblnApplyAutoBorder = true;
}

//____________________________________________________________________________________________________________________

void clsBoard::ChangeEmpty() {
	mOobjCanvas->SetOobjCursor(NULL); // resets the shortcut to clsCursor
	mSobjCursor = CP_NULL;
	
	// Fixed selection
	if(mSobjFixedSelection.IsValid()) {
		// There is a valid instance of clsFixedSelection
		if(!mSobjFixedSelection->blnEmpty()) {
			// There is an active fixed selection
			mSobjFixedSelection->Erase();
		}

		mSobjFixedSelection = CP_NULL;
	}
	
	mVarrTerrain.resize(0, 0);
	mVarrBoardObject.resize(0, 0);
}

//____________________________________________________________________________________________________________________

void clsBoard::DrawAll() {
	this->DrawPixelArea(mOobjCanvas->GetVrctVisibleRegion());
}

//____________________________________________________________________________________________________________________

void clsBoard::Draw(int PvIntRow, int PvIntColumn) {
	wxRect rctArea; // area used by the square
	wxRect rctAreaToUpdate; // area that must be updated. May differ from "rctArea" if the square is not completely showed in the screen.
	wxPoint pntOffset;
	clsPointer<clsTerrain> oObjTerrain;

	// Calculates the area used by the square to be updated
	rctArea = this->rctSquareArea(PvIntRow, PvIntColumn);
	rctAreaToUpdate = mOobjCanvas->rctUpdatableArea(rctArea);

	if(rctAreaToUpdate.IsEmpty()) {
		// It's not necessary to draw, because the update take place outside the visible region in the screen
		return;
	}

	// Calculates the offsets for the case in that the square doesn't have to draw entirely
	// Only calculates the left and upper offsets and not the right and bottom ones, because they still simply excluded after coping the secondary screen in the primary (as long as the secondary screen would had enough extra pixeles to support the excess).
	pntOffset.x = rctAreaToUpdate.GetLeft() - rctArea.GetLeft();
	pntOffset.y = rctAreaToUpdate.GetTop() - rctArea.GetTop();

	// Draw the terrain (lower) level
	oObjTerrain = mVarrTerrain.GetAt(PvIntRow, PvIntColumn); // due the fact that there is only one terrain object of each type and they are reuse by several squares, the position must be set before draw one of them
	oObjTerrain->SetVpntPosition(wxPoint(PvIntColumn, PvIntRow));
	oObjTerrain->Draw(pntOffset);
	
	// Draw the upper level
	if(this->blnBusySquare(PvIntRow, PvIntColumn)) {
		// There is an upper level object to be drawn
		mVarrBoardObject.GetAt(PvIntRow, PvIntColumn)->Draw(pntOffset);
	}
	
	// Check to draw the fixed selection
	if(mSobjFixedSelection->GetVintRow() == PvIntRow && mSobjFixedSelection->GetVintColumn() == PvIntColumn) {
		// The square owns the fixed selection
		mSobjFixedSelection->Draw();
	}
	
	// NOTE: Here should be drawn the normal selection (the flicker cursor) but due the fact that it is drawn each a given amount of time by a timer, this task can be skipped for better performance without major problems.
}

//____________________________________________________________________________________________________________________

void clsBoard::DrawPixelArea(wxRect pVrctArea) {
	wxRect rctAreaSquares = rctPixel2Square(pVrctArea);
	int intRow;
	int intCol;
	
	for(intRow = rctAreaSquares.GetTop(); intRow < rctAreaSquares.GetBottom() + 1; intRow++) {
		for(intCol = rctAreaSquares.GetLeft(); intCol < rctAreaSquares.GetRight() + 1; intCol++) {
			this->Draw(intRow, intCol);
		}
	}
}

//____________________________________________________________________________________________________________________

wxRect clsBoard::rctSquareArea(int PvIntRow, int PvIntColumn) {
	wxRect rctArea;

	// Calculate the pixel coordinate of the square
	rctArea.SetLeft(PvIntColumn * mVintSQUARE_LENGTH);
	rctArea.SetTop(PvIntRow * mVintSQUARE_LENGTH);
	rctArea.SetWidth(mVintSQUARE_LENGTH);
	rctArea.SetHeight(mVintSQUARE_LENGTH);

	return(rctArea);

}

//____________________________________________________________________________________________________________________

wxRect clsBoard::rctPixel2Square(const wxRect& PvRctPixelArea) const {
	wxRect rctSquareArea;
	int intSquare;
	
	intSquare = (int) floor((double) (PvRctPixelArea.GetTop() / mVintSQUARE_LENGTH));
	rctSquareArea.SetTop(intSquare);
	
	intSquare = (int) ceil((double) (PvRctPixelArea.GetBottom() / mVintSQUARE_LENGTH));
	rctSquareArea.SetBottom(intSquare);
	
	intSquare = (int) floor((double) (PvRctPixelArea.GetLeft() / mVintSQUARE_LENGTH));
	rctSquareArea.SetLeft(intSquare);
	
	intSquare = (int) ceil((double) (PvRctPixelArea.GetRight() / mVintSQUARE_LENGTH));
	rctSquareArea.SetRight(intSquare);
	
	return(rctSquareArea);
}

//____________________________________________________________________________________________________________________

wxRect clsBoard::rctBoard2Screen(wxRect pVrctBoardCoordinateArea) {
	wxRect rctScreenCoordinateArea;

	rctScreenCoordinateArea.SetLeft(pVrctBoardCoordinateArea.GetLeft() - mOobjCanvas->GetVrctVisibleRegion().GetLeft());
	rctScreenCoordinateArea.SetTop(pVrctBoardCoordinateArea.GetTop() - mOobjCanvas->GetVrctVisibleRegion().GetTop());
	rctScreenCoordinateArea.SetRight(pVrctBoardCoordinateArea.GetRight() - mOobjCanvas->GetVrctVisibleRegion().GetLeft());
	rctScreenCoordinateArea.SetBottom(pVrctBoardCoordinateArea.GetBottom() - mOobjCanvas->GetVrctVisibleRegion().GetTop());

	return(rctScreenCoordinateArea);
}

//____________________________________________________________________________________________________________________

void clsBoard::DrawCursor() {
	mSobjCursor->Draw();
	mOobjCanvas->Draw();
}

//____________________________________________________________________________________________________________________

void clsBoard::ScrollUpWithSelection() {
	int intSelectionY = this->intSquare2Pixel(this->GetVintRowSelected());
	wxRect rctVisibleRegion = mOobjCanvas->GetVrctVisibleRegion();
	
	if(intSelectionY < rctVisibleRegion.GetTop()) {
		// The whole cursor (or some part of it) overtook the top of the visible region. It's necessary to make a scroll.
		mOobjCanvas->DirectScroll(wxVERTICAL, intSelectionY);
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::CenterSelection() {
	bool blnPrevDrawOnScroll = mOobjCanvas->GetVblnDrawOnScroll();
	int intBeginSelection; // selection beginning, in pixels
	int intCenterDistance; // distance that must exists from the screen border to the beginning of the selected square, in order to make the selection centered in the screen
	wxRect rctVisibleRegion(mOobjCanvas->GetVrctVisibleRegion());
	int intTmpValue;
	int intBeginVisibleArea;

	mOobjCanvas->SetVblnDrawOnScroll(false); // for draw only once in each scroll (horizontal and vertical)

	// -------------------------------------------------------------------------------------------------------------------
	// Compute horizontal scroll
	intBeginSelection = this->GetVintColumnSelected() * mVintSQUARE_LENGTH;
	
	intTmpValue = rctVisibleRegion.GetWidth() - mVintSQUARE_LENGTH;
	intTmpValue = clsLibrary::intFromDouble((double) intTmpValue / 2);
	intCenterDistance = clsLibrary::intNegativeToZero(intTmpValue);
	
	intBeginVisibleArea = clsLibrary::intNegativeToZero(intBeginSelection - intCenterDistance);
	if(intBeginVisibleArea > mOobjCanvas->GetScrollRange(wxHORIZONTAL)) {
		// Scroll range overloaded
		intBeginVisibleArea = mOobjCanvas->GetScrollRange(wxHORIZONTAL);
	}

	mOobjCanvas->DirectScroll(wxHORIZONTAL, intBeginVisibleArea);

	// -------------------------------------------------------------------------------------------------------------------
	// Compute vertical scroll
	intBeginSelection = this->GetVintRowSelected() * mVintSQUARE_LENGTH;

	intTmpValue = rctVisibleRegion.GetHeight() - mVintSQUARE_LENGTH;
	intTmpValue = clsLibrary::intFromDouble((double) intTmpValue / 2);
	intCenterDistance = clsLibrary::intNegativeToZero(intTmpValue);

	intBeginVisibleArea = clsLibrary::intNegativeToZero(intBeginSelection - intCenterDistance);
	if(intBeginVisibleArea > mOobjCanvas->GetScrollRange(wxVERTICAL)) {
		// Scroll range overloaded
		intBeginVisibleArea = mOobjCanvas->GetScrollRange(wxVERTICAL);
	}

	mOobjCanvas->DirectScroll(wxVERTICAL, intBeginVisibleArea);

	// -------------------------------------------------------------------------------------------------------------------
	
	this->DrawAll();
	mOobjCanvas->Draw(true);
	mOobjCanvas->SetVblnDrawOnScroll(blnPrevDrawOnScroll); // restore the previous setting
}

//____________________________________________________________________________________________________________________

void clsBoard::ScrollDownWithSelection() {
	int intSelectionY = this->intSquare2Pixel(this->GetVintRowSelected());
	wxRect rctVisibleRegion = mOobjCanvas->GetVrctVisibleRegion();
	int intOffsetY;
	
	if(intSelectionY > rctVisibleRegion.GetBottom() - mVintSQUARE_LENGTH) {
		// The whole cursor (or some part of it) overtook the bottom of the visible region. It's necessary to make a scroll.
		intOffsetY = intSelectionY - (rctVisibleRegion.GetHeight() - mVintSQUARE_LENGTH);
		mOobjCanvas->DirectScroll(wxVERTICAL, intOffsetY);
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::ScrollLeftWithSelection() {
	int intSelectionX = this->intSquare2Pixel(this->GetVintColumnSelected());
	wxRect rctVisibleRegion = mOobjCanvas->GetVrctVisibleRegion();
	
	if(intSelectionX < rctVisibleRegion.GetLeft()) {
		// The whole cursor (or some part of it) overtook the left of the visible region. It's necessary to make a scroll.
		mOobjCanvas->DirectScroll(wxHORIZONTAL, intSelectionX);
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::ScrollRightWithSelection() {
	int intSelectionX = this->intSquare2Pixel(this->GetVintColumnSelected());
	wxRect rctVisibleRegion = mOobjCanvas->GetVrctVisibleRegion();
	int intOffsetX;
	
	if(intSelectionX > rctVisibleRegion.GetRight() - mVintSQUARE_LENGTH) {
		// The whole cursor (or some part of it) overtook the right of the visible region. It's necessary to make a scroll.
		intOffsetX = intSelectionX - (rctVisibleRegion.GetWidth() - mVintSQUARE_LENGTH);
		mOobjCanvas->DirectScroll(wxHORIZONTAL, intOffsetX);
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::UpdateCoordinates() {
	// TODO: implement
}

//____________________________________________________________________________________________________________________

void clsBoard::ScrollWithSelection() {
	ScrollUpWithSelection();
	ScrollDownWithSelection();
	ScrollLeftWithSelection();
	ScrollRightWithSelection();
}

//____________________________________________________________________________________________________________________

void clsBoard::SetFixedSelection() {
	mSobjFixedSelection->Set(this->GetVintColumnSelected(), this->GetVintRowSelected());
}

//____________________________________________________________________________________________________________________

bool clsBoard::blnFixedSelection() const { 
	if(mSobjFixedSelection.IsValid()) {
		return(!mSobjFixedSelection->blnEmpty());
	} else {
		// There is not a valid Fixed Selection object
		return(false);
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::EraseFixedSelection() {
	mSobjFixedSelection->Erase();
}

//____________________________________________________________________________________________________________________

void clsBoard::SetOobjObjectToInsert(clsPointer<clsBoardObject> pOobjObjectToInsert) {
	mOobjObjectToInsert = pOobjObjectToInsert;
	mOobjFrame->GetPobjStatusBar()->SetAction(_("Insert ") + pOobjObjectToInsert->GetVstrBaseDescription());
	
	// Set the action selected depending of what object was passed as parameter
	if(pOobjObjectToInsert->IsKindOf(CLASSINFO(clsTerrain))) {
		gOobjApplication->SetVienActionSelected(enmActionSelected_InsertSurface);
	} else {
		// It's going to insert a natural resource
		gOobjApplication->SetVienActionSelected(enmActionSelected_InsertNaturalResource);
	}
	
	mOobjCanvas->SetCursor(*mScrsInsertObject);
} 

//____________________________________________________________________________________________________________________

void clsBoard::SetEraser() {
	mOobjObjectToInsert = NULL;
	mOobjFrame->GetPobjStatusBar()->SetAction(_("Eraser"));
	gOobjApplication->SetVienActionSelected(enmActionSelected_Eraser);
	mOobjCanvas->SetCursor(*mScrsEraser);
}

//____________________________________________________________________________________________________________________

void clsBoard::ReleaseObjectToInsert() {
	mOobjObjectToInsert = NULL;
	mOobjFrame->GetPobjStatusBar()->SetAction(_T(""));
	gOobjApplication->SetVienActionSelected(enmActionSelected_Empty);
	mOobjCanvas->SetCursor(wxNullCursor);
}

//____________________________________________________________________________________________________________________

void clsBoard::CreateMouseCursors() {
	mScrsInsertObject = Make_CP(new wxCursor(wxCURSOR_PAINT_BRUSH));
	wxImage imgEraser;
	
	// Creates the eraser cursor from the eraser menu icon  in xpm format
	wxBitmap bmpEraser(eraser_xpm);
	imgEraser = bmpEraser.ConvertToImage();
	imgEraser.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_X, 7);
	imgEraser.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_Y, 7);
	mScrsEraser = Make_CP(new wxCursor(imgEraser));
}

//____________________________________________________________________________________________________________________

void clsBoard::AddObject() {
	if(gOobjApplication->GetVienActionSelected() == enmActionSelected_Eraser) {
		// The eraser tool is selected
		ExecuteEraser();
	
	} else if(mOobjObjectToInsert->IsKindOf(CLASSINFO(clsTerrain))) {
		// Insertion of a terrain
		if(this->blnFixedSelection()) {
			ApplyFixedSelection();
		} else {
			// Inserts a terrain in only one square
			clsPointer<clsTerrain> OobjTerrainToInsert = clsPointer_static_cast<clsTerrain>(mOobjObjectToInsert);
			SetTerrain(GetVintRowSelected(), GetVintColumnSelected(), OobjTerrainToInsert);
			this->Draw(GetVintRowSelected(), GetVintColumnSelected());
		}
	
	} else {
		// Insertion of a Natural Resource
		if(this->blnFixedSelection()) {
			ApplyFixedSelection();
		} else {
			// Inserts a Natural Resource object in only one square
			clsPointer<clsNaturalResource> OobjNaturalResourceToInsert = clsPointer_static_cast<clsNaturalResource>(mOobjObjectToInsert);
			SetNaturalResource(GetVintRowSelected(), GetVintColumnSelected(), OobjNaturalResourceToInsert);
			this->Draw(GetVintRowSelected(), GetVintColumnSelected());
		}
	}
}

//____________________________________________________________________________________________________________________

int clsBoard::GetVintRowSelected() const { 
	return(mSobjCursor->GetVpntPosition().y); // can't be an inline function because clsCursor is not defined in the header, it's only declared there
}

//____________________________________________________________________________________________________________________

void clsBoard::SetSelectedSquare(int pVintRow, int pVintColumn) {
	mSobjCursor->SetVpntPosition(wxPoint(pVintColumn, pVintRow));
}

//____________________________________________________________________________________________________________________

int clsBoard::GetVintColumnSelected() const {
	return(mSobjCursor->GetVpntPosition().x); // can't be an inline function because clsCursor is not defined in the header, it's only declared there
}

//____________________________________________________________________________________________________________________

void clsBoard::SetTerrain(int pVintRow, int pVintColumn, clsPointer<clsTerrain> pOobjTerrain, bool pVblnApplyAutoborder /*= true*/) {
	clsPointer<clsTerrain> OobjTerrain = OobjRandomizeTerrain(pOobjTerrain);

	mVarrTerrain.SetAt(pVintRow, pVintColumn, OobjTerrain);
	UpdateMap(pVintRow, pVintColumn);
	
	if(mVblnApplyAutoBorder) {
		// The auto-border function is enabled
		if(pVblnApplyAutoborder) {
			// It have to apply auto-border function around the terrain just inserted
			ApplyAutoBorder(pVintRow, pVintColumn, pOobjTerrain);
		}
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::ApplyAutoBorder(int pVintRow, int pVintColumn, clsPointer<clsTerrain> pOobjInsertedTerrain) {
	wxRect rctUpdatingArea(this->rctGetSurroundingArea(pVintRow, pVintColumn));
	int intRow;
	int intColumn;
	bool blnRightTerrain = false;
	
	// Determine if the terrain is able to be used in the auto-border function
	if(pOobjInsertedTerrain == gOobjApplication->GetOobjTerrain(_T("Water"))) {
		blnRightTerrain = true;
	} else if(pOobjInsertedTerrain == gOobjApplication->GetOobjTerrain(_T("Grassland"))) {
		blnRightTerrain = true;
	} else if(pOobjInsertedTerrain == gOobjApplication->GetOobjTerrain(_T("Mountain"))) {
		blnRightTerrain = true;
	}
	
	if(!blnRightTerrain) {
		// It can't apply auto-border to the terrain inserted
		return;
	}

	for(intRow = rctUpdatingArea.GetTop(); intRow < rctUpdatingArea.GetBottom() + 1; intRow++) {
		for(intColumn = rctUpdatingArea.GetLeft(); intColumn < rctUpdatingArea.GetRight() + 1; intColumn++) {
			if(intRow != pVintRow || intColumn != pVintColumn) {
				// The actual square isn't the insertion point
				if(pOobjInsertedTerrain == gOobjApplication->GetOobjTerrain(_T("Water"))) {
					fVblnAutoBorderSquare(wxPoint(intColumn, intRow), wxPoint(pVintColumn, pVintRow), pOobjInsertedTerrain, enmBaseType_GrassLand);
				
				} else if(pOobjInsertedTerrain == gOobjApplication->GetOobjTerrain(_T("Grassland"))) {
					if(!fVblnAutoBorderSquare(wxPoint(intColumn, intRow), wxPoint(pVintColumn, pVintRow), pOobjInsertedTerrain, enmBaseType_Water)) {
						// Could not perform auto-border with water.
						// Try with mountain
						fVblnAutoBorderSquare(wxPoint(intColumn, intRow), wxPoint(pVintColumn, pVintRow), pOobjInsertedTerrain, enmBaseType_Mountain);
					}
				
				} else if(pOobjInsertedTerrain == gOobjApplication->GetOobjTerrain(_T("Mountain"))) {
					fVblnAutoBorderSquare(wxPoint(intColumn, intRow), wxPoint(pVintColumn, pVintRow), pOobjInsertedTerrain, enmBaseType_GrassLand);
				}
				
			}
		}
	}
	
	if(mVblnAutoBorderInstantRefresh) {
		// The auto-border changes have to be updated immediately
		mOobjCanvas->Draw();
	}
}

//____________________________________________________________________________________________________________________

bool clsBoard::fVblnAutoBorderSquare(wxPoint pVpntToAnalyzedSquare, wxPoint pVpntInsertionSquare
                                     , clsPointer<clsTerrain> pOobjInsertedTerrain, enmBaseType pVienTargetBaseType) 
{
	int intRow;
	int intColumn;
	enmBaseType ienBaseTypeInserted = pOobjInsertedTerrain->GetVienBaseType();
	clsPointer<clsTerrain> OobjTerrainToInsert;
	unsigned int intTerrainCombination = 0; // number made of a combination of all the squares surrounding the analyzed square
	unsigned int intMask;
	int intPosition = 0; // position around the analyzed square
	enmAnalysisResult ienAnalysisReslt;

	for(intRow = pVpntToAnalyzedSquare.y - 1; intRow < pVpntToAnalyzedSquare.y + 2; intRow++) {
		for(intColumn = pVpntToAnalyzedSquare.x - 1; intColumn < pVpntToAnalyzedSquare.x + 2; intColumn++) {
			if(intRow == pVpntToAnalyzedSquare.y && intColumn == pVpntToAnalyzedSquare.x) {
				// The actual square is the analyzed one
				continue;
			}
			
			ienAnalysisReslt = fVienCheckSurroundingSquare(pVpntToAnalyzedSquare, wxPoint(intColumn, intRow), ienBaseTypeInserted, pVpntInsertionSquare, pVienTargetBaseType);
			
			if(ienAnalysisReslt == enmAnalysisResult_Wrong) {
				// The current terrain base doesn't match neither with the inserted nor with the target one
				// The auto-border action can't be completed
				return(false);
			}
			
			if(ienAnalysisReslt == enmAnalysisResult_Same) {
				// The current terrain base type is equal to the inserted one
				// Put a 1 in the correct binary position of intTerrainCombination
				intMask = (unsigned int) pow((double) 2, (double) intPosition);
				intTerrainCombination |= intMask;
			}

			intPosition++;
		}
	}
	
	// Extract the right terrain from the rule array
	if(ienBaseTypeInserted == enmBaseType_Water) {
		// Source base type: water - Target base type: grassland
		OobjTerrainToInsert = mVarrWaterAutoborderRules[intTerrainCombination];
	} else if(ienBaseTypeInserted == enmBaseType_Mountain) {
		// Source base type: mountain - Target base type: grassland
		OobjTerrainToInsert = mVarrMountainAutoborderRules[intTerrainCombination];
	} else {
		// Source base type: grassland
		if(pVienTargetBaseType == enmBaseType_Water) {
			// Target base type: water
			OobjTerrainToInsert = mVarrGrasslandWaterAutoborderRules[intTerrainCombination];
		} else {
			// Target base type: mountain
			OobjTerrainToInsert = mVarrGrasslandMountainAutoborderRules[intTerrainCombination];
		}
	}
	
	if(!OobjTerrainToInsert.blnValid()) {
		// There isn't a valid terrain for the current surrounding combination
		return(false);
	}
	
	SetTerrain(pVpntToAnalyzedSquare.y, pVpntToAnalyzedSquare.x, OobjTerrainToInsert, false /* Auto-border */);
	this->Draw(pVpntToAnalyzedSquare.y, pVpntToAnalyzedSquare.x);
	
	return(true);
}

//____________________________________________________________________________________________________________________

clsBoard::enmAnalysisResult clsBoard::fVienCheckSurroundingSquare(wxPoint pVpntAnalysisPositon
	, wxPoint pVpntSurroundingPosition, enmBaseType pVienInsertedBaseType
	, wxPoint pVpntInsertionPosition, enmBaseType pVienTargetBaseType) 
{
	int intDistanceX = abs(pVpntSurroundingPosition.x - pVpntInsertionPosition.x); // X distance from the surrounding square to the insertion one
	int intDistanceY = abs(pVpntSurroundingPosition.y - pVpntInsertionPosition.y); // Y distance from the surrounding square to the insertion one
	enmBoardDirection ienRelativePosition;	
	enmBaseType ienRelativeBaseType; // base type of the surrounding square, relative to the square analyzed
	clsPointer<clsTerrain> OobjSurroundingTerrain; // terrain surrounding the analyzed square
	
	if(intDistanceX < 2 && intDistanceY < 2) {
		// The surrounding square is next to the insertion one, so the base type is (or will be) equal between them
		return(enmAnalysisResult_Same);
	}
	
	if(fVblnCoordinateOutOfRange(pVpntSurroundingPosition.y, pVpntSurroundingPosition.x)) {
		// The actual coordinate is outside the board area
		// Supposed the same base type as the target
		return(enmAnalysisResult_Same);
	}
	
	OobjSurroundingTerrain = mVarrTerrain.GetAt(pVpntSurroundingPosition.y, pVpntSurroundingPosition.x);
	ienRelativePosition = fVienGetRelativePosition(pVpntSurroundingPosition, pVpntAnalysisPositon);
	ienRelativeBaseType = OobjSurroundingTerrain->GetVienBorderingBaseType(ienRelativePosition);
		                                                
	if(fVblnBaseTypesRelated(pVienInsertedBaseType, ienRelativeBaseType)) {
		// The base type is equal (or at least related) to the inserted one
		return(enmAnalysisResult_Same);
	} else if(fVblnBaseTypesRelated(pVienTargetBaseType, ienRelativeBaseType)) {
		// The base type is different from the inserted one, but equal (or at least related) to the base type found to perform the auto-border
		return(enmAnalysisResult_Different);
	} else {
		// The base type is different from both the inserted one and the base type found to perform the auto-border function
		return(enmAnalysisResult_Wrong);
	}
}

//____________________________________________________________________________________________________________________

bool clsBoard::fVblnBaseTypesRelated(enmBaseType pVienFirstBaseType, enmBaseType pVienSecondBaseType) {
	if(pVienFirstBaseType == enmBaseType_GrassLand) {
		if(pVienSecondBaseType == enmBaseType_GrassLand) {
			return(true);
		} else if(pVienSecondBaseType == enmBaseType_Coast) {
			return(true);
		} else if(pVienSecondBaseType == enmBaseType_Mount) {
			return(true);
		}
	
	} else if(pVienFirstBaseType == enmBaseType_Water) {
		if(pVienSecondBaseType == enmBaseType_Water) {
			return(true);
		} else if(pVienSecondBaseType == enmBaseType_Coast) {
			return(true);
		}
	
	} else if(pVienFirstBaseType == enmBaseType_Mountain) {
		if(pVienSecondBaseType == enmBaseType_Mountain) {
			return(true);
		} else if(pVienSecondBaseType == enmBaseType_Mount) {
			return(true);
		}
	}
	
	return(false);
}

//____________________________________________________________________________________________________________________

enmBoardDirection clsBoard::fVienGetRelativePosition(wxPoint pVpntCentralSquare, wxPoint pVpntSurroundingSquare) {
	int intDistanceX = pVpntCentralSquare.x - pVpntSurroundingSquare.x;
	int intDistanceY = pVpntCentralSquare.y - pVpntSurroundingSquare.y;
	
	return(mVarrRelativePositions.GetAt(intDistanceX + 1, intDistanceY + 1));
}

//____________________________________________________________________________________________________________________

wxRect clsBoard::rctGetSurroundingArea(int pVintRow, int pVintColumn) {
	wxRect rctResult(pVintColumn - 1, pVintRow - 1, 3, 3);

	//------------------------------------------------------------------------------------------------------------------
	// Fix bypass problems
	if(rctResult.GetLeft() == -1) {
		// The left limit has been overpass
		rctResult.SetWidth(2); // reduce the rectangle size
		rctResult.SetLeft(0);
	}
	if(rctResult.GetTop() < 0) {
		// The top limit has been overpass
		rctResult.SetHeight(2); // reduce the rectangle size
		rctResult.SetTop(0);
	}
	if(rctResult.GetRight() == mVintWidth) {
		// The right limit has been overpass
		rctResult.SetWidth(2); // reduce the rectangle size
		rctResult.SetRight(mVintWidth - 1);
	}
	if(rctResult.GetBottom() == mVintHeight) {
		// The bottom limit has been overpass
		rctResult.SetHeight(2); // reduce the rectangle size
		rctResult.SetBottom(mVintHeight - 1);
	}

	//------------------------------------------------------------------------------------------------------------------

	return(rctResult);
}

//____________________________________________________________________________________________________________________

bool clsBoard::fVblnCoordinateOutOfRange(int pVintRow, int pVintColumn) {
	if(pVintColumn < 0) {
		return(true);
	}
	if(pVintRow < 0) {
		return(true);
	}
	if(pVintColumn > mVintWidth - 1) {
		return(true);
	}
	if(pVintRow > mVintHeight - 1) {
		return(true);
	}
	
	return(false);
}

//____________________________________________________________________________________________________________________

void clsBoard::UpdateMap(int pVintRow, int pVintColumn) {
	clsPointer<clsBoardObject> OobjBoardObject;
	clsPointer<wxColour> OclrMapColor;

	if(this->blnBusySquare(pVintRow, pVintColumn)) {
		// There is an upper level object in the square
		OobjBoardObject = mVarrBoardObject.GetAt(pVintRow, pVintColumn);
	} else {
		// There is not an upper level object in the square. The terrain type is used instead.
		OobjBoardObject = mVarrTerrain.GetAt(pVintRow, pVintColumn);
	}

	OclrMapColor = OobjBoardObject->GetVclrMapColor();
	mOobjMap->DrawPixel(pVintRow, pVintColumn, *OclrMapColor);
}

//____________________________________________________________________________________________________________________

void clsBoard::UpdateEntireMap() {
	for(int intRow = 0; intRow < mVintHeight; intRow++) {
		for(int intColumn = 0; intColumn < mVintWidth; intColumn++) {
			UpdateMap(intRow, intColumn);
		}
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::SetNaturalResource(int pVintRow, int pVintColumn, clsPointer<clsNaturalResource> pOobjNaturalResourceType) {
	clsPointer<clsNaturalResource> OobjNaturalResource;
	
	if(mVarrBoardObject.GetAt(pVintRow, pVintColumn).blnValid()) {
		// The square is already occupied
		DeleteNaturalResource(pVintRow, pVintColumn);
	}

	OobjNaturalResource = pOobjNaturalResourceType->OobjCreateNew();
	mVarrBoardObject.SetAt(pVintRow, pVintColumn, OobjNaturalResource);
	
	OobjNaturalResource->SetVpntPosition(wxPoint(pVintColumn, pVintRow));
	this->Draw(pVintRow, pVintColumn);
	
	UpdateMap(pVintRow, pVintColumn);
}

//____________________________________________________________________________________________________________________

void clsBoard::ExecuteEraser() {
	if(this->blnFixedSelection()) {
		ApplyFixedSelection();
	} else {
		// Erases only one square
		EraseSquare(GetVintRowSelected(), GetVintColumnSelected());
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::EraseSquare(int pVintRow, int pVintColumn) {
	if(mVarrBoardObject.GetAt(pVintRow, pVintColumn).blnValid()) {
		// There is an object at the upper level
		DeleteNaturalResource(pVintRow, pVintColumn);
		this->Draw(pVintRow, pVintColumn);
		UpdateMap(pVintRow, pVintColumn);
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::EraseSelectedSquare() {
	EraseSquare(GetVintRowSelected(), GetVintColumnSelected());
}

//____________________________________________________________________________________________________________________

void clsBoard::ApplyFixedSelection() {
	int intFromRow, intToRow;
	int intFromColumn, intToColumn;
	clsPointer<clsTerrain> OobjTerrainToInsert;
	clsPointer<clsNaturalResource> OobjNaturalResourceToInsert;
	bool blnApplyAutoBorder;
	bool blnPrevAutoBorderInstantRefresh = mVblnAutoBorderInstantRefresh;
	
	mVblnAutoBorderInstantRefresh = false; // only for optimize the surface inserts
	
	// ------------------------------------------------------------------------------------------------------------------
	// Initialization of variables
	
	if(gOobjApplication->GetVienActionSelected() == enmActionSelected_InsertSurface) {
		// Insertion of surface
		OobjTerrainToInsert = clsPointer_static_cast<clsTerrain>(mOobjObjectToInsert);
	} else if(gOobjApplication->GetVienActionSelected() == enmActionSelected_InsertNaturalResource) {
		// Insertion of natural resource
		OobjNaturalResourceToInsert = clsPointer_static_cast<clsNaturalResource>(mOobjObjectToInsert);
	}

	// ------------------------------------------------------------------------------------------------------------------
	// Determines the begin and the end for the nested loops
	
	if(mSobjFixedSelection->GetVintRow() > this->GetVintRowSelected()) {
		// The row loop must be execute in reverse order
		intFromRow = this->GetVintRowSelected();
		intToRow = mSobjFixedSelection->GetVintRow() + 1; // +1 to use the comparison operator "<" rather than "<="
	} else {
		intFromRow = mSobjFixedSelection->GetVintRow();
		intToRow = this->GetVintRowSelected() + 1; // +1 to use the comparison operator "<" rather than "<="
	}
	
	if(mSobjFixedSelection->GetVintColumn() > this->GetVintColumnSelected()) {
		// The column loop must be execute in reverse order
		intFromColumn = this->GetVintColumnSelected();
		intToColumn = mSobjFixedSelection->GetVintColumn() + 1; // +1 to use the comparison operator "<" rather than "<="
	} else {
		intFromColumn = mSobjFixedSelection->GetVintColumn();
		intToColumn = this->GetVintColumnSelected() + 1; // +1 to use the comparison operator "<" rather than "<="
	}
	
	// ------------------------------------------------------------------------------------------------------------------
	// Go over the selected area
	
	for(int intRow = intFromRow; intRow < intToRow; intRow++) {
		for(int intCol = intFromColumn; intCol < intToColumn; intCol++) {
			if(gOobjApplication->GetVienActionSelected() == enmActionSelected_Eraser) {
				// The eraser tool is selected
				EraseSquare(intRow, intCol);
			} else if(gOobjApplication->GetVienActionSelected() == enmActionSelected_InsertSurface) {
				// Insertion of surface
				
				// Determine if it has to apply the auto-border function
				if(intRow == intFromRow || intRow == intToRow - 1) {
					// It is in the upper or lower limit of the fixed selection area
					blnApplyAutoBorder = true;
				} else if(intCol == intFromColumn || intCol == intToColumn - 1) {
					// It is in the left or right limit of the fixed selection area
					blnApplyAutoBorder = true;
				} else {
					// It is processed an internal square in the fixed selection area
					blnApplyAutoBorder = false;
				}
				
				SetTerrain(intRow, intCol, OobjTerrainToInsert);
				this->Draw(intRow, intCol);
			} else {
				// Insertion of natural resource
				SetNaturalResource(intRow, intCol, OobjNaturalResourceToInsert);
				this->Draw(intRow, intCol);
			}
		}
	}
	
	// ------------------------------------------------------------------------------------------------------------------
	
	mVblnAutoBorderInstantRefresh = blnPrevAutoBorderInstantRefresh; // restore the previous setting
	mOobjCanvas->Draw(); // applies the changes to the visible area
	mOobjMap->ShowMap();
	this->EraseFixedSelection();
}

//____________________________________________________________________________________________________________________

void clsBoard::DeleteNaturalResource(int pVintRow, int pVintColumn) {
	clsPointer<clsBoardObject> OobjNaturalResource = mVarrBoardObject.GetAt(pVintRow, pVintColumn);
	
	safemap::erase(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNaturalResource->GetVstrName());
	safemap::erase(gOobjApplication->GetVdctNaturalResources(), OobjNaturalResource->GetVstrName());
	
	mVarrBoardObject.SetAt(pVintRow, pVintColumn, NULL);
	UpdateMap(pVintRow, pVintColumn);
}

//____________________________________________________________________________________________________________________

clsPointer<clsTerrain> clsBoard::OobjRandomizeTerrain(clsPointer<clsTerrain> pOobjTerrain) {
	int intTerrainType;

	if(pOobjTerrain->GetVstrName() == _T("Grassland")) {
		intTerrainType = clsLibrary::intGetRandomNumber(1, 2);

		if(intTerrainType == 1) {
			return(pOobjTerrain);
		} else {
			return(gOobjApplication->GetOobjTerrain(_T("Grassland2")));
		}
	} else if(pOobjTerrain->GetVstrName() == _T("Water")) {
		intTerrainType = clsLibrary::intGetRandomNumber(1, 2);

		if(intTerrainType == 1) {
			return(pOobjTerrain);
		} else {
			return(gOobjApplication->GetOobjTerrain(_T("Water2")));
		}
	} else {
		// The terrain passed isn't grassland nor water
		return(pOobjTerrain);
	}
}

//____________________________________________________________________________________________________________________

clsPointer<clsUpperLevelObject> clsBoard::OobjGetUpperLevelObject() {
	int intRow = this->GetVintRowSelected();
	int intColumn = this->GetVintColumnSelected();

	return(mVarrBoardObject.GetAt(intRow, intColumn));
}

//____________________________________________________________________________________________________________________

clsPointer<clsTerrain> clsBoard::OobjGetTerrain() {
	int intRow = this->GetVintRowSelected();
	int intColumn = this->GetVintColumnSelected();

	return(mVarrTerrain.GetAt(intRow, intColumn));
}

//____________________________________________________________________________________________________________________

void clsBoard::ProcessMouseLeftClick() {
	if(fVblnCanInsertOnDrag()) {
		// It is possible to insert an object with a mouse left click
		// Apply the selected tool
		this->AddObject();
		mOobjFrame->UpdateEnvironment();
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::ProcessMouseRightClick() {
	ExecuteEraser();
	mOobjFrame->UpdateEnvironment();
}

//____________________________________________________________________________________________________________________

void clsBoard::ProcessOnMouseEvent(wxMouseEvent& event) {
	wxPoint pntSquare = this->fVrctGetSquareFromPixel(event.GetX(), event.GetY()); // square corresponding to the coordinates where the mouse is overing
	
	if(pntSquare.x >= mVintWidth || pntSquare.x < 0 || pntSquare.y >= mVintHeight || pntSquare.y < 0) {
		// The given coordinates reference to a square out of range
		// This test must be done because while the user is draggin with his mouse the coordinates can be overpass the board limits
		return;
	}
	
	if(event.Dragging() && event.LeftIsDown()) {
		// The user is performing a dragging with the mouse left button
		if(fVblnCanInsertOnDrag()) {
			// It's possible to insert objects on the board while dragging
			if(this->GetVintRowSelected() != pntSquare.y || this->GetVintColumnSelected() != pntSquare.x) {
				// The mouse is over a new square
				mOobjCanvas->OnLeftDown(event);
			}
		}
	} else if(event.Dragging() && event.RightIsDown()) {
		// The user is performing a dragging with the right button of the mouse
		if(this->GetVintRowSelected() != pntSquare.y || this->GetVintColumnSelected() != pntSquare.x) {
			// The mouse is over a new square
			mOobjCanvas->OnRightDown(event);
		}
	}
}

//____________________________________________________________________________________________________________________

bool clsBoard::fVblnCanInsertOnDrag() {
	if(gOobjApplication->GetVienState() != enmStates_BoardEdition) {
		// The user isn't editing the board
		return(false);
	}

	if(gOobjApplication->GetVienActionSelected() == enmActionSelected_Empty) {
		// There is no object selected to insert
		return(false);
	}

	return(true);
}

//____________________________________________________________________________________________________________________

wxPoint clsBoard::fVrctGetSquareFromPixel(int pVintX, int pVintY) {
	wxRect rctVisibleRegion = mOobjCanvas->GetVrctVisibleRegion();
	double dblValue;
	wxPoint pntSquare;

	dblValue = (rctVisibleRegion.GetTop() + pVintY) / this->GetVintSQUARE_LENGTH();
	pntSquare.y = int(floor(dblValue));

	dblValue = (rctVisibleRegion.GetLeft() + pVintX) / this->GetVintSQUARE_LENGTH();
	pntSquare.x = int(floor(dblValue));

	return(pntSquare);
}

//____________________________________________________________________________________________________________________

void clsBoard::Save() {
	c4_View viwBoard;
	c4_Row objRow;
	wxString strStructure;
	
	strStructure = strGetPersistenceStructure();	
	viwBoard = gOobjApplication->GetOdbfDatabase()->GetAs(strStructure);
	
	mVprpStrTitle(objRow) = mVstrTitle;
	mVprpStrDescription(objRow) = mVstrDescription;
	mVprpIntHeight(objRow) = mVintHeight;
	mVprpIntWidth(objRow) = mVintWidth;
	mVprpIntPlayers(objRow) = mVintNumberPlayers;
	
	SavePreview(objRow);
	viwBoard.Add(objRow);

	SaveBoardContent();
}

//____________________________________________________________________________________________________________________

void clsBoard::Open() {
	c4_View viwBoard;
	c4_Row objRow;
	wxString strStructure;
	
	strStructure = strGetPersistenceStructure();	
	viwBoard = gOobjApplication->GetOdbfDatabase()->GetAs(strStructure);
	objRow = viwBoard[0];

	mVstrTitle = mVprpStrTitle(objRow);
	mVstrDescription = mVprpStrDescription(objRow);
	mVintHeight = mVprpIntHeight(objRow);
	mVintWidth = mVprpIntWidth(objRow);
	mVintNumberPlayers = mVprpIntPlayers(objRow);
	
	LoadBoardContent();
	
	// Make a new cursor at (0,0) position
	mSobjCursor = Make_CP(new clsCursor());
	mOobjCanvas->SetOobjCursor(mSobjCursor.GetPtr()); // sets the shortcut to clsCursor
	
	// Make a new fixed selection
	mSobjFixedSelection = Make_CP(new clsFixedSelection());
	
	mVblnApplyAutoBorder = true;
}

//_______________________________________________________________________________________________________________________

wxString clsBoard::strGetPersistenceStructure() {
	wxString strResult;
	
	strResult = _T("Board[");
	strResult += _T("strTitle:S");
	strResult += _T(",strDescription:S");
	strResult += _T(",intHeight:I");
	strResult += _T(",intWidth:I");
	strResult += _T(",intPlayers:I");
	strResult += _T(",bytPreview:B");
	strResult += _T("]");
	
	return(strResult);
}

//____________________________________________________________________________________________________________________

wxString clsBoard::strGetBoardContentStructure() {
	wxString strResult;
	
	strResult = _T("BoardContent[");
	strResult += _T("strSurface:S");
	strResult += _T(",strUpperObject:S"); // can be an empty string
	strResult += _T("]");
	
	return(strResult);
}

//____________________________________________________________________________________________________________________

void clsBoard::SaveBoardContent() {
	c4_View viwBoardContent;
	wxString strStructure;
	c4_Row objRow;
	
	strStructure = strGetBoardContentStructure();
	viwBoardContent = gOobjApplication->GetOdbfDatabase()->GetAs(strStructure);

	for(int intRow = 0; intRow < mVintHeight; intRow++) {
		for(int intColumn = 0; intColumn < mVintWidth; intColumn++) {
			mVprpStrSurface(objRow) = mVarrTerrain.GetAt(intRow, intColumn)->GetVstrName();
			
			if(this->blnBusySquare(intRow, intColumn)) {
				// The current square is busy at the upper level
				mVprpStrUpperObject(objRow) = mVarrBoardObject.GetAt(intRow, intColumn)->GetVstrName();
			} else {
				// The current square is empty at the upper level
				mVprpStrUpperObject(objRow) = "";
			}
			
			viwBoardContent.Add(objRow);
		}
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::LoadBoardContent() {
	c4_View viwBoardContent;
	wxString strStructure;
	c4_Row objRow;
	int i = 0;
	wxString strTerrainName;
	wxString strUpperLevelObjectName;
	clsPointer<clsTerrain> OobjTerrain;
	clsPointer<clsUpperLevelObject> OobjUpperLevelObject;
	
	mVarrTerrain.resize(mVintWidth, mVintHeight);
	mVarrBoardObject.resize(mVintWidth, mVintHeight);
	
	strStructure = strGetBoardContentStructure();
	viwBoardContent = gOobjApplication->GetOdbfDatabase()->GetAs(strStructure);
	
	for(int intRow = 0; intRow < mVintHeight; intRow++) {
		for(int intColumn = 0; intColumn < mVintWidth; intColumn++) {
			objRow = viwBoardContent[i];
			
			strTerrainName = mVprpStrSurface(objRow);
			OobjTerrain = gOobjApplication->GetOobjTerrain(strTerrainName);
			mVarrTerrain.SetAt(intRow, intColumn, OobjTerrain);
			
			strUpperLevelObjectName = mVprpStrUpperObject(objRow);
			if(strUpperLevelObjectName != "") {
				// The current square is busy at the upper level
				OobjUpperLevelObject = gOobjApplication->GetOobjUpperLevelObject(strUpperLevelObjectName);
				mVarrBoardObject.SetAt(intRow, intColumn, OobjUpperLevelObject);
			}
			
			UpdateMap(intRow, intColumn);
			i++;
		}
	}
}

//____________________________________________________________________________________________________________________

void clsBoard::SavePreview(c4_Row& pRrowRow) {
	wxMemoryOutputStream stmOutput;
	void* PvodPreviewData = NULL;
	wxImage imgPreview = mOobjMap->imgGetPreview();
	
	if (!imgPreview.SaveFile(stmOutput, wxBITMAP_TYPE_PNG)) {
		wxMessageBox(_("Error writing preview data"), _("Error"), wxICON_ERROR);
		return;
	}
	
	PvodPreviewData = malloc(stmOutput.GetSize());
	if (PvodPreviewData == NULL) {
		wxMessageBox(_("Can not assign memory for the preview image"), _("Error"), wxICON_ERROR);
		return;
	}
	
	if (stmOutput.CopyTo(PvodPreviewData, stmOutput.GetSize()) != stmOutput.GetSize()) {
		wxMessageBox(_("Error copying icon data to memory"), _("Error"), wxICON_ERROR);
		free(PvodPreviewData);
		return;
	}
	
	c4_Bytes objBytes(PvodPreviewData, stmOutput.GetSize());
	mVprpBytPreview(pRrowRow) = objBytes;
	
	free(PvodPreviewData);
}
