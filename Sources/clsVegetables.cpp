#include "clsVegetables.h"

#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

//.......................................................................................................................
// Includes
#include "Global.h"
#include "throwmap.h"
#include "ClsApplication.h"

//.......................................................................................................................
// Definition of static members

int clsVegetables::sVintInstances;
CountedPtr<wxColour> clsVegetables::sSclrMapColor;
int clsVegetables::sVintMaxHealth;
int clsVegetables::sVintMaxCreation;
int clsVegetables::sVintMaxConstruction;
wxImage clsVegetables::sVimgStaticRepresentation;
wxImage clsVegetables::sVimgEmptyGraphic;
wxString clsVegetables::sVstrBaseDescription;
wxString clsVegetables::sVstrInternalTypeName;

//_______________________________________________________________________________________________________________________

clsVegetables::clsVegetables() {
	NormalConstructor();
}

//_______________________________________________________________________________________________________________________

clsVegetables::clsVegetables(bool pVblnObjectType) {
	if(pVblnObjectType) {
		// Construction of the class Object Type
		ObjectTypeConstructor();
	} else {
		NormalConstructor();
	}
}

//_______________________________________________________________________________________________________________________

void clsVegetables::NormalConstructor() {
	mVintHealth = sVintMaxHealth;
	mVintCreation = sVintMaxCreation;
	mVintConstruction = sVintMaxConstruction;
	mVimgDynamicRepresentation = this->GetVimgStaticRepresentation(); // at the beginning, the representation must be equal to the static one (a non empty resource)
}

//_______________________________________________________________________________________________________________________

void clsVegetables::ObjectTypeConstructor() {
	mVstrName = _T("Vegetables");
	sVstrInternalTypeName = mVstrName; // to share the base type among all the specific child objects
	sVstrBaseDescription = _("Vegetables");

	sSclrMapColor = Make_CP(new wxColor(100, 143, 91));
	sVintMaxHealth = 1;
	sVintMaxCreation = 40;
	sVintMaxConstruction = 0;
	
	this->SetOffsetImage(wxPoint(160, 120));
	this->SetOffsetImageEmpty(wxPoint(360, 120));
}

//_______________________________________________________________________________________________________________________

clsVegetables::~clsVegetables()
{

}

//_______________________________________________________________________________________________________________________

void clsVegetables::ResetCounter() {
	sVintInstances = 0;
}

//_______________________________________________________________________________________________________________________

clsPointer<clsNaturalResource> clsVegetables::OobjCreateNew() {
	clsPointer<clsVegetables> OobjNewInstance = new clsVegetables();
	wxString strInstanceNumber;
	
	sVintInstances++;
	strInstanceNumber = wxString::Format(_T("%d"), sVintInstances);

	OobjNewInstance->SetVstrName(mVstrName + strInstanceNumber);
	OobjNewInstance->SetVstrDescription(sVstrBaseDescription + " " + strInstanceNumber);
	
	// Adds the new object on the corresponding collection
	safemap::insert(gOobjApplication->GetVdctNaturalResources(), OobjNewInstance->GetVstrName(), Make_CP(OobjNewInstance.get()));
	safemap::insert(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNewInstance->GetVstrName(), OobjNewInstance);
	
	return(OobjNewInstance);
}

//_______________________________________________________________________________________________________________________

clsPointer<clsNaturalResource> clsVegetables::OobjCreateFromFile(c4_RowRef pVrowRow) {
	clsPointer<clsVegetables> OobjNewInstance = new clsVegetables();
	
	OobjNewInstance->Load(pVrowRow);
	
	// Adds the new object on the corresponding collection
	safemap::insert(gOobjApplication->GetVdctNaturalResources(), OobjNewInstance->GetVstrName(), Make_CP(OobjNewInstance.get()));
	safemap::insert(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNewInstance->GetVstrName(), OobjNewInstance);
	
	return(OobjNewInstance);
}

//_______________________________________________________________________________________________________________________

void clsVegetables::ShowContent(clsPointer<wxTreeCtrl> pOtrcContent) {
	// TODO: maybe the best solution here is to hide the wxTreeCtrl when a non container object is selected
}
