// Array template class 
#ifndef ARRAYTEMPLATE_H
#define ARRAYTEMPLATE_H

#include <iostream>
#include <assert.h>
using namespace std;

//---------------------------------------------------------------------------
// Fixed Length Array class:  like an array (retains all functionality) but also
//   includes additional features:
//   -- allows input and output of the whole array
//   -- allows for comparison of 2 arrays, element by element
//   -- allows for assignment of 2 arrays
//   -- size is part of the class (so no longer needs to be passed)
//   -- includes range checking, program terminates for out-of-bound subscripts
//
// Assumptions:
//   -- size defaults to a fixed size of 10 if size is not specified
//   -- in <<, display 10 per line
//
// [Germ�n Blando] Aditional notes:
//   This class is used because the wxWidgets' wxArray didn't call the destructors
//   when it stored smart pointers with ownership.
//   The author of this class is unknown.
//---------------------------------------------------------------------------

template <typename T>
class Array {
   // g++ insists you include the <T> after the fn name;
   // I believe you must remove the <T> after operator<<
   // for it to compile using .net
   friend ostream& operator<< (ostream &, const Array &);

public:
   Array(int = 10);                        // default constructor
   Array(const Array &);                   // copy constructor

   ~Array();                               // destructor
   int getSize() const;                    // return size of array
   const Array& operator=(const Array &);  // assign arrays
   bool operator==(const Array &) const;   // compare for equal of 2 arrays
   bool operator!=(const Array &) const;   // compare not equal of 2 arrays
   T& operator[](int);                   // subscript operator

private:
   T* ptr;                               // pointer to first element of array
   int size;                               // size of the array
};

//-------------------------- Constructor ------------------------------------
// Default constructor for class Array
template <typename T>
Array<T>::Array(int arraySize) {
   size = (arraySize > 0 ? arraySize : 10); // default size is 10
   ptr = new T[size];                     // create space for array
   assert(ptr != NULL);                   // terminate if memory not allocated
}

//---------------------------- Copy -----------------------------------------
// Copy constructor for class Array
template <typename T>
Array<T>::Array(const Array<T> &init) {
   size = init.size;                   
   ptr = new T[size];                 
   assert(ptr != NULL);              

   for (int i = 0; i < size; i++)
      ptr[i] = init.ptr[i];         
}

//---------------------------- Destructor -----------------------------------
// Destructor for class Array
template <typename T>
Array<T>::~Array() {
   delete [] ptr;   
   ptr = NULL;
}

//----------------------------- getSize -------------------------------------
// Get the size of the array
template <typename T>
int Array<T>::getSize() const { return size; }

//-----------------------------  =  -----------------------------------------
// Overloaded assignment operator
template <typename T>
const Array<T>& Array<T>::operator=(const Array<T>& right) {
   if (&right != this) {               // check for self-assignment
      delete [] ptr;     
      size = right.size;
      ptr = new T[size];  
      assert(ptr != NULL); 

      for (int i = 0; i < size; i++)
         ptr[i] = right.ptr[i]; 
   }

   return *this;               
}

//------------------------------  ==  ---------------------------------------
// Determine if two arrays are equal.
// The type or object T has the responsibility for overloading the operator!=
template <typename T>
bool Array<T>::operator==(const Array<T>& right) const {
   if (size != right.size)
      return false;                    // arrays of different sizes

   for (int i = 0; i < size; i++)
      if (ptr[i] != right.ptr[i])
         return false;                 // arrays are not equal
   return true;                        // arrays are equal
}

//--------------------------------  !=  -------------------------------------
// Determine if two arrays are not equal.
template <typename T>
bool Array<T>::operator!=(const Array<T>& right) const {
   return !(*this == right);
}

//-------------------------------  []  --------------------------------------
// Overloaded subscript operator, terminates if subscript out of range error
template <typename T>
T& Array<T>::operator[](int subscript) {
   assert(0 <= subscript && subscript < size);
   return ptr[subscript];               // reference return creates lvalue
}

//-----------------------------  <<  ----------------------------------------
// Overloaded output operator for class Array
template <typename T>
ostream& operator<<(ostream &output, const Array<T> &a) {
   int i;
   for (i = 0; i < a.size; i++) {
      output << a.ptr[i] << ' ';
      if ((i + 1) % 10 == 0)           // display 10 per line
         output << endl;
   }

   if (i % 10 != 0)
      output << endl;
   return output;    
}

#endif
