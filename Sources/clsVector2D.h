#include "stdafx.h"

#include <vector>
using namespace std;

#include <wx/debug.h>

/**
* Fixed size two-dimensional array with range checking in debug mode.
*/
template <class T>
class clsVector2D
{
public:
	clsVector2D():m_dimRow(0), m_dimCol(0){;}
	
	//___________________________________________________________________________________________________________________
	
	clsVector2D(unsigned int nRow, unsigned int nCol) {
		m_dimRow = nRow;
		m_dimCol = nCol;
		Initialize(nRow, nCol);
	}
	
	//...................................................................................................................
	
	// Overload for accept an int instead of an unsigned int
	clsVector2D(int nRow, int nCol) {
		unsigned int uinRow = static_cast<unsigned int>(nRow);
		unsigned int uinCol = static_cast<unsigned int>(nCol);

		m_dimRow = nRow;
		m_dimCol = nCol;
		
		Initialize(uinRow, uinCol);
	}

	//___________________________________________________________________________________________________________________
	
	void SetAt(unsigned int nRow, unsigned int nCol, const T& value) {
		wxASSERT(nRow>= 0 && nRow < m_dimRow && nCol >= 0 && nCol < m_dimCol);
		m_2DVector[nRow][nCol] = value;
	}
	
	//...................................................................................................................
	
	// Overload for accept an int instead of an unsigned int
	void SetAt(int nRow, int nCol, const T& value) {
		unsigned int uinRow = static_cast<unsigned int>(nRow);
		unsigned int uinCol = static_cast<unsigned int>(nCol);
		
		SetAt(uinRow, uinCol, value);
	}

	//___________________________________________________________________________________________________________________
	
	T GetAt(unsigned int nRow, unsigned int nCol) {
		wxASSERT(nRow>= 0 && nRow < m_dimRow && nCol >= 0 && nCol < m_dimCol);
		return m_2DVector[nRow][nCol];
	}
	
	//...................................................................................................................
	
	// Overload for accept an int instead of an unsigned int
	T GetAt(int nRow, int nCol) {
		unsigned int uinRow = static_cast<unsigned int>(nRow);
		unsigned int uinCol = static_cast<unsigned int>(nCol);

		return GetAt(uinRow, uinCol);
	}
	
	//___________________________________________________________________________________________________________________
	
	void GrowRow(unsigned int newSize) {
		if (newSize <= m_dimRow)
			return;
		m_dimRow = newSize;
		for(unsigned int i = 0 ; i < newSize - m_dimCol; i++)   {
			vector<int> x(m_dimRow);
			m_2DVector.push_back(x);
		}
	}

	//...................................................................................................................
	
	// Overload for accept an int instead of an unsigned int
	void GrowRow(int newSize) {
		unsigned int uinNewSize = static_cast<unsigned int>(newSize);
		GrowRow(uinNewSize);
	}

	//___________________________________________________________________________________________________________________
	
	void GrowCol(unsigned int newSize) {
		if(newSize <= m_dimCol)
			return;
		m_dimCol = newSize;
		for (unsigned int i=0; i <m_dimRow; i++)
			m_2DVector[i].resize(newSize);
	}

	//...................................................................................................................
	
	// Overload for accept an int instead of an unsigned int
	void GrowCol(int newSize) {
		unsigned int uinNewSize = static_cast<unsigned int>(newSize);
		GrowCol(uinNewSize);
	}
	
	//___________________________________________________________________________________________________________________
	// This operator is disallowed to ensure the use of the methods "GetAt" and "SetAt"
	//	vector<T>& operator[](int x)    {
	//		return m_2DVector[x];
	//	}

	//___________________________________________________________________________________________________________________
	
	// Specifies a new size for a 2D vector. This new size can be greater or smaller
	void resize(unsigned int pVintRows, unsigned int pVintColums) {
		if(pVintRows != m_dimRow) {
			// The number of rows has changed
			m_2DVector.resize(pVintRows);
			m_dimRow = pVintRows;
		}

		if(pVintColums != m_dimCol) {
			// The number of columns has changed
			for(unsigned int i = 0; i < m_dimRow; i++) {
				m_2DVector[i].resize(pVintColums);
			}

			m_dimCol = pVintColums;
		}
	}
	
	//...................................................................................................................
	
	// Overload for accept an int instead of an unsigned int
	void resize(int pVintRows, int pVintColums) {
		unsigned int uinRows = static_cast<unsigned int>(pVintRows);
		unsigned int uinColumns = static_cast<unsigned int>(pVintColums);
		resize(uinRows, uinColumns);
	}
	
//_______________________________________________________________________________________________________________________

private:
	vector< vector <T> > m_2DVector;
	unsigned int m_dimRow;
	unsigned int m_dimCol;
	
	//___________________________________________________________________________________________________________________
	
	// Join the initialization tasks for all the constructors
	void Initialize(unsigned int pVuinRows, unsigned int pVuinColumns) {
		for (unsigned int i = 0; i < pVuinRows; i++){
			vector<T> x(pVuinColumns);
			m_2DVector.push_back(x);
		}
	}
};
