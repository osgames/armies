// clsToolBar.cpp: implementation of the clsToolBar class.
//
//////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

/*!
 * Includes
 */

#include "stdafx.h"
#include "clsToolBar.h"

//------------------------------------------------------------------------------------------------------------------------

clsToolBar::clsToolBar(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style
					   , const wxString& name)
					   : wxToolBar(parent, id, pos, size, style, name)
{
}

//------------------------------------------------------------------------------------------------------------------------

clsToolBar::~clsToolBar()
{
	EmptyTools();
}

//------------------------------------------------------------------------------------------------------------------------

void clsToolBar::EmptyTools() {
	for(int i = 0; i < mVarrToolObjects.size(); i++) {
		if(mVarrToolObjects[i].get() != NULL) {
			// There is a wxMenu object stored here
			delete(mVarrToolObjects[i].get());
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------

wxToolBarToolBase* clsToolBar::AddTool(int toolId, const wxString& label, const wxBitmap& bitmap1, const wxString& shortHelpString, wxItemKind kind) {
	wxToolBarToolBase* PobjTool = wxToolBar::AddTool(toolId, label, bitmap1, shortHelpString, kind);
	AddToolToArrays(toolId, PobjTool);
	
	return(PobjTool);
}

//------------------------------------------------------------------------------------------------------------------------

wxToolBarToolBase* clsToolBar::AddTool(int toolId, const wxString& label, const wxBitmap& bitmap1, const wxBitmap& bitmap2, wxItemKind kind, const wxString& shortHelpString, const wxString& longHelpString, wxObject* clientData) {
	wxToolBarToolBase* PobjTool = wxToolBar::AddTool(toolId, label, bitmap1, bitmap2, kind, shortHelpString, longHelpString, clientData);
	AddToolToArrays(toolId, PobjTool);
	
	return(PobjTool);
}

//------------------------------------------------------------------------------------------------------------------------

wxToolBarToolBase* clsToolBar::AddTool(wxToolBarToolBase* tool) {
	wxToolBarToolBase* PobjTool = wxToolBar::AddTool(tool);
	AddToolToArrays(tool->GetId(), PobjTool);
	
	return(PobjTool);
}

//------------------------------------------------------------------------------------------------------------------------

void clsToolBar::AddToolToArrays(int pVintToolId, clsPointer<wxToolBarToolBase> pOobjTool) {
	mVarrToolIDs.Add(pVintToolId);
	mVarrToolObjects.push_bak(NULL);
	mVarrToolVisibilities.push_bak(1);
}

//------------------------------------------------------------------------------------------------------------------------

void clsToolBar::ShowTool(int pVintToolId) {
	int intScreenPosition = intScreenPositionTool(pVintToolId);
	int intArrayPosition = intArrayPositionTool(pVintToolId);
	
	this->InsertTool(intScreenPosition, mVarrToolObjects[intArrayPosition]);
	mVarrToolVisibilities[intArrayPosition] = 1;
	mVarrToolObjects[intArrayPosition] = NULL;
}

// ----------------------------------------------------------------------------------------------------------------------

void clsToolBar::HideTool(int pVintToolId) {
	int intArrayPosition = intArrayPositionTool(pVintToolId);
	
	mVarrToolObjects[intArrayPosition] = this->FindById(pVintToolId);
	mVarrToolVisibilities[intArrayPosition] = 0;

	this->RemoveTool(pVintToolId);
}

// ----------------------------------------------------------------------------------------------------------------------

int clsToolBar::intScreenPositionTool(int pVintToolId) {
	int intPosition = 0;
	
	for(unsigned int i = 0; i < mVarrToolIDs.GetCount(); i++) {
		if(mVarrToolIDs[i] == pVintToolId) {
			// It's the wanted tool
			return(intPosition);
			
		} else {
			if(mVarrToolVisibilities[i] == 1) {
				// The current tool is visible
				intPosition++;
			}
		}
	}
	
	// If the execution reach this point, it means that the tool button ID can't be found
	
	wxString strError = _("The object wxToolBarToolBase cannot be found: ");
	strError += wxString::Format("%i", pVintToolId);
	strError += _(" - Function: intScreenPositionTool");
	
	wxMessageBox(strError, _("Error"), wxICON_ERROR);
	
	return(-1);
}

// ----------------------------------------------------------------------------------------------------------------------

int clsToolBar::intArrayPositionTool(int pVintToolId) {
	for(unsigned int i = 0; i < mVarrToolIDs.GetCount(); i++) {
		if(mVarrToolIDs[i] == pVintToolId) {
			// It's the wanted tool
			return(i);
		}
	}
	
	// If the execution reach this point, it means that the tool button ID can't be found
	
	wxString strError = _("The object wxToolBarToolBase cannot be found: ");
	strError += wxString::Format("%i", pVintToolId);
	strError += _(" - Function: intArrayPosition");
	
	wxMessageBox(strError, _("Error"), wxICON_ERROR);
	
	return(-1);
}
