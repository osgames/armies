// clsLibrary.h: interface for the clsLibrary class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLSLIBRARY_H__AC1CABBA_137F_43D9_9455_FEFC0DA062FA__INCLUDED_)
#define AFX_CLSLIBRARY_H__AC1CABBA_137F_43D9_9455_FEFC0DA062FA__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

#include <wx/textctrl.h>

class clsLibrary
{
public:
	clsLibrary();
	virtual ~clsLibrary();

	/**
	* Verifies if a wxTextCtrl is completed
	*/
	static bool blnFieldCompleted(wxTextCtrl* PpTxtControl, const wxString& PrStrLabel);
	
	/**
	* Seeds random number generator
	*/
	static void RandomSeed();
	
	/**
	* Generates a new random number in the range of accepted values
	* Based on: Bob Jacobs' tutorial. Link: http://www.robertjacobs.fsnet.co.uk/random.htm
	* @param lowest_number Minimum value in the accepted values range
	* @param highest_number Maximum value in the accepted values range
	* @return A random number between lowest_number and highest_number
	*/
	static int intGetRandomNumber(int lowest_number, int highest_number);
	
	/**
	* Parses an string by using a given separator and return the result in the same array passed as parameter
	*/
	static wxArrayString* Split(const wxString& PstrCadena, const wxString& PstrSeparador, wxArrayString& ParrResultado);

	/**
	* Shows a message to the user in a modal dialog.
	* This message can content several lines.
	* @param pVstrMessage The message to show to the user
	*/
	static void ShowError(wxString pVstrError);

	/**
	* Convert a number from double to integer (int), rounding the decimal part if it exists
	*/
	static int intFromDouble(double pVdblNumber);

	/**
	* If a negative number is passed, it returns zero, else it returns the same number.
	*/
	inline static int intNegativeToZero(int pVintValue) {
		if(pVintValue < 0) {
			return(0);
		} else {
			return(pVintValue);
		}
	}
	
	/**
	* Check for a directory existence. If it doesn't exist, then create it.
	*/
	static void EnsureDirectoryExistence(const wxString& pRstrDirectory);
};

#endif // !defined(AFX_CLSLIBRARY_H__AC1CABBA_137F_43D9_9455_FEFC0DA062FA__INCLUDED_)
