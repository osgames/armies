//-----------------------------------------------------------------------------------------------------------------------
// Contains all variable settings (not constants). Some of them are set by the user.
// Besides, it stores, load, and manage their edition.
// There is only one instance of this class.
//-----------------------------------------------------------------------------------------------------------------------

#if !defined(AFX_CLSOPTIONS_H__7A664E9B_D70F_46F3_9640_FD9218739161__INCLUDED_)
#define AFX_CLSOPTIONS_H__7A664E9B_D70F_46F3_9640_FD9218739161__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

#include <wx/config.h>
#include "jrb_ptr.h"

/*!
 * Namespaces
 */
using namespace jrb_sptr;

class clsOptions
{
public:
	virtual ~clsOptions();

	// Getters
	wxString GetVstrBoardsDirectory() const { return(MVstrBoardsDirectory); }
	wxString GetVstrGamesDirectory() const { return(MVstrGamesDirectory); }
	wxString GetVstrGraphicsDirectory() const { return(MVstrGraphicsDirectory); }

	wxString GetVstrGraphicsFileName() const { return(MVstrGraphicsFileName); }

	// Return of singleton instance
	static CountedPtr<clsOptions> GetInstance();

protected:
	clsOptions();

private:
	void Load();

	// Singleton instance
	static CountedPtr<clsOptions> mSobjInstance;

	wxString MVstrBoardsDirectory;
	wxString MVstrGamesDirectory;
	wxString MVstrGraphicsDirectory;

	wxString MVstrGraphicsFileName;
	bool MVblnFirstExecution; // for determine when occurs the firs execution of the game in a given machine
	wxConfig* MPcnfConfig; // stores the wxWidgets' configuration object
};

#endif // !defined(AFX_CLSOPTIONS_H__7A664E9B_D70F_46F3_9640_FD9218739161__INCLUDED_)
