#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "ClsApplication.h"

// -----------------------------------------------------------------------------------------------------------------------
// Embedded images

#include "../Graficos Desarrollo/Iconos/16x16/content.xpm"

// -----------------------------------------------------------------------------------------------------------------------
// Variables
clsPointer<clsApplication> gOobjApplication;
