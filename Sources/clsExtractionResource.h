// clsExtractionResource.h: interface for the clsExtractionResource class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLSEXTRACTIONRESOURCE_H__B3F7109C_5B62_4B00_A06C_4FA07F2F1D9B__INCLUDED_)
#define AFX_CLSEXTRACTIONRESOURCE_H__B3F7109C_5B62_4B00_A06C_4FA07F2F1D9B__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

class clsExtractionResource
{
public:
	clsExtractionResource();
	virtual ~clsExtractionResource();

};

#endif // !defined(AFX_CLSEXTRACTIONRESOURCE_H__B3F7109C_5B62_4B00_A06C_4FA07F2F1D9B__INCLUDED_)
