/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This header will declare all container classes, using pointers for contained types and declaring them with forward
// declarations.
// This is made to avoid possible cross dependencies between headers and classes.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _CONTAINERS_H_
#define _CONTAINERS_H_

#include <wx/hashmap.h>
#include <wx/dynarray.h>
#include <wx/gdicmn.h>
#include "jrb_ptr.h"
#include "clsPointer.h"

// Namespaces

using namespace jrb_sptr;

// Forward declarations
class clsNaturalResource;
class clsPlayer;
class clsColor;
class clsBuilding;
class clsMobileUnit;
class clsExtractionResource;
class clsUpperLevelObject;
class wxToolBarToolBase;
class clsTerrain;

// -----------------------------------------------------------------------------------------------------------------------
// Container declarations

// Containers without ownership
WX_DECLARE_STRING_HASH_MAP(clsPointer<clsUpperLevelObject>, cls_dco_clsUpperLevelObject);
WX_DECLARE_STRING_HASH_MAP(clsPointer<clsNaturalResource>, cls_dco_clsNaturalResource);
WX_DECLARE_STRING_HASH_MAP(clsPointer<clsBuilding>, cls_dco_clsBuilding);
WX_DECLARE_STRING_HASH_MAP(clsPointer<clsMobileUnit>, cls_dco_clsMobileUnit);
WX_DECLARE_STRING_HASH_MAP(clsPointer<clsPlayer>, cls_dco_clsPlayer);

// Containers with ownership
WX_DECLARE_STRING_HASH_MAP(CountedPtr<clsNaturalResource>, cls_dct_clsNaturalResource);
WX_DECLARE_STRING_HASH_MAP(CountedPtr<clsColor>, cls_dct_clsColor);
WX_DECLARE_STRING_HASH_MAP(CountedPtr<clsPlayer>, cls_dct_clsPlayer);
WX_DECLARE_STRING_HASH_MAP(CountedPtr<clsBuilding>, cls_dct_clsBuilding);
WX_DECLARE_STRING_HASH_MAP(CountedPtr<clsMobileUnit>, cls_dct_clsMobileUnit);
WX_DECLARE_STRING_HASH_MAP(CountedPtr<clsExtractionResource>, cls_dct_clsExtractionResource);
WX_DECLARE_STRING_HASH_MAP(CountedPtr<clsTerrain>, cls_dct_clsTerrain);
WX_DECLARE_STRING_HASH_MAP(int, cls_dct_Integer);

#endif // _CONTAINERS_H_
