#if !defined(AFX_CLSCURSOR_H__DEADD53E_F61D_4DFE_AD21_E23390265EE2__INCLUDED_)
#define AFX_CLSCURSOR_H__DEADD53E_F61D_4DFE_AD21_E23390265EE2__INCLUDED_

#include "stdafx.h"

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */
#include "arraytemplate.h"
#include "clsPointer.h"
#include "jrb_ptr.h"

/*!
 * Forward declarations
 */
class clsBoard;
class clsCanvas;
class clsFrame;

/*!
 * Namespaces
 */
using namespace jrb_sptr;

//_______________________________________________________________________________________________________________________

/**
* This class is created when a new board is constructed, and destroyed when the board is destroyed.
*/
class clsCursor
{

//_______________________________________________________________________________________________________________________

public:
	clsCursor();
	virtual ~clsCursor();

	//....................................................................................................................
	// Getters / Setters

	virtual wxPoint GetVpntPosition() const { return(wxPoint(mVintColumn, mVintRow)); }

	virtual void SetVpntPosition(wxPoint pVpntPosition)
	{
		mVintColumn = pVpntPosition.x;
		mVintRow = pVpntPosition.y;
	}

	//....................................................................................................................
	// Methods

	// Draw a new cursor slide in the screen
	void Draw();

	// Set the static shortcuts that the class need
	static void SetShortcuts();

	// Loads the pictures to make the cursor animation
	static void LoadGraphics();

	// Move the cursor one square up
	void MoveUp();

	// Move the cursor one square down
	void MoveDown();

	// Move the cursor one square left
	void MoveLeft();

	// Move the cursor one square right
	void MoveRight();

	// Move the cursor one page up
	void MovePageUp();

	// Move the cursor one page down
	void MovePageDown();

	// Move the cursor one page left
	void MovePageLeft();

	// Move the cursor one page right
	void MovePageRight();

	// Select the square that contains the point passed as parameter
	void SelectSquareFromPixel(int pVintPixelX, int pVintPixelY);

// -----------------------------------------------------------------------------------------------------------------------
protected:

	// Shortcuts
	static clsPointer<clsBoard> mOobjBoard;
	static clsPointer<clsCanvas> mOobjCanvas;
	clsPointer<clsFrame> mOobjFrame;

//-----------------------------------------------------------------------------------------------------------------------
private:

	// Stores the current slide showed in order to make the cursor animation
	int mVintSequence;

	// Row position in the board. Y-axis position.
	int mVintRow;

	// Column position in the board. X-axis position.
	int mVintColumn;

	static Array<wxImage> mVarrSlides;

	static const int mVintSlidesCount;

	//....................................................................................................................
	// Methods

	void AdvanceSecuence();

	// Execute a serie of actions that take place after change the cursor position
	void PostProcessMovementCursor(int pVintPrevRowPosition, int pVintPrevColumnPosition);
};

#endif // !defined(AFX_CLSCURSOR_H__DEADD53E_F61D_4DFE_AD21_E23390265EE2__INCLUDED_)
