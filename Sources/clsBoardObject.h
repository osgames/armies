#if !defined(AFX_CLSBOARDOBJECT_H__97A15F7D_A0FD_4CBE_AA64_D02F120FFC59__INCLUDED_)
#define AFX_CLSBOARDOBJECT_H__97A15F7D_A0FD_4CBE_AA64_D02F120FFC59__INCLUDED_

#include "stdafx.h"

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */

#include <wx/colour.h>
#include <wx/string.h>
#include <wx/image.h>
#include <mk4.h>

#include "jrb_ptr.h"
#include "clsPointer.h"

/*!
 * Namespaces
 */
using namespace jrb_sptr;

/*!
 * Forward declarations
 */
class clsBoard;
class clsCanvas;
class clsFrame;

//_______________________________________________________________________________________________________________________

/**
 * Represents any object that can be contained in the board
 * All objects derived from this class should implement RTTI features
 * This class inherits from wxObject only to implement RTTI functions without any problem
 */
class clsBoardObject : public wxObject
{
DECLARE_ABSTRACT_CLASS(clsBoardObject) // to enable RTTI
public:
	clsBoardObject();
	virtual ~clsBoardObject();

	//....................................................................................................................
	// Getters / Setters

	virtual wxString GetVstrBaseDescription() = 0;
	virtual void SetVstrBaseDescription(wxString pVstrBaseDescription) = 0;
	
	virtual wxString GetVstrDescription() const { return(mVstrDescription); }
	virtual void SetVstrDescription(const wxString& PVstrDescription) { mVstrDescription = PVstrDescription; }

	virtual wxString GetVstrName() const { return(mVstrName); }
	virtual void SetVstrName(const wxString& PVstrName) { mVstrName = PVstrName; }

	virtual wxPoint GetVpntPosition() const { return(mVpntPosition); }
	virtual void SetVpntPosition(wxPoint pVpntPosition) { mVpntPosition = pVpntPosition; }

	virtual wxString GetVstrTypeDescription() const = 0; // @ToOverride
	virtual void SetVstrTypeDescription(const wxString pVstrTypeDescription) = 0; // @ToOverride

	virtual wxColour* GetVclrMapColor() const = 0; // @ToOverride
	
	//....................................................................................................................
	// Methods

	/**
	 * Draws the current object into the secondary screen
	 * @param pVpntOffset Place to draw the object
	 */
	virtual void Draw(wxPoint pVpntOffset);
	
	/**
	* Return a string containing the structure to persist objects of a specific class.
	*/
	static wxString strGetPersistenceStructure();
	
	/**
	* Persist the object in a file
	*/
	virtual void Save(c4_Row& pRrowRow);

	/**
	* Load the object properties from a file
	*/
	virtual void Load(c4_RowRef pVrowRow);

//_______________________________________________________________________________________________________________________

protected:

	//....................................................................................................................
	// Shortcuts
	
	clsPointer<clsBoard> mOobjBoard;
	clsPointer<clsCanvas> mOobjCanvas;
	clsPointer<clsFrame> mOobjFrame;

	//....................................................................................................................
	// Variables
	
	/**
	 * Identifies uniquely a given object. For internal use only (e.g. in collection's keys)
	 */
	wxString mVstrName;
	
	/**
	* Represents the board object aspect at a given moment
	*/
	wxImage mVimgDynamicRepresentation;

	//....................................................................................................................
	// Getters / Setters
	
	/**
	 * Sets the offset to load the sector of image corresponding to the actual object
	 * Additionally, execute the load of the image
	 */
	virtual void SetOffsetImage(const wxPoint& PVpntOffset);
	
	/**
	* @ToOverride
	*/
	virtual wxImage GetVimgStaticRepresentation() const = 0;
	
	/**
	* @ToOverride
	*/
	virtual void SetVimgStaticRepresentation(wxImage pVimgGraphic) = 0;

//_______________________________________________________________________________________________________________________

private:
	/**
	 * User friendly name. This property must be translated
	 */
	wxString mVstrDescription;

	/**
	 * Area from where the object must load your own image
	 */
	wxRect mVrctSource;

	/**
	 * Row and column (in square units) where the object is inserted, beginning with zero
	 * In some situations, the point coordinates would be [-1, -1], when the object is contained in some other
	 * parent object.
	 * For clsTerrain, this property is set just before a terrain have to be drawn.
	 */
	wxPoint mVpntPosition;

	//.............................................................................................................
	// Metakit properties

	static c4_StringProp sVprpStrName;
	static c4_StringProp sVprpStrDescription;
	static c4_IntProp sVprpIntPositionX;
	static c4_IntProp sVprpIntPositionY;

};

#endif // !defined(AFX_CLSBOARDOBJECT_H__97A15F7D_A0FD_4CBE_AA64_D02F120FFC59__INCLUDED_)
