#if !defined(AFX_CLSCANVAS_H__C7F0E6C4_0580_42DB_8A15_604D6366495C__INCLUDED_)
#define AFX_CLSCANVAS_H__C7F0E6C4_0580_42DB_8A15_604D6366495C__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */

#include "wx/scrolwin.h"
#include "jrb_ptr.h"
#include "clsPointer.h"

/*!
 * Namespaces
 */

using namespace jrb_sptr;

/*!
 * Forward declarations
 */

class clsBoard;
class clsCursor;
class clsMap;
class clsFrame;

//_______________________________________________________________________________________________________________________

class clsCanvas : public wxWindow
{
public:
	clsCanvas(wxWindow *parent, int PvIntWidth, int PvintHeight);
	~clsCanvas();

	//-------------------------------------------------------------------------------------------------------------------
	// Getters / Setters

	CountedPtr<wxBitmap> GetSbmpSecondaryScreen() const { return(mSbmpSecondaryScreen); }
	wxRect GetVrctVisibleRegion() const { return(mVrctVisibleRegion); }
	
	void SetOobjBoard(clsPointer<clsBoard> value) { mOobjBoard = value; }
	void SetOobjCursor(clsPointer<clsCursor> value) { mOobjCursor = value; }
	void SetOobjMap(clsMap* value) { mOobjMap = value; }
	void SetOobjFrame(clsFrame* value) { mOobjFrame = value; }
	
	int GetVintHorizontalLongScroll() const { return(mVintHorizontalLongScroll); }
	int GetVintVerticalLongScroll() const { return(mVintVerticalLongScroll); }

	bool GetVblnDrawOnScroll() const { return(mVblnDrawOnScroll); }
	void SetVblnDrawOnScroll(bool value) { mVblnDrawOnScroll = value; }

	//-------------------------------------------------------------------------------------------------------------------
	// Events

	void Window_OnSize(wxSizeEvent& WXUNUSED(event));
	void Window_OnPaint(wxPaintEvent &event);
	void Window_OnScrollWinLineUp(wxScrollWinEvent& event);
	void Window_OnScrollWinLineDown(wxScrollWinEvent& event);
	void Window_OnScrollWinPageUp(wxScrollWinEvent& event);
	void Window_OnScrollWinPageDown(wxScrollWinEvent& event);
	void Window_OnScrollWinThumbTrack(wxScrollWinEvent& event);
	void Window_OnScrollWinTop(wxScrollWinEvent& event);
	void Window_OnScrollWinBottom(wxScrollWinEvent& event);
	void Window_OnScrollThumbRelease(wxScrollWinEvent& event);
	void Window_MouseWheel(wxMouseEvent& event);
	void OnKeyDown(wxKeyEvent& event);
	void OnLeftDown(wxMouseEvent& event);
	void OnRightDown(wxMouseEvent& event);
	void OnMouseEvent(wxMouseEvent& event);

	//-------------------------------------------------------------------------------------------------------------------
	// Methods

	// Returns the updatable part of the area giving in the parameter.
	// This updatable part is the intersection with the visible area.
	// In addition, due the fact that this method is called always before doing an update to some square, it updates
	// the modifications accumulator too. This way, it's possible to actualize later the primary screen.
	wxRect rctUpdatableArea(wxRect pVrctToUpdateArea);

	// Draws an image inside the secondary screen.
	// If this function is called, it means that clsCanvas.rctUpdatableArea returned a non empty rectangle.
	// The updates accumulator (clsCanvas.rctUpdatableArea) is not updated here, but in the function
	// clsCanvas.rctUpdatableArea. This is because this method would be called more than once for the same region,
	// for example one time for the terrain and another time for the object in the surface.
	// Though, clsCanvas.rctUpdatableArea only are called once before the update of all the layer in a square.
	void DrawSecondary(wxImage pVimgImage, wxRect pVrctTarget, wxPoint pVpntOffset);

	// Updates the visible region of the game
	void SetvRctVisibleRegion(int pVintVerticalOffset = -1, int pVintHorizontalOffset = -1);


	// Set the width and height of the total surface to show, making scrolls when necessary
	void SetTotalSize(int pVintWidth, int pVintHeight);

	// Copies the content of the secondary screen into the primary screen.
	// For doing this action, it access to MvRctDirtyRegion and draws only the areas that need to be updated,
	// unless pVblnRedrawAll is set. In the last case, all the primary screen is refreshed.
	// pVblnRefreshMap: when it's true, the map window graphic is showed again.
	void Draw(bool pVblnRedrawAll = false, bool pVblnRefreshMap = true);

	// Set the scrollbars in concordance with the part of the secondary screen that is showed
	// mVrctVisibleRegion must be set previously
	void AdjustScrollbars();

	/**
	* Scrolls in horizontal or vertical orientation, to a specific offset.
	* The scrollbars are updated.
	* The visible region is updated, unless mVblnDrawOnScroll is false.
	* No range checking is done at all inside this method, so you have to do it before calling it.
	*/
	void DirectScroll(int pVintOrientation, int pVintOffset);

//_______________________________________________________________________________________________________________________

private:
	
	//-------------------------------------------------------------------------------------------------------------------
	// Variables
	
	// It uses a double buffer for reduce flicking. This buffer only includes the visible area, rather than all the surface.
	// The primary goal of it is to optimize refresh tasks, when the content of the window must be repainted.
	// Additionally, this buffer can group several changes to the visible area, and copy then all together in a unique transaction.
	// To avoid continuous resizing in the Secondary Screen, this bitmap size will be fixed to the screen dimensions. This will 
	// ensure the highest size wont never be exceed.
	CountedPtr<wxBitmap> mSbmpSecondaryScreen;

	// Contains the sector of the game that is visible. It works in board coordinates and pixel unites.
	wxRect mVrctVisibleRegion;

	// Modifications accumulator. Stores a region of the secondary screen containing all the modifications done.
	// It works in board coordinates and pixel unites.
	// Its origin must match with the origin of the mVrctVisibleRegion variable, talking in board coordinates system
	wxRect MvRctDirtyRegion;

	// Stores the total width of the virtual surface to show, in pixels
	int mVintTotalWidth;

	// Stores the total height of the virtual surface to show, in pixels
	int mVintTotalHeight;

	// Enables or disables the automatic redraw inside the scroll events
	bool mVblnDrawOnScroll;

	// To manage line up and down events from horizontal scrollbars
	int mVintHorizontalShortScroll;

	// To manage page up and down events from horizontal scrollbars
	int mVintHorizontalLongScroll;

	// To manage line up and down events from vertical scrollbars
	int mVintVerticalShortScroll;

	// To manage page up and down events from vertical scrollbars
	int mVintVerticalLongScroll;

	// Maximum horizontal displacement supported
	int mVintMaxHorizontalDisp;

	// Maximum vertical displacement supported
	int mVintMaxVerticalDisp;

	// To manage the mouse wheel
	int mVintWheelRotation;

	// ...................................................................................................................
	// Shortcuts

	clsPointer<clsBoard> mOobjBoard;
	clsPointer<clsCursor> mOobjCursor;
	clsPointer<clsMap> mOobjMap;
	clsPointer<clsFrame> mOobjFrame;

	// ---------------------------------------------------------------------------------------------------------------------
	// Methods

	// Makes an horizontal scroll in the primary screen. pVintLength can be a negative number
	void ScrollHorizontal(int pVintLength);

	// Makes a vertical scroll in the primary screen. pVintLength can be a negative number
	void ScrollVertical(int pVintLength);

	// After a scroll event, it calculates which part of the primary screen has been only displaced and which one must
	// be draw as a new area to display. This distinction is in order to optimize the scrolls, calling to clsBoard
	// only to redraw the part of the board that was not display previously, and displace the rest.
	void DrawScrollOptimized(int pVintPrevDisplacement, int pVintOrientation);

	// ---------------------------------------------------------------------------------------------------------------------

	DECLARE_EVENT_TABLE()
};

#endif // !defined(AFX_CLSCANVAS_H__C7F0E6C4_0580_42DB_8A15_604D6366495C__INCLUDED_)
