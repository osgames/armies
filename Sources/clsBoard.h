// clsBoard.h: interface for the clsBoard class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLSBOARD_H__68E88277_A1FD_4FF9_BC2C_B02B9B035795__INCLUDED_)
#define AFX_CLSBOARD_H__68E88277_A1FD_4FF9_BC2C_B02B9B035795__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */

#include <vector>
#include <math.h>
#include <mk4.h>

#include "Containers.h"
#include "clsVector2D.h"
#include "clsPointer.h"
#include "Enumerations.h"
#include "arraytemplate.h"

/*!
 * Namespaces
 */
using namespace std;

/*!
 * Forward declarations
 */
class clsBoardObject;
class clsTerrain;
class clsCanvas;
class clsCursor;
class clsFixedSelection;
class clsFrame;
class clsUpperLevelObject;
class clsMap;

//_______________________________________________________________________________________________________________________

/**
 * Manages all the objects contained on the board
 */
class clsBoard
{
public:
	clsBoard();
	virtual ~clsBoard();

	//-------------------------------------------------------------------------------------------------------------------
	// Getters / Setters (public)

	int GetVintSQUARE_LENGTH() const { return(mVintSQUARE_LENGTH); }
	int GetVintWidth() const { return(mVintWidth); }
	int GetVintHeight() const { return(mVintHeight); }
	
	void SetvIntNumberPlayers(int pVintNumberPlayers) { mVintNumberPlayers = pVintNumberPlayers; }
	int GetVintNumberPlayers() const { return(mVintNumberPlayers); }

	void SetvStrTitle(const wxString& pRstrTitle) { mVstrTitle = pRstrTitle; }
	wxString GetVstrTitle() const { return(mVstrTitle); }
	
	void SetvStrDescription(const wxString& pRstrDescription) { mVstrDescription = pRstrDescription; }
	wxString GetVstrDescription() const { return(mVstrDescription); }
	
	int GetVintRowSelected() const;
	int GetVintColumnSelected() const;

	clsPointer<clsBoardObject> GetOobjObjectToInsert() const { return(mOobjObjectToInsert); }
	void SetOobjObjectToInsert(clsPointer<clsBoardObject> pOobjObjectToInsert);

	CountedPtr<clsCursor> GetSobjCursor() const { return(mSobjCursor); }
	CountedPtr<clsFixedSelection> GetSobjFixedSelection() const { return(mSobjFixedSelection); }
	
	int GetVintMAX_SQUARES() const { return(mVintMAX_SQUARES); }

	bool GetVblnApplyAutoBorder() const { return(mVblnApplyAutoBorder); }
	void SetVblnApplyAutoBorder(bool pVblnValue) { mVblnApplyAutoBorder = pVblnValue; }

	//-------------------------------------------------------------------------------------------------------------------
	// Methods (public)

	/**
	 * Create a new board 
	 * The board must be in "Empty" state before call to this method.
	 * In special, bidimensional arrays must be empty to.
	 */ 
	void New(int PvIntWidth, int PvIntHeight);

	/**
	 * Perform some clear task when the board is going to be disabled
	 */
	void ChangeEmpty();

	/**
	 * Redraws all the visible area of the board in the secondary screen
	 */
	void DrawAll();

	/**
	 * Draws a specific cell. Only if it's necessary, according to the area of the board that is shown at the moment.
	 */
	void Draw(int PvIntRow, int PvIntColumn);

	/**
	 * Draws a specific area. This area is expressed in pixel unit and board coordinate.
	 * If the area doesn't fit exactly the begin and the end of any given square, this function extends the area until
	 * it fit an exact number of squares.
	 */
	void DrawPixelArea(wxRect pVrctArea);

	/**
	 * Set the fixed selection with the square selected
	 */
	void SetFixedSelection();

	/**
	 * Returns the offset and dimensions corresponding to a given square. This area is expressed in pixel unit and
	 * board coordinate.
	 */
	wxRect rctSquareArea(int PvIntRow, int PvIntColumn);

	/**
	 * Convert a rectangle from pixels unit to squares.
	 * If the area doesn't fit exactly the begin and the end of any given square, this function extends the area until
	 * it fit an exact number of squares.
	 */
	wxRect rctPixel2Square(const wxRect& PvRctPixelArea) const;

	/**
	 * Convert a single dimension from square unit to pixel unit
	 */
	int intSquare2Pixel(int pVintSquareDimension) { return(pVintSquareDimension * mVintSQUARE_LENGTH); }

	/**
	 * Convert a single dimension from pixel unit to square unit.
	 * If the dimension doesn't fit an exact number of squares, this function extends the result to the next integer
	 * result.
	 */
	int intPixel2Square(int pVintPixelDimension) { return((int) ceil((double) (pVintPixelDimension / mVintSQUARE_LENGTH))); }

	/**
	 * Change the area coordinate from board to screen coordinate
	 */
	wxRect rctBoard2Screen(wxRect pVrctBoardCoordinateArea);

	/**
	 * Returns true if there is a fixed selection already set in the board
	 */
	bool blnFixedSelection() const;

	/**
	 * Returns true if the given square stores a upper level object
	 */
	bool blnBusySquare(int PvIntRow, int PvIntColumn) { return(mVarrBoardObject.GetAt(PvIntRow, PvIntColumn).blnValid()); }
	
	/**
	* @Overload
	* Returns true if the selected square stores a upper level object
	*/
	bool blnBusySquare() { return(this->blnBusySquare(this->GetVintRowSelected(), this->GetVintColumnSelected())); }

	/**
	 * Multiples calls to this method make the cursor animation. This method must be call from a timer event.
	 */
	void DrawCursor();

	/**
	 * If the selected square overtakes the limits of the visible area of the board, this function performs a scroll 
	 * to show it.
	 */
	void ScrollWithSelection();

	/**
	* Move the visible area so that the selected square stays in the middle of the window
	*/
	void CenterSelection();

	/**
	 * Update the selected square coordinates showed in the screen
	 */
	void UpdateCoordinates();

	/**
	 * Clear the actual fixed selection
	 */
	void EraseFixedSelection();

	/**
	 * Adds the selected terrain or upper level object in the current position of the cursor
	 */
	void AddObject();

	/**
	 * Releases the current object selected to do insertions on the board
	 */
	void ReleaseObjectToInsert();
	
	/**
	* Selects the eraser object
	*/
	void SetEraser();
	
	/**
	* Deletes the upper level object in the active square, if any.
	* The selected object doesn't matter in this case.
	*/
	void EraseSelectedSquare();

	/**
	* Return the upper level object placed in the selected square (null if it doesn't exist)
	*/
	clsPointer<clsUpperLevelObject> OobjGetUpperLevelObject();
	
	/**
	* Return the terrain type in the selected square
	*/
	clsPointer<clsTerrain> OobjGetTerrain();
	
	/**
	* Change the selected square
	*/
	void SetSelectedSquare(int pVintRow, int pVintColumn);

	/**
	* Return the area surrounding a given square.
	* The returning rectangle is expressed in board coordinates and square units.
	* The maximum length for the returning rectangle is 3 squares, unless the center square is in a board border.
	* In that case the rectangle will be cut in order to don't bypass the board limits.
	* The returning rectangle can be used, for example, in a for loop to do something.
	*/
	wxRect rctGetSurroundingArea(int pVintRow, int pVintColumn);

	/**
	* Process a mouse left click event fired in the clsCanvas object
	*/
	void ProcessMouseLeftClick();

	/**
	* Process a mouse right click event fired in the clsCanvas object
	*/
	void ProcessMouseRightClick();
	
	/**
	* Process a generic mouse event fired in the clsCanvas object
	*/
	void ProcessOnMouseEvent(wxMouseEvent& event);

	/**
	* Return a pointer object representing the square that is containing the pixel referred by the input
	* parameters. It's useful, for example, to select a square from a mouse click.
	*/
	wxPoint fVrctGetSquareFromPixel(int pVintX, int pVintY);
	
	/**
	* Fill the arrays used in the auto-border functionality
	*/
	void FillArraysAutoborderRules();

	/**
	* Save the current board
	*/
	void Save();

	/**
	* Open the current board
	*/
	void Open();

//_______________________________________________________________________________________________________________________

private:

	//-------------------------------------------------------------------------------------------------------------------
	// Constants / Enumerations (private)
	
	/**
	* It's useful as a result of the analysis of the surrounding squares
	*/
	enum enmAnalysisResult {
		enmAnalysisResult_Same // when the base type of the surrounding square is equal to the current one
		, enmAnalysisResult_Different // when the base type of the surrounding square is different to the current one, but it has the complementary base type to make a border
		, enmAnalysisResult_Wrong // when the base type of the surrounding square is different to the current one, and it hasn't the complementary base type to make a border
	};
	
	//-------------------------------------------------------------------------------------------------------------------
	// Methods (private)

	/**
	 * Load images required by the object
	 */
	void LoadImages();

	/**
	 * If the selected square overtakes the top limit of the visible area of the board, this function performs a
	 * scroll to show it. 
	 * Return true if an scroll has happened.
	 */
	void ScrollUpWithSelection();

	/**
	 * If the selected square overtakes the bottom limit of the visible area of the board, this function performs 
	 * a scroll to show it.
	 * Return true if an scroll has happened.
	 */
	void ScrollDownWithSelection();

	/**
	 * If the selected square overtakes the left limit of the visible area of the board, this function performs a 
	 * scroll to show it.
	 * Return true if an scroll has happened.
	 */
	void ScrollLeftWithSelection();

	/**
	 * If the selected square overtakes the right limit of the visible area of the board, this function performs a
	 * scroll to show it.
	 * Return true if an scroll has happened.
	 */
	void ScrollRightWithSelection();

	/**
	 * Create all the mouse cursors needed in the application
	 */
	void CreateMouseCursors();

	/**
	 * Sets a terrain type in a specific board square
	 */
	void SetTerrain(int pVintRow, int pVintColumn, clsPointer<clsTerrain> pOobjTerrain, bool pVblnApplyAutoborder = true);

	/**
	 * Sets a natural resource object in a specific board square. It's calling while editing a board.
	 */
	void SetNaturalResource(int pVintRow, int pVintColumn, clsPointer<clsNaturalResource> pOobjNaturalResourceType);

	/**
	 * Deletes the natural resource that stay in the indicated square. It's calling while editing a board.
	 */
	void DeleteNaturalResource(int pVintRow, int pVintColumn);
	
	/**
	 * To create an effect of "irregularity", both grassland and water have two versions slightly different,
	 * and the program interchange between this two versions randomly at the moment of inserting.
	 * The two types of grassland are "Grassland" and "Grassland2", and the two types of water are "Water"
	 * and "Water2".
	 * When inserting, the client code always indicates the primary types ("Grassland" or "Water") and this
	 * function recognizes these types and decides what type to use in a random way.
	 */
	clsPointer<clsTerrain> OobjRandomizeTerrain(clsPointer<clsTerrain> pOobjTerrain);
	
	/**
	* Executes the eraser tool for the square or squares selected
	*/
	void ExecuteEraser();
	
	/**
	* Erases the indicated square.
	* Note: you have to call to mOobjCanvas->Draw() after one or more call to this method, in order to update the
	* screen.
	*/
	void EraseSquare(int pVintRow, int pVintColumn);
	
	/**
	* Uses the current selected object in all the squares included inside the fixed selection.
	*/
	void ApplyFixedSelection();

	/**
	* Update the map in relation with a specific square
	*/
	void UpdateMap(int pVintRow, int pVintColumn);
	
	/**
	* Update the map completely
	*/
	void UpdateEntireMap();

	/**
	* Execute the auto-border function in the specific insertion square
	*/
	void ApplyAutoBorder(int pVintRow, int pVintColumn, clsPointer<clsTerrain> pOobjInsertedTerrain);

	/**
	* Apply the auto-border function to a specific square based on the surrounding terrains.
	* pVpntToAnalyzedSquare: square to analyzed to arrange its terrain type.
	* pVpntInsertioSquare: square where the user inserted the terrain that triggered the auto-border function.
	* @param pVienTargetBaseType The base type found to perform the auto-border.
	* @return True if the auto-border could be performed, otherwise false.
	*/
	bool fVblnAutoBorderSquare(wxPoint pVpntToAnalyzedSquare, wxPoint pVpntInsertionSquare
	                           , clsPointer<clsTerrain> pOobjInsertedTerrain, enmBaseType pVienTargetBaseType);
	
	/**
	* Return the base type for a given square, considering its proximity to the insertion point and its base type.
	*/
	enmBaseType fVienGetTerrainBaseType(clsPointer<clsTerrain> pOobjTerrain, wxPoint pVpntSquare
	                                    , enmBaseType pVienBaseTypeInserted, wxPoint pVpntInsertionPoint);
	
	/**
	* Check a surrounding square in the process of determining which is the best terrain to make an auto-border.
	* @param pVpntAnalysisPositon the position of the square that is analyzed to put an auto-border on it.
	* @param pVpntSurroundingPosition a square surrounding the analysis position. This analysis requires that all the surrounding squares are analyzed.
	* @param pVpntInsertionPosition the position in where the terrain was inserted manually by the user
	* @param pVienTargetBaseType a base type different from the inserted one, to make the auto-border with it. If other base type
	* different from these two is found, then this function will return enmAnalysisResult_Wrong, and the auto-border
	* function to this pair of base types will be canceled.
	*/
	enmAnalysisResult fVienCheckSurroundingSquare(wxPoint pVpntAnalysisPositon
	                                              , wxPoint pVpntSurroundingPosition
	                                              , enmBaseType pVienInsertedBaseType
	                                              , wxPoint pVpntInsertionPosition
	                                              , enmBaseType pVienTargetBaseType);
	
	/**
	* Compare two base type to determine if they are related (they don't have to be necessary equal)
	* @param pVienFirstBaseType Is a fundamental type: grassland, water or mountain
	* @param pVienSecondBaseType Can be a fundamental type or a subtype (coast or mount)
	*/
	bool fVblnBaseTypesRelated(enmBaseType pVienFirstBaseType, enmBaseType pVienSecondBaseType);
	
	/**
	* Return the direction in where is the surrounding square, relative to the central one.
	* This function only works if the two squares are one next the other. The direction is not important.
	*/
	enmBoardDirection fVienGetRelativePosition(wxPoint pVpntCentralSquare, wxPoint pVpntSurroundingSquare);
	
	/**
	* Return true if the given coordinate is out of the board dimensions.
	*/
	bool fVblnCoordinateOutOfRange(int pVintRow, int pVintColumn);
	
	/**
	* Fill the relative positions bidimensional array
	*/
	void FillRelativePositions();
	
	/**
	* Determine if it is possible to insert objects on the board when dragging the mouse with the left 
	* button pressed or when the left button of the mouse is clicked.
	*/
	bool fVblnCanInsertOnDrag();
	
	/**
	* Save a map preview, to later be able to show it to the user when he is choosing a board to play or edit.
	*/
	void SavePreview(c4_Row& pRrowRow);
	
	/**
	* Persist the upper and lower levels in the board.
	*/
	void SaveBoardContent();
	
	/**
	* Retrieve the upper and lower levels in the board.
	*/
	void LoadBoardContent();
	
	/**
	* Return the persistence structure used in this class
	*/
	wxString strGetPersistenceStructure();

	/**
	* Return the persistence structure used to store the board content
	*/
	wxString strGetBoardContentStructure();
	
	//-------------------------------------------------------------------------------------------------------------------
	// Constants / Enumerations (private)

	const int mVintSQUARE_LENGTH;
	const int mVintMAX_SQUARES;
	
	//-------------------------------------------------------------------------------------------------------------------
	// Variables (private)

	// Board dimensions expressed in squares

	int mVintWidth;
	int mVintHeight;

	/**
	 * Descriptive name that the user sees, for example, when he selects a board for play
	 */
	wxString mVstrTitle;

	/**
	 * Additional notes about the board, in multiple lines format
	 */
	wxString mVstrDescription;

	/**
	 * Amount of players required by the board
	 */
	int mVintNumberPlayers;

	/**
	 * Bidimensional arrays that manage the lower level of the board. They have coordinates [Y][X] (row and column)
	 */
	clsVector2D< clsPointer<clsTerrain> > mVarrTerrain;

	/**
	 * Bidimensional arrays that manage the upper level of the board. They have coordinates [Y][X] (row and column)
	 */
	clsVector2D< clsPointer<clsUpperLevelObject> > mVarrBoardObject;

	/**
	 * Cursor used to interact with objects in the board
	 */
	CountedPtr<clsCursor> mSobjCursor;

	/**
	 * Fixed selection to make a rectangular area while the board is edited
	 */
	CountedPtr<clsFixedSelection> mSobjFixedSelection;

	/**
	 * Used when a board object is destroyed. This is only an aesthetic feature.
	 */
	CountedPtr<wxBitmap> mSbmpExplosion;

	/**
	 * Object selected to insert in board edition mode. It can be of type clsNaturalResource (to insert upper level
	 * objects) or clsTerrain (to insert lower level objects)
	 */
	clsPointer<clsBoardObject> mOobjObjectToInsert;

	CountedPtr<wxCursor> mScrsInsertObject;
	CountedPtr<wxCursor> mScrsEraser;

	/**
	* Store the rules to make seamless limits between water and grassland.
	*/
	Array< clsPointer<clsTerrain> > mVarrWaterAutoborderRules;
	
	/**
	* Store the rules to make seamless limits between mountain and grassland.
	*/
	Array< clsPointer<clsTerrain> > mVarrMountainAutoborderRules;
	
	/**
	* Store the rules to make seamless limits between grassland and water.
	*/
	Array< clsPointer<clsTerrain> > mVarrGrasslandWaterAutoborderRules;
	
	/**
	* Store the rules to make seamless limits between grassland and mountain.
	*/
	Array< clsPointer<clsTerrain> > mVarrGrasslandMountainAutoborderRules;
	
	/**
	* This array is used to find out the position of a square relative to other.
	* The first dimension correspond to the X axis, and the second one correspond to the Y axis.
	*/
	clsVector2D<enmBoardDirection> mVarrRelativePositions;

	/**
	* Enable or disable the auto-border function
	*/
	bool mVblnApplyAutoBorder;
	
	/**
	* When this variable is true, the auto-border changes are updated on the board just after them were applied.
	* When it's false, the board refresh have to be done by the calling code in other moment. This is for optimizing
	* purposes.
	*/
	bool mVblnAutoBorderInstantRefresh;
	
	//...................................................................................................................
	// Metakit properties
	
	c4_StringProp mVprpStrTitle;
	c4_StringProp mVprpStrDescription;
	c4_IntProp mVprpIntHeight;
	c4_IntProp mVprpIntWidth;
	c4_IntProp mVprpIntPlayers;
	c4_BytesProp mVprpBytPreview;
	
	c4_StringProp mVprpStrSurface;
	c4_StringProp mVprpStrUpperObject;	
	
	//-------------------------------------------------------------------------------------------------------------------
	// Shortcuts (private)

	clsPointer<clsCanvas> mOobjCanvas;
	clsPointer<clsFrame> mOobjFrame;
	clsPointer<clsMap> mOobjMap;
};

#endif // !defined(AFX_CLSBOARD_H__68E88277_A1FD_4FF9_BC2C_B02B9B035795__INCLUDED_)
