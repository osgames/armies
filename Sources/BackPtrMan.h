// ***********************************
// Copyright 1999 John R. Bandela
// This work may be freely used, copied, distributed, and extended, provided
// the copyright notice above and this statement are maintained at the top of the code


#include "stdafx.h"

//This header is used internally by jrb_ptr.h
// DO NOT INCLUDE THIS FILE
#include <set>

namespace jrb_sptr{

	typedef unsigned long JRB_BPCOUNT_TYPE; 
	
	// The back reference manager
	// Used internally, no need to use
	class BackRefManager{
	private:
		// The current count
		JRB_BPCOUNT_TYPE nCount;
		// A set of all the back pointers valid
		std::set<JRB_BPCOUNT_TYPE> BackPointers;
	public:
		// Constructor
		BackRefManager(){nCount = 1;};
		// Gets the next ID
		JRB_BPCOUNT_TYPE GetNewID(){return nCount++;}

		// Adds an ID to the list of valid BP
		void AddID(JRB_BPCOUNT_TYPE nID){
			BackPointers.insert(nID);
			assert(BackPointers.count(nID)==1);
		}

		// Removes an ID from list of valid BP
		void RemoveID(JRB_BPCOUNT_TYPE nID){
			BackPointers.erase(nID);
		}

		// Checks if ID is valid BP
		bool QueryID(JRB_BPCOUNT_TYPE nID){
			if(BackPointers.count(nID)){
				return true;
			}
			else{
				return false;
			}
		}

	};
	// Returns a reference to the Back Ref Manager for the App
	BackRefManager& GetBackRefManager();
}
