// clsStatusBar.cpp: implementation of the clsStatusBar class.
//
//////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------
#include "clsStatusBar.h"

#include <wx/settings.h>
#include <wx/tooltip.h>

// ----------------------------------------------------------------------------------------------------------------------
// Static graphics included manually

#include "../Graficos Desarrollo/Iconos/16x16/cursor_position.xpm"

// ----------------------------------------------------------------------------------------------------------------------

IMPLEMENT_CLASS(clsStatusBar, wxStatusBar)

BEGIN_EVENT_TABLE(clsStatusBar, wxStatusBar)
	EVT_SIZE(clsStatusBar::OnSize)
END_EVENT_TABLE()

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//_______________________________________________________________________________________________________________________

clsStatusBar::clsStatusBar(wxWindow *parent)
		: wxStatusBar(parent, wxID_ANY, wxST_SIZEGRIP | wxNO_BORDER)
{
    MVintFieldsWidth[this->enmFields_Help] = 0; // for now, help is disabled
    MVintFieldsWidth[this->enmFields_Modality] = 220;
	MVintFieldsWidth[this->enmFields_CursorPosition] = 80;
	MVintFieldsWidth[this->enmFields_PlayerColor] = 25;
	MVintFieldsWidth[this->enmFields_Player] = 125;
	MVintFieldsWidth[this->enmFields_Construction] = 80;
	MVintFieldsWidth[this->enmFields_Creation] = 80;
	MVintFieldsWidth[this->enmFields_TerrainType] = 70;
	MVintFieldsWidth[this->enmFields_ActionIcon] = 25;
	MVintFieldsWidth[this->enmFields_Action] = -1;

	this->SetFieldsCount(this->enmFields_TopValue);
    this->SetStatusWidths(this->enmFields_TopValue, MVintFieldsWidth);

	// Player name label
	// 	MPlblPlayer = new wxStaticText( this, wxID_STATIC, _T("Germ�n"), wxDefaultPosition, wxDefaultSize, 0 );
	// 	MPlblPlayer->SetToolTip(_("Player name"));
	// 	MPlblPlayer->SetBackgroundColour(wxNullColour);

	// Player color icon
	MPsbmPlayerColor = new wxStaticBitmap(this, wxID_ANY, wxNullIcon, wxDefaultPosition, wxSize(16, 16));
	MPsbmPlayerColor->Show(false); // it have to use visibility because set a wxStaticBitmap to wxNullIcon in GTK doesn't work
	
	// Action icon
	MPsbmActionIcon = new wxStaticBitmap(this, wxID_ANY, wxNullIcon, wxDefaultPosition, wxSize(16, 16));
	MPsbmActionIcon->Show(false); // it have to use visibility because set a wxStaticBitmap to wxNullIcon in GTK doesn't work

	// ..................................................................................................................
	// Cursor position
	// Cursor position panel
	mOpnlCursorPosition = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNO_BORDER|wxTAB_TRAVERSAL);
	mOpnlCursorPosition->SetToolTip(_("Row and column for the cursor position"));
	
	// Cursor Position sizer
	mOszrCursorPositionSizer = new wxBoxSizer(wxHORIZONTAL);
	mOpnlCursorPosition->SetSizer(mOszrCursorPositionSizer);
	
	// Cursor Position icon
	wxStaticBitmap* PsbmStaticBitmap = new wxStaticBitmap(mOpnlCursorPosition, wxID_STATIC, wxBitmap(cursor_position_xpm), wxDefaultPosition, wxSize(16, 16), 0);
	PsbmStaticBitmap->SetToolTip(mOpnlCursorPosition->GetToolTip()->GetTip()); // to show the tooltip information as in the panel
	mOszrCursorPositionSizer->Add(PsbmStaticBitmap, 0, wxALIGN_CENTER_VERTICAL|wxALL, 0);
	
	// Cursor Position label
	mOlblCursorPosition = new wxStaticText(mOpnlCursorPosition, wxID_ANY, _("10, 20"), wxDefaultPosition, wxSize(20, -1), wxALIGN_CENTRE);
	mOlblCursorPosition->SetToolTip(mOpnlCursorPosition->GetToolTip()->GetTip()); // to show the tooltip information as in the panel
	mOszrCursorPositionSizer->Add(mOlblCursorPosition, 1, wxALIGN_CENTER_VERTICAL|wxALL, 0);
}

//_______________________________________________________________________________________________________________________

clsStatusBar::~clsStatusBar()
{

}

//_______________________________________________________________________________________________________________________

void clsStatusBar::OnSize(wxSizeEvent& event)
{
	wxRect rctField;
    wxSize sizStaticBitmap;
	
	#if defined(__WXMSW__)
		int intPlatformOffsetX = -2;
	#else
		int intPlatformOffsetX = 0;
	#endif
	
	// Arrange player color icon
	sizStaticBitmap = MPsbmPlayerColor->GetSize();
	GetFieldRect(enmFields_PlayerColor, rctField);
    MPsbmPlayerColor->Move(rctField.x + (rctField.width - sizStaticBitmap.x) / 2 + intPlatformOffsetX, rctField.y + (rctField.height - sizStaticBitmap.y) / 2);

	// Arrange action icon
	sizStaticBitmap = MPsbmActionIcon->GetSize();
	GetFieldRect(enmFields_ActionIcon, rctField);
    MPsbmActionIcon->Move(rctField.x + (rctField.width - sizStaticBitmap.x) / 2 + intPlatformOffsetX, rctField.y + (rctField.height - sizStaticBitmap.y) / 2);
    
	// Arrange player name label
	//GetFieldRect(enmFields_Player, rctField);
	//MPlblPlayer->SetSize(rctField.x + 2, rctField.y + 2, rctField.width - 2, rctField.height - 2);
	
	// Arrange cursor position panel
	GetFieldRect(enmFields_CursorPosition, rctField);
	mOpnlCursorPosition->SetSize(rctField.x + 2, rctField.y + 2, rctField.width - 2, rctField.height - 2);
	mOszrCursorPositionSizer->Layout();
	
	
    event.Skip();
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::SetModality(const wxString& PRstrModality) {
	this->SetStatusText(PRstrModality, this->enmFields_Modality);
}

//_______________________________________________________________________________________________________________________

wxString clsStatusBar::strGetModality() const {
	return(this->GetStatusText(this->enmFields_Modality));
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::SetPlayerColor(const wxIcon& PRicnIcon) {
	MPsbmPlayerColor->SetIcon(PRicnIcon);
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::SetPlayer(const wxString& PRstrPlayer) {
	this->SetStatusText(PRstrPlayer, this->enmFields_Player);
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::SetConstruction(const wxString& PRstrConstruction) {
	this->SetStatusText(PRstrConstruction, this->enmFields_Construction);
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::SetCreation(const wxString& PRstrCreation) {
	this->SetStatusText(PRstrCreation, this->enmFields_Creation);
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::SetTerrainType(const wxString& PRstrTerrainType) {
	this->SetStatusText(PRstrTerrainType, this->enmFields_TerrainType);
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::SetAction(const wxString& PRstrAction) {
	this->SetStatusText(PRstrAction, this->enmFields_Action);
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::SetActionIcon(const wxIcon& PRicnIcon) { 
	MPsbmActionIcon->SetIcon(PRicnIcon);
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::ShowActionIcon() {
	MPsbmActionIcon->Show();
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::HideActionIcon() {
	MPsbmActionIcon->Show(false);
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::ShowPlayerColor() {
	MPsbmPlayerColor->Show();
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::HidePlayerColor() {
	MPsbmPlayerColor->Show(false);
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::SetCursorPosition(int pVintRow, int pVintColumn) {
	wxString strRow;
	wxString strColumn;
	wxString strLabel;
	
	strRow = wxString::Format(_T("%d"), pVintRow);
	strColumn = wxString::Format(_T("%d"), pVintColumn);
	
	strLabel = strRow + ", " + strColumn;
	mOlblCursorPosition->SetLabel(strLabel);
	
	mOszrCursorPositionSizer->Layout(); // because the setting of the label change the position of the wxStaticText
}

//_______________________________________________________________________________________________________________________

void clsStatusBar::HideCursorPosition() {
	mOlblCursorPosition->SetLabel(_T(""));
}
