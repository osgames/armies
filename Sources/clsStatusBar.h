#if !defined(AFX_CLSSTATUSBAR_H__3C79879F_046F_4D38_90CB_0AEA58FFEB94__INCLUDED_)
#define AFX_CLSSTATUSBAR_H__3C79879F_046F_4D38_90CB_0AEA58FFEB94__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */

#include "wx/statusbr.h"
#include "wx/stattext.h"
#include "wx/statbmp.h"

#include "clsPointer.h"

//_______________________________________________________________________________________________________________________

/**
* Manage all the actions made in the statusbar, like write text in some of its panels and resize its special
* elements (panels for example).
* It inherits from the standard wxStatusBar.
*/
class clsStatusBar : public wxStatusBar
{
public:
	clsStatusBar(wxWindow *parent);
	virtual ~clsStatusBar();

    // Define the Statusbar fields
	enum enmFields
    {
        enmFields_Help,
        enmFields_Modality,
		enmFields_CursorPosition,
		enmFields_PlayerColor,
		enmFields_Player,
		enmFields_Construction,
		enmFields_Creation,
		enmFields_TerrainType,
		enmFields_ActionIcon,
		enmFields_Action,
		enmFields_TopValue
    };

	// ..................................................................................................................
	// Events

	void OnSize(wxSizeEvent& event);

	// ..................................................................................................................
	// Methods

	void SetModality(const wxString& PRstrModality);
	wxString strGetModality() const;
		
	void SetPlayerColor(const wxIcon& PRicnIcon);
	void HidePlayerColor();
	void ShowPlayerColor();
	void SetPlayer(const wxString& PRstrPlayer);
	void SetConstruction(const wxString& PRstrConstruction);
	void SetCreation(const wxString& PRstrCreation);
	void SetTerrainType(const wxString& PRstrTerrainType);
	void SetActionIcon(const wxIcon& PRicnIcon);
	void HideActionIcon();
	void ShowActionIcon();
	void SetAction(const wxString& PRstrAction);
	void SetCursorPosition(int pVintRow, int pVintColumn);
	void HideCursorPosition();

//_______________________________________________________________________________________________________________________

private:
	int MVintFieldsWidth[clsStatusBar::enmFields_TopValue];
	wxStaticText* MPlblPlayer;
	wxStaticBitmap* MPsbmPlayerColor;
	wxStaticBitmap* MPsbmActionIcon;
	
	clsPointer<wxPanel> mOpnlCursorPosition;
	clsPointer<wxStaticText> mOlblCursorPosition;
	clsPointer<wxBoxSizer> mOszrCursorPositionSizer;

    DECLARE_CLASS(clsStatusBar)
	DECLARE_EVENT_TABLE()
};

#endif // !defined(AFX_CLSSTATUSBAR_H__3C79879F_046F_4D38_90CB_0AEA58FFEB94__INCLUDED_)
