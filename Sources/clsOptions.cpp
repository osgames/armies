// clsOptions.cpp: implementation of the clsOptions class.
//
//////////////////////////////////////////////////////////////////////

#include "clsOptions.h"
#include "Global.h"
#include "ClsApplication.h"

CountedPtr<clsOptions> clsOptions::mSobjInstance; // definition of static member

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------------------------------------------------
clsOptions::clsOptions()
{
	MPcnfConfig = new wxConfig(_T("Armies"));
	MPcnfConfig->SetRecordDefaults(true); // in order to store default values when the entry doesn't exist
	wxConfigBase::Set(MPcnfConfig);

	Load();
}

//-----------------------------------------------------------------------------------------------------------------------
clsOptions::~clsOptions()
{
	// Deletes the default configuration object
	wxConfigBase::Set(NULL);
}

//-----------------------------------------------------------------------------------------------------------------------
void clsOptions::Load()
{
	wxString VstrDefaultValue;
	wxConfigBase& RobjConfig = *wxConfigBase::Get();
	wxString strResourcesPath = gOobjApplication->GetVstrAppPath();
	wxString strDocumentsPath = gOobjApplication->GetVstrDocumentsPath();

	VstrDefaultValue = _T("classic_tiles.bmp");
	MVstrGraphicsFileName = RobjConfig.Read(_T("/General/Graphics File Name"), VstrDefaultValue);
	
	// Directories -----------------------------------------------------------------------------------------------------------------------
	VstrDefaultValue = strDocumentsPath + _T("Boards/");
	MVstrBoardsDirectory = RobjConfig.Read(_T("/Folders/Boards"), VstrDefaultValue);
	clsLibrary::EnsureDirectoryExistence(MVstrBoardsDirectory);

	VstrDefaultValue = strDocumentsPath + _T("Games/");
	MVstrGamesDirectory = RobjConfig.Read(_T("/Folders/Games"), VstrDefaultValue);
	clsLibrary::EnsureDirectoryExistence(MVstrBoardsDirectory);

	VstrDefaultValue = strResourcesPath + _T("Graphics/");
	MVstrGraphicsDirectory = RobjConfig.Read(_T("/Folders/Graphics"), VstrDefaultValue);
}

//-----------------------------------------------------------------------------------------------------------------------
CountedPtr<clsOptions> clsOptions::GetInstance()
{
    if(!mSobjInstance.IsValid()) {
		// It is the first call
		mSobjInstance = Make_CP(new clsOptions); // create sole instance
    }
    
	return mSobjInstance; // address of sole instance
}
