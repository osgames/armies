#if !defined(AFX_CLSNATURALRESOURCE_H__0AF9B749_A908_4795_B54D_849D7B4FF14E__INCLUDED_)
#define AFX_CLSNATURALRESOURCE_H__0AF9B749_A908_4795_B54D_849D7B4FF14E__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */

#include <mk4.h>

#include "clsUpperLevelObject.h"
#include "jrb_ptr.h"

/*!
 * Namespaces
 */

using namespace jrb_sptr;

//_____________________________________________________________________________________________________________________

/**
 * It contains resources which some mobile units can extract
 */
class clsNaturalResource : public clsUpperLevelObject
{
DECLARE_ABSTRACT_CLASS(clsNaturalResource) // to enable RTTI
public:
	//....................................................................................................................
	// Getters / Setters

	int GetVintCreation() const { return(mVintCreation); }
	void SetVintCreation(int PVintCreation) { mVintCreation = PVintCreation; }

	int GetVintConstruction() const { return(mVintConstruction); }
	void SetVintConstruction(int PVintConstruction) { mVintConstruction = PVintConstruction; }
	
	/**
	* @ToOverride
	*/
	virtual int GetVintMaxCreation() const = 0;

	/**
	* @ToOverride
	*/
	virtual int GetVintMaxConstruction() const = 0;
	
	virtual wxString GetVstrTypeDescription() const { return(sVstrTypeDescription); } // @Override
	virtual void SetVstrTypeDescription(const wxString pVstrTypeDescription) { sVstrTypeDescription = pVstrTypeDescription; } // @Override
	
	//....................................................................................................................
	// Methods

	/**
	 * Creates an object of a specific type. This function must be specialized in the most specific classes
	 * like clsTree, clsWater or clsSoldier.
	 */
	virtual clsPointer<clsNaturalResource> OobjCreateNew() = 0;
	
	/**
	 * Creates an object from a file. This function must be specialized in the most specific classes
	 * like clsTree, clsWater or clsSoldier.
	 */
	virtual clsPointer<clsNaturalResource> OobjCreateFromFile(c4_RowRef pVrowRow) = 0;
	
	/**
	* @Override
	*/
	virtual void ShowProperties(clsPointer<wxListCtrl> pOlvwProperties);

	/**
	* Return a string containing the structure to persist objects of a specific class.
	*/
	static wxString strGetPersistenceStructure();
	
	/**
	* @Override
	*/
	virtual void Save(c4_Row& pRrowRow);
	
	/**
	* @Override
	*/
	virtual void Load(c4_RowRef pVrowRow);
	
	static wxString GetVstrPersistenceName() { return(sVstrPersistenceName); }

	//....................................................................................................................

	clsNaturalResource();
	virtual ~clsNaturalResource();

//_____________________________________________________________________________________________________________________

protected:

	//.................................................................................................................
	// Variables
	
	/**
	* Units of Creation Resource that the object owns.
	*/
	int mVintCreation;
	
	/**
	* Units of Construction Resource that the object owns.
	*/
	int mVintConstruction;
	
	//....................................................................................................................
	// Getters / Setters

	/**
	 * Sets the offset to load the sector of image corresponding to the actual object, when doesn't contain any resource.
	 * Additionally, make effective the load this image.
	 */
	void SetOffsetImageEmpty(const wxPoint& PVpntOffset);

	/**
	* @ToOverride
	*/
	virtual wxImage GetVimgEmptyGraphic() const = 0;
	
	/**
	* @ToOverride
	*/
	virtual void SetVimgEmptyGraphic(wxImage pVimgImage) = 0;

//_____________________________________________________________________________________________________________________

private:

	//.................................................................................................................
	// Variables

	/**
	* Area from where the object must load the image that denotes absence of any resource
	*/
	wxRect mVrctSourceEmpty;
	
	/**
	 * For identify the object's type. This text is user friendly (must be translated).
	 */
	static wxString sVstrTypeDescription;

	/**
	* Identifier used to persist objects derived from this class (by persist the correct owner collection)
	*/
	static wxString sVstrPersistenceName;

	//...................................................................................................................
	// Metakit properties
	
	static c4_IntProp sVprpIntConstruction;
	static c4_IntProp sVprpIntCreation;
};

#endif // !defined(AFX_CLSNATURALRESOURCE_H__0AF9B749_A908_4795_B54D_849D7B4FF14E__INCLUDED_)
