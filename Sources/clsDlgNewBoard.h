/////////////////////////////////////////////////////////////////////////////
// Name:        clsDlgNewBoard.h
// Purpose:     
// Author:      Germ�n Blando
// Modified by: 
// Created:     05/29/05 08:05:02
// RCS-ID:      
// Copyright:   Interfase
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _CLSDLGNEWBOARD_H_
#define _CLSDLGNEWBOARD_H_

/*!
 * Includes
 */

////@begin includes
////@end includes

#include "Enumerations.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_DIALOG 10019
#define ID_txtTitle 10001
#define ID_cmbNumberrOfPlayers 10002
#define ID_txtWidth 10003
#define ID_txtHeight 10000
#define ID_txaDescription 10004
#define SYMBOL_CLSDLGNEWBOARD_STYLE wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxCLOSE_BOX
#define SYMBOL_CLSDLGNEWBOARD_TITLE _("New Board")
#define SYMBOL_CLSDLGNEWBOARD_IDNAME ID_DIALOG
#define SYMBOL_CLSDLGNEWBOARD_SIZE wxSize(400, 300)
#define SYMBOL_CLSDLGNEWBOARD_POSITION wxDefaultPosition
////@end control identifiers

/*!
 * Compatibility
 */

#ifndef wxCLOSE_BOX
#define wxCLOSE_BOX 0x1000
#endif
#ifndef wxFIXED_MINSIZE
#define wxFIXED_MINSIZE 0
#endif

/*!
 * clsDlgNewBoard class declaration
 */

//____________________________________________________________________________________________________________________

class clsDlgNewBoard: public wxDialog
{    
    DECLARE_DYNAMIC_CLASS( clsDlgNewBoard )
    DECLARE_EVENT_TABLE()

//____________________________________________________________________________________________________________________
		
public:
    /// Constructors
    clsDlgNewBoard( );
    clsDlgNewBoard( wxWindow* parent, wxWindowID id = SYMBOL_CLSDLGNEWBOARD_IDNAME, const wxString& caption = SYMBOL_CLSDLGNEWBOARD_TITLE, const wxPoint& pos = SYMBOL_CLSDLGNEWBOARD_POSITION, const wxSize& size = SYMBOL_CLSDLGNEWBOARD_SIZE, long style = SYMBOL_CLSDLGNEWBOARD_STYLE );

	/// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_CLSDLGNEWBOARD_IDNAME, const wxString& caption = SYMBOL_CLSDLGNEWBOARD_TITLE, const wxPoint& pos = SYMBOL_CLSDLGNEWBOARD_POSITION, const wxSize& size = SYMBOL_CLSDLGNEWBOARD_SIZE, long style = SYMBOL_CLSDLGNEWBOARD_STYLE );

    /// Creates the controls and sizers
    void CreateControls();

////@begin clsDlgNewBoard event handler declarations

 /// wxEVT_COMMAND_COMBOBOX_SELECTED event handler for ID_cmbNumberrOfPlayers
 void OnCmbNumberrOfPlayersSelected( wxCommandEvent& event );

 /// wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK
 void OnOk( wxCommandEvent& event );

////@end clsDlgNewBoard event handler declarations

////@begin clsDlgNewBoard member function declarations

 /// Retrieves bitmap resources
 wxBitmap GetBitmapResource( const wxString& name );

 /// Retrieves icon resources
 wxIcon GetIconResource( const wxString& name );
////@end clsDlgNewBoard member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin clsDlgNewBoard member variables
 wxTextCtrl* pTxtTitle;
 wxComboBox* pCmbNumberOfPlayers;
 wxTextCtrl* pTxtWidth;
 wxTextCtrl* pTxtHeight;
 wxTextCtrl* pTxaDescription;
////@end clsDlgNewBoard member variables
	
	//-------------------------------------------------------------------------------------------------------------------
	// Getters / Setters
	
	enmDialogAction GetVienDialogAction() const { return(mVienDialogAction); }
	void SetVienDialogAction(enmDialogAction pVienValue) { mVienDialogAction = pVienValue; }
	
	void SetVintNumberOfPlayersIndex(int pVintNumberOfPlayersIndex) { mVintNumberOfPlayersIndex = pVintNumberOfPlayersIndex; }
	
//____________________________________________________________________________________________________________________

private:
	//-------------------------------------------------------------------------------------------------------------------
	// Variables
	
	// For use with validators, because if no variable is supply, then an error occurs in the dialog initialization (in the data transfer function)
	wxString mVstrDummyDataTransfer;
	
	/**
	* To know the current action for the dialog, and make decisions according to that.
	*/
	enmDialogAction mVienDialogAction;
	
	/**
	* Due the fact that the combo elements are loaded after the ShowModal method, it's impossible to set the combo index
	* before. Instead, this variable is used to store the value temporally.
	*/
	int mVintNumberOfPlayersIndex;
	
	//-------------------------------------------------------------------------------------------------------------------
	// Methods
	
	void ArrangeDialog();
	bool blnValid();

	// Events
	void OnInitDialog(wxInitDialogEvent & event);
};

#endif // _CLSDLGNEWBOARD_H_
