#include "clsOil.h"

#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

//.......................................................................................................................
// Includes
#include "Global.h"
#include "throwmap.h"
#include "ClsApplication.h"

//.......................................................................................................................
// Definition of static members

int clsOil::sVintInstances;
CountedPtr<wxColour> clsOil::sSclrMapColor;
int clsOil::sVintMaxHealth;
int clsOil::sVintMaxCreation;
int clsOil::sVintMaxConstruction;
wxImage clsOil::sVimgStaticRepresentation;
wxImage clsOil::sVimgEmptyGraphic;
wxString clsOil::sVstrBaseDescription;
wxString clsOil::sVstrInternalTypeName;

//_______________________________________________________________________________________________________________________

clsOil::clsOil() {
	NormalConstructor();
}

//_______________________________________________________________________________________________________________________

clsOil::clsOil(bool pVblnObjectType) {
	if(pVblnObjectType) {
		// Construction of the class Object Type
		ObjectTypeConstructor();
	} else {
		NormalConstructor();
	}
}

//_______________________________________________________________________________________________________________________

void clsOil::NormalConstructor() {
	mVintHealth = sVintMaxHealth;
	mVintCreation = sVintMaxCreation;
	mVintConstruction = sVintMaxConstruction;
	mVimgDynamicRepresentation = this->GetVimgStaticRepresentation(); // at the beginning, the representation must be equal to the static one (a non empty resource)
}

//_______________________________________________________________________________________________________________________

void clsOil::ObjectTypeConstructor() {
	mVstrName = _T("Oil");
	sVstrInternalTypeName = mVstrName; // to share the base type among all the specific child objects
	sVstrBaseDescription = _("Oil");

	sSclrMapColor = Make_CP(new wxColor(0, 0, 0));
	sVintMaxHealth = 7;
	sVintMaxCreation = 0;
	sVintMaxConstruction = 40;

	this->SetOffsetImage(wxPoint(120, 120));
	this->SetOffsetImageEmpty(wxPoint(320, 120));
}

//_______________________________________________________________________________________________________________________

clsOil::~clsOil()
{

}

//_______________________________________________________________________________________________________________________

void clsOil::ResetCounter() {
	sVintInstances = 0;
}

//_______________________________________________________________________________________________________________________

clsPointer<clsNaturalResource> clsOil::OobjCreateNew() {
	clsPointer<clsOil> OobjNewInstance = new clsOil();
	wxString strInstanceNumber;
	
	sVintInstances++;
	strInstanceNumber = wxString::Format(_T("%d"), sVintInstances);

	OobjNewInstance->SetVstrName(mVstrName + strInstanceNumber);
	OobjNewInstance->SetVstrDescription(sVstrBaseDescription + " " + strInstanceNumber);
	
	// Adds the new object on the corresponding collection
	safemap::insert(gOobjApplication->GetVdctNaturalResources(), OobjNewInstance->GetVstrName(), Make_CP(OobjNewInstance.get()));
	safemap::insert(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNewInstance->GetVstrName(), OobjNewInstance);
	
	return(OobjNewInstance);
}

//_______________________________________________________________________________________________________________________

clsPointer<clsNaturalResource> clsOil::OobjCreateFromFile(c4_RowRef pVrowRow) {
	clsPointer<clsOil> OobjNewInstance = new clsOil();
	
	OobjNewInstance->Load(pVrowRow);
	
	// Adds the new object on the corresponding collection
	safemap::insert(gOobjApplication->GetVdctNaturalResources(), OobjNewInstance->GetVstrName(), Make_CP(OobjNewInstance.get()));
	safemap::insert(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNewInstance->GetVstrName(), OobjNewInstance);
	
	return(OobjNewInstance);
}

//_______________________________________________________________________________________________________________________

void clsOil::ShowContent(clsPointer<wxTreeCtrl> pOtrcContent) {
	// TODO: maybe the best solution here is to hide the wxTreeCtrl when a non container object is selected
}
