// clsUserObject.h: interface for the clsUserObject class.
// This class represent objects that can be owned by any user
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLSUSEROBJECT_H__F9E9A81E_8A4D_4607_89FA_9119976361D2__INCLUDED_)
#define AFX_CLSUSEROBJECT_H__F9E9A81E_8A4D_4607_89FA_9119976361D2__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */

#include "clsUpperLevelObject.h"
#include "jrb_ptr.h"

/*!
 * Namespaces
 */
using namespace jrb_sptr;

/*!
 * Forward declarations
 */
class clsPlayer;

class clsUserObject : public clsUpperLevelObject
{
DECLARE_ABSTRACT_CLASS(clsUserObject) // to enable RTTI
public:
	clsUserObject();
	virtual ~clsUserObject();

// -----------------------------------------------------------------------------------------------------------------------
private:
	/**
	* Graphic for representing the object, colored according to the user that owns it
	*/
	wxImage mVimgUserGraphic;

	/**
	* The object owner
	*/
	BackPtr<clsPlayer> mBobjPlayer;
};

#endif // !defined(AFX_CLSUSEROBJECT_H__F9E9A81E_8A4D_4607_89FA_9119976361D2__INCLUDED_)
