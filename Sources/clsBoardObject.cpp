#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

/*!
* Includes
*/
#include "clsBoardObject.h"

#include "Global.h"
#include "ClsApplication.h"
#include "clsBoard.h"
#include "clsCanvas.h"
#include "clsFrame.h"

IMPLEMENT_ABSTRACT_CLASS(clsBoardObject, wxObject) // to enable RTTI

//.......................................................................................................................
// Definition of static members

c4_StringProp clsBoardObject::sVprpStrName(_T("strName"));
c4_StringProp clsBoardObject::sVprpStrDescription(_T("strDescription"));
c4_IntProp clsBoardObject::sVprpIntPositionX(_T("intPositionX"));
c4_IntProp clsBoardObject::sVprpIntPositionY(_T("intPositionY"));

//_______________________________________________________________________________________________________________________

clsBoardObject::clsBoardObject() {
	mOobjBoard = gOobjApplication->GetSobjBoard();
	mOobjFrame = gOobjApplication->GetPobjFrame();
	mOobjCanvas = mOobjFrame->GetpObjCanvas();
}

//_______________________________________________________________________________________________________________________

clsBoardObject::~clsBoardObject()
{
}

//_______________________________________________________________________________________________________________________

void clsBoardObject::SetOffsetImage(const wxPoint &PvPntOffset) {
	int intSquareLength = gOobjApplication->GetSobjBoard()->GetVintSQUARE_LENGTH();
	wxBitmap bmpSubBitmap;
	wxColor clrColor = gOobjApplication->GetVclrTransparentBackground();
	wxImage imgImage;
	
	mVrctSource.SetTopLeft(PvPntOffset);
	mVrctSource.SetWidth(intSquareLength);
	mVrctSource.SetHeight(intSquareLength);
	
	bmpSubBitmap = gOobjApplication->GetSbmpGraphics()->GetSubBitmap(mVrctSource);
	imgImage = bmpSubBitmap.ConvertToImage();
	imgImage.SetMaskColour(clrColor.Red(), clrColor.Green(), clrColor.Blue());
	this->SetVimgStaticRepresentation(imgImage);
}


//_______________________________________________________________________________________________________________________

void clsBoardObject::Draw(wxPoint pVpntOffset) {
	wxRect rctDestination = mOobjBoard->rctSquareArea(mVpntPosition.y, mVpntPosition.x);

	// Changes the rectangle in concordance with the offset
	rctDestination.SetLeft(rctDestination.GetLeft() + pVpntOffset.x);
	rctDestination.SetTop(rctDestination.GetTop() + pVpntOffset.y);

	mOobjCanvas->DrawSecondary(mVimgDynamicRepresentation, rctDestination, pVpntOffset);
}

//_______________________________________________________________________________________________________________________

wxString clsBoardObject::strGetPersistenceStructure() {
	wxString strResult;
	
	strResult = _T("strName:S");
	strResult += _T(",strDescription:S");
	strResult += _T(",intPositionX:I");
	strResult += _T(",intPositionY:I");
	
	return(strResult);
}

//_______________________________________________________________________________________________________________________

void clsBoardObject::Save(c4_Row& pRrowRow) {
	sVprpStrName(pRrowRow) = mVstrName;
	sVprpStrDescription(pRrowRow) = mVstrDescription;
	sVprpIntPositionX(pRrowRow) = mVpntPosition.x;
	sVprpIntPositionY(pRrowRow) = mVpntPosition.y;
}

//_______________________________________________________________________________________________________________________

void clsBoardObject::Load(c4_RowRef pVrowRow) {
	mVstrName = sVprpStrName(pVrowRow);
	mVstrDescription = sVprpStrDescription(pVrowRow);
	mVpntPosition.x = sVprpIntPositionX(pVrowRow);
	mVpntPosition.y = sVprpIntPositionY(pVrowRow);
}
