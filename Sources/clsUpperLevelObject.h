#if !defined(_CLSUPPERLEVELOBJECT_H_)
#define _CLSUPPERLEVELOBJECT_H_

#include <wx/listctrl.h>
#include <wx/treectrl.h>

#include "clsBoardObject.h"

//_____________________________________________________________________________________________________________________

/**
* It represents any object that occupies the upper board level
*/
class clsUpperLevelObject : public clsBoardObject
{
DECLARE_ABSTRACT_CLASS(clsUpperLevelObject) // to enable RTTI
public:
	clsUpperLevelObject();
	virtual ~clsUpperLevelObject();

	//-------------------------------------------------------------------------------------------------------------------
	// Getters / Setters (public)
	
	int GetVintHealth() const { return(mVintHealth); }
	void SetVintHealth(int pVintHealth) { mVintHealth = pVintHealth; }
	
	virtual int GetVintMaxHealth() const = 0; // @ToOverride

	virtual wxString GetVstrInternalTypeName() const = 0; // @ToOverride
	
	//-------------------------------------------------------------------------------------------------------------------
	// Methods (public)

	/**
	* Resets the counter of a specific class.
	* This method is created to be specialized in the derived classes
	*/
	virtual void ResetCounter() = 0;
	
	/**
	* @ToOverride
	* Show the properties' object in a list widget
	*/
	virtual void ShowProperties(clsPointer<wxListCtrl> pOlvwProperties) = 0;

	/**
	* @ToOverride
	* Show the content's object in a tree widget
	*/
	virtual void ShowContent(clsPointer<wxTreeCtrl> pOtrcContent) = 0;
	
	/**
	* Return a string containing the structure to persist objects of a specific class.
	*/
	static wxString strGetPersistenceStructure();
	
	/**
	* @Override
	*/
	virtual void Save(c4_Row& pRrowRow);
	
	/**
	* @Override
	*/
	virtual void Load(c4_RowRef pVrowRow);
	
	/**
	* Persist the information related to the class Object Type
	*/
	virtual void SaveObjectTypeInformation(c4_Row& pRrowRow);

	/**
	* Persist the information related to the class Object Type
	*/
	virtual void UpdateObjectTypeInformation(c4_RowRef pVrowRow);
	
//_____________________________________________________________________________________________________________________

protected:

	//-------------------------------------------------------------------------------------------------------------------
	// Variables (protected)

	/**
	* When the object is created this member variable is set to the mVintMaxHealth value.
	* When the health comes to 0, the object will be destroyed.
	*/
	int mVintHealth;

	//-------------------------------------------------------------------------------------------------------------------
	// Getters / Setters (protected)

	virtual int GetVintInstances() const = 0; // @ToOverride
	virtual void SetVintInstances(int pVintValue) = 0; // @ToOverride
	
	virtual void SetVstrInternalTypeName(wxString pVstrValue) = 0; // @ToOverride

//_____________________________________________________________________________________________________________________

private:

	//-------------------------------------------------------------------------------------------------------------------
	// Variables (private)

	//...................................................................................................................
	// Metakit properties
	
	static c4_StringProp sVprpStrName;
	static c4_IntProp sVprpIntInstances;
	static c4_IntProp sVprpIntHealth;
	static c4_StringProp sVprpStrInternalTypeName;
};

#endif // #if !defined(_CLSUPPERLEVELOBJECT_H_)
