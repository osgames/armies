// clsLibrary.cpp: implementation of the clsLibrary class.
//
//////////////////////////////////////////////////////////////////////

#include "clsLibrary.h"

#include <wx/msgdlg.h>
#include <wx/intl.h>
#include <wx/debug.h>
#include <wx/app.h>
#include <wx/filename.h>
#include <wx/dir.h>
#include <wx/log.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//_______________________________________________________________________________________________________________________

clsLibrary::clsLibrary()
{

}

//_______________________________________________________________________________________________________________________

clsLibrary::~clsLibrary()
{

}

//_______________________________________________________________________________________________________________________

bool clsLibrary::blnFieldCompleted(wxTextCtrl* PpTxtControl, const wxString& PrStrLabel) {
	wxString strMessage;
	
	if(PpTxtControl->GetValue().IsEmpty()) {
		// The control doesn't contain any value
		strMessage.Printf(_("The field '%s' is not optional"), PrStrLabel.c_str());
		wxMessageBox(strMessage, _("Error"), wxICON_ERROR);
		PpTxtControl->SetFocus();
		return(false);
	}

	return(true);
}

//_______________________________________________________________________________________________________________________

void clsLibrary::RandomSeed() {
	srand(static_cast<unsigned>(time(0)));
}

//_______________________________________________________________________________________________________________________

int clsLibrary::intGetRandomNumber(int lowest_number, int highest_number) {
	wxASSERT(lowest_number <= highest_number);
	return lowest_number + rand() % highest_number;
}

//_______________________________________________________________________________________________________________________

wxArrayString* clsLibrary::Split(const wxString& PstrCadena, const wxString& PstrSeparador, wxArrayString& ParrResultado) {
    int intPosicion;
    wxString strCadena = PstrCadena;
    wxString strSeccion;
    int intLargoSeparador = PstrSeparador.Len();
	
    intPosicion = strCadena.Find(PstrSeparador);
	
    while(intPosicion != -1) {
        strSeccion = strCadena.Left(intPosicion);
        ParrResultado.Add(strSeccion);
        strCadena = strCadena.Mid(intPosicion + intLargoSeparador); // remove the characters already read together with the separator found
		
        intPosicion = strCadena.Find(PstrSeparador);
    }
	
	// Add the rest of the string as a last element
    ParrResultado.Add(strCadena);
	
    return(&ParrResultado);
}

//_______________________________________________________________________________________________________________________

void clsLibrary::ShowError(wxString pVstrError) {
	// TODO: implement a better modal dialog than the standard one, with the possibility of copying the message to the clipboard, showing icons, etc.
	wxMessageBox(pVstrError, _T("An error has been detected"), wxICON_ERROR, wxTheApp->GetTopWindow());
}

//_______________________________________________________________________________________________________________________

int clsLibrary::intFromDouble(double pVdblNumber) {
	if(pVdblNumber > 0.0) {
		// The given number is positive
		return(int(pVdblNumber + 0.5));
	} else if(pVdblNumber > 0.0) {
		// The given number is negative
		return(int(pVdblNumber - 0.5));
	} else {
		// The given number is 0
		return(int(0));
	}
}

//_______________________________________________________________________________________________________________________

void clsLibrary::EnsureDirectoryExistence(const wxString& pRstrDirectory) {
	if(!wxDir::Exists(pRstrDirectory)) {
		// The directory doesn't exist
		// It have to create it
		if(!wxFileName::Mkdir(pRstrDirectory, 0777, wxPATH_MKDIR_FULL)) {
			wxString strError = _T("The directory could not be created: %s");
			wxLogWarning(strError, pRstrDirectory.c_str());
		}
	}
}
