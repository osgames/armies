// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "clsTerrain.h"

IMPLEMENT_DYNAMIC_CLASS(clsTerrain, clsBoardObject) // to enable RTTI

/*!
* Definition of static variables
*/

wxString clsTerrain::sVstrTypeDescription = "";

//_______________________________________________________________________________________________________________________

clsTerrain::clsTerrain() 
	: mVarrBorderingBaseTypes(8)
{
	if(sVstrTypeDescription == "") {
		// The property is not set jet (it's set when the first instance of this class is instantiated)
		sVstrTypeDescription = _("Terrain");
	}
	
	InitializeBorderingBaseTypes();
}

//_______________________________________________________________________________________________________________________

clsTerrain::~clsTerrain() {
}

//_______________________________________________________________________________________________________________________

void clsTerrain::InitializeBorderingBaseTypes() {
	for(int i = 0; i < 8; i++) {
		mVarrBorderingBaseTypes[i] = enmBaseType_Empty;
	}
}

//_______________________________________________________________________________________________________________________

void clsTerrain::InitializeBorderingBaseTypes(enmBaseType pVienBaseType) {
	for(int i = 0; i < 8; i++) {
		if(mVarrBorderingBaseTypes[i] == enmBaseType_Empty) {
			// The current element is not set
			mVarrBorderingBaseTypes[i] = pVienBaseType;
		}
	}
}

//_______________________________________________________________________________________________________________________

void clsTerrain::SetVeniBaseType(const enmBaseType pVeniBaseType)
{
	mVienBaseType = pVeniBaseType;
	InitializeBorderingBaseTypes(pVeniBaseType);
}

//_______________________________________________________________________________________________________________________

void clsTerrain::SetVstrName(const wxString& PVstrName) {
	clsBoardObject::SetVstrName(PVstrName); // calls default implementation

	if(PVstrName == _T("Grassland")) {
		this->SetVstrDescription(_("Grassland"));
		this->SetVeniBaseType(enmBaseType_GrassLand);
		mSclrMapColor = Make_CP(new wxColor(78, 129, 58));
		this->SetOffsetImage(wxPoint(0, 0));
		this->SetVstrBaseDescription(_("Grassland"));

	} else if(PVstrName == _T("Grassland2")) {
		this->SetVstrDescription(_("Grassland"));
		this->SetVeniBaseType(enmBaseType_GrassLand);
		mSclrMapColor = Make_CP(new wxColor(78, 129, 58));
		this->SetOffsetImage(wxPoint(600, 0));
		this->SetVstrBaseDescription(_("Grassland"));
		
	} else if(PVstrName == _T("Water")) {
		this->SetVstrDescription(_("Water"));
		this->SetVeniBaseType(enmBaseType_Water);
		mSclrMapColor = Make_CP(new wxColor(64, 90, 225));
		this->SetOffsetImage(wxPoint(40, 0));
		this->SetVstrBaseDescription(_("Water"));

	} else if(PVstrName == _T("Water2")) {
		this->SetVstrDescription(_("Water"));
		this->SetVeniBaseType(enmBaseType_Water);
		mSclrMapColor = Make_CP(new wxColor(64, 90, 225));
		this->SetOffsetImage(wxPoint(560, 0));
		this->SetVstrBaseDescription(_("Water"));
  
	} else if(PVstrName == _T("Mountain")) {
		this->SetVstrDescription(_("Mountain"));
		this->SetVeniBaseType(enmBaseType_Mountain);
		mSclrMapColor = Make_CP(new wxColor(198, 156, 109));
		this->SetOffsetImage(wxPoint(40, 80));
		this->SetVstrBaseDescription(_("Mountain"));

	// -------------------------------------------------------------------------------------------------------------------
	// Coast

	} else if(PVstrName == _T("LeftCoast")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(80, 0));
		this->SetVstrBaseDescription(_("Coast"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Water);
  
	} else if(PVstrName == _T("RightCoast")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(120, 0));
		this->SetVstrBaseDescription(_("Coast"));
  
		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);

	} else if(PVstrName == _T("TopCoast")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(160, 0));
		this->SetVstrBaseDescription(_("Coast"));
  
		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Water);

	} else if(PVstrName == _T("BottomCoast")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(200, 0));
		this->SetVstrBaseDescription(_("Coast"));
  
		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);

	// -------------------------------------------------------------------------------------------------------------------
	// Island

	} else if(PVstrName == _T("TopLeftIsland")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(280, 0));
		this->SetVstrBaseDescription(_("Island"));
  
		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);

	} else if(PVstrName == _T("TopRightIsland")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(240, 0));
		this->SetVstrBaseDescription(_("Island"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Water);

	} else if(PVstrName == _T("BottomLeftIsland")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(360, 0));
		this->SetVstrBaseDescription(_("Island"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Water);
  
	} else if(PVstrName == _T("BottomRightIsland")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(320, 0));
		this->SetVstrBaseDescription(_("Island"));
  
		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Water);

	// -------------------------------------------------------------------------------------------------------------------
	// Lake

	} else if(PVstrName == _T("TopRightLake")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(400, 0));
		this->SetVstrBaseDescription(_("Lake"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);
  
	} else if(PVstrName == _T("TopLeftLake")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(440, 0));
		this->SetVstrBaseDescription(_("Lake"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Water);
  
	} else if(PVstrName == _T("BottomRightLake")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(480, 0));
		this->SetVstrBaseDescription(_("Lake"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);
  
	} else if(PVstrName == _T("BottomLeftLake")) {
		this->SetVstrDescription(_("Coast"));
		mSclrMapColor = Make_CP(new wxColor(1, 184, 182));
		this->SetOffsetImage(wxPoint(520, 0));
		this->SetVstrBaseDescription(_("Lake"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Coast);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Water);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);
  
	// -------------------------------------------------------------------------------------------------------------------
	// Mountain

	} else if(PVstrName == _T("LeftMountain")) {
		this->SetVstrDescription(_("Mountain"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(80, 80));
		this->SetVstrBaseDescription(_("Mountain"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Mountain);
  
	} else if(PVstrName == _T("RightMountain")) {
		this->SetVstrDescription(_("Mountain"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(120, 80));
		this->SetVstrBaseDescription(_("Mountain"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);
  
	} else if(PVstrName == _T("TopMountain")) {
		this->SetVstrDescription(_("Mountain"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(160, 80));
		this->SetVstrBaseDescription(_("Mountain"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Mountain);
  
	} else if(PVstrName == _T("BottomMountain")) {
		this->SetVstrDescription(_("Mountain"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(200, 80));
		this->SetVstrBaseDescription(_("Mountain"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);
  
	// -------------------------------------------------------------------------------------------------------------------
	// Valley

	} else if(PVstrName == _T("TopRightValley")) {
		this->SetVstrDescription(_("Mountain"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(240, 80));
		this->SetVstrBaseDescription(_("Valley"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Mountain);
  
	} else if(PVstrName == _T("TopLeftValley")) {
		this->SetVstrDescription(_("Mountain"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(280, 80));
		this->SetVstrBaseDescription(_("Valley"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);
  
	} else if(PVstrName == _T("BottomRightValley")) {
		this->SetVstrDescription(_("Mountain"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(320, 80));
		this->SetVstrBaseDescription(_("Valley"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Mountain);
  
	} else if(PVstrName == _T("BottomLeftValley")) {
		this->SetVstrDescription(_("Mountain"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(360, 80));
		this->SetVstrBaseDescription(_("Valley"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Mountain);
  
	// ------------------------------------------------------------------------------------------------------------------
	// Mount
	
	} else if(PVstrName == _T("TopRightMount")) {
		this->SetVstrDescription(_("Mount"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(400, 80));
		this->SetVstrBaseDescription(_("Mount"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);
  
	} else if(PVstrName == _T("TopLeftMount")) {
		this->SetVstrDescription(_("Mount"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(440, 80));
		this->SetVstrBaseDescription(_("Mount"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Top, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_Mountain);
  
	} else if(PVstrName == _T("BottomRightMount")) {
		this->SetVstrDescription(_("Mount"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(480, 80));
		this->SetVstrBaseDescription(_("Mount"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Right, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);
  
	} else if(PVstrName == _T("BottomLeftMount")) {
		this->SetVstrDescription(_("Mount"));
		mSclrMapColor = Make_CP(new wxColor(210, 171, 127));
		this->SetOffsetImage(wxPoint(520, 80));
		this->SetVstrBaseDescription(_("Mount"));

		// Base Types
		this->SetVeniBaseType(enmBaseType_Mount);
		this->SetVienBorderingBaseType(enmBoardDirection_Bottom, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_Left, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_TopRight, enmBaseType_Mountain);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomLeft, enmBaseType_GrassLand);
		this->SetVienBorderingBaseType(enmBoardDirection_BottomRight, enmBaseType_GrassLand);
	}
}
