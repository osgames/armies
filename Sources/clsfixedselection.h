#ifndef CLSFIXEDSELECTION_H
#define CLSFIXEDSELECTION_H

/*!
	Represents the fixed position used to mark a multi-square area in board edition mode.

	The fixed selection is used when it's necessary to determine a source square and a target one.
	The source square is represented by the Fixed Selection, whereas the Target Square is the normal fickle cursor.
	This concept is useful for filling rectangular areas as well as for executing unit actions in another sector of
	the board, different from where it is currently positioned.
	If both row and column are set to -1, this means that there isn't any Fixed Selection active.
	This class is responsible for update the status bar indicator too.

	@author Germ�n Blando <germanblando@yahoo.com.ar>
*/

#include "stdafx.h"

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */
#include "clsPointer.h"
#include "clsBoard.h"
#include "clsCanvas.h"
#include <wx/image.h>

/*!
 * Forward declarations
 */

/*!
 * Namespaces
 */

//_____________________________________________________________________________________________________________________

class clsFixedSelection{
public:
    clsFixedSelection();
    ~clsFixedSelection();

	//...........................................................................................................................
	// Methods

	//! Draw a new cursor slide in the screen
	void Draw();

	//! Return true when there isn't an active fixed selection
	bool blnEmpty() const { return(mVintColumn == -1); }

	//! Clear the fixed selection
	void Erase();

	//! Loads the graphics used by the object
	static void LoadGraphics();

	//! Set the static shortcuts that the class need
	static void SetShortcuts();

	//! Set the fixed selection
	void Set(int pVintColumn, int pVintRow);

	//! Restore the previous fixed selection
	void Restore();

	//! Return true if exists a previous fixed selection that can be restored
	bool blnExistsPrevious() const { return(mVintPrevRow != -1); }

	//...........................................................................................................................
	// Getters / Setters

	int GetVintRow() const { return(mVintRow); }
	int GetVintColumn() const { return(mVintColumn); }

//_____________________________________________________________________________________________________________________

protected:

	// Shortcuts

	static clsPointer<clsBoard> mOobjBoard;
	static clsPointer<clsCanvas> mOobjCanvas;

//_____________________________________________________________________________________________________________________

private:

	//....................................................................................................................
	// Variables

	//! Row position in the board. Y-axis position. If there is not any fixed selection, this variable is set to -1.
	int mVintRow;

	//! Column position in the board. X-axis position. If there is not any fixed selection, this variable is set to -1.
	int mVintColumn;

	//! Store the previous row selected. This is used to restore a previous fixed selection
	int mVintPrevRow;

	//! Store the previous column selected. This is used to restore a previous fixed selection
	int mVintPrevColumn;

	//! Graphic for represent the object
	static wxImage mVimgGraphic;

	//! Icon for represent the object
	static wxIcon mVicnIcon;

	//....................................................................................................................
	// Methods

	static void LoadGraphic();
	static void LoadIcon();
};

#endif
