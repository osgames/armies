#include "clsMap.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

/*!
* Includes
*/
#include "Global.h"
#include "ClsApplication.h"
#include "clsBoard.h"
#include "clsFrame.h"

BEGIN_EVENT_TABLE(clsMap, wxWindow)
	EVT_LEFT_DOWN(clsMap::OnLeftDown)
END_EVENT_TABLE()

//_______________________________________________________________________________________________________________________

clsMap::clsMap(wxWindow *parent, wxWindowID id)
	: wxWindow(parent, id, wxDefaultPosition, wxDefaultSize, wxNO_FULL_REPAINT_ON_RESIZE)
{
}

//_______________________________________________________________________________________________________________________

clsMap::~clsMap() {
}

//_______________________________________________________________________________________________________________________

void clsMap::SetOobjBoard(clsPointer<clsBoard> value) {
	mOobjBoard = value;
	
	mSbmpBuffer = Make_CP(new wxBitmap(mOobjBoard->GetVintMAX_SQUARES(), mOobjBoard->GetVintMAX_SQUARES()));
}

//_______________________________________________________________________________________________________________________

void clsMap::DrawPixel(int pVintRow, int pVintColumn, const wxColour& pRclrColor) {
	wxMemoryDC mdcMemoryDC;
	wxPen objPen(pRclrColor);
	
	mdcMemoryDC.SelectObject(*mSbmpBuffer);
	mdcMemoryDC.SetPen(objPen);
	mdcMemoryDC.DrawPoint(pVintColumn, pVintRow);
	mdcMemoryDC.SelectObject(wxNullBitmap);
}

//_______________________________________________________________________________________________________________________

void clsMap::SetVisibleRegion(wxRect pVrctBoardVisibleRegion) {
	wxSize objMapWindowSize = this->GetSize();

	mVdblScreenToMapScale = (double) objMapWindowSize.GetWidth() / (mOobjBoard->GetVintWidth() * mOobjBoard->GetVintSQUARE_LENGTH());

	mVrctVisibleRegion.SetTop(clsLibrary::intFromDouble(pVrctBoardVisibleRegion.GetTop() * mVdblScreenToMapScale));
	mVrctVisibleRegion.SetLeft(clsLibrary::intFromDouble(pVrctBoardVisibleRegion.GetLeft() * mVdblScreenToMapScale));
	mVrctVisibleRegion.SetHeight(clsLibrary::intFromDouble(pVrctBoardVisibleRegion.GetHeight() * mVdblScreenToMapScale));
	mVrctVisibleRegion.SetWidth(clsLibrary::intFromDouble(pVrctBoardVisibleRegion.GetWidth() * mVdblScreenToMapScale));
}

//_______________________________________________________________________________________________________________________

void clsMap::ShowMap() {
	wxSize objMapWindowSize = this->GetSize();
	wxClientDC dctMapWindow(this);
	wxMemoryDC dctScaled;
	wxBitmap bmpBuffer(mSbmpBuffer->GetSubBitmap(wxRect(0, 0, mOobjBoard->GetVintWidth(), mOobjBoard->GetVintHeight())));
	wxImage imgScaled(bmpBuffer.ConvertToImage());
	wxPen objPen(*wxWHITE);

	if(objMapWindowSize.GetWidth() == 0 || objMapWindowSize.GetHeight() == 0) {
		// Some of the two dimensions is invalid
		// It is necessary to do this checking because sometimes the resize event ends calling to this point with an invalid dimension for the window map.
		// The invalid dimension of the window map happens due the hiding of it in some modes.
		return;
	}
	
	imgScaled.Rescale(objMapWindowSize.GetWidth(), objMapWindowSize.GetHeight());
	wxBitmap bmpScaled(imgScaled);
	dctScaled.SelectObject(bmpScaled);
	
	// Draw the visible region
	dctScaled.SetPen(objPen);
	dctScaled.SetBrush(*wxTRANSPARENT_BRUSH);
	dctScaled.DrawRectangle(mVrctVisibleRegion);
	
	dctMapWindow.Blit(0, 0, objMapWindowSize.GetWidth(), objMapWindowSize.GetHeight(), &dctScaled, 0, 0);
	dctScaled.SelectObject(wxNullBitmap);
}

//_______________________________________________________________________________________________________________________

void clsMap::OnLeftDown(wxMouseEvent& event) {
	int intPrevRow = mOobjBoard->GetVintRowSelected();
	int intPrevColumn = mOobjBoard->GetVintColumnSelected();
	int intNewRow; // square unit
	int intNewColumn; // square unit
	int intBoardCoordinate; // board pixels unit

	intBoardCoordinate = clsLibrary::intFromDouble(event.GetY() / mVdblScreenToMapScale); // transform from map to board coordinate
	intNewRow = mOobjBoard->intPixel2Square(intBoardCoordinate); // transform from board pixels to board squares coordinate
	if(intNewRow > mOobjBoard->GetVintHeight() - 1) {
		// Row overflow
		intNewRow = mOobjBoard->GetVintHeight() - 1;
	}

	intBoardCoordinate = clsLibrary::intFromDouble(event.GetX() / mVdblScreenToMapScale); // transform from map to board coordinate
	intNewColumn = mOobjBoard->intPixel2Square(intBoardCoordinate); // transform from board pixels to board squares coordinate
	if(intNewColumn > mOobjBoard->GetVintHeight() - 1) {
		// Column overflow
		intNewColumn = mOobjBoard->GetVintHeight() - 1;
	}

	// Redraw the square previously selected (to erase the cursor). The square currently selected will be drawn by the timer code that makes the cursor animation.
	mOobjBoard->Draw(intPrevRow, intPrevColumn);
	
	// Select the new square
	mOobjBoard->SetSelectedSquare(intNewRow, intNewColumn);

	mOobjBoard->CenterSelection();
	mOobjFrame->UpdateEnvironment();
	mOobjBoard->UpdateCoordinates();
}

//_______________________________________________________________________________________________________________________

wxImage clsMap::imgGetPreview() {
	wxBitmap bmpBuffer(mSbmpBuffer->GetSubBitmap(wxRect(0, 0, mOobjBoard->GetVintWidth(), mOobjBoard->GetVintHeight())));
	wxImage imgPreview(bmpBuffer.ConvertToImage());
	
	return(imgPreview);
}
