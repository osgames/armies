#ifndef _CLSFRAME_H_
#define _CLSFRAME_H_

/*!
 * Includes
 */

////@begin includes
#include "wx/frame.h"
////@end includes

#include "wx/frame.h"
#include "wx/gbsizer.h"
#include "wx/notebook.h"
#include "wx/listctrl.h"
#include "wx/treectrl.h"
#include "wx/statline.h"
#include "wx/imaglist.h"

#include "clsCanvas.h"
#include "clsStatusBar.h"
#include "jrb_ptr.h"
#include "Containers.h"
#include "arraytemplate.h"

//_______________________________________________________________________________________________________________________

/*!
 * Forward declarations
 */
class clsToolBar;
class clsMap;
class clsOptions;

////@begin forward declarations
class wxMenu;
////@end forward declarations

/*!
 * Namespaces
 */
using namespace jrb_sptr;

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_FRAME 10000
#define ID_File_New 10003
#define ID_File_New_LocalGame 10005
#define ID_File_New_Board 10018
#define ID_File_Open 10062
#define ID_File_Open_LocalGame 10006
#define ID_File_Open_Board 10063
#define ID_File_Close 10008
#define ID_File_Save 10009
#define ID_File_SaveAs 10010
#define ID_File_Settings 10011
#define wxID_File_Exit 10007
#define ID_MENUITEM 10020
#define ID_Board_AddObject 10021
#define ID_Board_DeleteObject 10022
#define ID_Board_CancelAction 10032
#define ID_Board_AutoBorder 10061
#define ID_Board_FillRectangularArea 10023
#define ID_Board_RecallLastMark 10035
#define ID_Board_Grassland 10024
#define ID_Board_Water 10025
#define ID_Board_Mountain 10026
#define ID_Board_GrasslandWaterLimits 10034
#define ID_LeftSea 10037
#define ID_RightSea 10037
#define ID_TopSea 10038
#define ID_BottomSea 10039
#define ID_TopLeftIsland 10040
#define ID_TopRightIsland 10041
#define ID_BottomLeftIsland 10042
#define ID_BottomRightIsland 10043
#define ID_TopLeftLake 10045
#define ID_TopRightLake 10044
#define ID_BottomLeftLake 10047
#define ID_BottomRightLake 10046
#define ID_Board_GrasslandMountainLimits 10048
#define ID_LeftMount 10049
#define ID_RightMount 10050
#define ID_TopMount 10051
#define ID_BottomMount 10052
#define ID_TopRightValley 10053
#define ID_TopLeftValley 10054
#define ID_BottomRightValley 10055
#define ID_BottomLeftValley 10056
#define ID_TopRightMountainRange 10057
#define ID_TopLeftMountainRange 10058
#define ID_BottomRightMountainRange 10059
#define ID_BottomLeftMountainRange 10060
#define ID_Eraser 10036
#define ID_Board_Tree 10027
#define ID_Board_Minerals 10028
#define ID_Board_Vegetables 10029
#define ID_Board_Fish 10030
#define ID_Board_Oil 10031
#define ID_Board_Properties 10033
#define ID_Help_Contents 10012
#define ID_Help_Tutorial 10013
#define ID_Help_HomePage 10014
#define ID_Help_SendComments 10015
#define ID_NewsGroup 10016
#define ID_MENU 10017
#define ID_Help_About 10002
#define SYMBOL_CLSFRAME_STYLE wxDEFAULT_FRAME_STYLE|wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxCLOSE_BOX
#define SYMBOL_CLSFRAME_TITLE _("Armies")
#define SYMBOL_CLSFRAME_IDNAME ID_FRAME
#define SYMBOL_CLSFRAME_SIZE wxSize(400, 300)
#define SYMBOL_CLSFRAME_POSITION wxDefaultPosition
////@end control identifiers

// IDs (0 to wxID_LOWEST - 1)
enum
{
	// Toolbar buttons IDs
	ID_TOOL_NewGame = 0
	, ID_TOOL_NewBoard
	, ID_TOOL_Open
	, ID_TOOL_Save
	, ID_TOOL_Eraser
	, ID_TOOL_Grassland
	, ID_TOOL_Water
	, ID_TOOL_Mountain
	, ID_TOOL_Tree
	, ID_TOOL_Minerals
	, ID_TOOL_Oil
	, ID_TOOL_Fish
	, ID_TOOL_Vegetables

	// Timers
	, ID_TIMER_Cursor
		
	// Properties
	, ID_tabProperties
	, ID_pagProperties
	, ID_lvwProperties
	, ID_pagContent
	, ID_trcContent
	, ID_lblSecondsLeft
	, ID_lblRound
	, ID_lblRoundLimit
	, ID_lblActionsLeft
	, ID_NoObject
};

/*!
 * Compatibility
 */

#ifndef wxCLOSE_BOX
#define wxCLOSE_BOX 0x1000
#endif
#ifndef wxFIXED_MINSIZE
#define wxFIXED_MINSIZE 0
#endif

//_______________________________________________________________________________________________________________________

/*!
 * Main application frame
 */
class clsFrame: public wxFrame
{    
    DECLARE_CLASS( clsFrame )
    DECLARE_EVENT_TABLE()

//_______________________________________________________________________________________________________________________

public:
	
	//-------------------------------------------------------------------------------------------------------------------
	// Constants / Enumerations
		
	enum enm_imlProperties {
		enm_imlProperties_Information
			, enm_imlProperties_Health_0
			, enm_imlProperties_Health_1
			, enm_imlProperties_Health_2
			, enm_imlProperties_Health_3
			, enm_imlProperties_Health_4
			, enm_imlProperties_Health_5
			, enm_imlProperties_Health_6
			, enm_imlProperties_Health_7
			, enm_imlProperties_Health_8
			, enm_imlProperties_Health_9
			, enm_imlProperties_Health_10
			, enm_imlProperties_Health_11
			, enm_imlProperties_Health_12
			, enm_imlProperties_Health_13
			, enm_imlProperties_Health_14
	};
	
	// ------------------------------------------------------------------------------------------------------------------
	// Constructor / Destructors

	/// Constructors
    clsFrame( wxWindow* parent, wxWindowID id = SYMBOL_CLSFRAME_IDNAME, const wxString& caption = SYMBOL_CLSFRAME_TITLE, const wxPoint& pos = SYMBOL_CLSFRAME_POSITION, const wxSize& size = SYMBOL_CLSFRAME_SIZE, long style = SYMBOL_CLSFRAME_STYLE );
	virtual ~clsFrame();

    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_CLSFRAME_IDNAME, const wxString& caption = SYMBOL_CLSFRAME_TITLE, const wxPoint& pos = SYMBOL_CLSFRAME_POSITION, const wxSize& size = SYMBOL_CLSFRAME_SIZE, long style = SYMBOL_CLSFRAME_STYLE );

    /// Creates the controls and sizers
    void CreateControls();

////@begin clsFrame event handler declarations

 /// wxEVT_KEY_DOWN event handler for ID_FRAME
 void OnKeyDown( wxKeyEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_File_New_Board
 void OnFileNewBoardClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_File_Open_Board
 void OnFileOpenBoardClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_File_Close
 void OnFileCloseClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_File_Save
 void OnFileSaveClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_File_SaveAs
 void OnFileSaveAsClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for wxID_File_Exit
 void OnFileExitClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM
 void Prueba_OnMenuitemClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_AddObject
 void OnBoardAddobjectClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_DeleteObject
 void OnBoardDeleteObjectClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_CancelAction
 void OnBoardCancelactionClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_AutoBorder
 void OnBoardAutoBorderClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_FillRectangularArea
 void OnBoardFillrectangularareaClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_RecallLastMark
 void OnBoardRecalllastmarkClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Grassland
 void OnBoardGrasslandClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Water
 void OnBoardWaterClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Mountain
 void OnBoardMountainClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_LeftSea
 void OnLeftSeaClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_RightSea
 void OnRightSeaClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopSea
 void OnTopSeaClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomSea
 void OnBottomSeaClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopLeftIsland
 void OnTopLeftIslandClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopRightIsland
 void OnTopRightIslandClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomLeftIsland
 void OnBottomLeftIslandClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomRightIsland
 void OnBottomRightIslandClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopLeftLake
 void OnTopLeftLakeClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopRightLake
 void OnTopRightLakeClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomLeftLake
 void OnBottomLeftLakeClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomRightLake
 void OnBottomRightLakeClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_LeftMount
 void OnLeftMountClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_RightMount
 void  OnRightMountClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopMount
 void OnTopMountClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomMount
 void OnBottomMountClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopRightValley
 void OnTopRightValleyClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopLeftValley
 void OnTopLeftValleyClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomRightValley
 void OnBottomRightValleyClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomLeftValley
 void OnBottomLeftValleyClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopRightMountainRange
 void OnTopRightMountainRangeClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TopLeftMountainRange
 void OnTopLeftMountainRangeClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomRightMountainRange
 void OnBottomRightMountainRangeClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_BottomLeftMountainRange
 void OnBottomLeftMountainRangeClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Eraser
 void OnEraserClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Tree
 void OnBoardTreeClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Minerals
 void OnBoardMineralsClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Vegetables
 void OnBoardVegetablesClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Fish
 void OnBoardFishClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Oil
 void OnBoardOilClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Board_Properties
 void OnBoardPropertiesClick( wxCommandEvent& event );

 /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_Help_About
 void OnHelpAboutClick( wxCommandEvent& event );

////@end clsFrame event handler declarations

////@begin clsFrame member function declarations

 /// Retrieves bitmap resources
 wxBitmap GetBitmapResource( const wxString& name );

 /// Retrieves icon resources
 wxIcon GetIconResource( const wxString& name );
////@end clsFrame member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin clsFrame member variables
 wxMenu* mPmnuBoard;
////@end clsFrame member variables

	// ------------------------------------------------------------------------------------------------------------------
	// Getters / Setters

	clsStatusBar* GetPobjStatusBar() const { return(mPobjStatusBar); }
	clsCanvas* GetpObjCanvas() const { return(mPobjCanvas); }
	void SetOobjBoard(clsPointer<clsBoard> value);
	clsMap* GetPobjMap() const { return(mOobjMap); }

	// ------------------------------------------------------------------------------------------------------------------
	// Methods

	/**
	* Make all changes in the user interface when doesn't exist any game loaded
	*/
	void ChangeStateEmpty();
	
	/**
	* It does all the changes required in the user interface when begins the edition of the board
	*/
	void ChangeStateBoardEdition();

	/**
	* Update actions and information in different times of the game playing.
	*/
	void UpdateEnvironment();

	/**
	* Insert a new variable property in a given list, adding a graphic level icon too
	*/
	void InsertVariableProperty(wxString pVstrLabel, int pVintActualLevel, int pVintMaxLevel, clsPointer<wxListCtrl> pOlvwProperties, int& pVintPosition);

	//-------------------------------------------------------------------------------------------------------------------
	// Events

	void OnTimer_Cursor(wxTimerEvent& event);
	void OnClose(wxCloseEvent& event);
	
	//...................................................................................................................
	// Common toolbar events
	
	void OnToolClick_NewBoard(wxCommandEvent& event);
	void OnToolClick_Eraser(wxCommandEvent& event);
	void OnToolClick_Grassland(wxCommandEvent& event);
	void OnToolClick_Water(wxCommandEvent& event);
	void OnToolClick_Mountain(wxCommandEvent& event);
	void OnToolClick_Tree(wxCommandEvent& event);
	void OnToolClick_Minerals(wxCommandEvent& event);
	void OnToolClick_Oil(wxCommandEvent& event);
	void OnToolClick_Fish(wxCommandEvent& event);
	void OnToolClick_Vegetables(wxCommandEvent& event);
	
//_______________________________________________________________________________________________________________________
	
private:
	
	// ------------------------------------------------------------------------------------------------------------------
	// Methods

	void CreateLayout();
	void ConstructStatusBar();

	/**
	* Create a new empty board
	*/
	void NewBoard();

	/**
	* Close the active document
	*/
	void CloseDocument();

	/**
	* Sets the menu arrays with information to identify and manage its visibility
	*/
	void FillMenuArrays();

	/**
	* If exists an active document, ask for close it.
	* This function doesn't close the document, only requires confirmation for do it in case of one is open.
	* Returns [true] if can continue with the action that requires close the previous document.
	* In PPblnCloseDocument return [true] if is necessary close a previous document before continue the action in course.
	*/
	bool blnConfirmClosePreviousDoc(bool& pRblnCloseDocument);
	
	/**
	* Overloading.
	* To call when it doesn't matter if it's neccesary to close a map or game.
	*/
	bool blnConfirmClosePreviousDoc();
	
	/**
	* Returns the ordinal position for a given object wxMenu into the main menu bar, referenced by its internal name.
	* If the wanted menu cannot be found, a runtime error is showed and the function returns -1.
	*/
	int intScreenPositionMenu(wxString pVstrMenuName);

	/**
	* Returns the position of the menu into the arrays menus
	*/
	int intArrayPositionMenu(wxString pVstrMenuName);
	
	/**
	* Simulates the hidden of a wxMenu object. Actually, this behavior cannot be done by wxWidgets.
	* Therefore, it is simulated by dropping the menu and preserving it in the mVarrMenuObjects array
	*/
	void HideMenu(wxString pVstrMenuName);

	/**
	* Simulates the showed of a wxMenu object. Actually, this behavior cannot be done by wxWidgets.
	* Therefore, it is simulated by inserting the menu into the menu bar, extracting it from the mVarrMenuObjects array
	*/
	void ShowMenu(wxString pVstrMenuName);

	/**
	* Clears the invisible menus which are stored inside the mVarrMenuObjects array
	*/
	void EmptyMenus();
	
	/**
	* Update actions in menus and toolbar buttons when the game is in "BoardEdition" mode.
	* Enables or disables the actions that can be executed both from menus and
	* from toolbar buttons, during the board edition and while a game is played.
	*/
	void UpdateActionsBoardEdition();

	/**
	* Update the information diplayed when the game is in "BoardEdition" mode.
	*/
	void UpdateInformationBoardEdition();
	
	/**
	* Updates actions in menus and toolbar buttons when the game is on
	* "Playing" mode.
	*/
	void UpdateActionsGame();

	/**
	* Creates the buttons that are common to several toolbars
	*/
	void CreateCommonToolbarButtons(clsPointer<wxToolBar> pOtlbToolbar);
	
	void CreateEmptyToolbar();
	void CreateBoardEditionToolbar();

	/**
	* Activate timers in the frame
	*/
	void StartTimers();

	/**
	* Stop timers in the frame
	*/
	void StopTimers();
	
	/**
	* Updates the toolbar while editing a board, according with the state of the differnts menus.
	*/
	void UpdateToolbarBoardEdition();
	
	/**
	* Makes the property panel
	*/
	void CreateProperties(wxPanel* pPpnlPanel);
	
	/**
	* Formats the list properties
	*/
	void ArrangeListProperties();
	
	/**
	* Return an icon representation that shows the actual level of a given property
	*/
	clsFrame::enm_imlProperties FienPropertyLevelBar(int pVintActualLevel, int pVintMaxLevel);
	
	/**
	* Change the state (enable / disable) of all the wxMenuItem objects inside a given wxMenu
	*/
	void ChangeMenuState(clsPointer<wxMenu> pOmnuMenu, bool pVblnEnable);
	
	// ------------------------------------------------------------------------------------------------------------------
	// Variables

	clsCanvas* mPobjCanvas;
	clsStatusBar* mPobjStatusBar;
	clsPointer<wxMenuBar> mOobjMenuBar;
	wxImageList mVimlProperties;

	// ..................................................................................................................
	// Arrays to control the menu identities and visibilities.
	// To work properly with this set of arrays, all menus created in the UI have to be defined here.
	// Once defined, wxArrays object can be freely hidden or showed
	
	Array<wxString> mVarrMenuNames;
	Array<bool> mVarrMenuVisibilities;
	
	/**
	* This is a non owner collection. Therefore, the responsibility for the clean 
	* of the wxMenu objects contained here is yours.
	*/
	Array< clsPointer<wxMenu> > mVarrMenuObjects;

	/**
	* Stores the captions assigned to each wxMenu object. This array is necessary due to internationalization issues
	* on one hand, and due the fact that it's impossible retrieving the wxMenu caption after the object has been
	* deleted, on the other hand.
	*/
	Array<wxString> mVarrMenuCaptions;

	/**
	* Timer to make the cursor animation
	*/
	wxTimer mVtmrCursor;

	//...................................................................................................................
	// Widget pointers
	
	clsPointer<wxNotebook> mOtabProperties;
	clsPointer<wxListCtrl> mOlvwProperties;
	clsPointer<wxTreeCtrl> mOtrcContent;
	clsPointer<wxStaticText> mOlblSecondsLeft;
	clsPointer<wxStaticText> mOlblRound;
	clsPointer<wxStaticText> mOlblRoundLimit;
	clsPointer<wxStaticText> mOlblActionsLeft;
	clsPointer<wxPanel> mOpnlNoObject;
	clsPointer<wxBoxSizer> mOszrPropertiesTop;
	clsPointer<clsMap> mOobjMap;
	clsPointer<wxPanel> mOpnlCanvasMap;
	
	// ------------------------------------------------------------------------------------------------------------------
	// Shortcuts
	
	clsPointer<clsBoard> mOobjBoard;
	clsPointer<clsToolBar> mOobjToolBar;
	clsPointer<wxToolBar> mOobjEmptyToolBar;
	clsPointer<wxToolBar> mOobjBoardEditionToolBar;
	clsPointer<clsOptions> mOobjOptions;

};

#endif // _CLSFRAME_H_
