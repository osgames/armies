#ifndef CLSIMAGELIST_H
#define CLSIMAGELIST_H

/**
	This class is used to storage shared images and those that will be inserted in controls that work with
	wxImageList objects.
	Images can be load from the images file or from included XPMs.

	@author Germ�n Blando <germanblando@yahoo.com.ar>
*/

#include "stdafx.h"

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */
#include "Containers.h"
#include <wx/imaglist.h>

/*!
 * Forward declarations
 */

/*!
 * Namespaces
 */

//--------------------------------------------------------------------------------------------------------------------------------

class clsImageList : public wxImageList
{
public:
    clsImageList(int width, int height, const bool mask = true, int initialCount = 1);
    ~clsImageList();

	//............................................................................................................................
	// Methods

	int Add(const wxBitmap& bitmap, const wxColour& maskColour, wxString pVstrKey);
	int Add(const wxIcon& icon, wxString pVstrKey);
	wxIcon GetIcon(wxString pVstrKey);

	// Return the index corresponding to the given key
	int intGetIndex(wxString pVstrKey);


//--------------------------------------------------------------------------------------------------------------------------------
private:

	//............................................................................................................................
	// Variables

	// Store the pairs Key/Index given to the images in the wxImageList.
	// Using this hashmap, it's possible to find an image by its name instead of by index
	cls_dct_Integer mVdctIndexes;

	//............................................................................................................................
	// Methods
};

#endif
