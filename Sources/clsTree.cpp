#include "clsTree.h"

#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

//.......................................................................................................................
// Includes
#include "Global.h"
#include "throwmap.h"
#include "ClsApplication.h"

//.......................................................................................................................
// Definition of static members

int clsTree::sVintInstances;
CountedPtr<wxColour> clsTree::sSclrMapColor;
int clsTree::sVintMaxHealth;
int clsTree::sVintMaxCreation;
int clsTree::sVintMaxConstruction;
wxImage clsTree::sVimgStaticRepresentation;
wxImage clsTree::sVimgEmptyGraphic;
wxString clsTree::sVstrBaseDescription;
wxString clsTree::sVstrInternalTypeName;

//_______________________________________________________________________________________________________________________

clsTree::clsTree() {
	NormalConstructor();
}

//_______________________________________________________________________________________________________________________

clsTree::clsTree(bool pVblnObjectType) {
	if(pVblnObjectType) {
		// Construction of the class Object Type
		ObjectTypeConstructor();
	} else {
		NormalConstructor();
	}
}

//_______________________________________________________________________________________________________________________

void clsTree::NormalConstructor() {
	mVintHealth = sVintMaxHealth;
	mVintCreation = sVintMaxCreation;
	mVintConstruction = sVintMaxConstruction;
	mVimgDynamicRepresentation = this->GetVimgStaticRepresentation(); // at the beginning, the representation must be equal to the static one (a non empty resource)
}

//_______________________________________________________________________________________________________________________

void clsTree::ObjectTypeConstructor() {
	mVstrName = _T("Tree");
	sVstrInternalTypeName = mVstrName; // to share the base type among all the specific child objects
	sVstrBaseDescription = _("Tree");

	sSclrMapColor = Make_CP(new wxColor(42, 117, 42));
	sVintMaxHealth = 5;
	sVintMaxCreation = 10;
	sVintMaxConstruction = 5;
	
	this->SetOffsetImage(wxPoint(0, 120));
	this->SetOffsetImageEmpty(wxPoint(200, 120));
}

//_______________________________________________________________________________________________________________________

clsTree::~clsTree()
{

}

//_______________________________________________________________________________________________________________________

void clsTree::ResetCounter() {
	sVintInstances = 0;
}

//_______________________________________________________________________________________________________________________

clsPointer<clsNaturalResource> clsTree::OobjCreateNew() {
	clsPointer<clsTree> OobjNewInstance = new clsTree();
	wxString strInstanceNumber;
	
	sVintInstances++;
	strInstanceNumber = wxString::Format(_T("%d"), sVintInstances);

	OobjNewInstance->SetVstrName(mVstrName + strInstanceNumber);
	OobjNewInstance->SetVstrDescription(sVstrBaseDescription + " " + strInstanceNumber);
	
	// Adds the new object on the corresponding collection
	safemap::insert(gOobjApplication->GetVdctNaturalResources(), OobjNewInstance->GetVstrName(), Make_CP(OobjNewInstance.get()));
	safemap::insert(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNewInstance->GetVstrName(), OobjNewInstance);
	
	return(OobjNewInstance);
}

//_______________________________________________________________________________________________________________________

clsPointer<clsNaturalResource> clsTree::OobjCreateFromFile(c4_RowRef pVrowRow) {
	clsPointer<clsTree> OobjNewInstance = new clsTree();
	
	OobjNewInstance->Load(pVrowRow);
	
	// Adds the new object on the corresponding collection
	safemap::insert(gOobjApplication->GetVdctNaturalResources(), OobjNewInstance->GetVstrName(), Make_CP(OobjNewInstance.get()));
	safemap::insert(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNewInstance->GetVstrName(), OobjNewInstance);
	
	return(OobjNewInstance);
}

//_______________________________________________________________________________________________________________________

void clsTree::ShowContent(clsPointer<wxTreeCtrl> pOtrcContent) {
	// TODO: maybe the best solution here is to hide the wxTreeCtrl when a non container object is selected
}
