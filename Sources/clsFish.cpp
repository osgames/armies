#include "clsFish.h"

#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

//.......................................................................................................................
// Includes
#include "Global.h"
#include "throwmap.h"
#include "ClsApplication.h"

//.......................................................................................................................
// Definition of static members

int clsFish::sVintInstances;
CountedPtr<wxColour> clsFish::sSclrMapColor;
int clsFish::sVintMaxHealth;
int clsFish::sVintMaxCreation;
int clsFish::sVintMaxConstruction;
wxImage clsFish::sVimgStaticRepresentation;
wxImage clsFish::sVimgEmptyGraphic;
wxString clsFish::sVstrBaseDescription;
wxString clsFish::sVstrInternalTypeName;

//_______________________________________________________________________________________________________________________

clsFish::clsFish() {
	NormalConstructor();
}

//_______________________________________________________________________________________________________________________

clsFish::clsFish(bool pVblnObjectType) {
	if(pVblnObjectType) {
		// Construction of the class Object Type
		ObjectTypeConstructor();
	} else {
		NormalConstructor();
	}
}

//_______________________________________________________________________________________________________________________

void clsFish::NormalConstructor() {
	mVintHealth = sVintMaxHealth;
	mVintCreation = sVintMaxCreation;
	mVintConstruction = sVintMaxConstruction;
	mVimgDynamicRepresentation = this->GetVimgStaticRepresentation(); // at the beginning, the representation must be equal to the static one (a non empty resource)
}

//_______________________________________________________________________________________________________________________

void clsFish::ObjectTypeConstructor() {
	mVstrName = _T("Fish");
	sVstrInternalTypeName = mVstrName; // to share the base type among all the specific child objects
	sVstrBaseDescription = _("Fish");

	sSclrMapColor = Make_CP(new wxColor(196, 198, 0));
	sVintMaxHealth = 3;
	sVintMaxCreation = 50;
	sVintMaxConstruction = 0;
	
	this->SetOffsetImage(wxPoint(40, 120));
	this->SetOffsetImageEmpty(wxPoint(240, 120));
}

//_______________________________________________________________________________________________________________________

clsFish::~clsFish()
{

}

//_______________________________________________________________________________________________________________________

void clsFish::ResetCounter() {
	sVintInstances = 0;
}

//_______________________________________________________________________________________________________________________

clsPointer<clsNaturalResource> clsFish::OobjCreateNew() {
	clsPointer<clsFish> OobjNewInstance = new clsFish();
	wxString strInstanceNumber;
	
	sVintInstances++;
	strInstanceNumber = wxString::Format(_T("%d"), sVintInstances);

	OobjNewInstance->SetVstrName(mVstrName + strInstanceNumber);
	OobjNewInstance->SetVstrDescription(sVstrBaseDescription + " " + strInstanceNumber);
	
	// Adds the new object on the corresponding collection
	safemap::insert(gOobjApplication->GetVdctNaturalResources(), OobjNewInstance->GetVstrName(), Make_CP(OobjNewInstance.get()));
	safemap::insert(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNewInstance->GetVstrName(), OobjNewInstance);
	
	return(OobjNewInstance);
}

//_______________________________________________________________________________________________________________________

clsPointer<clsNaturalResource> clsFish::OobjCreateFromFile(c4_RowRef pVrowRow) {
	clsPointer<clsFish> OobjNewInstance = new clsFish();
	
	OobjNewInstance->Load(pVrowRow);
	
	// Adds the new object on the corresponding collection
	safemap::insert(gOobjApplication->GetVdctNaturalResources(), OobjNewInstance->GetVstrName(), Make_CP(OobjNewInstance.get()));
	safemap::insert(gOobjApplication->GetVdcoUpperLevelObjects(), OobjNewInstance->GetVstrName(), OobjNewInstance);
	
	return(OobjNewInstance);
}

//_______________________________________________________________________________________________________________________

void clsFish::ShowContent(clsPointer<wxTreeCtrl> pOtrcContent) {
	// TODO: maybe the best solution here is to hide the wxTreeCtrl when a non container object is selected
}
