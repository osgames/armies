#include "stdafx.h"

#include <vector>
using namespace std;

#include <wx/debug.h>

//========================================================================================================================
// This class is only a thin wrapper around the standard STL vector class
// It mainly provides bounds checking through wxASSERT
//========================================================================================================================

template <class T>
class clsVector {
public:
	clsVector() : mVintElements(0) {;}
	
	//___________________________________________________________________________________________________________________

	clsVector(unsigned int pVintDimension)
		: mVarrVector(pVintDimension) {
		
		mVintElements = pVintDimension;
	}
	
	//...................................................................................................................
	
	// Overload for accept an int instead of an unsigned int
	clsVector(int pVintDimension) 
		: mVarrVector(static_cast<unsigned int>(pVintDimension)) {
	}

	//___________________________________________________________________________________________________________________
	
	// It should works for get and set values
	T& operator[](unsigned int pVintElement) {
		wxASSERT(pVintElement >= 0 && pVintElement < mVarrVector.size());
		return mVarrVector[pVintElement];
	}

	//...................................................................................................................

	// Overload for accept an int instead of an unsigned int
	T& operator[](int pVintElement) {
		unsigned int uinElement = static_cast<unsigned int>(pVintElement);
		return mVarrVector[uinElement];
	}

	//___________________________________________________________________________________________________________________
	
	// It should works for get and set values
	const T& operator[](unsigned int pVintElement) const {
		wxASSERT(pVintElement >= 0 && pVintElement < mVarrVector.size());
		return mVarrVector[pVintElement];
	}
	
	//...................................................................................................................
	
	// Overload for accept an int instead of an unsigned int
	const T& operator[](int pVintElement) const {
		unsigned int uinElement = static_cast<unsigned int>(pVintElement);
		return mVarrVector[uinElement];
	}

	//___________________________________________________________________________________________________________________
	
	// Specifies a new size for a 2D vector. This new size can be greater or smaller
	void resize(unsigned int pVintNewSize) {
		wxASSERT(pVintNewSize >= 0);
		mVarrVector.resize(pVintNewSize);
	}
	
	//...................................................................................................................
	
	// Overload for accept an int instead of an unsigned int
	void resize(int pVintNewSize) {
		unsigned int uinNewSize = static_cast<unsigned int>(pVintNewSize);
		resize(uinNewSize);
	}

	//___________________________________________________________________________________________________________________

	int size() const {
		int intSize = static_cast<int>(mVarrVector.size());
		return(intSize);
	}

	//___________________________________________________________________________________________________________________

	void push_bak(const T& pRvrnElement) {
		mVarrVector.push_back(pRvrnElement);
	}
	
//___________________________________________________________________________________________________________________

private:
	vector<T> mVarrVector;
	unsigned int mVintElements;
};
