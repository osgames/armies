// Turn off warning about symbols too long for debugger
#ifdef __WXMSW__ 

/*
Pragmas to VC++ 6
#pragma warning( disable : 4786 ) // identifier was truncated to 'number' characters in the debug information (a Visual C++ bug)
#pragma warning( disable : 4710 ) // 'function' : function not inlined
#pragma warning( disable : 4284 ) // Will produce errors if applied using infix notation
#pragma warning( disable : 4018 ) // signed/unsigned mismatch
*/

#if (_MSC_VER >= 1400)       // VC8+
#pragma warning(disable : 4996)    // Either disable all deprecation warnings,
// Or just turn off warnings about the newly deprecated CRT functions.
// #ifndef _CRT_SECURE_NO_DEPRECATE
// #define _CRT_SECURE_NO_DEPRECATE
// #endif
// #ifndef _CRT_NONSTDC_NO_DEPRECATE
// #define _CRT_NONSTDC_NO_DEPRECATE
// #endif
#endif   // VC8+

#endif
