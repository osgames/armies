//-----------------------------------------------------------------------------------------------------------------------
// Title: Armies
// Version: 2
// Analyst / Architect / Programmer: Germán Blando
// Graphic design: Pablo Catalani
// Project's start date: 04-18-2005
//-----------------------------------------------------------------------------------------------------------------------

#ifndef ClsApplication_H
#define ClsApplication_H

#include "stdafx.h"

/*!
 * Includes
 */

#include <wx/app.h>
#include <wx/intl.h>
#include <wx/colour.h>
#include <mk4.h>

#include "Containers.h"
#include "jrb_ptr.h"
#include <vector>
#include "clsPointer.h"
#include "Enumerations.h"
#include "arraytemplate.h"
#include "throwmap.h"

/*!
 * Forward declarations
 */
class clsFrame;
class clsBoard;
class clsTerrain;
class clsBoardObject;
class clsImageList;

/*!
 * Namespaces
 */
using namespace jrb_sptr;

//____________________________________________________________________________________________________________________

/**
 * Root object for the application
 */
class clsApplication : public wxApp
{
    DECLARE_CLASS( clsApplication )
    DECLARE_EVENT_TABLE()

//____________________________________________________________________________________________________________________

public:
    clsApplication();
    virtual ~clsApplication();

	//-------------------------------------------------------------------------------------------------------------------
	// Methods (public)
	
	/**
	* override base class virtuals
	* 
	* this one is called on application startup and is a good place for the app
	* initialization (doing it here and not in the ctor allows to have an error
	* return: if OnInit() returns false, the application terminates)
	* 'Main program' equivalent: the program execution "starts" here
	*/
    virtual bool OnInit();

	void ChangeStateEmpty();
	void ChangeStateBoardEdition();
	
	/**
	* Retrieve a terrain, referencing it by its name
	*/
	clsPointer<clsTerrain> GetOobjTerrain(wxString pVstrName) { return(safemap::find(mVdctTerrainsTypes, pVstrName).GetPtr()); }
	
	clsPointer<clsUpperLevelObject> GetOobjUpperLevelObject(wxString pVstrName) { return(safemap::find(mVdcoUpperLevelObjects, pVstrName).get()); }
	
	/**
	* Save the current map when the user is editing it
	*/
	void SaveBoard();

	/**
	* Load a map or game file.
	* The file type is inspected after open the file, by check a specific field.
	*/
	void OpenDocument(wxString pStrFileName);
	
	wxString fVstrVersion() const;

	//-------------------------------------------------------------------------------------------------------------------
	// Getters / Setters (public)

	wxString GetVstrAppPath() const { return(mVstrAppPath); }
	wxString GetVstrDocumentsPath() const { return(mVstrDocumentsPath); }
	CountedPtr<clsBoard> GetSobjBoard() const { return(mSobjBoard); }
	CountedPtr<wxBitmap> GetSbmpGraphics() const { return(mSbmpGraphics); }
	enmStates GetVienState() const { return(mVienState); }
	clsFrame* GetPobjFrame() const { return(mPobjFrame); }
	wxString GetVstrTitle() const { return(mVstrTitle); }
	wxColor GetVclrTransparentBackground() const { return(mVclrTransparentBackground); }
	wxColor GetVclrTransparentPlayer() const { return(mVclrTransparentPlayer); }
	wxString strGetStatesLabels(enmStates pVienState) { return(mVarrStatesLabels[pVienState]); }
	clsPointer<clsImageList> GetOimlImageList_16() const { return(mSimlImageList_16.GetPtr()); }
	cls_dct_clsNaturalResource& GetVdctNaturalResources() { return(mVdctNaturalResources); }
	cls_dct_clsNaturalResource& GetVdctNaturalResourceTypes() { return(mVdctNaturalResourcesTypes); }
	cls_dco_clsUpperLevelObject& GetVdcoUpperLevelObjects() { return(mVdcoUpperLevelObjects); }
	
	enmActionSelected GetVienActionSelected() const { return(mVienActionSelected); }
	void SetVienActionSelected(enmActionSelected pVienActionSelected) { mVienActionSelected = pVienActionSelected; }

	wxString GetVstrFileName() const { return(mVstrFileName); }
	void SetVstrFileName(const wxString& pRstrValue) { mVstrFileName = pRstrValue; }
	
	bool GetVblnFileOperationFailure() const { return(mVblnFileOperationFailure); }
	void SetVblnFileOperationFailure(bool pVblnFileOperationFailure) { mVblnFileOperationFailure = pVblnFileOperationFailure; }
	
	clsPointer<c4_Storage> GetOdbfDatabase() { return(mOdbfDatabase); }
	
	int GetVintMajorVersion() const { return(mVintMajorVersion); }
	void SetVintMajorVersion(int pVintValue) { mVintMajorVersion = pVintValue; }
	
	int GetVintMinorVersion() const { return(mVintMinorVersion); }
	void SetVintMinorVersion(int pVintValue) { mVintMinorVersion = pVintValue; }
	
	int GetVintReleaseNumber() const { return(mVintReleaseNumber); }
	void SetVintReleaseNumber(int pVintValue) { mVintReleaseNumber = pVintValue; }
	
	
//____________________________________________________________________________________________________________________

private:
	
	//-------------------------------------------------------------------------------------------------------------------
	// Constants (private)
	
	/**
	* Folder to contain local translations. It's relative to the application's path
	*/
	const wxString mVstrLOCALE_FOLDER;

	/**
	* Transparent color to show the terrain behind board objects
	*/
	const wxColor mVclrTransparentBackground;

	/**
	* Transparency to show the player specific color
	*/
	const wxColor mVclrTransparentPlayer;
	
	/**
	* To use in file load/save
	*/
	const wxString mVstrBOARD_FILE_TYPE;
	
	/**
	* To use in file load/save
	*/
	const wxString mVstrGAME_FILE_TYPE;

	//-------------------------------------------------------------------------------------------------------------------
	// Variables (private)
	
	/**
	* Main frame for the application
	*/
	clsFrame* mPobjFrame;

	/**
	* Board of game. Ever exists, even though doesn't exists any game or board edition activated
	*/
	CountedPtr<clsBoard> mSobjBoard;

	/**
	* For translations and localizations
	*/
	wxLocale mVobjLocale;

	/**
	* Stores the application's path
	*/
	wxString mVstrAppPath;
	
	/**
	* Path where the user can save his files
	*/
	wxString mVstrDocumentsPath;
	
	/**
	* Stores the file name to save a game or board
	*/
	wxString mVstrFileName;
	
	/**
	* All the graphics for draw game objects. All the graphics are placed together
	*/
	CountedPtr<wxBitmap> mSbmpGraphics;

	/**
	* Contains the actual game state
	*/
	enmStates mVienState;

	/**
	* Actions that the user has selected in board edition or game mode
	*/
	enmActionSelected mVienActionSelected;

	/**
	* Application's name. Used (for example) for put text into the title bar
	*/
	wxString mVstrTitle;

	/**
	* To store 16x16 images shared throughout all the project, and those that have to be in a control that works
	* with wxImageList.
	*/
	CountedPtr<clsImageList> mSimlImageList_16;

	//....................................................................................................................
	// Collections of Object Types

	cls_dct_clsNaturalResource mVdctNaturalResourcesTypes;
	cls_dct_clsBuilding mVdctBuildingsTypes;
	cls_dct_clsMobileUnit mVdctMobileUnitesTypes;
	cls_dct_clsTerrain mVdctTerrainsTypes;
	
	//....................................................................................................................
	// Owner Collections

	cls_dct_clsNaturalResource mVdctNaturalResources;

	/**
	* Contains all players included in the game (humans and computer-controlled)
	*/
	cls_dct_clsPlayer mVdctPlayers;
	
	/**
	* Colors used to distinguish different players
	*/
	cls_dct_clsColor mVdctColors;
	
	/**
	* Contains all the buildings existing in the board
	*/
	cls_dct_clsBuilding mVdctBuildings;
	
	/**
	* Contains all the mobile units existing in the board
	*/
	cls_dct_clsMobileUnit mVdctMovileUnits;
	
	/**
	* Contains all extractions that take place in the board at a given moment
	*/
	cls_dct_clsExtractionResource mVdctExtractions;
	
	//...................................................................................................................
	// Auxiliary Collections
	
	/**
	* This dictionary has been created to contain all the upper level objects, due to the difficult of creating
	* these ones from a saved file.
	*/
	cls_dco_clsUpperLevelObject mVdcoUpperLevelObjects;
	
	/**
	* The same that "mVdctPlayers" collection, but with turn number access key
	*/
	cls_dco_clsPlayer mVdctPlayersByTurn;
	
	//...................................................................................................................
	
	/**
	* Labels for the differents states of the game
	*/
	Array<wxString> mVarrStatesLabels;

	/**
	* This object is used to save and load board and game files.
	* This object is created on the moment of saving or loading, and at the end of these actions it is deleted. The pointer
	* is stored here to offer an easy access to other classes.
	*/
	clsPointer<c4_Storage> mOdbfDatabase;
	
	/**
	* To indicate when an error has been found opening or saving a file.
	* Any method used in the opening or saving process can throw an error by change the value of this property to "true".
	* This method of error reporting is used here instead of the scheme of a boolean return in each function.
	*/
	bool mVblnFileOperationFailure;
	
	//...................................................................................................................
	// Variables to hold the application version
	
	int mVintMajorVersion;
	int mVintMinorVersion;
	int mVintReleaseNumber;
	
	//...................................................................................................................
	// Metakit properties
	
	c4_StringProp mVprpStrType;
	c4_IntProp mVprpIntFileVersion;
	c4_StringProp mVprpStrName;
	c4_StringProp mVprpStrInternalTypeName;
	
	//-------------------------------------------------------------------------------------------------------------------
	// Methods (private)

	void LoadHandlers();
	void CreateObjects();
	void CreateColors();
	void CreateTerrains();
	void CreateNaturalResourcesTypes();
	void SetPaths();
	void LoadGraphics();
	void CreateStatesLabels();
	
	/**
	* Load icons in mSimlImageList_16
	*/
	void LoadIcons();
	
	/**
	* Resets the main collections of the game
	*/
	void EmptyCollections();
	
	/**
	* Read the language set for the application
	*/
	wxString strReadConfigLanguage();

	/**
	* Sets i18n and l10n
	*/
	void SetInternationalInfo();

	/**
	* Resets all counters inside a type object collection
	* It is necessary to pass the iterator rather than the collection pointer due problems while compiling the templates
	*/
	template <typename T> void ResetCounters(T it, T end);

	/**
	* Persist the collection with the given name.
	*/
	template <typename T> void SaveCollection(T& pRdctCollection, wxString pVstrCollectionName, wxString pVstrPersistenceStructure);
	
	/**
	* Load a collection from a file.
	*/
	template <typename T> void LoadCollection(T& pRdctCollection, wxString pVstrCollectionName
		, wxString pVstrPersistenceStructure);
	
	/**
	* Persist a collection of object types.
	*/
	template <typename T> void SaveObjectTypeCollection(T& pRdctCollection, c4_View& pRviwView);
		
	/**
	* Update an object type collection with the information retrieved from a file
	*/
	template <typename T> void UpdateObjectTypeCollection(T& pRdctCollection, c4_View& pRviwView);

	/**
	* Save application data to game or board files
	*/
	void Save();

	/**
	* Return the persistence structure used in this class
	*/
	wxString strGetPersistenceStructure();

	/**
	* Return the persistence structure used in this class
	*/
	wxString strGetObjectTypeCollectionStructure();
	
	/**
	* Open a board from a file
	*/
	void OpenBoard();
};

DECLARE_APP(clsApplication)

#endif // ClsApplication_H
