#if !defined(AFX_CLSTERRAIN_H__C80357D0_6E86_463B_90E8_9EA3C5BFD0F8__INCLUDED_)
#define AFX_CLSTERRAIN_H__C80357D0_6E86_463B_90E8_9EA3C5BFD0F8__INCLUDED_

#if __WXMSW__
#pragma once
#endif // __WXMSW__

/*!
 * Includes
 */

#include "clsBoardObject.h"
#include "Enumerations.h"
#include "arraytemplate.h"

/*!
 * Namespaces
 */
using namespace std;

//_______________________________________________________________________________________________________________________

/**
* Only one instance of each terrain type will exist in the application, therefore clsBoardObject::mVpntPosition
* must be set before draw one of them.
*/
class clsTerrain : public clsBoardObject
{
DECLARE_DYNAMIC_CLASS(clsTerrain) // to enable RTTI
public:
	clsTerrain();
	virtual ~clsTerrain();

	//-------------------------------------------------------------------------------------------------------------------
	// Getters / Setters
	
	void SetVeniBaseType(const enmBaseType pVeniBaseType);
	void SetVstrName(const wxString& PVstrName); // @Override

	virtual wxString GetVstrBaseDescription() { return(mVstrBaseDescription); }
	virtual void SetVstrBaseDescription(wxString pVstrBaseDescription) { mVstrBaseDescription = pVstrBaseDescription; }
	
	enmBaseType GetVienBaseType() const { return(mVienBaseType); }
	
	/**
	* For this class, the static and dynamic representation are identical
	*/
	virtual wxImage GetVimgStaticRepresentation() const { return(mVimgDynamicRepresentation); } // @Override
	
	/**
	* For this class, the static and dynamic representation are identical
	*/
	virtual void SetVimgStaticRepresentation(wxImage pVimgGraphic) { mVimgDynamicRepresentation = pVimgGraphic; } // @Override

	virtual wxString GetVstrTypeDescription() const { return(sVstrTypeDescription); } // @Override
	virtual void SetVstrTypeDescription(const wxString pVstrTypeDescription) { sVstrTypeDescription = pVstrTypeDescription; } // @Override

	virtual wxColour* GetVclrMapColor() const { return(mSclrMapColor); } // @Override
	
	enmBaseType GetVienBorderingBaseType(enmBoardDirection pVienBoardDirection) { return(mVarrBorderingBaseTypes[pVienBoardDirection]); }
	void SetVienBorderingBaseType(enmBoardDirection pVienBoardDirection, enmBaseType pVienBaseType) { mVarrBorderingBaseTypes[pVienBoardDirection] = pVienBaseType; }

//_______________________________________________________________________________________________________________________

private:

	//-------------------------------------------------------------------------------------------------------------------
	// Variables
	
	/**
	* Terrain base type (e.g. for all types of coast (LeftCoast, RightCoast), the basic type is "Coast")
	*/
	enmBaseType mVienBaseType;
	
	/**
	* Color to show the object in the Map
	*/
	CountedPtr<wxColour> mSclrMapColor;
	
	/**
	* Generic description for the terrain
	*/
	wxString mVstrBaseDescription;
	
	/**
	* For identify the object's type. This text is user friendly (must be translated).
	*/
	static wxString sVstrTypeDescription;
	
	/**
	* Contain the base terrain type for each direction (in the same order as enmBoardDirection)
	* This information is used for the board editor to do the auto-border function
	*/
	Array<enmBaseType> mVarrBorderingBaseTypes;

	//-------------------------------------------------------------------------------------------------------------------
	// Methods
	
	/**
	* Put enmBaseType_Empty in all the elements of the terrain limits array.
	*/
	void InitializeBorderingBaseTypes();
	
	/**
	* Put the given base type in all the elements of the terrain limits array that has the enmBaseType_Empty value.
	*/
	void InitializeBorderingBaseTypes(enmBaseType pVienBaseType);
};

#endif // !defined(AFX_CLSTERRAIN_H__C80357D0_6E86_463B_90E8_9EA3C5BFD0F8__INCLUDED_)
