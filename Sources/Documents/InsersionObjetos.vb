Insersi�n objetos en el tablero
-------------------------------

Principal.bas
-------------
Public Const gcINSERCION_PRADERA As String = "Inserci�n Pradera"


Principal.frm
-------------
Private moObjetoParaInsertar As Object 'contiene el objeto que se ha seleccionado para insertar en la modalidad de edici�n de mapas
(Esta variable se va a tener que declarar de tipo "clsBoardObject")

Private Sub mnuTabPradera_Click()
    stbBarraEstado.Panels("Accion").Text = gcINSERCION_PRADERA
    pctJuego.MouseIcon = LoadResPicture("InsercionTerreno", vbResCursor)
    pctJuego.MousePointer = vbCustom
    Set moObjetoParaInsertar = cltTiposTerrenos("Pradera")
    Call Me.ActualizarAcciones
End Sub

Private Sub mnuEdiInsertarObjeto_Click()
    Call RealizarAccion
    Call Me.ActualizarAcciones
End Sub

Private Sub RealizarAccion()
    If moObjetoParaInsertar Is Nothing Then
        'No existe ninguna acci�n activa para llevar a cabo
        Beep
        Exit Sub
    ElseIf UCase(TypeName(moObjetoParaInsertar)) = UCase("clsTipoTerreno") Then
        'Inserci�n de tipos de terreno
        If Me.oTablero.lExisteSeleccionFija Then
            Call RellenarRectanguloTerreno
        Else
            Call IntercambiarSuperficies
            Call oTablero.SetearTerreno(oTablero.iFilaSeleccionada, oTablero.iColumnaSeleccionada, moObjetoParaInsertar)
            Call oTablero.Dibujar(oTablero.iFilaSeleccionada, oTablero.iColumnaSeleccionada)
            Call Dibujar
        End If
    Else
        'Inserci�n de Recursos
        If Me.oTablero.lExisteSeleccionFija Then
            Call RellenarRectanguloSuperior(moObjetoParaInsertar)
        Else
            Call InsertarRecurso(Me.oTablero.iFilaSeleccionada, Me.oTablero.iColumnaSeleccionada, moObjetoParaInsertar)
            Call Dibujar
        End If
    End If
End Sub

'Intercambia al azar el par de superficies para pradera o agua.
'
Private Sub IntercambiarSuperficies()
    Select Case LCase(moObjetoParaInsertar.cNombre)
    Case "pradera", "pradera2"
        If Int(2 * Rnd()) = 0 Then
            Set moObjetoParaInsertar = cltTiposTerrenos("Pradera")
        Else
            Set moObjetoParaInsertar = cltTiposTerrenos("Pradera2")
        End If
    Case "agua", "agua2"
        If Int(2 * Rnd()) = 0 Then
            Set moObjetoParaInsertar = cltTiposTerrenos("Agua")
        Else
            Set moObjetoParaInsertar = cltTiposTerrenos("Agua2")
        End If
    End Select
End Sub


clsTablero
----------

'Setea un tipo de terreno espec�fico para el casillero indicado.
'
Public Sub SetearTerreno(tiFila As Integer, tiColumna As Integer, toTipoTerreno As clsTipoTerreno)
    Set moTerreno(tiFila, tiColumna) = toTipoTerreno
    
    'Actualiza el mapa
    Call moRaiz.oMapa.Dibujar(tiFila, tiColumna)
End Sub
