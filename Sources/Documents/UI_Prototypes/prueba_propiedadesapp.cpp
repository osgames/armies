/////////////////////////////////////////////////////////////////////////////
// Name:        prueba_propiedadesapp.cpp
// Purpose:     
// Author:      Elcho
// Modified by: 
// Created:     Sat 01 Sep 2007 07:22:58 ART
// RCS-ID:      
// Copyright:   Truchisoft
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "prueba_propiedadesapp.h"

////@begin XPM images

////@end XPM images


/*!
 * Application instance implementation
 */

////@begin implement app
IMPLEMENT_APP( Prueba_propiedadesApp )
////@end implement app


/*!
 * Prueba_propiedadesApp type definition
 */

IMPLEMENT_CLASS( Prueba_propiedadesApp, wxApp )


/*!
 * Prueba_propiedadesApp event table definition
 */

BEGIN_EVENT_TABLE( Prueba_propiedadesApp, wxApp )

////@begin Prueba_propiedadesApp event table entries
////@end Prueba_propiedadesApp event table entries

END_EVENT_TABLE()


/*!
 * Constructor for Prueba_propiedadesApp
 */

Prueba_propiedadesApp::Prueba_propiedadesApp()
{
    Init();
}


/*!
 * Member initialisation
 */

void Prueba_propiedadesApp::Init()
{
////@begin Prueba_propiedadesApp member initialisation
////@end Prueba_propiedadesApp member initialisation
}

/*!
 * Initialisation for Prueba_propiedadesApp
 */

bool Prueba_propiedadesApp::OnInit()
{    
////@begin Prueba_propiedadesApp initialisation
    // Remove the comment markers above and below this block
    // to make permanent changes to the code.

#if wxUSE_XPM
    wxImage::AddHandler(new wxXPMHandler);
#endif
#if wxUSE_LIBPNG
    wxImage::AddHandler(new wxPNGHandler);
#endif
#if wxUSE_LIBJPEG
    wxImage::AddHandler(new wxJPEGHandler);
#endif
#if wxUSE_GIF
    wxImage::AddHandler(new wxGIFHandler);
#endif
////@end Prueba_propiedadesApp initialisation

    return true;
}


/*!
 * Cleanup for Prueba_propiedadesApp
 */

int Prueba_propiedadesApp::OnExit()
{    
////@begin Prueba_propiedadesApp cleanup
    return wxApp::OnExit();
////@end Prueba_propiedadesApp cleanup
}

