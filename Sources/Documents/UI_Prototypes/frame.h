/////////////////////////////////////////////////////////////////////////////
// Name:        frame.h
// Purpose:     
// Author:      Elcho
// Modified by: 
// Created:     Sun 09 Sep 2007 08:12:52 ART
// RCS-ID:      
// Copyright:   Truchisoft
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _FRAME_H_
#define _FRAME_H_

/*!
 * Includes
 */

////@begin includes
#include "wx/frame.h"
#include "wx/gbsizer.h"
#include "wx/notebook.h"
#include "wx/listctrl.h"
#include "wx/treectrl.h"
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
class wxBoxSizer;
class wxFlexGridSizer;
class wxGridBagSizer;
class wxNotebook;
class wxListCtrl;
class wxTreeCtrl;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_FRAME 10017
#define ID_pnlJuego 10018
#define ID_pnlIzquierda 10019
#define ID_pnlMap 10020
#define ID_pnlCanvasMap 10000
#define ID_wndCanvasMap 10029
#define ID_PpnlProperties 10021
#define ID_mOpnlNoObject 10022
#define wxID_PlblNoObject 10023
#define ID_mOtabProperties 10024
#define ID_Properties 10025
#define ID_mOlvwProperties 10026
#define ID_Content 10027
#define ID_mOtrcContent 10028
#define SYMBOL_FRAME_STYLE wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxCLOSE_BOX
#define SYMBOL_FRAME_TITLE _("Frame")
#define SYMBOL_FRAME_IDNAME ID_FRAME
#define SYMBOL_FRAME_SIZE wxSize(500, 400)
#define SYMBOL_FRAME_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * Frame class declaration
 */

class Frame: public wxFrame
{    
    DECLARE_CLASS( Frame )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    Frame();
    Frame( wxWindow* parent, wxWindowID id = SYMBOL_FRAME_IDNAME, const wxString& caption = SYMBOL_FRAME_TITLE, const wxPoint& pos = SYMBOL_FRAME_POSITION, const wxSize& size = SYMBOL_FRAME_SIZE, long style = SYMBOL_FRAME_STYLE );

    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_FRAME_IDNAME, const wxString& caption = SYMBOL_FRAME_TITLE, const wxPoint& pos = SYMBOL_FRAME_POSITION, const wxSize& size = SYMBOL_FRAME_SIZE, long style = SYMBOL_FRAME_STYLE );

    /// Destructor
    ~Frame();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin Frame event handler declarations

////@end Frame event handler declarations

////@begin Frame member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end Frame member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin Frame member variables
    wxBoxSizer* szrPrincipal;
    wxPanel* pnlJuego;
    wxPanel* pnlIzquierda;
    wxBoxSizer* szrIzquierda;
    wxPanel* PpnlMap;
    wxFlexGridSizer* PszrMap;
    wxPanel* PpnlCanvasMap;
    wxBoxSizer* PszrCanvasMap;
    wxTextCtrl* mOwndCanvasMap;
    wxPanel* PpnlProperties;
    wxGridBagSizer* PszrProperties;
    wxBoxSizer* mOszrPropertiesTop;
    wxPanel* mOpnlNoObject;
    wxBoxSizer* PszrNoObject;
    wxStaticText* PlblNoObject;
    wxNotebook* mOtabProperties;
    wxPanel* pagProperties;
    wxListCtrl* mOlvwProperties;
    wxPanel* pagContent;
    wxTreeCtrl* mOtrcContent;
////@end Frame member variables
};

#endif
    // _FRAME_H_
