/////////////////////////////////////////////////////////////////////////////
// Name:        frmpropiedades.cpp
// Purpose:     
// Author:      Elcho
// Modified by: 
// Created:     Sat 01 Sep 2007 07:24:45 ART
// RCS-ID:      
// Copyright:   Truchisoft
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "frmpropiedades.h"

////@begin XPM images
#include "../../Graficos Desarrollo/Iconos/16x16/properties.xpm"
#include "../../Graficos Desarrollo/Iconos/16x16/content.xpm"
#include "../../Graficos Desarrollo/Iconos/16x16/clock.xpm"
#include "../../Graficos Desarrollo/Iconos/16x16/turn_round.xpm"
#include "../../Graficos Desarrollo/Iconos/16x16/actions.xpm"
////@end XPM images


/*!
 * frmPropiedades type definition
 */

IMPLEMENT_CLASS( frmPropiedades, wxFrame )


/*!
 * frmPropiedades event table definition
 */

BEGIN_EVENT_TABLE( frmPropiedades, wxFrame )

////@begin frmPropiedades event table entries
////@end frmPropiedades event table entries

END_EVENT_TABLE()


/*!
 * frmPropiedades constructors
 */

frmPropiedades::frmPropiedades()
{
    Init();
}

frmPropiedades::frmPropiedades( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create( parent, id, caption, pos, size, style );
}


/*!
 * frmPropiedades creator
 */

bool frmPropiedades::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin frmPropiedades creation
    wxFrame::Create( parent, id, caption, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end frmPropiedades creation
    return true;
}


/*!
 * frmPropiedades destructor
 */

frmPropiedades::~frmPropiedades()
{
////@begin frmPropiedades destruction
////@end frmPropiedades destruction
}


/*!
 * Member initialisation
 */

void frmPropiedades::Init()
{
////@begin frmPropiedades member initialisation
    mOtabProperties = NULL;
    mOlvwProperties = NULL;
    mOtrcContent = NULL;
    mOlblSecondsLeft = NULL;
    mOlblRound = NULL;
    mOlblRoundLimit = NULL;
    mOlblActionsLeft = NULL;
////@end frmPropiedades member initialisation
}


/*!
 * Control creation for frmPropiedades
 */

void frmPropiedades::CreateControls()
{    
////@begin frmPropiedades content construction
    frmPropiedades* itemFrame1 = this;

    wxGridBagSizer* itemGridBagSizer2 = new wxGridBagSizer(0, 0);
    itemGridBagSizer2->AddGrowableRow(0);
    itemGridBagSizer2->AddGrowableCol(0);
    itemGridBagSizer2->SetEmptyCellSize(wxSize(1, 1));
    itemFrame1->SetSizer(itemGridBagSizer2);

    mOtabProperties = new wxNotebook( itemFrame1, ID_tabProperties, wxDefaultPosition, wxDefaultSize, wxBK_DEFAULT );
    wxImageList* mOtabPropertiesImageList = new wxImageList(16, 16, true, 2);
    {
        wxIcon mOtabPropertiesIcon0(itemFrame1->GetIconResource(wxT("../../Graficos Desarrollo/Iconos/16x16/properties.xpm")));
        mOtabPropertiesImageList->Add(mOtabPropertiesIcon0);
        wxIcon mOtabPropertiesIcon1(itemFrame1->GetIconResource(wxT("../../Graficos Desarrollo/Iconos/16x16/content.xpm")));
        mOtabPropertiesImageList->Add(mOtabPropertiesIcon1);
    }
    mOtabProperties->AssignImageList(mOtabPropertiesImageList);

    wxPanel* itemPanel4 = new wxPanel( mOtabProperties, ID_pagProperties, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    wxBoxSizer* itemBoxSizer5 = new wxBoxSizer(wxVERTICAL);
    itemPanel4->SetSizer(itemBoxSizer5);

    mOlvwProperties = new wxListCtrl( itemPanel4, ID_lvwProperties, wxDefaultPosition, wxSize(100, 100), wxLC_REPORT );
    itemBoxSizer5->Add(mOlvwProperties, 1, wxGROW|wxALL, 5);

    mOtabProperties->AddPage(itemPanel4, _("Properties"), false, 0);

    wxPanel* itemPanel7 = new wxPanel( mOtabProperties, ID_pagContent, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    wxBoxSizer* itemBoxSizer8 = new wxBoxSizer(wxVERTICAL);
    itemPanel7->SetSizer(itemBoxSizer8);

    mOtrcContent = new wxTreeCtrl( itemPanel7, ID_trcContent, wxDefaultPosition, wxSize(100, 100), wxTR_SINGLE );
    itemBoxSizer8->Add(mOtrcContent, 1, wxGROW|wxALL, 5);

    mOtabProperties->AddPage(itemPanel7, _("Content"), false, 1);

    wxPanel* itemPanel10 = new wxPanel( mOtabProperties, ID_PANEL2, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    wxBoxSizer* itemBoxSizer11 = new wxBoxSizer(wxHORIZONTAL);
    itemPanel10->SetSizer(itemBoxSizer11);

    wxStaticText* itemStaticText12 = new wxStaticText( itemPanel10, wxID_STATIC, _("Derecha"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    itemBoxSizer11->Add(itemStaticText12, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticText* itemStaticText13 = new wxStaticText( itemPanel10, wxID_STATIC, _("Izquierda"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    itemBoxSizer11->Add(itemStaticText13, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBoxSizer* itemBoxSizer14 = new wxBoxSizer(wxHORIZONTAL);
    itemBoxSizer11->Add(itemBoxSizer14, 0, wxGROW|wxALL, 5);
    wxPanel* itemPanel15 = new wxPanel( itemPanel10, ID_PANEL3, wxDefaultPosition, wxSize(100, 50), wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    itemBoxSizer14->Add(itemPanel15, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);
    wxBoxSizer* itemBoxSizer16 = new wxBoxSizer(wxHORIZONTAL);
    itemPanel15->SetSizer(itemBoxSizer16);

    wxPanel* itemPanel17 = new wxPanel( itemPanel15, ID_PANEL4, wxDefaultPosition, wxSize(100, 300), wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    itemBoxSizer16->Add(itemPanel17, 1, wxGROW|wxALL|wxFIXED_MINSIZE, 2);

    mOtabProperties->AddPage(itemPanel10, _("Tab"));

    itemGridBagSizer2->Add(mOtabProperties, wxGBPosition(0, 0), wxGBSpan(1, 1), wxGROW|wxGROW|wxALL, 2);

    wxStaticLine* itemStaticLine18 = new wxStaticLine( itemFrame1, ID_STATICLINE, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL|wxNO_BORDER );
    itemGridBagSizer2->Add(itemStaticLine18, wxGBPosition(1, 0), wxGBSpan(1, 1), wxGROW|wxALIGN_CENTER_VERTICAL|wxALL, 2);

    wxBoxSizer* itemBoxSizer19 = new wxBoxSizer(wxHORIZONTAL);
    itemGridBagSizer2->Add(itemBoxSizer19, wxGBPosition(2, 0), wxGBSpan(1, 1), wxGROW|wxALIGN_CENTER_VERTICAL|wxALL, 2);

    wxPanel* itemPanel20 = new wxPanel( itemFrame1, ID_pnlTime, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    if (frmPropiedades::ShowToolTips())
        itemPanel20->SetToolTip(_("Seconds left in the actual turn"));
    itemBoxSizer19->Add(itemPanel20, 1, wxALIGN_CENTER_VERTICAL|wxALL, 1);

    wxBoxSizer* itemBoxSizer21 = new wxBoxSizer(wxHORIZONTAL);
    itemPanel20->SetSizer(itemBoxSizer21);

    wxStaticBitmap* itemStaticBitmap22 = new wxStaticBitmap( itemPanel20, wxID_STATIC, itemFrame1->GetBitmapResource(wxT("../../Graficos Desarrollo/Iconos/16x16/clock.xpm")), wxDefaultPosition, wxSize(16, 16), 0 );
    if (frmPropiedades::ShowToolTips())
        itemStaticBitmap22->SetToolTip(_("Seconds left in the actual turn"));
    itemBoxSizer21->Add(itemStaticBitmap22, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    mOlblSecondsLeft = new wxStaticText( itemPanel20, wxID_lblSecondsLeft, _("120"), wxDefaultPosition, wxSize(20, -1), wxALIGN_CENTRE );
    if (frmPropiedades::ShowToolTips())
        mOlblSecondsLeft->SetToolTip(_("Seconds left in the actual turn"));
    itemBoxSizer21->Add(mOlblSecondsLeft, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    itemBoxSizer19->Add(5, 5, 0, wxALIGN_CENTER_VERTICAL|wxALL, 0);

    wxPanel* itemPanel25 = new wxPanel( itemFrame1, ID_PANEL, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    if (frmPropiedades::ShowToolTips())
        itemPanel25->SetToolTip(_("Round (Actual / Limit)"));
    itemBoxSizer19->Add(itemPanel25, 1, wxALIGN_CENTER_VERTICAL|wxALL, 1);

    wxBoxSizer* itemBoxSizer26 = new wxBoxSizer(wxHORIZONTAL);
    itemPanel25->SetSizer(itemBoxSizer26);

    wxStaticBitmap* itemStaticBitmap27 = new wxStaticBitmap( itemPanel25, wxID_STATIC, itemFrame1->GetBitmapResource(wxT("../../Graficos Desarrollo/Iconos/16x16/turn_round.xpm")), wxDefaultPosition, wxSize(16, 16), 0 );
    itemBoxSizer26->Add(itemStaticBitmap27, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    mOlblRound = new wxStaticText( itemPanel25, wxID_lblRound, _("999"), wxDefaultPosition, wxSize(20, -1), wxALIGN_CENTRE );
    itemBoxSizer26->Add(mOlblRound, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    mOlblRoundLimit = new wxStaticText( itemPanel25, ID_lblRoundLimit, _("1"), wxDefaultPosition, wxSize(20, -1), wxALIGN_CENTRE );
    itemBoxSizer26->Add(mOlblRoundLimit, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    itemBoxSizer19->Add(5, 5, 0, wxALIGN_CENTER_VERTICAL|wxALL, 0);

    wxPanel* itemPanel31 = new wxPanel( itemFrame1, ID_PANEL1, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    if (frmPropiedades::ShowToolTips())
        itemPanel31->SetToolTip(_("Actions left in the actual turn"));
    itemBoxSizer19->Add(itemPanel31, 1, wxALIGN_CENTER_VERTICAL|wxALL, 1);

    wxBoxSizer* itemBoxSizer32 = new wxBoxSizer(wxHORIZONTAL);
    itemPanel31->SetSizer(itemBoxSizer32);

    wxStaticBitmap* itemStaticBitmap33 = new wxStaticBitmap( itemPanel31, wxID_STATIC, itemFrame1->GetBitmapResource(wxT("../../Graficos Desarrollo/Iconos/16x16/actions.xpm")), wxDefaultPosition, wxSize(16, 16), 0 );
    itemBoxSizer32->Add(itemStaticBitmap33, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    mOlblActionsLeft = new wxStaticText( itemPanel31, ID_lblActionsLeft, _("120"), wxDefaultPosition, wxSize(20, -1), wxALIGN_CENTRE );
    itemBoxSizer32->Add(mOlblActionsLeft, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);

////@end frmPropiedades content construction
}


/*!
 * Should we show tooltips?
 */

bool frmPropiedades::ShowToolTips()
{
    return true;
}

/*!
 * Get bitmap resources
 */

wxBitmap frmPropiedades::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin frmPropiedades bitmap retrieval
    wxUnusedVar(name);
    if (name == _T("../../Graficos Desarrollo/Iconos/16x16/clock.xpm"))
    {
        wxBitmap bitmap( clock_xpm);
        return bitmap;
    }
    else if (name == _T("../../Graficos Desarrollo/Iconos/16x16/turn_round.xpm"))
    {
        wxBitmap bitmap( turn_round_xpm);
        return bitmap;
    }
    else if (name == _T("../../Graficos Desarrollo/Iconos/16x16/actions.xpm"))
    {
        wxBitmap bitmap( actions_xpm);
        return bitmap;
    }
    return wxNullBitmap;
////@end frmPropiedades bitmap retrieval
}

/*!
 * Get icon resources
 */

wxIcon frmPropiedades::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin frmPropiedades icon retrieval
    wxUnusedVar(name);
    if (name == _T("../../Graficos Desarrollo/Iconos/16x16/properties.xpm"))
    {
        wxIcon icon( properties_xpm);
        return icon;
    }
    else if (name == _T("../../Graficos Desarrollo/Iconos/16x16/content.xpm"))
    {
        wxIcon icon( content_xpm);
        return icon;
    }
    return wxNullIcon;
////@end frmPropiedades icon retrieval
}
