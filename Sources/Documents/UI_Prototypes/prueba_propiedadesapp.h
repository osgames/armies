/////////////////////////////////////////////////////////////////////////////
// Name:        prueba_propiedadesapp.h
// Purpose:     
// Author:      Elcho
// Modified by: 
// Created:     Sat 01 Sep 2007 07:22:58 ART
// RCS-ID:      
// Copyright:   Truchisoft
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _PRUEBA_PROPIEDADESAPP_H_
#define _PRUEBA_PROPIEDADESAPP_H_

/*!
 * Includes
 */

////@begin includes
#include "wx/image.h"
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
////@end control identifiers

/*!
 * Prueba_propiedadesApp class declaration
 */

class Prueba_propiedadesApp: public wxApp
{    
    DECLARE_CLASS( Prueba_propiedadesApp )
    DECLARE_EVENT_TABLE()

public:
    /// Constructor
    Prueba_propiedadesApp();

    void Init();

    /// Initialises the application
    virtual bool OnInit();

    /// Called on exit
    virtual int OnExit();

////@begin Prueba_propiedadesApp event handler declarations
////@end Prueba_propiedadesApp event handler declarations

////@begin Prueba_propiedadesApp member function declarations
////@end Prueba_propiedadesApp member function declarations

////@begin Prueba_propiedadesApp member variables
////@end Prueba_propiedadesApp member variables
};

/*!
 * Application instance declaration 
 */

////@begin declare app
DECLARE_APP(Prueba_propiedadesApp)
////@end declare app

#endif
    // _PRUEBA_PROPIEDADESAPP_H_
