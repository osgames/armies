/////////////////////////////////////////////////////////////////////////////
// Name:        frmpropiedades.h
// Purpose:     
// Author:      Elcho
// Modified by: 
// Created:     Sat 01 Sep 2007 07:24:45 ART
// RCS-ID:      
// Copyright:   Truchisoft
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _FRMPROPIEDADES_H_
#define _FRMPROPIEDADES_H_

/*!
 * Includes
 */

////@begin includes
#include "wx/frame.h"
#include "wx/gbsizer.h"
#include "wx/notebook.h"
#include "wx/listctrl.h"
#include "wx/treectrl.h"
#include "wx/statline.h"
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
class wxNotebook;
class wxListCtrl;
class wxTreeCtrl;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_FRMPROPIEDADES 10000
#define ID_tabProperties 10001
#define ID_pagProperties 10002
#define ID_lvwProperties 10004
#define ID_pagContent 10003
#define ID_trcContent 10005
#define ID_PANEL2 10014
#define ID_PANEL3 10015
#define ID_PANEL4 10016
#define ID_STATICLINE 10006
#define ID_pnlTime 10007
#define wxID_lblSecondsLeft 10009
#define ID_PANEL 10008
#define wxID_lblRound 10010
#define ID_lblRoundLimit 10013
#define ID_PANEL1 10011
#define ID_lblActionsLeft 10012
#define SYMBOL_FRMPROPIEDADES_STYLE wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxCLOSE_BOX
#define SYMBOL_FRMPROPIEDADES_TITLE _("frmPropiedades")
#define SYMBOL_FRMPROPIEDADES_IDNAME ID_FRMPROPIEDADES
#define SYMBOL_FRMPROPIEDADES_SIZE wxSize(400, 300)
#define SYMBOL_FRMPROPIEDADES_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * frmPropiedades class declaration
 */

class frmPropiedades: public wxFrame
{    
    DECLARE_CLASS( frmPropiedades )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    frmPropiedades();
    frmPropiedades( wxWindow* parent, wxWindowID id = SYMBOL_FRMPROPIEDADES_IDNAME, const wxString& caption = SYMBOL_FRMPROPIEDADES_TITLE, const wxPoint& pos = SYMBOL_FRMPROPIEDADES_POSITION, const wxSize& size = SYMBOL_FRMPROPIEDADES_SIZE, long style = SYMBOL_FRMPROPIEDADES_STYLE );

    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_FRMPROPIEDADES_IDNAME, const wxString& caption = SYMBOL_FRMPROPIEDADES_TITLE, const wxPoint& pos = SYMBOL_FRMPROPIEDADES_POSITION, const wxSize& size = SYMBOL_FRMPROPIEDADES_SIZE, long style = SYMBOL_FRMPROPIEDADES_STYLE );

    /// Destructor
    ~frmPropiedades();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin frmPropiedades event handler declarations

////@end frmPropiedades event handler declarations

////@begin frmPropiedades member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end frmPropiedades member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin frmPropiedades member variables
    wxNotebook* mOtabProperties;
    wxListCtrl* mOlvwProperties;
    wxTreeCtrl* mOtrcContent;
    wxStaticText* mOlblSecondsLeft;
    wxStaticText* mOlblRound;
    wxStaticText* mOlblRoundLimit;
    wxStaticText* mOlblActionsLeft;
////@end frmPropiedades member variables
};

#endif
    // _FRMPROPIEDADES_H_
