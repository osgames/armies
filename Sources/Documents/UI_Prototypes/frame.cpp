/////////////////////////////////////////////////////////////////////////////
// Name:        frame.cpp
// Purpose:     
// Author:      Elcho
// Modified by: 
// Created:     Sun 09 Sep 2007 08:12:52 ART
// RCS-ID:      
// Copyright:   Truchisoft
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "frame.h"

////@begin XPM images
////@end XPM images


/*!
 * Frame type definition
 */

IMPLEMENT_CLASS( Frame, wxFrame )


/*!
 * Frame event table definition
 */

BEGIN_EVENT_TABLE( Frame, wxFrame )

////@begin Frame event table entries
////@end Frame event table entries

END_EVENT_TABLE()


/*!
 * Frame constructors
 */

Frame::Frame()
{
    Init();
}

Frame::Frame( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create( parent, id, caption, pos, size, style );
}


/*!
 * Frame creator
 */

bool Frame::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin Frame creation
    wxFrame::Create( parent, id, caption, pos, size, style );

    CreateControls();
    Centre();
////@end Frame creation
    return true;
}


/*!
 * Frame destructor
 */

Frame::~Frame()
{
////@begin Frame destruction
////@end Frame destruction
}


/*!
 * Member initialisation
 */

void Frame::Init()
{
////@begin Frame member initialisation
    szrPrincipal = NULL;
    pnlJuego = NULL;
    pnlIzquierda = NULL;
    szrIzquierda = NULL;
    PpnlMap = NULL;
    PszrMap = NULL;
    PpnlCanvasMap = NULL;
    PszrCanvasMap = NULL;
    mOwndCanvasMap = NULL;
    PpnlProperties = NULL;
    PszrProperties = NULL;
    mOszrPropertiesTop = NULL;
    mOpnlNoObject = NULL;
    PszrNoObject = NULL;
    PlblNoObject = NULL;
    mOtabProperties = NULL;
    pagProperties = NULL;
    mOlvwProperties = NULL;
    pagContent = NULL;
    mOtrcContent = NULL;
////@end Frame member initialisation
}


/*!
 * Control creation for Frame
 */

void Frame::CreateControls()
{    
////@begin Frame content construction
    Frame* itemFrame1 = this;

    szrPrincipal = new wxBoxSizer(wxHORIZONTAL);
    itemFrame1->SetSizer(szrPrincipal);

    pnlJuego = new wxPanel( itemFrame1, ID_pnlJuego, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    szrPrincipal->Add(pnlJuego, 1, wxGROW|wxALL, 0);

    pnlIzquierda = new wxPanel( itemFrame1, ID_pnlIzquierda, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    szrPrincipal->Add(pnlIzquierda, 0, wxGROW|wxALL, 5);

    szrIzquierda = new wxBoxSizer(wxVERTICAL);
    pnlIzquierda->SetSizer(szrIzquierda);

    PpnlMap = new wxPanel( pnlIzquierda, ID_pnlMap, wxDefaultPosition, wxSize(300, -1), wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    szrIzquierda->Add(PpnlMap, 1, wxGROW|wxALL, 2);

    PszrMap = new wxFlexGridSizer(3, 1, 0, 0);
    PszrMap->AddGrowableRow(0);
    PszrMap->AddGrowableCol(0);
    PpnlMap->SetSizer(PszrMap);

    PpnlCanvasMap = new wxPanel( PpnlMap, ID_pnlCanvasMap, wxPoint(1, 1), wxDefaultSize, wxRAISED_BORDER|wxTAB_TRAVERSAL );
    PszrMap->Add(PpnlCanvasMap, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL|wxSHAPED, 2);

    PszrCanvasMap = new wxBoxSizer(wxHORIZONTAL);
    PpnlCanvasMap->SetSizer(PszrCanvasMap);

    mOwndCanvasMap = new wxTextCtrl( PpnlCanvasMap, ID_wndCanvasMap, _T(""), wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE );
    PszrCanvasMap->Add(mOwndCanvasMap, 1, wxGROW|wxALL, 2);

    PpnlProperties = new wxPanel( pnlIzquierda, ID_PpnlProperties, wxDefaultPosition, wxSize(245, 319), wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    szrIzquierda->Add(PpnlProperties, 1, wxGROW|wxALL, 0);

    PszrProperties = new wxGridBagSizer(0, 0);
    PszrProperties->AddGrowableRow(0);
    PszrProperties->AddGrowableCol(0);
    PszrProperties->SetEmptyCellSize(wxSize(1, 1));
    PpnlProperties->SetSizer(PszrProperties);

    mOszrPropertiesTop = new wxBoxSizer(wxHORIZONTAL);
    PszrProperties->Add(mOszrPropertiesTop, wxGBPosition(0, 0), wxGBSpan(1, 1), wxGROW|wxGROW|wxALL, 2);

    mOpnlNoObject = new wxPanel( PpnlProperties, ID_mOpnlNoObject, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    mOpnlNoObject->Show(false);
    mOszrPropertiesTop->Add(mOpnlNoObject, 1, wxGROW|wxALL, 2);

    PszrNoObject = new wxBoxSizer(wxHORIZONTAL);
    mOpnlNoObject->SetSizer(PszrNoObject);

    PlblNoObject = new wxStaticText( mOpnlNoObject, wxID_PlblNoObject, _("No Object Selected"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    PszrNoObject->Add(PlblNoObject, 1, wxALIGN_CENTER_VERTICAL|wxALL, 2);

    mOtabProperties = new wxNotebook( PpnlProperties, ID_mOtabProperties, wxDefaultPosition, wxSize(245, -1), wxBK_DEFAULT );

    pagProperties = new wxPanel( mOtabProperties, ID_Properties, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    wxBoxSizer* itemBoxSizer19 = new wxBoxSizer(wxVERTICAL);
    pagProperties->SetSizer(itemBoxSizer19);

    mOlvwProperties = new wxListCtrl( pagProperties, ID_mOlvwProperties, wxDefaultPosition, wxSize(100, 100), wxLC_REPORT );
    itemBoxSizer19->Add(mOlvwProperties, 1, wxGROW|wxALL, 5);

    mOtabProperties->AddPage(pagProperties, _("Properties"));

    pagContent = new wxPanel( mOtabProperties, ID_Content, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );
    wxBoxSizer* itemBoxSizer22 = new wxBoxSizer(wxVERTICAL);
    pagContent->SetSizer(itemBoxSizer22);

    mOtrcContent = new wxTreeCtrl( pagContent, ID_mOtrcContent, wxDefaultPosition, wxSize(100, 100), wxTR_SINGLE );
    itemBoxSizer22->Add(mOtrcContent, 1, wxGROW|wxALL, 5);

    mOtabProperties->AddPage(pagContent, _("Content"));

    mOszrPropertiesTop->Add(mOtabProperties, 1, wxGROW|wxALL, 5);

////@end Frame content construction
}


/*!
 * Should we show tooltips?
 */

bool Frame::ShowToolTips()
{
    return true;
}

/*!
 * Get bitmap resources
 */

wxBitmap Frame::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin Frame bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end Frame bitmap retrieval
}

/*!
 * Get icon resources
 */

wxIcon Frame::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin Frame icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end Frame icon retrieval
}
