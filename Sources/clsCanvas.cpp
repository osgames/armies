#include "stdafx.h"

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

/*!
 * Includes
 */

#include "clsCanvas.h"

#include <wx/dcmemory.h>
#include <wx/log.h>
#include <math.h>
#include <wx/image.h>

#include "clsFrame.h"
#include "Global.h"
#include "ClsApplication.h"
#include "clsBoard.h"
#include "clsCursor.h"
#include "clsMap.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE(clsCanvas, wxWindow)
    EVT_PAINT(clsCanvas::Window_OnPaint)
	EVT_SIZE(clsCanvas::Window_OnSize)
	EVT_SCROLLWIN_LINEUP(clsCanvas::Window_OnScrollWinLineUp)
	EVT_SCROLLWIN_LINEDOWN(clsCanvas::Window_OnScrollWinLineDown)
	EVT_SCROLLWIN_PAGEUP(clsCanvas::Window_OnScrollWinPageUp)
	EVT_SCROLLWIN_PAGEDOWN(clsCanvas::Window_OnScrollWinPageDown)
	EVT_SCROLLWIN_THUMBTRACK(clsCanvas::Window_OnScrollWinThumbTrack)
	EVT_SCROLLWIN_TOP(clsCanvas::Window_OnScrollWinTop)
	EVT_SCROLLWIN_BOTTOM(clsCanvas::Window_OnScrollWinBottom)
	EVT_SCROLLWIN_THUMBRELEASE(clsCanvas::Window_OnScrollThumbRelease)
	EVT_MOUSEWHEEL(clsCanvas::Window_MouseWheel)
	EVT_KEY_DOWN(clsCanvas::OnKeyDown)
	EVT_LEFT_DOWN(clsCanvas::OnLeftDown)
	EVT_RIGHT_DOWN(clsCanvas::OnRightDown)
	EVT_MOUSE_EVENTS(clsCanvas::OnMouseEvent)
END_EVENT_TABLE()

//_______________________________________________________________________________________________________________________

clsCanvas::clsCanvas(wxWindow *parent, int PvIntWidth, int PvintHeight)
		: wxWindow(parent, -1, wxDefaultPosition, wxDefaultSize,
                           wxHSCROLL | wxVSCROLL | wxNO_FULL_REPAINT_ON_RESIZE | wxWANTS_CHARS)
{
	int intWidth;
	int intHeight;

	wxDisplaySize(&intWidth, &intHeight); // retrieves the screen sizes
	mSbmpSecondaryScreen = Make_CP(new wxBitmap(intWidth, intHeight));
	
	this->SetTotalSize(PvIntWidth, PvintHeight);

    SetBackgroundColour(*wxWHITE);
    SetCursor(wxCursor(wxCURSOR_ARROW));

	mVblnDrawOnScroll = true;

	mVintHorizontalShortScroll = 0;
	mVintHorizontalLongScroll = 0;
	mVintVerticalShortScroll = 0;
	mVintVerticalLongScroll = 0;
	mVintMaxHorizontalDisp = 0;
	mVintMaxVerticalDisp = 0;
	mVintWheelRotation = 0;
}

//_______________________________________________________________________________________________________________________

clsCanvas::~clsCanvas()
{
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_OnPaint(wxPaintEvent &WXUNUSED(event))
{
    wxMemoryDC mdcBuffer;
	wxPaintDC pdcPrimaria(this);
	int intXOffset, intYOffset;
	wxRect rctArea;
	wxRect rctAreaOffset;
	wxRegionIterator itrRegion(this->GetUpdateRegion()); // get the regions in the canvas that need to be updated
	int intCont = 0;

	mdcBuffer.SelectObject(*mSbmpSecondaryScreen);
	
	intXOffset = mVrctVisibleRegion.GetLeft();
	intYOffset = mVrctVisibleRegion.GetTop();

	// Iterate through all the regions in the canvas that need to be updated
	while(itrRegion) {
		intCont++;

		rctArea = itrRegion.GetRect();
		rctAreaOffset = rctArea;

		//rctAreaOffset.Offset(intXOffset, intYOffset);

		pdcPrimaria.Blit(rctArea.GetLeft(), rctArea.GetTop(), rctArea.GetWidth(), rctArea.GetHeight(), &mdcBuffer, rctAreaOffset.GetLeft(), rctAreaOffset.GetTop());
		
		itrRegion++;
	}
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Draw(bool pVblnRedrawAll, bool pVblnRefreshMap) {
	wxRect rctZoneToCopy; // zone to copy from the secondary screen, in screen coordinates and pixel unites
    wxMemoryDC mdcBuffer;
	wxClientDC pdcPrimaria(this);
	
	if(!mVrctVisibleRegion.IsEmpty()) {
		// There is a visible area

		if(!pVblnRedrawAll) {
			// Redraw only the dirty area
			if(MvRctDirtyRegion.IsEmpty()) {
				// The secondary screen doesn't has modifications to copy to the primary screen
				return;
			}

			rctZoneToCopy = mOobjBoard->rctBoard2Screen(MvRctDirtyRegion);
		
		} else {
			// It have to redraw the entire primary screen
			rctZoneToCopy.SetWidth(mVrctVisibleRegion.GetWidth());
			rctZoneToCopy.SetHeight(mVrctVisibleRegion.GetHeight());
		}
		
		mdcBuffer.SelectObject(*mSbmpSecondaryScreen);
		pdcPrimaria.Blit(rctZoneToCopy.GetLeft(), rctZoneToCopy.GetTop(), rctZoneToCopy.GetWidth(), rctZoneToCopy.GetHeight(), &mdcBuffer, rctZoneToCopy.GetLeft(), rctZoneToCopy.GetTop());

		if(pVblnRefreshMap) {
			// The map window have to be refreshed
			mOobjMap->ShowMap();
		}
	}
	
	// Reset the dirty region
	MvRctDirtyRegion.SetSize(wxSize(0, 0));
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_OnSize(wxSizeEvent& WXUNUSED(event)) {
	if(gOobjApplication->GetVienState() != enmStates_Empty) {
		// There is a game o board edition active
		this->SetvRctVisibleRegion();
		AdjustScrollbars();

		mOobjBoard->DrawAll();
		this->Draw(); // redraw all the primary screen because in some situations SetvRctVisibleRegion can change the offset of the visible area
	}
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_OnScrollWinLineUp(wxScrollWinEvent& event) {
	if(event.GetOrientation() == wxHORIZONTAL) {
		ScrollHorizontal(-mVintHorizontalShortScroll);
	} else {
		ScrollVertical(-mVintVerticalShortScroll);
	}
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_OnScrollWinLineDown(wxScrollWinEvent& event) {
	if(event.GetOrientation() == wxHORIZONTAL) {
		ScrollHorizontal(mVintHorizontalShortScroll);
	} else {
		ScrollVertical(mVintVerticalShortScroll);
	}
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_OnScrollWinPageUp(wxScrollWinEvent& event) {
	if(event.GetOrientation() == wxHORIZONTAL) {
		ScrollHorizontal(-mVintHorizontalLongScroll);
	} else {
		ScrollVertical(-mVintVerticalLongScroll);
	}
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_OnScrollWinPageDown(wxScrollWinEvent& event) {
	if(event.GetOrientation() == wxHORIZONTAL) {
		ScrollHorizontal(mVintHorizontalLongScroll);
	} else {
		ScrollVertical(mVintVerticalLongScroll);
	}
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_OnScrollWinThumbTrack(wxScrollWinEvent& event) {
	DirectScroll(event.GetOrientation(), event.GetPosition());
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_OnScrollWinTop(wxScrollWinEvent& event) {
	DirectScroll(event.GetOrientation(), 0);
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_OnScrollWinBottom(wxScrollWinEvent& event) {
	if(event.GetOrientation() == wxHORIZONTAL) {
		// Horizontal displacement
		DirectScroll(wxHORIZONTAL, mVintMaxHorizontalDisp);
	} else {
		// Vertical displacement
		DirectScroll(wxVERTICAL, mVintMaxVerticalDisp);
	}
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_OnScrollThumbRelease(wxScrollWinEvent& event) {
	DirectScroll(event.GetOrientation(), event.GetPosition());
}

//_______________________________________________________________________________________________________________________

void clsCanvas::Window_MouseWheel(wxMouseEvent& event) {
    mVintWheelRotation += event.GetWheelRotation();
    int intLines = mVintWheelRotation / event.GetWheelDelta();
    mVintWheelRotation -= intLines * event.GetWheelDelta();
	
    if(intLines != 0) {
		// There is some units to scroll
        wxScrollWinEvent objNewEvent;
		
        objNewEvent.SetPosition(0);
        objNewEvent.SetEventObject(this);
		
        if(event.ShiftDown()) {
			// The shift key was pressed
			objNewEvent.SetOrientation(wxHORIZONTAL);
		} else  {
			objNewEvent.SetOrientation(wxVERTICAL);
		}
		
		if(event.IsPageScroll()) {
			// The wheel scroll is set to displace at page level
            if (intLines > 0) {
				// Positive scroll
                objNewEvent.SetEventType(wxEVT_SCROLLWIN_PAGEUP);
            } else {
				// Negative scroll
                objNewEvent.SetEventType(wxEVT_SCROLLWIN_PAGEDOWN);
			}
			
            this->GetEventHandler()->ProcessEvent(objNewEvent);
        
		} else {
			// The wheel scroll is set to displace at line level
            intLines *= event.GetLinesPerAction();
            if(intLines > 0)  {
				// Positive scroll
                objNewEvent.SetEventType(wxEVT_SCROLLWIN_LINEUP);
            } else {
				// Negative scroll
                objNewEvent.SetEventType(wxEVT_SCROLLWIN_LINEDOWN);
			}
			
            int intTimes = abs(intLines);
            
			for(; intTimes > 0; intTimes--) {
                this->GetEventHandler()->ProcessEvent(objNewEvent);
			}
        }
    }
}

//_______________________________________________________________________________________________________________________

void clsCanvas::DirectScroll(int pVintOrientation, int pVintOffset) {
	int intPrevOffset;
	
	if(pVintOrientation == wxHORIZONTAL) {
		// Horizontal displacement
		intPrevOffset = mVrctVisibleRegion.GetLeft();
		this->SetvRctVisibleRegion(-1, pVintOffset);
	} else {
		// Vertical displacement
		intPrevOffset = mVrctVisibleRegion.GetTop();
		this->SetvRctVisibleRegion(pVintOffset, -1);
	}
	
	this->SetScrollPos(pVintOrientation, pVintOffset);
	
	if(mVblnDrawOnScroll) {
		// It's necessary to redraw
		DrawScrollOptimized(intPrevOffset, pVintOrientation);
		this->Draw(true);
	}
}

//_______________________________________________________________________________________________________________________

void clsCanvas::ScrollHorizontal(int pVintLength) {
	int intPrevOffset;
	int intNewOffset;
	
	intPrevOffset = mVrctVisibleRegion.GetLeft();
	intNewOffset = mVrctVisibleRegion.GetLeft() + pVintLength;

	if(intNewOffset > mVintMaxHorizontalDisp) {
		// The scroll has exceeded the maximum scroll allowed
		intNewOffset = mVintMaxHorizontalDisp;
	} else if(intNewOffset < 0) {
		// The scroll has exceeded the minimum scroll allowed
		intNewOffset = 0;
	}

	this->SetvRctVisibleRegion(-1, intNewOffset);
	this->SetScrollPos(wxHORIZONTAL, intNewOffset);
	
	if(mVblnDrawOnScroll) {
		// It's necessary to redraw
		DrawScrollOptimized(intPrevOffset, wxHORIZONTAL);
		this->Draw(true);
	}
}

//_______________________________________________________________________________________________________________________

void clsCanvas::ScrollVertical(int pVintLength) {
	int intPrevOffset;
	int intNewOffset;
	
	intPrevOffset = mVrctVisibleRegion.GetTop();
	intNewOffset = mVrctVisibleRegion.GetTop() + pVintLength;

	if(intNewOffset > mVintMaxVerticalDisp) {
		// The scroll has exceeded the maximum scroll allowed
		intNewOffset = mVintMaxVerticalDisp;
	} else if(intNewOffset < 0) {
		// The scroll has exceeded the minimum scroll allowed
		intNewOffset = 0;
	}
	
	this->SetvRctVisibleRegion(intNewOffset);
	this->SetScrollPos(wxVERTICAL, intNewOffset);
	
	if(mVblnDrawOnScroll) {
		// It's necessary to redraw
		DrawScrollOptimized(intPrevOffset, wxVERTICAL);
		this->Draw(true);
	}
}

//_______________________________________________________________________________________________________________________

void clsCanvas::DrawScrollOptimized(int pVintPrevDisplacement, int pVintOrientation)  {
	int intDispLength;
	wxRect rctSourceArea; // area in the secondary screen from where will be copied the area to displaced. In screen coordinates.
	wxRect rctTargetArea; // area in the secondary screen to copy the area displaced. In screen coordinates.
	wxRect rctRedrawArea; // area that clsBoard will redraw. In board coordinates.
	wxMemoryDC mdcSecondaryScreen;
	
	if(pVintOrientation == wxVERTICAL) {
		// The visible area has been displaced vertically
		intDispLength = abs(mVrctVisibleRegion.GetTop() - pVintPrevDisplacement);

		if(intDispLength < mVrctVisibleRegion.GetHeight()) {
			// The displacement is less than the height of the primary screen
			if(pVintPrevDisplacement < mVrctVisibleRegion.GetTop()) {
				// Downward movement
				rctSourceArea.SetTop(intDispLength);
				rctSourceArea.SetBottom(mVrctVisibleRegion.GetHeight());
				
				rctTargetArea.SetTop(0);
				rctTargetArea.SetHeight(rctSourceArea.GetHeight());

				rctRedrawArea.SetTop(mVrctVisibleRegion.GetTop() + rctTargetArea.GetHeight());
				rctRedrawArea.SetBottom(mVrctVisibleRegion.GetBottom());
			
			} else  {
				// Upward movement
				rctSourceArea.SetTop(0);
				rctSourceArea.SetBottom(mVrctVisibleRegion.GetHeight() - intDispLength);

				rctTargetArea.SetTop(intDispLength);
				rctTargetArea.SetHeight(rctSourceArea.GetHeight());

				rctRedrawArea.SetTop(mVrctVisibleRegion.GetTop());
				rctRedrawArea.SetBottom(mVrctVisibleRegion.GetTop() + intDispLength);
			}

			// The values of the X axis for the source, target and redraw area don't change in the vertical displacements
			rctSourceArea.SetLeft(0);
			rctSourceArea.SetWidth(mVrctVisibleRegion.GetWidth());

			rctTargetArea.SetLeft(0);
			rctTargetArea.SetWidth(mVrctVisibleRegion.GetWidth());

			rctRedrawArea.SetLeft(mVrctVisibleRegion.GetLeft());
			rctRedrawArea.SetWidth(mVrctVisibleRegion.GetWidth());
		
		} else {
			// The displacement is greater than the height of the screen
			// All the visible area must be repainted by clsBoard
			rctRedrawArea = mVrctVisibleRegion;
		}
	
	} else  {
		// The visible area has been displaced horizontally
		intDispLength = abs(mVrctVisibleRegion.GetLeft() - pVintPrevDisplacement);
		
		if(intDispLength < mVrctVisibleRegion.GetWidth()) {
			// The displacement is less than the height of the primary screen
			if(pVintPrevDisplacement < mVrctVisibleRegion.GetLeft()) {
				// Downward movement
				rctSourceArea.SetLeft(intDispLength);
				rctSourceArea.SetRight(mVrctVisibleRegion.GetWidth());
				
				rctTargetArea.SetLeft(0);
				rctTargetArea.SetWidth(rctSourceArea.GetWidth());
				
				rctRedrawArea.SetLeft(mVrctVisibleRegion.GetLeft() + rctTargetArea.GetWidth());
				rctRedrawArea.SetRight(mVrctVisibleRegion.GetRight());
				
			} else  {
				// Upward movement
				rctSourceArea.SetLeft(0);
				rctSourceArea.SetRight(mVrctVisibleRegion.GetWidth() - intDispLength);
				
				rctTargetArea.SetLeft(intDispLength);
				rctTargetArea.SetWidth(rctSourceArea.GetWidth());
				
				rctRedrawArea.SetLeft(mVrctVisibleRegion.GetLeft());
				rctRedrawArea.SetRight(mVrctVisibleRegion.GetLeft() + intDispLength);
			}
			
			// The values of the Y axis for the source, target and redraw area don't change in the horizontal displacements
			rctSourceArea.SetTop(0);
			rctSourceArea.SetHeight(mVrctVisibleRegion.GetHeight());

			rctTargetArea.SetTop(0);
			rctTargetArea.SetHeight(mVrctVisibleRegion.GetHeight());

			rctRedrawArea.SetTop(mVrctVisibleRegion.GetTop());
			rctRedrawArea.SetHeight(mVrctVisibleRegion.GetHeight());

		} else {
			// The displacement is greater than the height of the screen
			// All the visible area must be repainted by clsBoard
			rctRedrawArea = mVrctVisibleRegion;
		}
	}

	// Move the subarea that will remain visible
	if(!rctTargetArea.IsEmpty()) {
		// There is some area to move in order to improve performance
		mdcSecondaryScreen.SelectObject(*mSbmpSecondaryScreen);
		mdcSecondaryScreen.Blit(rctTargetArea.GetX(), rctTargetArea.GetY(), rctSourceArea.GetWidth()
			, rctSourceArea.GetHeight(), &mdcSecondaryScreen, rctSourceArea.GetX(), rctSourceArea.GetY(), wxCOPY, true);
		mdcSecondaryScreen.SelectObject(wxNullBitmap);
	}
	
	// Redraw the new area visible after the scroll
	mOobjBoard->DrawPixelArea(rctRedrawArea);
}

//_______________________________________________________________________________________________________________________

void clsCanvas::SetvRctVisibleRegion(int pVintVerticalOffset, int pVintHorizontalOffset) {
	int intWidth, intHeight;

	// Set the offset of the visible region
	
	if(pVintHorizontalOffset != -1) {
		// There is a new horizontal offset to set
		mVrctVisibleRegion.SetLeft(pVintHorizontalOffset);
	}
	
	if(pVintVerticalOffset != -1) {
		// There is a new horizontal offset to set
		mVrctVisibleRegion.SetTop(pVintVerticalOffset);
	}

	//...................................................................................................................
	
	this->GetClientSize(&intWidth, &intHeight);

	mVrctVisibleRegion.SetWidth(intWidth);
	mVrctVisibleRegion.SetHeight(intHeight);

	//...................................................................................................................
	// In some situations, when the application is about to exit or when the window is resized with some of its scrollbars set at the maximum offset position, the event "Size" is fired with a bigger client area than the maximum supported. For this reason it's necessary to do the following checks.
	
	if(mVrctVisibleRegion.GetRight() > mVintTotalWidth - 1) {
		// The visible region exceeds the maximum width
		// Subtract the excess size from the offset to fix the problem
		int intExcess = mVrctVisibleRegion.GetRight() - (mVintTotalWidth - 1);
		mVrctVisibleRegion.SetLeft(mVrctVisibleRegion.GetLeft() - intExcess);
		
		wxLogDebug("The visible region exceeds the maximum width");
	}

	if(mVrctVisibleRegion.GetBottom() > mVintTotalHeight - 1) {
		// The visible region exceeds the maximum height
		// Subtract the excess size from the offset to fix the problem
		int intExcess = mVrctVisibleRegion.GetBottom() - (mVintTotalHeight - 1);
		mVrctVisibleRegion.SetTop(mVrctVisibleRegion.GetTop() - intExcess);

		wxLogDebug("The visible region exceeds the maximum height");
	}

	// Update the visible region showed in the map
	mOobjMap->SetVisibleRegion(mVrctVisibleRegion);
}

//_______________________________________________________________________________________________________________________

void clsCanvas::AdjustScrollbars() {
	int intMarginLongScroll = 2 * mOobjBoard->GetVintSQUARE_LENGTH(); // amount of area that remains visible after a long scroll, in pixels. In this case, it's the equivalent to the size of two squares
	
	mVintMaxHorizontalDisp = mVintTotalWidth - mVrctVisibleRegion.GetWidth();
	mVintMaxVerticalDisp = mVintTotalHeight - mVrctVisibleRegion.GetHeight();
	
	//...................................................................................................................
	// Horizontal displacement

	//this->SetScrollbar(wxHORIZONTAL, mVrctVisibleRegion.GetLeft(), mVrctVisibleRegion.GetWidth(), mVintMaxHorizontalDisp);
	this->SetScrollbar(wxHORIZONTAL, mVrctVisibleRegion.GetLeft(), mVrctVisibleRegion.GetWidth(), mVintTotalWidth);
		
	if(mVintMaxHorizontalDisp > 0) {
		// It is necessary to scroll horizontally
		mVintHorizontalShortScroll = mOobjBoard->GetVintSQUARE_LENGTH();
		
		mVintHorizontalLongScroll = mVrctVisibleRegion.GetWidth() - intMarginLongScroll;
		
		if(mVintHorizontalLongScroll < 1) {
			// The low limit of the displacement has been passed
			mVintHorizontalLongScroll = 1;
		} else if(mVintHorizontalLongScroll > mVintMaxHorizontalDisp) {
			// The upper limit of the displacement has been passed
			mVintHorizontalLongScroll = mVintMaxHorizontalDisp;
		}
	
	} else {
		// It's not necessary to scroll horizontally
		mVintHorizontalLongScroll = 0;
	}

	mVintHorizontalShortScroll = mOobjBoard->GetVintSQUARE_LENGTH();
	
	//...................................................................................................................
	// Vertical displacement

	//this->SetScrollbar(wxVERTICAL, mVrctVisibleRegion.GetTop(), mVrctVisibleRegion.GetHeight(), mVintMaxVerticalDisp);
	this->SetScrollbar(wxVERTICAL, mVrctVisibleRegion.GetTop(), mVrctVisibleRegion.GetHeight(), mVintTotalHeight);

	if(mVintMaxVerticalDisp > 0) {
		// It is necessary to scroll Vertically
		mVintVerticalShortScroll = mOobjBoard->GetVintSQUARE_LENGTH();
		
		mVintVerticalLongScroll = mVrctVisibleRegion.GetHeight() - intMarginLongScroll;
		
		if(mVintVerticalLongScroll < 1) {
			// The low limit of the displacement has been passed
			mVintVerticalLongScroll = 1;
		} else if(mVintVerticalLongScroll > mVintMaxVerticalDisp) {
			// The upper limit of the displacement has been passed
			mVintVerticalLongScroll = mVintMaxVerticalDisp;
		}
		
	} else {
		// It's not necessary to scroll Vertically
		mVintVerticalLongScroll = 0;
	}
	
	mVintVerticalShortScroll = mOobjBoard->GetVintSQUARE_LENGTH();
}

//_______________________________________________________________________________________________________________________

void clsCanvas::SetTotalSize(int pVintWidth, int pVintHeight) {
	mVintTotalWidth = pVintWidth;
	mVintTotalHeight = pVintHeight;
}

//_______________________________________________________________________________________________________________________

wxRect clsCanvas::rctUpdatableArea(wxRect pVrctToUpdateArea) {
	pVrctToUpdateArea.Intersect(mVrctVisibleRegion);
	
	// Update the modifications accumulator
	if(MvRctDirtyRegion.IsEmpty()) {
		// There is not any changes up to now
		MvRctDirtyRegion = pVrctToUpdateArea;
	} else {
		// There were some changes
		MvRctDirtyRegion.Union(pVrctToUpdateArea);
	}
	
	return(pVrctToUpdateArea);
}

//_______________________________________________________________________________________________________________________

void clsCanvas::DrawSecondary(wxImage pVimgSource, wxRect pVrctDestination, wxPoint pVpntOffset) {
	wxBitmap bmpSource(pVimgSource);
	wxMemoryDC mdcSecondaryScreen;
	wxMemoryDC mdcSource;
	wxSize sizSizeToCopy(pVrctDestination.GetX(), pVrctDestination.GetY());
	wxRect rctSource;
	wxRect rctTarget;

	mdcSecondaryScreen.SelectObject(*mSbmpSecondaryScreen);
	mdcSource.SelectObject(bmpSource);

	rctSource.SetLeft(pVpntOffset.x);
	rctSource.SetTop(pVpntOffset.y);
	rctSource.SetWidth(pVrctDestination.GetRight() - pVrctDestination.GetLeft() + 1);
	rctSource.SetHeight(pVrctDestination.GetBottom() - pVrctDestination.GetTop() + 1);
	
	rctTarget = mOobjBoard->rctBoard2Screen(pVrctDestination);
	
	// Draws the bitmap into the secondary screen buffer
	mdcSecondaryScreen.Blit(rctTarget.GetX(), rctTarget.GetY(), rctSource.GetWidth()
		, rctSource.GetHeight(), &mdcSource, rctSource.GetX(), rctSource.GetY(), wxCOPY, true);
	
	
	mdcSecondaryScreen.SelectObject(wxNullBitmap);
	mdcSource.SelectObject(wxNullBitmap);
}

//_______________________________________________________________________________________________________________________

void clsCanvas::OnKeyDown(wxKeyEvent& event) {
	int intKeyCode = event.GetKeyCode();

	if (!event.ControlDown() && !event.AltDown() && !event.ShiftDown()) {
		// There are no modifiers pressed
		if (intKeyCode == WXK_UP) {
			mOobjCursor->MoveUp();
			return;

		} else if (intKeyCode == WXK_DOWN) {
			mOobjCursor->MoveDown();
			return;
			
		} else if (intKeyCode == WXK_LEFT) {
			mOobjCursor->MoveLeft();
			return;
		
		} else if (intKeyCode == WXK_RIGHT) {
			mOobjCursor->MoveRight();
			return;
		}
		
	} else if(event.ControlDown()) {
		// The modifier "Control" was pressed
		if (intKeyCode == WXK_UP) {
			mOobjCursor->MovePageUp();
			return;
			
		} else if (intKeyCode == WXK_DOWN) {
			mOobjCursor->MovePageDown();
			return;
			
		} else if (intKeyCode == WXK_LEFT) {
			mOobjCursor->MovePageLeft();
			return;
			
		} else if (intKeyCode == WXK_RIGHT) {
			mOobjCursor->MovePageRight();
			return;
		}
	}

	// If the process reach this point, it means that the key pressed doesn't has been caught
	event.ResumePropagation(wxEVENT_PROPAGATE_MAX); // to propagate the event to the parent container
	event.Skip();
}

//_______________________________________________________________________________________________________________________

void clsCanvas::OnLeftDown(wxMouseEvent& event) {
	mOobjCursor->SelectSquareFromPixel(event.GetX(), event.GetY());
	this->SetFocus(); // because in some platforms the focus is not set automatically

	mOobjBoard->ProcessMouseLeftClick();
}

//_______________________________________________________________________________________________________________________

void clsCanvas::OnRightDown(wxMouseEvent& event) {
	mOobjCursor->SelectSquareFromPixel(event.GetX(), event.GetY());
	this->SetFocus(); // because in some platforms the focus is not set automatically
	
	mOobjBoard->ProcessMouseRightClick();
}

//_______________________________________________________________________________________________________________________

void clsCanvas::OnMouseEvent(wxMouseEvent& event) {
	mOobjBoard->ProcessOnMouseEvent(event);
}
